/* packetFapiSdk.h
*Copyright (c) 2019 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/
#ifndef SCI_FAPI_SDK_H
#define SCI_FAPI_SDK_H
#define ENDIANESS  LITTLE_ENDIAN
#define PROTO_TAG_fapi_sdk                                  "SCI_FAPI_SDK"
#define PROTO_SCI_FAPI_SDK_ETHERNET                         (0xFFFE)
/* Direction */
#define UPLINK      (0)
#define DOWNLINK    (1)
/* Bit fields in the SFN/SF */
#define SFN_MASK        (~0x000F)       /* Frame Number */
#define SF_MASK          (0x000F)       /* Sub-frame Number */
#define ERROR_LENGTH    (7)
/* Net To Host and Host to Net Macros */
#if ENDIANESS == BIG_ENDIAN
    #define htons(A) (A)
    #define htonl(A) (A)
    #define ntohs(A) (A)
    #define ntohl(A) (A)
    #elif ENDIANESS == LITTLE_ENDIAN
    #define htons(A) ((((unsigned short)(A) & 0xff00) >> 8) | \
    (((unsigned short)(A) & 0x00ff) << 8))
    #define htonl(A) ((((unsigned long)(A) & 0xff000000) >> 24) | \
    (((unsigned long)(A) & 0x00ff0000) >> 8)  | \
    (((unsigned long)(A) & 0x0000ff00) << 8)  | \
    (((unsigned long)(A) & 0x000000ff) << 24))
    #define ntohs  htons
    #define ntohl  htonl
    #else
    #error: "Must define one of BIG_ENDIAN or LITTLE_ENDIAN"
#endif
#define ntohll(x) (((_int64)(ntohl((int)((x << 32) >> 32))) << 32) | (unsigned int)ntohl(((int)(x >> 32))))
#define htonll(x) ntohll(x)
#define MAX_DL_TX_PDUS  20
#define MAX_UL_IND_PDUS 128 //For Yosi: Number of CQI active users
typedef struct{
    guint32 rnti;
    guint32 pdu_idx;
    guint32 length;
}pdu_lengths_t;
pdu_lengths_t   dl_pdu_lengths[MAX_DL_TX_PDUS];
guint32         dl_pdu_lengths_idx = 0;
pdu_lengths_t   ul_pdu_lengths[MAX_UL_IND_PDUS];
/*********************************
    *  MAC-PHY Interface message
    *  types
*********************************/
typedef enum
{
    FAPI_E_PARAM_REQ        = 0x00,
    FAPI_E_PARAM_RSP        = 0x01,
    FAPI_E_CONFIG_REQ       = 0x02,
    FAPI_E_CONFIG_RSP       = 0x03,
    FAPI_E_START_REQ        = 0x04,
    FAPI_E_STOP_REQ         = 0x05,
    FAPI_E_STOP_IND         = 0x06,
    FAPI_E_UE_CONFIG_REQ    = 0x07,
    FAPI_E_UE_CONFIG_RSP    = 0x08,
    FAPI_E_ERROR_IND        = 0x09,
    FAPI_E_UE_RELEASE_REQ   = 0x0a,
    FAPI_E_UE_RELEASE_RSP   = 0x0b,
    FAPI_E_DL_CONFIG_REQ    = 0x80,
    FAPI_E_UL_CONFIG_REQ    = 0x81,
    FAPI_E_SF_IND           = 0x82,
    FAPI_E_HI_DCI0_REQ      = 0x83,
    FAPI_E_TX_REQ           = 0x84,
    FAPI_E_HARQ_IND         = 0x85,
    FAPI_E_CRC_IND          = 0x86,
    FAPI_E_RX_ULSCH_IND     = 0x87,
    FAPI_E_RACH_IND         = 0x88,
    FAPI_E_SRS_IND          = 0x89,
    FAPI_E_RX_SR_IND        = 0x8a,
    FAPI_E_RX_CQI_IND       = 0x8b,
    FAPI_E_RX_VSM_MEAS_IND  = 0x8c,
    FAPI_E_RX_VSM_NI_IND    = 0x8d,
    FAPI_E_VSM_CFG_REPORT   = 0x8e,
    FAPI_E_VSM_INFRA_REQ    = 0x8f,
    PNF_CAPABILITY_REQ      = 0x100,
    FAPI_E_PNF_CAPABILITY_RSP= 0x101,
    PNF_CONFIG_REQ          = 0x102,
    PNF_CONFIG_RESP         = 0x103,
    PNF_Start_request       = 0x104,
    PNF_START_RESP          = 0x105,
    PNF_STOP_REQ            = 0x106,
    PNF_STOP_RESP           = 0x107,
    PHY_CAPABILITY_REQ      = 0x110,
    PHY_CAPABILITY_RESP     = 0x111,
    PHY_CONFIG_REQ          = 0x112,
    PHY_CONFIG_RESP         = 0x113,
    PHY_START_REQ           = 0x114,
    PHY_START_RESP          = 0x115,
    PHY_STOP_REQ            = 0x116,
    PHY_STOP_RESP           = 0x117,
    Error_Indication        = 0x118,
    TX_CONTROL_MESSAGE      = 0x140,
    RX_CONTROL_MESSAGE      = 0x142,
    TX_DATA_MESSAGE         = 0x143,
    ULSCH_Decode_indication = 0x144,
    RX_DATA_MESSAGE         = 0x145,
    UCI_INDICATION          = 0x146,
    RACH_IND                = 0x147,
    FAPI_E_SLOT_IND         = 0x149,
    FAPI_PDCCH              = 0x150,
    FAPI_CSI_RS             = 0x151,
    FAPI_PDSCH              = 0x152,
    FAPI_E_INFRA_CONTROL_CMD= 0x200,
} FAPI_E_MSG_TYPE;
typedef enum
{
    DCI_DL_PDU    = 0x00,
    BCH_PDU       = 0x01,
    MCH_PDU       = 0x02,
    DLSCH_PDU     = 0x03,
    PCH_PDU       = 0x04,
    PRS_PDU       = 0x05,
    CSI_RS_PDU    = 0x06,
    rel8ETM_DLSCH_PDU  = 0x05,
    ETM_DLSCH_PDU = 0x80,
} FAPI_E_DL_PDU_TYPE;
typedef enum
{
    ULSCH               = 0x00,
    ULSCH_CQI_RI        = 0x01,
    ULSCH_HARQ          = 0x02,
    ULSCH_CQI_HARQ_RI   = 0x03,
    UCI_CQI             = 0x04,
    UCI_SR              = 0x05,
    UCI_HARQ            = 0x06,
    UCI_SR_HARQ         = 0x07,
    UCI_CQI_HARQ        = 0x08,
    UCI_CQI_SR          = 0x09,
    UCI_CQI_SR_HARQ     = 0x0a,
    SRS                 = 0x0b,
    HARQ_BUFFER         = 0x0c,
    ULSCH_UCI_CSI       = 0x0d,
    ULSCH_UCI_HARQ      = 0x0e,
    ULSCH_CSI_UCI_HARQ  = 0x0f,
} FAPI_E_UL_PDU_TYPE;
typedef enum
{
    HI_PDU              = 0x00,
    DCI_UL_PDU          = 0x01,
} FAPI_E_HI_DCI0_PDU_TYPE;
typedef enum
{
    RPT_ULSCH_VSM               = 0x01,
    RPT_HARQ_PUCCH_VSM          = 0x02,
    RPT_HARQ_PUSCH_VSM          = 0x03,
    RPT_SR_VSM                  = 0x04,
    RPT_CQI_PUCCH_VSM           = 0x05,
    RPT_CQI_PUSCH_VSM           = 0x06,
    RPT_RACH_VSM                = 0x07,
    RPT_SRS_VSM                 = 0x08,
    CFG_CFI_VSM                 = 0x0e,
    CFG_VERSION_VSM             = 0x0f,
    CFG_ETM_VSM                 = 0x10,
    PARAM_RSP_VSM               = 0x11,
    FAPI_E_CFG_NI_VSM           = 0x12,
} FAPI_E_VSM_TAG;
typedef enum
{
    DPD_CONFIG                      = 0x00,
    TRIGGER_REQUEST                 = 0x01,
    UNSOLICITED_ACK_NACK_THRESHOLD  = 0x02,
} FAPI_E_VSM_INFRA_PDU;
typedef enum
{
    MSG_OK                      = 0,
    MSG_INVALID_STATE           = 1,
    MSG_INVALID_CONFIG          = 2,
    SFN_OUT_OF_SYNC             = 3,
    MSG_PDU_ERR                 = 4,
    MSG_BCH_MISSING             = 5,
    MSG_INVALID_SFN             = 6,
    MSG_HI_ERR                  = 7,
    MSG_TX_ERR                  = 8,
    // vendor specific error types will start at an offset of 200
    GLOB_PARAM_ERR              = 200, // vendor specific error type, for global params in a FAPI message.
    N_PDU_LIMIT_SURPASSED       = 201, // vendor specific error type, PHY number of PDUs limitation was surpassed.
    START_REQ_RECEIVED_BEFORE_HARD_SYNC= 202, // vendor specific error type, Start request received before hard sync was received
} FAPI_E_ERR_CODE;
// Error Code Direction
typedef enum
{
    FAPI_E_DOWNLINK = 0,
    FAPI_E_UPLINK   = 1,
}FAPI_E_DIR_ERR_CODE;
// VSM N_PDU_LIMIT_SURPASSED Sub Error Codes
typedef enum
{
    FAPI_E_VSM_PDU_LIMIT_DCI    = 0,
    FAPI_E_VSM_PDU_LIMIT_BCH    = 1,
    FAPI_E_VSM_PDU_LIMIT_MCH    = 2,
    FAPI_E_VSM_PDU_LIMIT_SCH    = 3,
    FAPI_E_VSM_PDU_LIMIT_PCH    = 4,
    FAPI_E_VSM_PDU_LIMIT_UCI    = 5,
    FAPI_E_VSM_PDU_LIMIT_SRS    = 6,
}FAPI_E_VSM_PDU_LIMIT_SUB_ERROR_CODE;
// VSM N_PDU_LIMIT_SURPASSED RNTI Type
typedef enum
{
    FAPI_E_VSM_PDU_LIMIT_Unimportant = 0,
    FAPI_E_VSM_PDU_LIMIT_C_RNTI      = 1,
    FAPI_E_VSM_PDU_LIMIT_RA_RNTI     = 2,
    FAPI_E_VSM_PDU_LIMIT_P_RNTI      = 3,
    FAPI_E_VSM_PDU_LIMIT_SI_RNTI     = 4,
}FAPI_E_VSM_PDU_LIMIT_RNTI_TYPE;
// VSM Configured Params Report Categories
typedef enum
{
    FAPI_E_VSM_CAT_GEN      = 0,
    FAPI_E_VSM_CAT_RF       = 1,
    FAPI_E_VSM_CAT_PHICH    = 2,
    FAPI_E_VSM_CAT_SCH      = 3,
    FAPI_E_VSM_CAT_PRACH    = 4,
    FAPI_E_VSM_CAT_PUSCH    = 5,
    FAPI_E_VSM_CAT_PUCCH    = 6,
    FAPI_E_VSM_CAT_SRS      = 7,
    FAPI_E_VSM_CAT_UL_RS    = 8,
    FAPI_E_VSM_CAT_TDD      = 9,
    FAPI_E_VSM_CAT_PHY_CAP  = 10,
    FAPI_E_VSM_CAT_VSM      = 11,
}FAPI_E_VSM_CFG_REP_CAT;
typedef enum {
    NO_RNTI_VAL  = 0x0,
    M_RNTI_VAL   = 0xFFFD,
    P_RNTI_VAL   = 0xFFFE,
    SI_RNTI_VAL  = 0xFFFF,
}FAPI_E_RNTI_TYPE;
/* Global Tree's */
static proto_tree  *msg_head = NULL;
static const value_string onoff_string[] = {
    { 0, "OFF" },
    { 1, "ON" },
    { 0, NULL }
};
/* PIE-E Header strings */
typedef enum {
    FR_FULL = 0,
    FR_FIRST= 1,
    FR_MID  = 2,
    FR_LAST = 3,
}SCI_E_PIE_E_HDR_FRAG;

typedef enum
{
    //Common type of errors
    FAPI_E_PHY_IPTAR_NULL                   = 0,
    FAPI_E_PHY_MAX_POLAR_DEC_EXCEED         = 1,
    FAPI_E_PHY_HARQ_RT_PDU                  = 2,
    FAPI_E_PHY_INVALID_CMD_PDU              = 3,
    FAPI_E_PHY_MAX_POLAR_ENC_EXCEED         = 4,
    FAPI_E_DUPLICATE_MSG                    = 5,
    /* 4 - 9 Reserve for future use */
    //PRACH INVALID CONFIG
    FAPI_E_PHY_PRACH_CONFIG_BITMASK         = 10,
    FAPI_E_PHY_PRACH_ERROR_BITMASK          = 11,
    /* 12 - 19 Reserve for future use */
    //PUCCH INVALID CONFIG
    FAPI_E_PHY_PUCCH_F0_SLOT_EXCEED         = 20,
    FAPI_E_PHY_PUCCH_F2_SLOT_EXCEED         = 21,
    /* 22 - 29 Reserve for future use */
    //PUSCH INVALID CONFIG
    FAPI_E_PHY_PUSCH_PER_CC_MAX_EXCEED      = 30,
    FAPI_E_PHY_PUSCH_PER_SLOT_MAX_EXCEED    = 31,
    FAPI_E_PHY_PUSCH_UCI_SLOT_MAX_EXCEED    = 32,
    FAPI_E_PHY_PUSCH_DFTS_CONFIG            = 33,
    FAPI_E_PHY_PUSCH_INVALID_PDU            = 34,
    FAPI_E_PHY_PUSCH_HARQ_RT_PDU            = 35,
    //PDSCH INVALID CONFIG
    FAPI_E_PHY_PDSCH_PER_CC_MAX_EXCEED      = 40,
    FAPI_E_PHY_PDSCH_TB_PER_SLOT_EXCEED     = 41,
    /* 42 - 49 Reserve for future use */
    //PDCCH INVALID CONFIG
    FAPI_E_PHY_PDCCH_PER_CC_MAX_EXCEED      = 50,
    FAPI_E_PHY_PDCCH_PER_SLOT_EXCEED        = 51,
    /* 52 - 59 Reserve for future use */
    //SSB INVALID CONFIG
    FAPI_E_PHY_SSB_PER_CC_MAX_EXCEED      = 60,
    FAPI_E_PHY_SSB_BLOCK_PER_SLOT_EXCEED    = 61,
    /* 62 - 69 Reserve for future use */
}FAPI_E_PHY_INVALID_MSG_TYPE;

static const value_string invalid_message_type[] = {
    { FAPI_E_PHY_IPTAR_NULL , "FAPI_E_PHY_IPTAR_NULL" },
    { FAPI_E_PHY_MAX_POLAR_DEC_EXCEED, "FAPI_E_PHY_MAX_POLAR_DEC_EXCEED"},
    { FR_MID  , "FR_MID"  },
    { FAPI_E_PHY_HARQ_RT_PDU , "FAPI_E_PHY_HARQ_RT_PDU" },
    { FAPI_E_PHY_INVALID_CMD_PDU , "FAPI_E_PHY_INVALID_CMD_PDU" },
    { FAPI_E_PHY_MAX_POLAR_ENC_EXCEED , "FAPI_E_PHY_MAX_POLAR_ENC_EXCEED" },
    { FAPI_E_DUPLICATE_MSG , "FAPI_E_DUPLICATE_MSG" },
    { FAPI_E_PHY_PRACH_CONFIG_BITMASK , "FAPI_E_PHY_PRACH_CONFIG_BITMASK" },
    { FAPI_E_PHY_PRACH_ERROR_BITMASK , "FAPI_E_PHY_PRACH_ERROR_BITMASK" },
    { FAPI_E_PHY_PUCCH_F0_SLOT_EXCEED , "FAPI_E_PHY_PUCCH_F0_SLOT_EXCEED" },
    { FAPI_E_PHY_PUCCH_F2_SLOT_EXCEED , "FAPI_E_PHY_PUCCH_F2_SLOT_EXCEED" },
    { FAPI_E_PHY_PUSCH_PER_CC_MAX_EXCEED , "FAPI_E_PHY_PUSCH_PER_CC_MAX_EXCEED" },
    { FAPI_E_PHY_PUSCH_PER_SLOT_MAX_EXCEED , "FAPI_E_PHY_PUSCH_PER_SLOT_MAX_EXCEED" },
    { FAPI_E_PHY_PUSCH_UCI_SLOT_MAX_EXCEED , "FAPI_E_PHY_PUSCH_UCI_SLOT_MAX_EXCEED" },
    { FAPI_E_PHY_PUSCH_DFTS_CONFIG , "FAPI_E_PHY_PUSCH_DFTS_CONFIG" },
    { FAPI_E_PHY_PUSCH_INVALID_PDU , "FAPI_E_PHY_PUSCH_INVALID_PDU" },
    { FAPI_E_PHY_PDSCH_PER_CC_MAX_EXCEED , "FAPI_E_PHY_PDSCH_PER_CC_MAX_EXCEED" },
    { FAPI_E_PHY_PDSCH_TB_PER_SLOT_EXCEED , "FAPI_E_PHY_PDSCH_TB_PER_SLOT_EXCEED" },
    { FAPI_E_PHY_PUSCH_HARQ_RT_PDU , "FAPI_E_PHY_PUSCH_HARQ_RT_PDU" },
    { FAPI_E_PHY_PDCCH_PER_CC_MAX_EXCEED , "FAPI_E_PHY_PDCCH_PER_CC_MAX_EXCEED" },
    { FAPI_E_PHY_PDCCH_PER_SLOT_EXCEED , "FAPI_E_PHY_PDCCH_PER_SLOT_EXCEED" },
    { FAPI_E_PHY_SSB_PER_CC_MAX_EXCEED , "FAPI_E_PHY_SSB_PER_CC_MAX_EXCEED" },
    { FAPI_E_PHY_SSB_BLOCK_PER_SLOT_EXCEED , "FAPI_E_PHY_SSB_BLOCK_PER_SLOT_EXCEED" },
    { 0, NULL }
};
static const value_string pie_e_header_fragment_string[] = {
    { FR_FULL , "FULL" },
    { FR_FIRST, "FIRST"},
    { FR_MID  , "MID"  },
    { FR_LAST , "LAST" },
    { 0, NULL }
};
/* FAPI Message Header strings */
static const value_string fapi_msg_header_string[] = {
    { FAPI_E_PARAM_REQ      , "FAPI_E_PARAM_REQ"},
    { FAPI_E_PARAM_RSP      , "FAPI_E_PARAM_RSP"},
    { FAPI_E_CONFIG_REQ     , "FAPI_E_CONFIG_REQ"},
    { FAPI_E_CONFIG_RSP     , "FAPI_E_CONFIG_RSP"},
    { FAPI_E_START_REQ      , "FAPI_E_START_REQ"},
    { FAPI_E_STOP_REQ       , "FAPI_E_STOP_REQ"},
    { FAPI_E_STOP_IND       , "FAPI_E_STOP_IND"},
    { FAPI_E_UE_CONFIG_REQ  , "FAPI_E_UE_CONFIG_REQ"},
    { FAPI_E_UE_CONFIG_RSP  , "FAPI_E_UE_CONFIG_RSP"},
    { FAPI_E_ERROR_IND      , "FAPI_E_ERROR_IND"},
    { FAPI_E_UE_RELEASE_REQ , "FAPI_E_UE_RELEASE_REQ"},
    { FAPI_E_UE_RELEASE_RSP , "FAPI_E_UE_RELEASE_RSP"},
    { FAPI_E_DL_CONFIG_REQ  , "FAPI_E_DL_CONFIG_REQ"},
    { FAPI_E_UL_CONFIG_REQ  , "FAPI_E_UL_CONFIG_REQ"},
    { FAPI_E_SF_IND         , "FAPI_E_SF_IND"},
    { FAPI_E_HI_DCI0_REQ    , "FAPI_E_HI_DCI0_REQ"},
    { FAPI_E_TX_REQ         , "FAPI_E_TX_REQ"},
    { FAPI_E_HARQ_IND       , "FAPI_E_HARQ_IND"},
    { FAPI_E_CRC_IND        , "FAPI_E_CRC_IND"},
    { FAPI_E_RX_ULSCH_IND   , "FAPI_E_RX_ULSCH_IND"},
    { FAPI_E_RACH_IND       , "FAPI_E_RACH_IND"},
    { FAPI_E_SRS_IND        , "FAPI_E_SRS_IND"},
    { FAPI_E_RX_SR_IND      , "FAPI_E_RX_SR_IND"},
    { FAPI_E_RX_CQI_IND     , "FAPI_E_RX_CQI_IND"},
    { FAPI_E_RX_VSM_MEAS_IND, "FAPI_E_RX_VSM_MEAS_IND"},
    { FAPI_E_RX_VSM_NI_IND  , "FAPI_E_RX_VSM_NI_IND"},
    { FAPI_E_VSM_CFG_REPORT , "FAPI_E_CFG_REPORT"},
    { FAPI_E_VSM_INFRA_REQ  , "FAPI_E_VSM_INFRA_REQ"},
    {PNF_CAPABILITY_REQ , "PNF_CAPABILITY_REQ"},
    {PHY_CAPABILITY_REQ , "PHY_CAPABILITY_REQ"},
    {FAPI_E_PNF_CAPABILITY_RSP , "PNF_CAPABILITY_RSP"},
    {PNF_CONFIG_REQ , "PNF_CONFIG_REQ"},
    {PNF_CONFIG_RESP , "PNF_CONFIG_RESP"},
    {PNF_Start_request , "PNF_Start_request"},
    {PNF_START_RESP , "PNF_START_RESP"},
    {PNF_STOP_REQ , "PNF_STOP_REQ"},
    {PNF_STOP_RESP , "PNF_STOP_RESP"},
    {PHY_CAPABILITY_RESP , "PHY_CAPABILITY_RESP"},
    {PHY_CONFIG_RESP , "PHY_CONFIG_RESP"},
    {PHY_CONFIG_REQ , "PHY_CONFIG_REQ"},
    {PHY_START_REQ , "PHY_START"},
    {PHY_START_RESP , "PHY_START_RESP"},
    {PHY_STOP_REQ , "PHY_STOP"},
    {PHY_STOP_RESP , "PHY_STOP_RESP"},
    {TX_CONTROL_MESSAGE , "TX_CONTROL_MESSAGE"},
    {Error_Indication   ,"Error_Indication  "},
    {RX_CONTROL_MESSAGE , "RX_CONTROL_MESSAGE"},
    {TX_DATA_MESSAGE , "TX_DATA_MESSAGE"},
    {RX_DATA_MESSAGE , "RX_DATA_MESSAGE"},
    {FAPI_E_SLOT_IND,"FAPI_E_SLOT_IND"},
    {UCI_INDICATION,"UCI_INDICATION"},
    {FAPI_PDCCH,"FAPI_PDCCH"},
    {FAPI_CSI_RS,"FAPI_CSI_RS"},
    {ULSCH_Decode_indication,"ULSCH_Decode_indication"},
    {RACH_IND, "Rach_Ind"},
    {FAPI_E_INFRA_CONTROL_CMD, "FAPI_E_INFRA_CONTROL_CMD"},
    { 0, NULL }
};
/* FAPI Error Codes */
static const value_string fapi_msg_error_codes_string[] = {
    {MSG_OK             , "MSG_OK"},
    {MSG_INVALID_STATE  , "MSG_INVALID_STATE"},
    {MSG_INVALID_CONFIG , "MSG_INVALID_CONFIG"},
    {SFN_OUT_OF_SYNC    , "SFN_OUT_OF_SYNC"},
    {MSG_PDU_ERR        , "MSG_PDU_ERR"},
    {MSG_BCH_MISSING    , "MSG_BCH_MISSING"},
    {MSG_INVALID_SFN    , "MSG_INVALID_SFN"},
    {MSG_HI_ERR         , "MSG_HI_ERR"},
    {MSG_TX_ERR         , "MSG_TX_ERR"},
    // vendor specific error types will start at an offset of 200
    {GLOB_PARAM_ERR     , "GLOB_PARAM_ERR"},
    {N_PDU_LIMIT_SURPASSED, "N_PDU_LIMIT_SURPASSED"},
    {START_REQ_RECEIVED_BEFORE_HARD_SYNC, "START_REQ_RECEIVED_BEFORE_HARD_SYNC"},
    { 0, NULL }
};
static const value_string msg_error_codes_string[] = {
    {MSG_OK             , "MSG_OK"},
    {MSG_INVALID_STATE  , "MSG_INVALID_STATE"},
    {MSG_INVALID_CONFIG , "MSG_INVALID_CONFIG"},
    {SFN_OUT_OF_SYNC    , "SFN_OUT_OF_SYNC"},
    {MSG_PDU_ERR        , "MSG_PDU_ERR"},
    {MSG_BCH_MISSING    , "MSG_BCH_MISSING"},
    {MSG_INVALID_SFN    , "MSG_INVALID_SFN"},
    {MSG_HI_ERR         , "MSG_HI_ERR"},
    {MSG_TX_ERR         , "MSG_TX_ERR"},
    // vendor specific error types will start at an offset of 200
    {GLOB_PARAM_ERR     , "GLOB_PARAM_ERR"},
    {N_PDU_LIMIT_SURPASSED, "N_PDU_LIMIT_SURPASSED"},
    {START_REQ_RECEIVED_BEFORE_HARD_SYNC, "START_REQ_RECEIVED_BEFORE_HARD_SYNC"},
    { 0, NULL }
};
static const value_string PNF_config_error_codes_string[] = {
    {MSG_OK             , "PNF MSG_OK"},
    {MSG_INVALID_STATE  , "PNF Config.request was received when PNF was not in PNF IDLE or PNF Configured state"},
    {MSG_INVALID_CONFIG , "The configuration provided contains parameters that are invalid or unsupported by the PNF."},
    { 0, NULL }
};
static const value_string PNF_start_RESP_error_codes_string[] = {
    {MSG_OK             , "PNF MSG_OK"},
    {MSG_INVALID_STATE  , "PNF Start.request was received when PNF was not in PNF Configured state"},
    { 0, NULL }
};
static const value_string PNF_stop_REQ_error_codes_string[] = {
    {MSG_OK             , "PNF MSG_OK"},
    {MSG_INVALID_STATE  , "PNF Stop.request was received when PNF was not in PNF Running state"},
    { 0, NULL }
};
static const value_string PNF_stop_RESP_error_codes_string[] = {
    {MSG_OK             , "PNF MSG_OK"},
    {MSG_INVALID_STATE  , "PNF Stop.response was received when PNF was not in PNF Running state"},
    { 0, NULL }
};
//pusch_error
static const value_string FAPI_E_ERROR_CODEs[] = {
    {0      , "FAPI_E_MSG_OK"},
    {1      , "FAPI_E_MSG_INVALID_STATE"},
    {2      , "FAPI_E_MSG_INVALID_CONFIG"},
    {3      , "FAPI_E_SFN_OUT_OF_SYNC"},
    {4      , "FAPI_E_MSG_SLOT_ERR"},
    {5      , "FAPI_E_MSG_BCH_MISSING"},
    {6      , "FAPI_E_MSG_INVALID_SFN"},
    {7      , "FAPI_E_MSG_UL_DCI_ERR"},
    {8      , "FAPI_E_MSG_TX_ERR"},
    { 0     , NULL }
};
/* FAPI Sub Error Codes */
static const value_string fapi_msg_sub_error_codes_string[] = {
    { 0, "Invalid Field Offset"},
    { 0, NULL }
};
/* FAPI MSG_PDU_ERR Directions */
static const value_string fapi_msg_data_msg_pdu_error_direction_string[] = {
    {FAPI_E_DOWNLINK, "Downlink"},
    {FAPI_E_UPLINK, "Uplink"},
    {0, NULL }
};
/* Error.indication - DCI DL PDU Fields */
static const value_string fapi_msg_data_dci_dl_pdu_sub_error_code_string[] = {
    { 0, "DCI Format" },
    { 1, "CCE Index" },
    { 2, "Aggregation Level" },
    { 3, "RNTI" },
    { 4, "Alloc. Type" },
    { 5, "Virtual RB Type" },
    { 6, "RB Coding" },
    { 7, "MCS 1" },
    { 8, "RV 1" },
    { 9, "NDI 1" },
    { 10, "Swap Flag" },
    { 11, "MCS 2" },
    { 12, "RV 2" },
    { 13, "NDI 2" },
    { 14, "HARQ Process" },
    { 15, "PMI Codebook Index" },
    { 16, "PMI Confirmation" },
    { 17, "Precoding" },
    { 18, "TPC" },
    { 19, "DAI" },
    { 20, "Ngap" },
    { 21, "TB Size Index" },
    { 22, "PA" },
    { 23, "PRACH Flag" },
    { 24, "PRACH Preamble" },
    { 25, "PRACH Mask" },
    { 26, "RNTI type" },
    { 27, "Boosting" },
    { 28, "MCCH flag" },
    { 29, "MCCH Change Notification" },
    { 30, "Scrambling ID" },
    { 31, "Cross Carrier Scheduling Flag" },
    { 32, "Carrier Indicator" },
    { 33, "SRS Flag" },
    { 34, "SRS Request" },
    { 35, "Ant. Ports, Scramb. & Layers" },
    { 0, NULL }
};
/* Error.indication - BCH PDU Fields */
static const value_string fapi_msg_data_bch_pdu_sub_error_code_string[] = {
    { 0, "Length" },
    { 1, "PDU Index" },
    { 2, "Transmission power" },
    { 0, NULL }
};
/* Error.indication - MCH PDU Fields */
static const value_string fapi_msg_data_mch_pdu_sub_error_code_string[] = {
    { 0, "Length" },
    { 1, "PDU Index" },
    { 2, "RNTI" },
    { 3, "Alloc. Type" },
    { 4, "RB Coding" },
    { 5, "Modulation" },
    { 6, "Transmission power" },
    { 0, NULL }
};
/* Error.indication - DLSCH PDU Fields */
static const value_string fapi_msg_data_dlsch_pdu_sub_error_code_string[] = {
    { 0, "Length" },
    { 1, "PDU Index" },
    { 2, "RNTI" },
    { 3, "Alloc. Type" },
    { 4, "Virtual RB Type" },
    { 5, "RB Coding" },
    { 6, "Modulation" },
    { 7, "RV" },
    { 8, "Codeword ID" },
    { 9, "Swap Flag" },
    { 10, "Transmission Scheme" },
    { 11, "No Of Layers" },
    { 12, "No of subbands" },
    { 13, "PMI Codebook Index" },
    { 14, "UE Category" },
    { 15, "P-A" },
    { 16, "Delta Power Offset" },
    { 17, "Ngap" },
    { 18, "No of PRB" },
    { 19, "Transmission Mode" },
    { 20, "No of PRB per Subband" },
    { 21, "No of BFVs" },
    { 22, "bfVector" },
    { 23, "Nscid" },
    { 24, "CSI-RS Flag" },
    { 25, "CSI-RS Resource Config" },
    { 26, "CSI-RS Zero Tx Power bitmap" },
    { 0, NULL }
};
/* Error.indication - PRS PDU Fields */
static const value_string fapi_msg_data_pch_pdu_sub_error_code_string[] = {
    { 0, "Length" },
    { 1, "PDU Index" },
    { 2, "RNTI" },
    { 3, "Alloc. Type" },
    { 4, "Virtual RB Type" },
    { 5, "RB Coding" },
    { 6, "Modulation" },
    { 7, "RV" },
    { 8, "Codeword ID" },
    { 9, "Swap Flag" },
    { 10, "Transmission Scheme" },
    { 11, "No Of Layers" },
    { 13, "PMI Codebook Index" },
    { 14, "UE Category" },
    { 14, "P-A" },
    { 15, "Transmission Power" },
    { 16, "No of PRB" },
    { 17, "Ngap" },
    { 0, NULL }
};
/* Error.indication - PCH PDU Fields */
static const value_string fapi_msg_data_prs_pdu_sub_error_code_string[] = {
    { 0, "Transmission Power" },
    { 0, NULL }
};
/* Error.indication - CSI-SR PDU Fields */
static const value_string fapi_msg_data_csi_rs_pdu_sub_error_code_string[] = {
    { 0, "Num. of CSI-RS Ant. Ports" },
    { 1, "CSI-RS Resource Config" },
    { 2, "Transmission Power" },
    { 3, "CSI-RS Zero Tx Power bitmap" },
    { 0, NULL }
};
/* Error.indication - ULSCH PDU Fields */
static const value_string fapi_msg_data_ulsch_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "Size" },
    { 2, "RNTI" },
    { 3, "RB Start" },
    { 4, "RB Num" },
    { 5, "MCS" },
    { 6, "CS2DMRS" },
    { 7, "Freq. Hopping Flag" },
    { 8, "Freq. Hopping Bits" },
    { 9, "NDI" },
    { 10, "RV" },
    { 11, "HARQ Ret Tx" },
    { 12, "UL Tx Mode" },
    { 13, "Curr. TX NB" },
    { 14, "SRS Present" },
    { 15, "Alloc. Type" },
    { 16, "RB Coding" },
    { 17, "Codeword ID" },
    { 18, "Transmission Scheme" },
    { 19, "Num. of Layers" },
    { 20, "PMI Codebook Index" },
    { 21, "Disable Sequence Hopping Flag" },
    { 0, NULL }
};
/* Error.indication - CQI RI Information Fields */
static const value_string fapi_msg_data_cqi_ri_information_sub_error_code_string[] = {
    { 22, "Report Type" },
    { 23, "Delta Offset CQI" },
    { 24, "Delta Offset RI" },
    { 25, "CQI/PMI/RI Report" },
    { 26, "CQI/PMI/RI aperiodic Report" },
    { 27, "CQI/PMI/RI aperiodic Report" },
    { 28, "CQI/PMI/RI aperiodic Report" },
    { 0, NULL }
};
/* Error.indication - Initial Transmission Information Fields  */
static const value_string fapi_msg_data_initial_transmission_information_sub_error_code_string[] = {
    { 25, "SRS Init" },
    { 26, "INIT INFO" }, // Could be either "SRS Init" or "RB Num Init" based on PDU Type
    { 27, "RB Num Init" },
    { 29, "SRS Init" },
    { 30, "RB Num Init" },
    { 32, "SRS Init" },
    { 33, "RB Num Init" },
    { 0, NULL }
};
/* Error.indication - HARQ Information Fields  */
static const value_string fapi_msg_data_harq_information_sub_error_code_string[] = {
    { 22, "ACK/NACK Num of Bits" },
    { 23, "Delta Offset HARQ" },
    { 24, "ACK/NACK Mode" },
    { 26, "ACK/NACK Num of Bits" },
    { 27, "Delta Offset HARQ" },
    { 28, "ACK/NACK Mode" },
    { 29, "ACK/NACK Num of Bits" },
    { 30, "Delta Offset HARQ" },
    { 31, "ACK/NACK Mode" },
    { 0, NULL }
};
/* Error.indication - UCI CQI PDU Fields */
static const value_string fapi_msg_data_uci_cqi_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "PUCCH Index 0" },
    { 3, "DL CQI/PMI No of Bits" },
    { 4, "Num. of PUCCH Resources" },
    { 5, "PUCCH Index 1" },
    { 0, NULL }
};
/* Error.indication - UCI SR PDU Fields */
static const value_string fapi_msg_data_uci_sr_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "PUCCH Index 0" },
    { 3, "Num. of PUCCH Resources" },
    { 4, "PUCCH Index 1" },
    { 0, NULL }
};
/* Error.indication - UCI HARQ PDU Fields */
static const value_string fapi_msg_data_uci_harq_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "ACK/NACK No of Bits" },
    { 3, "ACK/NACK Mode" },
    { 4, "Num. of PUCCH Resources" },
    { 5, "PUCCH Index 0" },
    { 6, "PUCCH Index 1" },
    { 7, "PUCCH Index 2" },
    { 8, "PUCCH Index 3" },
    { 0, NULL }
};
/* Error.indication - UCI SR HARQ PDU Fields */
static const value_string fapi_msg_data_uci_sr_harq_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "SR PUCCH Index 0" },
    { 3, "SR Num. of PUCCH Resources" },
    { 4, "SR PUCCH Index 1" },
    { 5, "ACK/NACK No of Bits" },
    { 6, "ACK/NACK Mode" },
    { 7, "HARQ Num. of PUCCH Resources" },
    { 8, "HARQ PUCCH Index 0" },
    { 9, "HARQ PUCCH Index 1" },
    { 10,"HARQ PUCCH Index 2" },
    { 11,"HARQ PUCCH Index 3" },
    { 0, NULL }
};
/* Error.indication - UCI CQI HARQ PDU Fields */
static const value_string fapi_msg_data_uci_cqi_harq_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "CQI PUCCH Index 0" },
    { 3, "DL CQI/PMI No of Bits" },
    { 4, "CQI Num. of PUCCH Resources" },
    { 5, "CQI PUCCH Index 1" },
    { 6, "ACK/NACK No of Bits" },
    { 7, "ACK/NACK Mode" },
    { 8, "HARQ Num. of PUCCH Resources" },
    { 9, "HARQ PUCCH Index 0" },
    { 10,"HARQ PUCCH Index 1" },
    { 11,"HARQ PUCCH Index 2" },
    { 12,"HARQ PUCCH Index 3" },
    { 0, NULL }
};
/* Error.indication - UCI CQI SR PDU Fields */
static const value_string fapi_msg_data_uci_cqi_sr_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "CQI PUCCH Index 0" },
    { 3, "DL CQI/PMI No of Bits" },
    { 4, "CQI Num. of PUCCH Resources" },
    { 5, "CQI PUCCH Index 1" },
    { 6, "SR PUCCH Index 0" },
    { 7, "SR Num. of PUCCH Resources" },
    { 8, "SR PUCCH Index 1" },
    { 0, NULL }
};
/* Error.indication - UCI CQI SR HARQ PDU Fields */
static const value_string fapi_msg_data_uci_cqi_sr_harq_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 2, "CQI PUCCH Index 0" },
    { 3, "DL CQI/PMI No of Bits" },
    { 4, "CQI Num. of PUCCH Resources" },
    { 5, "CQI PUCCH Index 1" },
    { 6, "SR PUCCH Index 0" },
    { 7, "SR Num. of PUCCH Resources" },
    { 8, "SR PUCCH Index 1" },
    { 9, "ACK/NACK No of Bits" },
    { 10,"ACK/NACK Mode" },
    { 11,"HARQ Num. of PUCCH Resources" },
    { 12,"HARQ PUCCH Index 0" },
    { 13,"HARQ PUCCH Index 1" },
    { 14,"HARQ PUCCH Index 2" },
    { 15,"HARQ PUCCH Index 3" },
    { 0, NULL }
};
//Infra.Request - PDU Type Field
static const value_string fapi_msg_data_infra_pdu_type_string[] = {
    { 0, "DPD Config" },
    { 1, "Trigger Request" },
    { 0, NULL }
};
/* Error.indication - SRS PDU Fields */
static const value_string fapi_msg_data_srs_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "PDU Length" },
    { 2, "RNTI" },
    { 3, "SRS Bandwidth" },
    { 4, "Freq. Domain Position" },
    { 5, "SRS Freq. Hopping" },
    { 6, "Transmission Comb" },
    { 7, "Configuration Index" },
    { 8, "Cyclic Shift" },
    { 9, "Ant. Ports" },
    { 0, NULL }
};
/* Error.indication - HARQ BUFFER PDU Fields */
static const value_string fapi_msg_data_harq_buffer_pdu_sub_error_code_string[] = {
    { 0, "Handle" },
    { 1, "RNTI" },
    { 0, NULL }
};
/*  Error.indication - TX_REQ Fields */
static const value_string fapi_msg_data_tx_request_sub_error_code_string[] = {
    { 0, "Num. of TLVs" },
    { 1, "TLV Tag" },
    { 2, "TLV Length" },
    { 0, NULL }
};
/*  Error.indication - GLOB_PARAM_ERR DL_CONFIG Fields */
static const value_string fapi_msg_data_GLOB_PARAM_ERR_dl_config_sub_error_code_string[] = {
    { 0, "SFN/SF" },
    { 1, "Length" },
    { 2, "Num. of PDCCH OFDM Symb." },
    { 3, "Num. of DCIs" },
    { 4, "Num. of PDUs" },
    { 5, "Num. of PDSCH RNTIs" },
    { 6, "Trans. Power for PCFICH" },
    { 0, NULL }
};
/*  Error.indication - GLOB_PARAM_ERR UL_CONFIG Fields */
static const value_string fapi_msg_data_GLOB_PARAM_ERR_ul_config_sub_error_code_string[] = {
    { 0, "SFN/SF" },
    { 1, "Length" },
    { 2, "Num. of PDUs" },
    { 3, "RACH Freq. Resource" },
    { 4, "SRS Present" },
    { 0, NULL }
};
/*  Error.indication - GLOB_PARAM_ERR TX_REQ Fields */
static const value_string fapi_msg_data_GLOB_PARAM_ERR_tx_request_sub_error_code_string[] = {
    { 0, "SFN/SF" },
    { 1, "No of PDUs" },
    { 0, NULL }
};
/*  Error.indication - N_PDU_LIMIT_SURPASSED channel types */
static const value_string fapi_msg_data_N_PDU_LIMIT_sub_error_code_string[] = {
    { FAPI_E_VSM_PDU_LIMIT_DCI, "DCI (DL/UL)" },
    { FAPI_E_VSM_PDU_LIMIT_BCH, "BCH" },
    { FAPI_E_VSM_PDU_LIMIT_MCH, "MCH" },
    { FAPI_E_VSM_PDU_LIMIT_SCH, "SCH (DL/UL)" },
    { FAPI_E_VSM_PDU_LIMIT_PCH, "PCH" },
    { FAPI_E_VSM_PDU_LIMIT_UCI, "UCI" },
    { FAPI_E_VSM_PDU_LIMIT_SRS, "SRS" },
    { 0, NULL }
};
/*  Error.indication - N_PDU_LIMIT_SURPASSED RNTI types */
static const value_string fapi_msg_data_N_PDU_LIMIT_rnti_type_string[] = {
    { FAPI_E_VSM_PDU_LIMIT_Unimportant  , "Unimportant" },
    { FAPI_E_VSM_PDU_LIMIT_C_RNTI       , "C-RNTI" },
    { FAPI_E_VSM_PDU_LIMIT_RA_RNTI      , "RA-RNTI"},
    { FAPI_E_VSM_PDU_LIMIT_P_RNTI       , "P-RNTI" },
    { FAPI_E_VSM_PDU_LIMIT_SI_RNTI      , "SI-RNTI"},
    { 0, NULL }
};
/* FAPI Vendor Specific TLV Tags */
static const value_string fapi_vsm_TLV_tag_string[] = {
    { RPT_ULSCH_VSM     , "RPT_ULSCH_VSM"},
    { RPT_HARQ_PUCCH_VSM, "RPT_HARQ_PUCCH_VSM"},
    { RPT_HARQ_PUSCH_VSM, "RPT_HARQ_PUSCH_VSM"},
    { RPT_SR_VSM        , "RPT_SR_VSM"},
    { RPT_CQI_PUCCH_VSM , "RPT_CQI_PUCCH_VSM"},
    { RPT_CQI_PUSCH_VSM , "RPT_CQI_PUSCH_VSM"},
    { RPT_RACH_VSM      , "RPT_RACH_VSM"},
    { RPT_SRS_VSM       , "RPT_SRS_VSM"},
    { CFG_CFI_VSM       , "CFG_CFI_VSM"},
    { CFG_VERSION_VSM   , "CFG_VERSION_VSM"},
    { CFG_ETM_VSM       , "CFG_ETM_VSM"},
    {PARAM_RSP_VSM      , "PARAM_RSP_VSM"},
    {FAPI_E_CFG_NI_VSM  , "FAPI_E_CFG_NI_VSM"},
    { 0, NULL }
};
/* VSM Configured Params Report Categories */
static const value_string fapi_vsm_cfg_rep_cat_string[] = {
    { FAPI_E_VSM_CAT_GEN    , "General"},
    { FAPI_E_VSM_CAT_RF     , "RF Config"},
    { FAPI_E_VSM_CAT_PHICH  , "PHICH Config"},
    { FAPI_E_VSM_CAT_SCH    , "SCH Config"},
    { FAPI_E_VSM_CAT_PRACH  , "PRACH Config"},
    { FAPI_E_VSM_CAT_PUSCH  , "PUSCH Config"},
    { FAPI_E_VSM_CAT_PUCCH  , "PUCCH Config"},
    { FAPI_E_VSM_CAT_SRS    , "SRS Config"},
    { FAPI_E_VSM_CAT_UL_RS  , "UL RS Config"},
    { FAPI_E_VSM_CAT_TDD    , "TDD Config"},
    { FAPI_E_VSM_CAT_PHY_CAP, "PHY Capabilities"},
    { FAPI_E_VSM_CAT_VSM    , "VSM Config"},
    { 0, NULL }
};
static const value_string fapi_msg_data_dl_pdu_type_string[] = {
    {DCI_DL_PDU  , "DCI_DL_PDU"},
    {BCH_PDU     , "BCH_PDU"   },
    {MCH_PDU     , "MCH_PDU"   },
    {DLSCH_PDU   , "DLSCH_PDU" },
    {PCH_PDU     , "PCH_PDU"   },
    {rel8ETM_DLSCH_PDU, "ETM_DLSCH_PDU"},
    { 0, NULL }
};
static const value_string fapi_msg_data_dl_pdu_type_release10_string[] = {
    {DCI_DL_PDU  , "DCI_DL_PDU"},
    {BCH_PDU     , "BCH_PDU"   },
    {MCH_PDU     , "MCH_PDU"   },
    {DLSCH_PDU   , "DLSCH_PDU" },
    {PCH_PDU     , "PCH_PDU"   },
    {PRS_PDU     , "PRS_PDU"   },
    {CSI_RS_PDU  , "CSI_RS_PDU"},
    {ETM_DLSCH_PDU, "ETM_DLSCH_PDU"},
    { 0, NULL }
};
static const value_string fapi_msg_data_ul_pdu_type_string[] = {
    {ULSCH              , "ULSCH"             },
    {ULSCH_CQI_RI       , "ULSCH_CQI_RI"      },
    {ULSCH_HARQ         , "ULSCH_HARQ"        },
    {ULSCH_CQI_HARQ_RI  , "ULSCH_CQI_HARQ_RI" },
    {UCI_CQI            , "UCI_CQI"           },
    {UCI_SR             , "UCI_SR"            },
    {UCI_HARQ           , "UCI_HARQ"          },
    {UCI_SR_HARQ        , "UCI_SR_HARQ"       },
    {UCI_CQI_HARQ       , "UCI_CQI_HARQ"      },
    {UCI_CQI_SR         , "UCI_CQI_SR"        },
    {UCI_CQI_SR_HARQ    , "UCI_CQI_SR_HARQ"   },
    {SRS                , "SRS"               },
    {HARQ_BUFFER        , "HARQ_BUFFER"       },
    {ULSCH_UCI_CSI      , "ULSCH_UCI_CSI"     },
    {ULSCH_UCI_HARQ     , "ULSCH_UCI_HARQ"    },
    {ULSCH_CSI_UCI_HARQ , "ULSCH_CSI_UCI_HARQ"},
    { 0, NULL }
};
static const value_string fapi_msg_data_hi_dci0_pdu_type_string[] = {
    {HI_PDU     , "HI_PDU"    },
    {DCI_UL_PDU , "DCI_UL_PDU"},
    {0, NULL        }
};
static const value_string fapi_msg_data_dl_ant_method_string[] = {
    { 0, "Single Ant. Port 0" },
    { 1, "TX Diversity" },
    { 2, "Large Delay CDD" },
    { 3, "CL Spacial Multiplexing" },
    { 4, "Multi User MIMO" },
    { 5, "CL Rank 1 Precoding" },
    { 6, "Single Ant. Port 5" },
    { 7, "Single Ant. Port 7" },
    { 8, "Single Ant. Port 8" },
    { 9, "Dual Layer TX Port 7&8" },
    {10, "Up to 8 Layer TX" },
    { 0, NULL }
};
static const value_string fapi_msg_data_ul_ant_method_string[] = {
    { 0, "Single Ant. Port 10" },
    { 1, "CL Spacial Multiplexing" },
    { 0, NULL }
};
static const value_string fapi_msg_data_pa_string[] = {
    { 0, "-6 dB" },
    { 1, "-4.77 dB" },
    { 2, "-3 dB" },
    { 3, "-1.77 dB" },
    { 4, "0 dB" },
    { 5, "1 dB" },
    { 6, "2 dB" },
    { 7, "3 dB" },
    { 0, NULL }
};
static const value_string fapi_msg_data_dl_dci_format_string[] = {
    { 0, "1"  },
    { 1, "1A" },
    { 2, "1B" },
    { 3, "1C" },
    { 4, "1D" },
    { 5, "2"  },
    { 6, "2A" },
    { 7, "2B" },
    { 8, "2C" },
    { 0, NULL }
};
static const value_string fapi_msg_data_ul_dci_format_string[] = {
    { 0, "0"  },
    { 1, "3"  },
    { 2, "3A" },
    { 3, "4" },
    { 0, NULL }
};
static const value_string fapi_msg_data_ack_nack_string[] = {
    { 0, "NACK TB0 & TB1" },
    { 1, "ACK TB0, NACK TB1" },
    { 2, "NACK TB0, ACK TB1" },
    { 3, "ACK TB0 & TB1" },
    { 0, NULL }
};
static const value_string fapi_msg_data_ack_nack_presence_string[] = {
    { 0, "ACK/NACK Not Present" },
    { 1, "ACK/NACK Present for TB0" },
    { 2, "ACK/NACK Present for TB1" },
    { 3, "ACK/NACK Present for TB0 & TB1" },
    { 0, NULL }
};
static const value_string fapi_msg_data_param_type_string[] = {
    { 0, "NUM" },
    { 1, "DATA" },
    { 2, "NUM ARRAY" },
    { 0, NULL }
};
static const value_string fapi_msg_data_all_type_string[] = {
    {0, "Type 0"},
    {1, "Type 1"},
    {2, "Type 2"},
    {3, "Type 2 (1C)"},
    {0, NULL }
};
static const value_string fapi_msg_data_rnti_type_string[] = {
    {1, "C-RNTI"},
    {2, "RA-RNTI, P-RNTI or SI-RNTI"},
    {3, "SPS-CRNTI"},
    {0, NULL }
};
static const value_string fapi_msg_data_modulation_string[] = {
    {2, "QPSK"},
    {4, "16QAM"},
    {6, "64QAM"},
    {0, NULL }
};
static const value_string fapi_msg_antenna_selection_string[] = {
    {0, "Not Configured"},
    {1, "UE port 0"},
    {2, "UE port 1"},
    {0, NULL }
};
static const value_string fapi_msg_data_acknack_mode_string[] = {
    {0, "Bundling"},
    {1, "Multiplexing"},
    {2, "Special Bundling"},
    {0, NULL }
};
static const true_false_string fapi_msg_data_ul_tx_mode_tfs = {
    "MIMO",
    "SISO/SIMO"
};
static const true_false_string fapi_msg_presence_tfs = {
    "Present",
    "Not Present"
};
static const true_false_string fapi_msg_csi_report_type_tfs = {
    "Aperiodic",
    "Periodic"
};
static const true_false_string fapi_msg_data_swap_flag_tfs = {
    "TB2/TB1",
    "TB1/TB2"
};
static const true_false_string fapi_msg_data_shift_tfs = {
    "No Shift",
    "Shift"
};
static const true_false_string fapi_msg_data_vir_rb_type_tfs = {
    "Distributed",
    "Localized"
};
static const true_false_string fapi_msg_data_pmi_conf_tfs = {
    "Use PMI from last PUSCH",
    "Use current PMI"
};
static const true_false_string fapi_msg_data_Hop_tfs = {
    "Hopping",
    "No Hopping"
};
static const true_false_string fapi_msg_data_all_Type_tfs = {
    "Type 1",
    "Type 0"
};
static const true_false_string fapi_msg_data_ACK_NACK_tfs = {
    "ACK",
    "NACK"
};
static const true_false_string fapi_msg_data_tx_req_tag_tfs = {
    "Pointer",
    "Value"
};
static const true_false_string fapi_msg_data_highspeedflag_tfs = {
    "Restricted Set",
    "Non Restricted Set"
};
static const true_false_string fapi_msg_data_trans_comb_tfs = {
    "Odd",
    "Even"
};
static const true_false_string fapi_msg_data_requested_flag_tfs = {
    "Requested",
    "Not Requested"
};
static const true_false_string fapi_msg_data_crc_tfs = {
    "Error",
    "Correct"
};
static const true_false_string fapi_msg_data_pch_mcs_tfs = {
    "Unknown",
    "QPSK"
};
static const true_false_string fapi_msg_data_valid_flag_tfs = {
    "Valid Parameters",
    "Invalid Parameters"
};
static const true_false_string fapi_msg_data_disable_flag_tfs = {
    "Disabled",
    "Not Disabled"
};
static const value_string fapi_msg_data_cqi_sci_size_string[] = {
    { 0, "1 bit"       },
    { 1, "2 bits"      },
    { 0, NULL          }
};
static const value_string fapi_msg_data_harq_type_string[] = {
    { 1, "ACK"         },
    { 2, "NACK"        },
    { 3, "ACK/NACK"    },
    { 4, "DTX"         },
    { 5, "ACK/DTX"     },
    { 6, "NACK/DTX"    },
    { 7, "ACK/NACK/DTX"},
    { 0, NULL          }
};
static const value_string fapi_msg_data_config_tlv_tag_string[] = {
    { 1, "Duplexing Mode" },
    { 2, "PCFICH Power Offset" },
    { 3, "P-B" },
    { 4, "DL Cyclic Prefix Type" },
    { 5, "UL Cyclic Prefix Type" },
    /* RF Config */
    { 6, "DL Channel BW" },
    { 7, "UL Channel BW" },
    { 8, "Reference Signal Power" },
    { 9, "Tx Antenna Ports" },
    { 10, "Rx Antenna Ports" },
    /* PHICH Config */
    { 11, "PHICH Resource" },
    { 12, "PHICH Duration" },
    { 13, "PHICH Power Offset" },
    /* SCH Config */
    { 14, "Primary Sync. signal EPRE/EPRERS" },
    { 15, "Secondary Sync. signal EPRE/EPRERS" },
    { 16, "Physical Cell ID" },
    /* PRACH Config */
    { 17, "Config. Index" },
    { 18, "Root Sequence Index" },
    { 19, "Zero Correlation Zone Config." },
    { 20, "High Speed Flag" },
    { 21, "Frequency Offset" },
    /* PUSCH Config */
    { 22, "Hopping Mode" },
    { 23, "Hopping Offset" },
    { 24, "No of Subbands" },
    /* PUCCH Config */
    { 25, "Delta PUCCH Shift" },
    { 26, "No of CQI RBs" },
    { 27, "N_AN CS" },
    { 28, "N1Pucch-AN" },
    /* SRS Config */
    { 29, "BW Config." },
    { 30, "MaxUpPTS" },
    { 31, "SRS SF Config." },
    { 32, "SRS-AckNack Simultaneous Transmission" },
    /* Uplink Reference Signal Config */
    { 33, "Uplink RS Hopping" },
    { 34, "Group Assignment" },
    { 35, "CS 1 for DMRS" },
    /* TDD Frame Structure Config */
    { 36, "Subframe Assignment" },
    { 37, "Special Subframe Patterns" },
    /* Physical capabilities for L2/L3 Software */
    { 40, "Downlink Bandwidth Support" },
    { 41, "Uplink Bandwidth Support" },
    { 42, "Downlink Modulation Support" },
    { 43, "Uplink Modulation Support" },
    { 44, "PHY Antenna Capability" },
    { 50, "Data Report Mode" },
    { 51, "SFN/SF" },
    { 60, "PHY State" },
    { 0, NULL }
};
static const value_string fapi_msg_data_config_tlv_tag_release10_string[] = {
    { 1, "Duplexing Mode" },
    { 2, "PCFICH Power Offset" },
    { 3, "P-B" },
    { 4, "DL Cyclic Prefix Type" },
    { 5, "UL Cyclic Prefix Type" },
    /* RF Config */
    { 10, "DL Channel BW" },
    { 11, "UL Channel BW" },
    { 12, "Reference Signal Power" },
    { 13, "Tx Antenna Ports" },
    { 14, "Rx Antenna Ports" },
    /* PHICH Config */
    { 20, "PHICH Resource" },
    { 21, "PHICH Duration" },
    { 22, "PHICH Power Offset" },
    /* SCH Config */
    { 30, "Primary Sync. signal EPRE/EPRERS" },
    { 31, "Secondary Sync. signal EPRE/EPRERS" },
    { 32, "Physical Cell ID" },
    /* PRACH Config */
    { 40, "Config. Index" },
    { 41, "Root Sequence Index" },
    { 42, "Zero Correlation Zone Config." },
    { 43, "High Speed Flag" },
    { 44, "Frequency Offset" },
    /* PUSCH Config */
    { 50, "Hopping Mode" },
    { 51, "Hopping Offset" },
    { 52, "No of Subbands" },
    /* PUCCH Config */
    { 60, "Delta PUCCH Shift" },
    { 61, "No of CQI RBs" },
    { 62, "N_AN CS" },
    { 63, "N1Pucch-AN" },
    /* SRS Config */
    { 70, "BW Config." },
    { 71, "MaxUpPTS" },
    { 72, "SRS SF Config." },
    { 73, "SRS-AckNack Simultaneous Transmission" },
    /* Uplink Reference Signal Config */
    { 80, "Uplink RS Hopping" },
    { 81, "Group Assignment" },
    { 82, "CS 1 for DMRS" },
    /* TDD Frame Structure Config */
    { 90, "Subframe Assignment" },
    { 91, "Special Subframe Patterns" },
    /* MBSFN Config */
    { 100, "MBSFN area ID" },
    /* PRS Config */
    { 110, "PRS Bandwidth" },
    { 111, "PRS cyclic prefix type" },
    /* Physical capabilities for L2/L3 Software */
    { 200, "Downlink Bandwidth Support" },
    { 201, "Uplink Bandwidth Support" },
    { 202, "Downlink Modulation Support" },
    { 203, "Uplink Modulation Support" },
    { 204, "PHY Antenna Capability" },
    { 205, "Release Capability" },
    { 206, "MBSFN Capability" },
    { 240, "Data Report Mode" },
    { 241, "SFN/SF" },
    { 250, "PHY State" },
    { 0, NULL }
};
static const value_string fapi_msg_data_carrier_bw[] = {
    { 0, "1.4 MHz" },
    { 1, "3 MHz" },
    { 2, "5 MHz" },
    { 3, "10 MHz" },
    { 4, "15 MHz" },
    { 5, "20 MHz" },
    { 0, NULL }
};
static const value_string fapi_msg_data_phich_duration[] = {
    { 0, "Normal" },
    { 1, "Extended" },
    { 0, NULL }
};
static const value_string FAPI_E_RX_PDU_TYPEs[] = {
    { 0, "FAPI_E_PRACH_CMD" },
    { 1, "FAPI_E_PUSCH_CMD" },
    { 2, "FAPI_E_PUCCH_CMD" },
    { 3, "FAPI_E_SRS_CMD" },
    { 4, "FAPI_E_HARQ_CMD" },
    { 5, "DUPLICATE" },
    { 0, NULL }
};
static const value_string DUPLICATE_ERROR[] = {
    { 0, "FAPI_E_CORESET_CMD" },
    { 1, "FAPI_E_SSB_CMD" },
    { 2, "FAPI_E_PDSCH_CMD" },
    { 3, "FAPI_E_CSIRS_CMD" },
    { 5, "DUPLICATE_PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_TX_PDU_TYPEs[] = {
    { 0, "FAPI_E_CORESET_CMD" },
    { 1, "FAPI_E_SSB_CMD" },
    { 2, "FAPI_E_PDSCH_CMD" },
    { 3, "FAPI_E_CSIRS_CMD" },
    { 0, NULL }
};
static const value_string FAPI_E_UCI_PDU_TYPE[] = {
    { 0, "FAPI_E_UCI_PUSCH_PDU" },
    { 1, "FAPI_E_UCI_PUCCH_PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_INFRA_PDU_TYPE[] = {
    { 4, "FAPI_E_HARQ_RT_CMD" },
    { 5, "FAPI_E_FRAME_TRIGGER_CMD" },
    { 6, "FAPI_E_INFRA_PDU_LAS" },
    { 0, NULL }
};
static const value_string FAPI_E_RACH_PDU_TYPE[] = {
    { 0, "FPAI_E_PRACH_PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_PUSCH_PDU_TYPE[] = {
    { 0, "FAPI_E_PUSCH_DECODER_PDU" },
    { 1, "FAPI_E_PUSCH_UCI_PDU" },
    { 2, "FAPI_E_PUSCH_PTRS_PDU" },
    { 3, "FAPI_E_DFTS_OFDM_PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_RX_DATA_PDU_TYPE[] = {
    { 0, "FAPI_E_DECODE_STATUS_PDU" },
    { 1, "FAPI_E_UCI_INDICATION_PUSCH_PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_ULSCH_DECODE[] = {
    { 0x2160, "Decode Status base PDU" },
    { 0, NULL }
};
static const value_string FAPI_E_RX_TAG[] = {
    { 0x21B0, "PADDING TAG PDU" },
    { 0x2150, "Base ULSCH Data PDU" },
    { 0, NULL }
};
static const value_string fapi_msg_data_phich_resource[] = {
    { 0, "1/6" },
    { 1, "1/2" },
    { 2, "1" },
    { 3, "2" },
    { 0, NULL }
};
/* Preferences bool to control whether we are in big or little endians */
static gboolean global_fapi_sdk_BIG_ENDIAN = FALSE;
/* Preferences bool to control whether or not to dissect MAC Layer */
static gboolean global_fapi_sdk_dissect_MAC_DL = FALSE;
static gboolean global_fapi_sdk_dissect_MAC_UL = FALSE;
/* Preferences bool to control whether to parse as API 1.7 (7) */
static gint global_fapi_sdk_parse_as_api = 8;
static enum_val_t global_fapi_sdk_parse_as_api_enum[] = {
    {"API_1_8","1.8",8},
    { NULL, NULL, 0 }
};
/* Preferences bool to control for Vendor Specific*/
static gint global_fapi_sdk_parse_vsm_api_ver = 6;
static enum_val_t global_fapi_sdk_parse_vsm_api_ver_enum[] = {
    {"NO_VSM","None",0},
    {"API_1_2","1.2",2},
    {"API_1_3","1.3",3},
    {"API_1_4","1.4",4},
    {"API_1_5","1.5",5},
    {"API_1_6","1.6",6},
    { NULL, NULL, 0 }
};
/* Preferences bool to control for Vendor Specific*/
static gint global_fapi_sdk_parse_lte_release_ver = 8;
static enum_val_t global_fapi_sdk_parse_lte_release_ver_enum[] = {
    {"RELEASE_8","8",8},
    {"RELEASE_10","10",10},
    { NULL, NULL, 0 }
};
/* Preferences bool to control the duplex mode used when dissecting payload */
static gint global_fapi_sdk_duplex_mode_val = FDD_RADIO;
static const enum_val_t global_fapi_sdk_duplex_mode_enum[] =
{
    { "FDD", "FDD", FDD_RADIO},
    { "TDD", "TDD", TDD_RADIO},
    { NULL, NULL, 0},
};
/* Preferences bool to control whether or not to dissect 3 new fields in release 10: DL_DCI:associated UL, CSI Size req field; UL_DCI: trans_mode */
static gboolean global_fapi_sdk_dissect_rel10_dci_fields = TRUE;
/* Wireshark ID of the DAN LTE SDK protocol */
static int proto_fapi_sdk = -1;
static int proto_phy_sdk=-1;
/* These are the handles of our sub-dissectors */
static dissector_handle_t data_handle=NULL;
static dissector_handle_t fapi_sdk_handle;
static dissector_handle_t mac_lte_handle;
static void dissect_fapi_sdk(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);
/* The following hf_* variables are used to hold the Wireshark IDs of
    * our header fields; they are filled out when we call
    * proto_register_field_array() in proto_register_fapi_sdk()
*/
/* FAPI pi-e header*/
static gint hf_fapi_sdk_pi_e_header_type = -1;
static gint hf_fapi_sdk_pi_e_header_seq = -1;
static gint hf_fapi_sdk_pi_e_header_size = -1;
static gint hf_fapi_sdk_pi_e_header_frag = -1;
static gint hf_fapi_sdk_pi_e_header_nf = -1;
static gint hf_fapi_sdk_pi_e_header_nsf = -1;
static gint hf_fapi_sdk_pi_e_header_new_type = -1;
static gint hf_fapi_sdk_pi_e_header_new_seq = -1;
static gint hf_fapi_sdk_pi_e_header_padding = -1;
static gint hf_fapi_sdk_pi_e_header_timestamp = -1;
static gint hf_fapi_sdk_pi_e_header_sector_id = -1;
/* FAPI msg header */
static gint hf_fapi_sdk_msg_header_type = -1;
static gint hf_fapi_sdk_msg_header_len_ven = -1;
static gint hf_fapi_sdk_msg_header_len_body = -1;
static gint hf_fapi_sdk_msg_header_len_flag = -1;
static gint hf_fapi_sdk_msg_header_len_sequence = -1;
static gint hf_fapi_sdk_msg_header_len_timestamp = -1;
static gint hf_fapi_sdk_msg_header_len_numphy = -1;
static gint hf_fapi_sdk_msg_header_len_phyid =-1;
static gint hf_fapi_sdk_msg_header_len_phylen=-1;
/* FAPI msg header ends */
static gint dlPtrsPresent=-1;
static gint dlDataPduIndex=-1;
static gint PDUcount=-1;
static gint dlDataPduLength=-1;
static gint dlDataPduBytes=-1;
//5G
static gint PNF_CAPABILITY_RSP_error_code =-1;
static gint PNF_CAPABILITY_RSP_num_TLV =-1;
static gint ssPbchMultipleCarriersInABand = -1;
static gint multipleCellSsPbchsInACarrier = -1;
static gint ssPbchDigitalBfTechniques= -1;
static gint prachLongFormats=-1;
static gint prachShortFormats=-1;
static gint prachRestrictedSets=-1;
static gint prachMultipleCarriersInABand=-1;
static gint numberOfPrachSlotsAllowedIna60kHzSlot=-1;
static gint nSsPbchsPerRachOccasion=-1;
static gint phy_prach_ncs=-1;
static gint cceMappingType  =-1;
static gint coresetOutsideFirst3OfdmSymsOfSlot=-1;
static gint coresetNonContigFreqDomainAlloc=-1;
static gint precoderGranularityCoreset=-1;
static gint pdcchMuMimo=-1;
static gint pdcchPrecoderCycling=-1;
static gint maxPdcchPrecoderCycleLength=-1;
static gint pucchFormats=-1;
static gint pucchIntraslotFreqHopping=-1;
static gint pucchInterslotFreqHopping=-1;
static gint twoSybmolShortFormat=-1;
static gint pucchGroupHopping=-1;
static gint pucchNrofSlots=-1;
static gint pucchMultiplexingUe=-1;
static gint additionalDmrs=-1;
static gint pi2Bpsk=-1;
static gint pnftype=-1;
static gint pnfsize=-1;
static gint pdschMappingTypes=-1;
static gint pdschAllocationTypes=-1;
static gint pdschMuMimoOverlap=-1;
static gint pdschVrbToPrbMapping=-1;
static gint pdschAggregationFactor=-1;
static gint pdschCbg=-1;
static gint pdschDmrsConfigTypes=-1;
static gint pdschDmrsMaxLength=-1;
static gint pdschDmrsAdditionalPos=-1;
static gint pdschDataInDmrsSymbols=-1;
static gint preemptionSupport=-1;
static gint pdschNonSlotSupport=-1;
static gint pdschPrecoderCycling=-1;
static gint maxPdschPrecoderCycleLength=-1;
static gint uciMuxUlschInPusch=-1;
static gint uciOnlyPusch=-1;
static gint harqAckNackInUciOnPusch=-1;
static gint puschDataInDmrsSymbols=-1;
static gint maxDigBfTableEntries=-1;
static gint pdschCsirsTimeFrequencyOverlap=-1;
static gint pdschPdcchTimeFrequencyOverlap=-1;
static gint pdschSspbchTimeFrequencyOverlap=-1;
static gint puschPrecodingType=-1;
static gint puschFrequencyHopping=-1;
static gint puschDmrsConfigTypes=-1;
static gint puschDmrsMaxLen=-1;
static gint puschDmrsAdditionalPos=-1;
static gint puschCbg=-1;
static gint puschMappingTypes=-1;
static gint puschAllocationTypes=-1;
static gint puschMuMimoOverlap=-1;
static gint puschVrbToPrbMapping=-1;
static gint puschAggregationFactor=-1;
static gint puschLbrm=-1;
static gint puschMaxPtrsPorts=-1;
static gint cdmDmrsPortsOfMuMimoUsers=-1;
static gint puschNonSlotSupport=-1;
static gint srsSupport=-1;
static gint freqHoppingSupport=-1;
static gint groupHoppingSupport=-1;
static gint sequenceHoppingSupport=-1;
static gint reservedResourceTypes=-1;
static gint Cp=-1;
static gint ssPbchRmsiMuxPatterns=-1;
static gint sulSupport=-1;
static gint supportedUserMuxSchemesInSlot=-1;
static gint dynamicSlotFormatSupport=-1;
static gint nBandCombinations=-1;
static gint nBands=-1;
static gint bandList=-1;
static gint featureSetCombinationId=-1;
static gint  nFeatureSetsDownlinkPerCc=-1;
static gint  nFeatureSetsDownlink=-1;
static gint  nFeatureSetsUplinkPerCc=-1;
static gint  nFeatureSetsUplink=-1;
static gint  nFeatureSetPerBand=-1;
static gint  nFeatureSetCombinations=-1;
static gint  supportedSubcarrierSpacingsDl=-1;
static gint  supportedBandwidthDl=-1;
static gint  maxNumberMimoLayersPdsch=-1;
static gint  supportedModulationOrderDl=-1;
static gint  maxMuMimoUsersDl=-1;
static gint  supportedSubcarrierSpacingsUl=-1;
static gint  supportedBandwidthUl=-1;
static gint  maxNumberMimoLayersNonCbPusch=-1;
static gint  supportedModulationOrderUl=-1;
static gint  dftsOfdmSupport=-1;
static gint  maxMuMimoUsersUl=-1;
static gint  featureSetDownlinkId=-1;
static gint  featureSetUplinkId=-1;
static gint  nTrxus=-1;
static gint  nCcs=-1;
static gint  featureSetDownlinkPerCcIds=-1;
static gint  intraBandFreqSeparationDl=-1;
static gint  maxTotalBw=-1;
static gint  UnCcs=-1;
static gint  UfeatureSetUplinkCcIds=-1;
static gint  UintraBandFreqSeparationUl=-1;
static gint  UmaxTotalBw=-1;
static gint  CnBands=-1;
static gint  featureSetPerBandIds=-1;
//ssPbch
static gint  maxSsbsPerCarrierPerCellInBurst=-1;
static gint  minSsPbchBurstPeriodicity=-1;
static gint  maxSsPbchBlocksPerSlot=-1;
//Prach
static gint  maxPrachFdOccasionsInASlot=-1;
static gint  maxPrachFdOccasionsInASlotPerCarrier=-1;
static gint  maxRootSequencesPerSlotPerFd=-1;
static gint  supportedSubcarrierSpacingsPrachShort=-1;
static gint  supportedSubcarrierSpacingsPrachLong=-1;
static gint  N5=-1;
static gint  prachConfigIdx=-1;
static gint  cp1=-1;
//pdcch
static gint  maxPdcchsPerSlot=-1;
//pucch
static gint  pucchPolarMaxK=-1;
static gint  pucchPolarMaxN=-1;
static gint  pucchUciMaxE=-1;
static gint  maxPucchsPerSlot=-1;
static gint  betaPdcchRange=-1;
static gint  betaPdschRange=-1;
static gint  P2=-1;
//pdsch
static gint  maxPdschHarqProcesses=-1;
static gint  maxPdschTransportBlocks=-1;
static gint  N31=-1;
static gint  N32=-1;
static gint  K3=-1;
//pusch
static gint  maxPuschHarqProcesses=-1;
static gint  maxPuschTransportBlocks=-1;
static gint  maxUciOnlyPuschs=-1;
static gint  puschUciMaxK=-1;
static gint  puschUciMaxN=-1;
static gint  puschUciMaxE=-1;
static gint  N41=-1;
static gint  N42=-1;
static gint  P1=-1;
static gint  K4=-1;
static gint  maxDurationWithinReTxInSlots=-1;
//csiRs
static gint  maxNumPortsCsirsTxPerSymbol=-1;
static gint  maxNumAggrPortsCsirsTxPerSlot=-1;
//Srs
static gint  maxNumPortsPerUe=-1;
static gint  maxNumSymbolsPerUe=-1;
static gint  maxNumRepetitionsPerUe=-1;
static gint  maxNumAggrPortsPerUe=-1;
static gint  maxNumAggrPortsPerSlot=-1;
//Misc
static gint  dlusersPerSlot=-1;
static gint  ulusersPerSlot=-1;
static gint  maxNumPolarDecodesPerSlot=-1;
static gint  maxNumCsiReportsPerSlot=-1;
//pnf_confgi_req
static gint pnf_tag=-1;
static gint pnf_length=-1;
static gint tag=-1;
static gint length=-1;
static gint carrier_tag=-1;
static gint carrier_length=-1;
static gint numPhyConfig=-1;
static gint phyId=-1;
static gint nCarriers=-1;
static gint nCells=-1;
static gint cellConfig=-1;
static gint downlinkBandwidth=-1;
static gint downlinkFrequency=-1;
static gint downlinkGridStart=-1;
static gint downlinkGridSize=-1;
static gint uplinkBandwidth=-1;
static gint pnfPhyIndex=-1;
static gint uplinkFrequency=-1;
static gint uplinkGridStart=-1;
static gint uplinkGridSize=-1;
static gint uplinkFrequencyShift7p5khz=-1;
static gint referenceSsPbchPower=-1;
static gint BFtag=-1;
static gint BFlength=-1;
static gint BFnumEntries=-1;
static gint numRows=-1;
static gint numColumns=-1;
static gint digBeamValReal=-1;
static gint digBeamValImag=-1;
static gint phyCellId=-1;
static gint nPrachConfigs=-1;
static gint nPrachConfigs1=-1;
static gint prachConfig=-1;
static gint prachSequenceLength=-1;
static gint deltaFRa=-1;
static gint restrictedSetConfig=-1;
static gint numPrachFdOccasions=-1;
static gint prachRootSequenceIndex=-1;
static gint numRootSequences=-1;
static gint k1 =-1;
static gint k11 =-1;
static gint numUnusedRootSequences=-1;
static gint unusedRootSequences=-1;
static gint respErrorCode=-1;
static gint respnCarriers=-1;
static gint txDirectCurrentLocation=-1;
static gint pnfrspErrorCode=-1;
static gint config_resp_msg_ok=-1;
static gint config_resp_msg_invalid=-1;
static gint config_resp_msg_invalid_config=-1;
static gint pnf_start_resp_error_codes=-1;
static gint  phy_Error_Code=-1;
static gint  phy_Num_TLVs=-1;
static gint  phy_tag=-1;
static gint  phy_Length=-1;
static gint  timingInfoMode=-1;
static gint  timingInfoPeriod=-1;
static gint  phy_error_Msg_ID=-1;
static gint  Error_Code_dependent_values=-1;
static gint  SFN=-1;
static gint  Slot=-1;
static gint  Numerology=-1;
static gint  Num_PDU_TLVs=-1;
static gint  pduType=-1;
static gint  pduLength=-1;
static gint  bandwidthPart=-1;
static gint  frequencyDomainResources=-1;
//static gint  startSymbol=-1;
static gint  duration =-1;
static gint  regBundleSize=-1;
static gint  cceRegMappingType=-1;
static gint  interleaverSize=-1;
static gint  shiftIndex=-1;
static gint  precoderGranularity=-1;
static gint  nId=-1;
static gint  txnId=-1;
static gint  nId1=-1;
static gint  dmrsSubcarrier0Reference=-1;
static gint  nPdcch=-1;
static gint  pdcch=-1;
static gint  rnti=-1;
static gint  nRnti=-1;
static gint  cceIndex=-1;
static gint  aggegrationLevel=-1;
static gint  payloadSizeInBits=-1;
static gint  payload=-1;
static gint  betaPdcch=-1;
static gint  cycleLength=-1;
static gint  nCsiRsPorts=-1;
//static gint  precoderIndices=-1;
static gint  physCellId=-1;
static gint  digBfIndices=-1;
static gint  betaPss=-1;
static gint  ssbIndex=-1;
static gint  bchPayload=-1;
static gint  msbOfKssbInSspbchTypeA=-1;
static gint  ssbCrbOffset=-1;
static gint  ssbSubCarrierOffset=-1;
static gint  digBfMethod=-1;
static gint  digBfCyclicDelay=-1;
static gint  digBfBeamIndex=-1;
static gint  pduBitmap=-1;
static gint  upduBitmap=-1;
static gint  uHandle=-1;
static gint  ulocation=-1;
static gint  ubandwidth=-1;
static gint  ucyclicPrefix=-1;
static gint  ubandwidthPart=-1;
static gint  uqamModOrder=-1;
static gint  uinitialCodeRate=-1;
static gint  unRnti=-1;
static gint  ulDataScramblingId=-1;
static gint  ulDmrsSymPositions=-1;
static gint  ulDmrsConfigType=-1;
static gint  ulDmrsScramblingId =-1;
static gint  unScid =-1;
static gint  ulDmrsCdmGroupsNoData=-1;
static gint  ulDmrsPorts=-1;
static gint  urbBitmap=-1;
static gint  ustartSymbol=-1;
static gint  usymbolLength=-1;
static gint  ufreqHoppingEnable=-1;
static gint  utxDirectCurrentLocation=-1;
static gint  uuplinkFrequencyShift7p5khz=-1;
static gint  pduIndex=-1;
static gint  bwp=-1;
static gint  tbSizeBytes=-1;
static gint  tbSizeLbrmBytes=-1;
static gint  rvIndex=-1;
static gint  targetCodeRate=-1;
static gint  qamModOrder=-1;
static gint  dmrsSeqGenRefPoint=-1;
static gint  betaPdsch=-1;
static gint  pdschDmrs=-1;
static gint  pdschDataScrambling=-1;
static gint  pdschAllocation=-1;
static gint  digitalBf=-1;
static gint  dlDmrsSymbPos=-1;
static gint  dlDmrsConfigType=-1;
static gint  dlDmrsScramblingId=-1;
static gint  yonScid=-1;
static gint  numDmrsCdmGrpsNoData=-1;
static gint  dmrsPorts=-1;
static gint  erSFN=-1;
static gint  erSlot=-1;
static gint  ermsgId=-1;
static gint  ererrorCode=-1;
static gint  sub_error_code=-1;

static gint  duplicate_error=-1;
static gint  pdu_index_error=-1;
static gint  pdu_type_error=-1;
static gint  pdu_type_error1=-1;
static gint  error_code=-1;
static gint  error_value=-1;
static gint  expected_value=-1;

static gint  direction=-1;
static gint  errnti=-1;
static gint  pdu_type=-1;
static gint  pdu_type1=-1;
static gint  pdu_sub_type=-1;
static gint  target_sfn=-1;
static gint  current_sfn=-1;
static gint  target_slot=-1;
static gint  current_slot=-1;
static gint  rxSFN=-1;
static gint  rxSlot=-1;
static gint  rxNumerology=-1;
static gint  rxPDUcount=-1;
static gint  rxpduType=-1;
static gint  rxpduLength=-1;
static gint  rxpdu=-1;
static gint  rxtag=-1;
static gint  rxlength=-1;
static gint  rxhandle=-1;
static gint  rxrnti=-1;
static gint  rxharqId=-1;
static gint  rxpayloadSize=-1;
static gint  rxpayload=-1;
static gint  puschrvIndex=-1;
static gint  tbSize=-1;
static gint  harqNum=-1;
static gint  isReTx=-1;
static gint  numCb=-1;
static gint  cbPresentAndPosition=-1;
static gint  harqAckBitLength=-1;
static gint  onPuschScalingAlpha=-1;
static gint  betaOffsetHarqAck=-1;
static gint  betaOffsetCsi1=-1;
static gint  betaOffsetCsi2=-1;
static gint  csiPart2Indicator=-1;
static gint  numCsiReports=-1;
static gint  csiPart1BitLength=-1;
static gint  riOffset=-1;
static gint  riBitwidth=-1;
static gint  ulPtrs0ReOffset=-1;
static gint  ulPtrs1ReOffset=-1;
static gint  ulPtrs0DmrsPort=-1;
static gint  ulPtrs0DmrsAssoc=-1;
static gint  ulPtrs1DmrsPort=-1;
static gint  ulPtrs1DmrsAssoc=-1;
static gint  ulPtrsFrequencyDensity=-1;
static gint  ulPtrsTimeDensity=-1;
static gint  ulPtrsPower=-1;
static gint  lowPaprGroupNumber=-1;
static gint  lowPaprSequenceNumber=-1;
static gint  ulPtrsPreDftDensity=-1;
static gint  ulPtrsTimeDensityTransformPrecoding=-1;
static gint  csiPart2BitLength=-1;
static gint  rbBitmap=-1;
static gint  startSymbol=-1;
static gint  symbolLength=-1;
static gint  vrbToPrbMapping=-1;
static gint  prgSize=-1;
static gint  precoderCycleLength=-1;
static gint  precoderIndices=-1;
static gint  nPrg=-1;
static gint  Num_nPrg=-1;
static gint  pmi=-1;
static gint  digBfIndex=-1;
static gint  dlPtrsTimeDensity=-1;
static gint  dlPtrsFreqDensity=-1;
static gint  dlPtrsReOffset=-1;
static gint  betaPtrs=-1;
static gint  getDlTbCrc=-1;
static gint  IsLastCbPresent=-1;
static gint  isInlineTbCrc=-1;
static gint  dlTbCrc=-1;
static gint  nScid=-1;
static gint  type=-1;
static gint  startRb=-1;
static gint  numRbs=-1;
static gint  Density=-1;
static gint  cdmType=-1;
static gint  betaCsirsSs=-1;
static gint  freqStartLocation=-1;
static gint  timeStartLocation=-1;
static gint  location=-1;
static gint  bandwidth=-1;
static gint  yobandwidth=-1;
static gint  cyclicPrefix=-1;
static gint  Number_of_invalid_TLVs=-1;
static gint  Number_of_missing_TLVs=-1;
static gint  Number_configured_init=-1;
static gint  Number_configured_cell_stopped=-1;
static gint  phy_conf_invalidTLV=-1;
static gint  phy_conf_msiingTLV=-1;
static gint  phy_conf_configuredintit=-1;
static gint  phy_conf_configuredcellstop=-1;
static gint  sfn=-1;
static gint  slot=-1;
//PUCCH
static gint  pucch_tag=-1;
static gint  pucch_length=-1;
static gint  pucch_handle=-1;
static gint  pucch_nRnti=-1;
static gint  pucch_multiSlotTxIndicator=-1;
static gint  upucchFormat=-1;
static gint  upucchGroupHopping=-1;
static gint  pucch_hoppingId=-1;
static gint  pucch_startSymbol=-1;
static gint  pucch_numSyms=-1;
static gint  pucch_interSlotFreqHopEnable=-1;
static gint  pucch_intraSlotFreqHopEnable=-1;
static gint  pucch_nPucchRb=-1;
static gint  pucch_startingPrb=-1;
static gint  pucch_secondHopPrb=-1;
static gint  pucch_detectType=-1;
static gint  pucch_harqAckBitLength=-1;
static gint  pucch_initialCyclicShift=-1;
static gint  pucch_txDirectCurrentLocation=-1;
static gint  pucch_orthoSeqIndexFormat1=-1;
static gint  pucch_nId=-1;
static gint  scramblingId0=-1;
static gint  occLengthFormat4=-1;
static gint  occIndexFormat4=-1;
static gint  pucchPayloadLengthBits=-1;
static gint  pucchPayloadLenCSIPart2=-1;
//PRACH
static gint prach_tag = -1;
static gint prach_length = -1;
static gint prach_phys_cell_id = -1;
static gint prach_format = -1;
static gint prach_nRa_Slot_T = -1;
static gint prach_nRa = -1;
static gint prach_start_symbol = -1;
static gint prach_ncs = -1;
static gint prach_digital_bf_info = -1;
//
//RACH_IND
static gint Prach_sfn = -1;
static gint Prach_slot = -1;
static gint Prach_numerology = -1;
static gint Prach_pdu_count = -1;
static gint prach_pdu_type = -1;
static gint prch_pdu_length = -1;
static gint prach_ind_tag = -1;
static gint prach_ind_length = -1;
static gint prach_ind_phys_cell_id = -1;
static gint prach_ind_occasion_td = -1;
static gint prach_occasion_fd = -1;
static gint prach_avg_rssi_ind_db = -1;
static gint prach_detectionMetricdb =-1;
static gint prach_number_of_preamble = -1;
static gint prach_detected_premable_indices = -1;
static gint prach_timing_estimates = -1;
//
//ULSCH_DECODE
static gint  desfn=-1;
static gint  deslot=-1;
static gint  denumerology=-1;
static gint  depduCount=-1;
static gint  depduType=-1;
static gint  depduLength=-1;
static gint  detag=-1;
static gint  delength=-1;
static gint  depduBitmap=-1;
static gint  dehandle=-1;
static gint  dernti=-1;
static gint  deharqId=-1;
static gint  detbCrcStatus=-1;
static gint  denumCb=-1;
static gint  decbCrcStatus=-1;
static gint  deulSnr=-1;
static gint  detimingAdvance=-1;
//
//ULSCH_DATA
static gint rxdhandle=-1;
static gint rxdrnti=-1;
static gint rxdharqId=-1;
static gint rxdpayloadSize=-1;
static gint rxdpayload=-1;
static gint rx_padding_tag=-1;
static gint rx_length1=-1;
static gint rx_base_uldsch=-1;
static gint rx_length2=-1;
//
//UCI_INDICATION
static gint uci_sfn=-1;
static gint uci_slot=-1;
static gint uci_numerology=-1;
static gint uci_pduCount=-1;
static gint uci_pduType=-1;
static gint uci_pduLength=-1;
static gint uci_tag=-1;
static gint uci_length=-1;
static gint uci_pduBitmap=-1;
static gint uci_handle=-1;
static gint uci_rnti=-1;
static gint uci_uePucchIndex=-1;
static gint uci_numUciPayload=-1;
static gint uci_uciPayloadCrcStatus=-1;
static gint uciPayloadLength=-1;
static gint uciBits=-1;
static gint ulSnr=-1;
static gint timingAdvance=-1;
//PUSCH_UCI
static gint pusch_UCI_length=-1;
static gint pusch_UCI_tag=-1;
static gint pucch_pduBitmap=-1;
static gint pucch_Handle=-1;
static gint pucch_RNTI=-1;
static gint pucch1_harqAckBitLength=-1;
static gint pucch_harqAckCrcStatus=-1;
static gint pucch_harqAckBits=-1;
static gint pucch_csiPart1BitLength=-1;
static gint pucch_csiPart1CrcStatus=-1;
static gint pucch_csiPart1Bits=-1;
static gint pucch_csiPart2BitLength=-1;
static gint pucch_csiPart2CrcStatus=-1;
static gint pucch_csiPart2Bits=-1;
static gint pusch_ulSnr=-1;
static gint pusch_timingAdvance=-1;
//
//INFRA_CMD
static gint infra_pduType=-1;
static gint infra_pduLength=-1;
static gint infra_sfn=-1;
static gint infra_slot=-1;
static gint infra_numerology=-1;
static gint infra_numPdu=-1;
static gint infra_tag=-1;
static gint infra_length=-1;
static gint infra_gpio=-1;
static gint infra_enable=-1;
static gint infra_targetSfn=-1;
static gint infra_targetSlot=-1;
static gint infra_timingAdvance=-1;
static gint infra_repeat=-1;
static gint infra_tag2=-1;
static gint infra_length2=-1;
static gint infra_gpio2=-1;
static gint infra_enable2=-1;
static gint infra_timingAdvance2=-1;
static gint infra_baudrate=-1;
static gint infra_sfn2=-1;
static gint infra_slot2=-1;
static gint infra_numerology2=-1;
static gint infra_numDataBits=-1;
static gint infra_data=-1;
//
static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_header_sfn = -1;
static gint hf_fapi_sdk_msg_header_sf = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_trans_mode = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_codeword_id = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_n_layers = -1;
static gint hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost = -1;
/* FAPI msg data (FAPI_E_DL_CONFIG_REQ) */
//static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_sfn = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_length = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdcch = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_n_dci = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdu = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdsch = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_pcfich_boost = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type_rel10 = -1;
static gint hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_size = -1;
static gint hf_fapi_sdk_msg_DL_PDU_reserved = -1;
/* FAPI msg data (PDSCH_PDU) */
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_size = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_idx = -1;
static gint hf_fapi_sdk_msg_DL_DLSCH_PDU_rnti = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_vir_rb_type = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_mcs = -1;
static gint hf_fapi_sdk_msg_DL_PCH_PDU_mcs = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_rv_idx = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_swap_flag = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_ant_mode = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_n_subbands = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_category = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_pa = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_pdsch_boost_index = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_trans_mode = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb_per_subband = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_n_bf_vectors = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_subband_idx = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_n_ant = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_vector = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_n_scid = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_flag = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_cdi_rs_resrc_config = -1;
static gint hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_tx_pwr_resrc_bitmap = -1;
/* FAPI msg data (PDCCH_PDU) */
static gint hf_fapi_sdk_msg_DCI_PDU_cross_car_flag = -1;
static gint hf_fapi_sdk_msg_DCI_PDU_car_ind = -1;
static gint hf_fapi_sdk_msg_DCI_PDU_srs_flag = -1;
static gint hf_fapi_sdk_msg_DCI_PDU_srs_req = -1;
static gint hf_fapi_sdk_msg_DCI_PDU_mcs2 = -1;
static gint hf_fapi_sdk_msg_DCI_PDU_ndi2 = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_dci_format = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_cce_offset = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_aggr_level = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_rnti = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_vir_rb_type = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_mcs1 = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_rv1 = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_ndi1 = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_swap_flag = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_rv2 = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_harq = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_pmi_conf = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_precoding = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_tpc = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_dai = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_tb_size_1c = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_dl_pa_1d = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_prach_flag_1a = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_prach_preamble_1a = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_prach_mask_1a = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_rnti_type = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_mcch_flag = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_mcch_change_note = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_scramb_id = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_ant_port_scramb_layer = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_size_csi_request = -1;
static gint hf_fapi_sdk_msg_DL_DCI_PDU_associated_ul = -1;
static gint hf_fapi_sdk_msg_DL_PDU_n_gap = -1;
/* FAPI TTI evt (FAPI_E_SF_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
/* FAPI msg data (FAPI_E_PARAM_REQ) */
//None
/* FAPI PARAMS EVT (FAPI_E_PARAM_RSP) */
//static gint hf_fapi_sdk_msg_CONFIG_RSP_err_code = -1;
static gint hf_fapi_sdk_msg_PARAM_RSP_n_tlv = -1;
/* FAPI msg data (FAPI_E_CONFIG_REQ) */
static gint hf_fapi_sdk_msg_CONFIG_REQ_n_tlv = -1;
/* FAPI msg data (FAPI_E_CONFIG_RSP) */
static gint hf_fapi_sdk_msg_CONFIG_RSP_err_code = -1;
static gint hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_invalid = -1;
static gint hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_missing = -1;
/* FAPI msg data (Configuration TLV) */
static gint hf_fapi_sdk_msg_CONFIG_TLV_tag = -1;
static gint hf_fapi_sdk_msg_CONFIG_TLV_tag_rel10 = -1;
static gint hf_fapi_sdk_msg_CONFIG_TLV_length = -1;
static gint hf_fapi_sdk_msg_CONFIG_TLV_value = -1;
static gint hf_fapi_sdk_msg_Carrier_TLV_value =-1;
/* FAPI msg data (FAPI_E_START_REQ) */
//None
static gint hf_fapi_sdk_msg_TX_REQ_idx = -1;
/* FAPI msg data (FAPI_E_TX_REQ) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_TX_REQ_n_phy =-1;
static gint hf_fapi_sdk_msg_TX_REQ_n_tlv = -1;
static gint hf_fapi_sdk_msg_TX_REQ_n_pdu = -1;
static gint hf_fapi_sdk_msg_TX_REQ_length = -1;
/* FAPI msg data (TX_REQ TLV) */
static gint hf_fapi_sdk_msg_TX_REQ_TLV_tag = -1;
static gint hf_fapi_sdk_msg_TX_REQ_TLV_length = -1;
static gint hf_fapi_sdk_msg_TX_REQ_TLV_value_tag0 = -1;
static gint hf_fapi_sdk_msg_TX_REQ_TLV_value_tag1 = -1;
/* DAN msg data (FAPI_E_UL_CONFIG_REQ) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_length = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_n_pdu = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_prach_freq_res = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_srs_present = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_type = -1;
static gint hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_size = -1;
/* DAN msg data (UL Common PDU Fields) */
static gint hf_fapi_sdk_msg_UL_COMMON_PDU_handle = -1;
static gint hf_fapi_sdk_msg_UL_COMMON_PDU_rnti = -1;
static gint hf_fapi_sdk_msg_UL_COMMON_PDU_ant_ports = -1;
/* DAN msg data (UCI PDU) */
static gint hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx = -1;
static gint hf_fapi_sdk_msg_UL_UCI_PDU_cqi_size = -1;
static gint hf_fapi_sdk_msg_UL_UCI_PDU_harq_info = -1;
static gint hf_fapi_sdk_msg_UL_UCI_PDU_harq_size = -1;
static gint hf_fapi_sdk_msg_UL_UCI_PDU_acknack_mode = -1;
static gint hf_fapi_sdk_msg_UL_UCI_PDU_n_res = -1;
//static gint hf_fapi_sdk_msg_UL_UCI_PDU_harq_res = -1;
/* DAN msg data (SRS PDU) */
static gint hf_fapi_sdk_msg_UL_SRS_PDU_size = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_srs_bw = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_freq_dom_pos = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_srs_hop_bw = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_trans_comb = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_config_idx = -1;
static gint hf_fapi_sdk_msg_UL_SRS_PDU_cs = -1;
/* FAPI msg data (PUSCH PDU) */
static gint hf_fapi_sdk_msg_PUSCH_PDU_size = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_rb_start = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_rb_num = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_mcs = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_cs2dmrs = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_flag = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_bits = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_ndi = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_rv = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_harq_re_tx = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_ul_tx_mode = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_curr_harq_trans = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_srs_present = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_trans_scheme = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_seq_hopping_disable = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_ri_nbits = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_acknack_nbits = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_cqi = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_ri = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_acknack = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_report_type = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_ri_size = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_ri_size = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_size = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_n_cc = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_acknack_mode_TDD = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_nrb_init = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_srs_init = -1;
static gint hf_fapi_sdk_msg_PUSCH_PDU_MCS_init = -1;
/* FAPI msg data (FAPI_E_HI_DCI0_REQ) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_dci = -1;
static gint hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_phich = -1;
static gint hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_type = -1;
static gint hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_size = -1;
/* FAPI msg data (PHICH PDU) */
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_rb_start = -1;
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_cs2dmrs = -1;
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_value = -1;
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_i_phich = -1;
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_boost = -1;
static gint hf_fapi_sdk_msg_UL_PHICH_PDU_tb2_flag = -1;
/* FAPI msg data (UL DCI PDU) */
static gint hf_fapi_sdk_msg_UL_DCI_PDU_format = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_cce_offset = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_aggr_lvl = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_rnti = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_rb_start = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_n_rb = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_mcs = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_cs2dmrs = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_flag = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_bits = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_ndi = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_ant_sel = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_tpc = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_aperiodic_cqi = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_ul_idx = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_dl_idx = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_tpc_bitmap = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_cqi_csi_size = -1;
static gint hf_fapi_sdk_msg_UL_DCI_PDU_ra_flag = -1;
/* FAPI msg data (FAPI_E_UL_PRACH_REQ) */
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_logical_root_sn = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_prach_conf_index = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_Ncs = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_prach_req_offset = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_highSpeedFlag = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_prach_format = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_REQ_thr = -1;
/* FAPI msg data (FAPI_E_RX_ULSCH_IND or FAPI_E_RX_CQI_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_n_pdu = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_length = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data_offset = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_snr = -1;
static gint hf_fapi_sdk_msg_UL_CQI_EVT_ri = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset_R9 = -1;
static gint hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data = -1;
/* FAPI msg data (FAPI_E_RX_SR_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_SR_EVT_n_sr = -1;
/* FAPI msg data (FAPI_E_HARQ_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_n_harq = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_acknack1 = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_acknack2 = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_acknack3 = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_acknack4 = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_acknack_mode = -1;
static gint hf_fapi_sdk_msg_UL_HARQ_EVT_n_acknack = -1;
/* FAPI msg data (FAPI_E_CRC_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_CRC_EVT_n_crc = -1;
static gint hf_fapi_sdk_msg_UL_CRC_EVT_crc_flag = -1;
/* FAPI msg data (FAPI_E_SRS_IND) */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_n_srs = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_dopp_est = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_timing_offset = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_n_rb = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_rb_start = -1;
static gint hf_fapi_sdk_msg_UL_SRS_EVT_rb_snr = -1;
/* FAPI msg data (FAPI_E_RACH_IND)  */
//static gint hf_fapi_sdk_msg_header_sf_sfn = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_RSP_num_preambles = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_RSP_rnti = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_RSP_preamble_id = -1;
static gint hf_fapi_sdk_msg_UL_PRACH_RSP_timing_offset = -1;
/* FAPI msg data (FAPI_E_ERROR_IND) */
static gint hf_fapi_sdk_msg_ERROR_IND_msg_id     = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_sub_error_code = -1;
/* Error.indication - DL_CONFIG_REQ Sub-Error types */
static gint hf_fapi_sdk_msg_ERROR_IND_dci_dl_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_bch_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_mch_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_dlsch_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_pch_pdu_sub_error_code = -1;
/* Error.indication - UL_CONFIG_REQ Sub-Error types */
static gint hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_cqi_ri_information_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_initial_transmission_information_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_harq_information_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_cqi_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_sr_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_harq_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_sr_harq_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_cqi_harq_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_harq_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_srs_pdu_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_harq_buffer_pdu_sub_error_code = -1;
/* Error.indication - TX_REQ Sub-Error types */
static gint hf_fapi_sdk_msg_ERROR_IND_tx_request_sub_error_code = -1;
/* Error.indication - MSG_PDU_ERR - DL/UL */
static gint hf_fapi_sdk_msg_ERROR_IND_direction = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_rnti = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_dl_pdu_type = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_ul_pdu_type = -1;
/* Error.indication - MSG_HI_ERR */
static gint hf_fapi_sdk_msg_ERROR_IND_phich_lowest_ul_rb_index = -1;
/* Error.indication - MSG_TX_ERR */
static gint hf_fapi_sdk_msg_ERROR_IND_pdu_index = -1;
/* Error.indication - GLOB_PARAMS_ERR */
static gint hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_dl_config_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_ul_config_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_tx_request_sub_error_code = -1;
/* Error.indication - N_PDU_LIMIT_SURPASSED */
static gint hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_sub_error_code = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_received_val = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_limit = -1;
static gint hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_rnti_type = -1;
/* FAPI vend. specific msg data (FAPI_E_RX_VSM_MEAS_IND) */
static gint hf_fapi_sdk_vsm_MEAS_IND_length = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_avg_ni = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant0_rssi = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant1_rssi = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant2_rssi = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant3_rssi = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant0_gain = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant1_gain = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant2_gain = -1;
static gint hf_fapi_sdk_vsm_MEAS_IND_ant3_gain = -1;
/* FAPI vend. specific msg data (FAPI_E_RX_VSM_NI_IND) */
static gint hf_fapi_sdk_vsm_NI_IND_handle = -1;
//static gint hf_fapi_sdk_vsm_NI_IND_sf_sfn = -1;
static gint hf_fapi_sdk_vsm_NI_IND_n_rb = -1;
static gint hf_fapi_sdk_vsm_NI_IND_ni_per_rb = -1;
/* These are part of FAPI VSM header */
static gint hf_fapi_sdk_vsm_header_num_tlvs = -1;
static gint hf_fapi_sdk_vsm_header_size = -1;
/*Vend. Specific Common Fields */
static gint hf_fapi_sdk_vsm_padding1 = -1;
static gint hf_fapi_sdk_vsm_padding2 = -1;
static gint hf_fapi_sdk_vsm_padding3 = -1;
static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
static gint hf_fapi_sdk_vsm_UL_IND_COMMON_snr = -1;
/* These are part of FAPI VSM tlv */
static gint hf_fapi_sdk_vsm_tlv_tag = -1;
static gint hf_fapi_sdk_vsm_tlv_length = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* UL_CONFIG.req VSM - NI PDU */
//static gint hf_fapi_sdk_msg_UL_COMMON_PDU_handle = -1;
/* These are part of FAPI VSM tlv - RX.ULSCH.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - HARQ.PUCCH.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_snr = -1;
/* These are part of FAPI VSM tlv - HARQ.PUSCH_FDD.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - HARQ.PUSCH_TDD.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
static gint hf_fapi_sdk_vsm_harq_pusch_tdd_ind_bundling = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - SR.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_snr = -1;
/* These are part of FAPI VSM tlv - CQI.PUCCH.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - CQI.PUSCH.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - RACH.ind */
static gint hf_fapi_sdk_vsm_rach_ind_preamble_id = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
static gint hf_fapi_sdk_vsm_rach_ind_detection_metric = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM tlv - SRS.ind */
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rnti = -1;
//static gint hf_fapi_sdk_vsm_UL_IND_COMMON_rssi = -1;
//static gint hf_fapi_sdk_vsm_padding = -1;
/* These are part of FAPI VSM TLV - Configuration messages */
//HARQ_RX_CONTROL
static gint HARQ_tag = -1;
static gint HARQ_length = -1;
static gint HARQ_gpio = -1;               //Serial Transmission output GPIO PIN
static gint HARQ_enable = -1;             //1-Enable 0-Disable GPIO
static gint HARQ_timingAdvance = -1;      //Serial Transmission timing advance
static gint HARQ_baudrate = -1;           //Serial Transmission baud rate
static gint HARQ_sfn = -1;                //GPIO Serial Transmission start time, sfn:10
static gint HARQ_slot = -1;               //GPIO Serial Transmission start time, slot:8
static gint HARQ_numerology = -1;         //GPIO Serial Transmission start time, slot:4
static gint HARQ_numDataBits = -1;        //Serial Transmission data bits
static gint HARQ_data = -1;               //Serial Transmission data LSB transmit first
//CFI
static gint hf_fapi_sdk_vsm_cfg_cfi = -1;
//VERSION
static gint hf_fapi_sdk_vsm_cfg_version_major = -1;
static gint hf_fapi_sdk_vsm_cfg_version_minor = -1;
//Test Mode
static gint hf_fapi_sdk_vsm_cfg_etm_enable = -1;
static gint hf_fapi_sdk_vsm_cfg_etm_all_zeros = -1;
static gint hf_fapi_sdk_vsm_cfg_etm_mi_override = -1;
// PARAM.response
static gint hf_fapi_sdk_vsm_param_rsp_max_users = -1;
/* VSM Configured Params Report parameters */
static gint hf_fapi_sdk_msg_CFG_PARAMS_REP_n_cat = -1;
static gint hf_fapi_sdk_msg_CFG_PARAMS_REP_cat = -1;
static gint hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag = -1;
static gint hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag_rel10 = -1;
static gint hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_value = -1;
/* VSM PHY Infra Config Request parameters */
static gint hf_fapi_sdk_msg_INFRA_PARAM_REQ_length = -1;
static gint hf_fapi_sdk_msg_INFRA_PARAM_REQ_n_pdu = -1;
static gint hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_type = -1;
static gint hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_size = -1;
//VSM PHY Infra DPD PDU
static gint hf_fapi_sdk_msg_INFRA_DPD_PDU_valid_bitmap = -1;
//VSM PHY Infra TRIGGER PDU
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf_sfn = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sfn = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_io_index = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_pattern = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_bit_interval = -1;
static gint hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_start_time_off = -1;
//VSM PHY Infra UNSOLICITED_ACK_NACK_THRESHOLD PDU
static gint hf_fapi_sdk_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU_threshold = -1;
/* These are the ids of the subtrees that we may be creating */
static gint ett_fapi_sdk_msg_header = -1;
static gint ett_phy_header =-1;
static gint ett_fapi_sdk_msg_phy=-1;
static gint ett_fapi_sdk_msg_data = -1;
static gint ett_fapi_sdk_msg_data_subtree1 = -1;
static gint ett_fapi_sdk_msg_data_subtree2 = -1;
static gint ett_fapi_sdk_msg_data_subtree3 = -1;
static gint ett_fapi_sdk_msg_data_subtree4 = -1;
static gint ett_msg_fragment = -1;
static gint ett_msg_fragments = -1;
static void
dissect_fapi_SFN_SF(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_TTI_EVT(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_DL_CONFIG_REQ(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static guint32
dissect_fapi_msg_DL_DLSCH_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen, FAPI_E_DL_PDU_TYPE DLSCH_type);
static guint32
dissect_fapi_msg_DL_PCH_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_DL_BCH_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static guint32
dissect_fapi_msg_DL_DCI_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_CONFIG_REQ(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_PUSCH_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_CQI_RI_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_HARQ_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_INIT_TRANS_INFO(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_UCI_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen,
gboolean cqi_flag, gboolean sr_flag, gboolean harq_flag);
static void
dissect_fapi_msg_UL_SRS_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_HI_DCI0_REQ(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_PHICH_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_UL_DCI_PDU(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_TX_REQ(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_RX_ULSCH_CQI_IND(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen,
gboolean inc_ri);
static void
dissect_fapi_msg_HARQ_IND(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_CRC_IND(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_RX_SR_IND(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_msg_SRS_IND(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
static void
dissect_fapi_vsm_PARAM_RSP(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, guint32* plen);
#endif