/* packetFapiSdk.c
*Copyright (c) 2019-2020 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <stdio.h>
#include <glib.h>
#include <math.h>
#include <epan/packet.h>
#include <epan/expert.h>
#include <epan/proto.h>
#include <epan/prefs.h>
#include <epan/dissectors/packet-mac-lte.h>
#include <epan/tvbuff-int.h>
#include <epan/tvbuff.h>
#include <epan/reassemble.h>
#include <epan/value_string.h>
#include "packetFapiSdk.h"
#include <string.h>
#define FAPI_MAX(a,b) (((a) > (b)) ? (a) : (b))
#define FAPI_MIN(a,b) (((a) < (b)) ? (a) : (b))
static double api_val_to_lgcl_val(gint32 val, double start_val, double delt)
{
    double res = 0;
    res = (start_val + delt * val);
    return res;
}
void proto_reg_handoff_fapi_sdk(void)
{
    static gboolean initialized = FALSE;
    if (!initialized) {
        data_handle = find_dissector("fapi_sdk");
        mac_lte_handle = find_dissector("mac-lte");
        fapi_sdk_handle = create_dissector_handle(dissect_fapi_sdk, proto_fapi_sdk);
    }
}
void proto_register_fapi_sdk(void)
{
    /* A header field is something you can search/filter on.
    *
    * We create a structure to register our fields. It consists of an
    * array of hf_register_info structures, each of which are of the format
    * {&(field id), {name, abbrev, type, display, strings, bitmask, blurb, HFILL}}.
    */
    static hf_register_info hf[] = {
        /* FAPI Message Header */
        { &hf_fapi_sdk_msg_header_type,
        { "Msg Id", "hf_fapi_sdk.msg_header_type",
        FT_UINT16, BASE_HEX, VALS(fapi_msg_header_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_phyid,
        { "Phy Id", "hf_fapi_sdk_msg_header_len_phyid",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnfPhyIndex,
        { "pnfPhyIndex", "fapi_sdk.msg_data.PNF_CONFIG.pnfPhyIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPdsch,
        { "betaPdsch", "fapi_sdk.msg_data.TX_CONTROL.betaPdsch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_ven,
        { "Length", "fapi_sdk.msg_header.len_ven",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_body,
        { "Segment Num", "fapi_sdk.msg_header.len_body",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_flag,
        { "Flag", "fapi_sdk.msg_header.len_flag",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_sequence,
        { "Sequence Num", "fapi_sdk.msg_header.len_sequence",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_numphy,
        { "Num Of Phy", "fapi_sdk.msg_header.len_numphy",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_timestamp,
        { "Timestamp", "fapi_sdk.msg_header.len_timestamp",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_len_phylen,
        { "Phy Len", "fapi_sdk.msg_header.len_phylen",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI Message Header Ends*/
        { &nCarriers,
        { "nCarriers", "fapi_sdk.msg_data.PNF_CONFIG.nCarriers",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //5G
        { &phy_conf_invalidTLV,
        { "phy_conf_invalidTLV", "fapi_sdk.msg_data.PHY_CONFIG_RESP.phy_conf_invalidTLV",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_conf_msiingTLV,
        { "phy_conf_msiingTLV", "fapi_sdk.msg_data.PHY_CONFIG_RESP.phy_conf_msiingTLV",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_conf_configuredintit,
        { "phy_conf_configuredintit", "fapi_sdk.msg_data.PHY_CONFIG_RESP.phy_conf_configuredintit",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_conf_configuredcellstop,
        { "phy_conf_configuredcellstop", "fapi_sdk.msg_data.PHY_CONFIG_RESP.phy_conf_configuredcellstop",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Number_of_invalid_TLVs,
        { "Number_of_invalid_TLVs", "fapi_sdk.msg_data.PHY_CONFIG_RESP.Number_of_invalid_TLVs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Number_of_missing_TLVs,
        { "Number_of_missing_TLVs", "fapi_sdk.msg_data.PHY_CONFIG_RESP.Number_of_missing_TLVs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Number_configured_init,
        { "Number_configured_init", "fapi_sdk.msg_data.PHY_CONFIG_RESP.Number_configured_init",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Number_configured_cell_stopped,
        { "Number_configured_cell_stopped", "fapi_sdk.msg_data.PHY_CONFIG_RESP.Number_configured_cell_stopped",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //pnf_config_req
        { &tag,
        { " TAG", "fapi_sdk.msg_data.common_tag",
        FT_UINT16, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &length,
        { "Length", "fapi_sdk.msg_data.common_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numPhyConfig,
        { "numPhyConfig", "fapi_sdk.msg_data.PNF_CONFIG.numPhyConfig",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phyId,
        { "phy Id", "fapi_sdk.msg_data.PNF_CONFIG.phyId",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nCells,
        { "nCells", "fapi_sdk.msg_data.PNF_CONFIG.nCells",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phyCellId,
        { "phy Cell Id", "fapi_sdk.msg_data.PNF_CONFIG.phyCellId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nPrachConfigs,
        { "nPrachConfigs", "fapi_sdk.msg_data.PNF_CONFIG.nPrachConfigs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nPrachConfigs1,
        { "nPrachConfigs", "fapi_sdk.msg_data.PHY_CONFIG.nPrachConfigs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &downlinkBandwidth,
        { "Downlink Bandwidth", "fapi_sdk.msg_data.PNF_CONFIG.downlinkBandwidth",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &PNF_CAPABILITY_RSP_error_code,
        { "Error Code", "fapi_sdk.msg_data.PNF_CONFIG.PNF_CAPABILITY_RSP_error_code",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachSequenceLength,
        { "prachSequenceLength", "fapi_sdk.msg_data.PHY_CONFIG_REQ.prachSequenceLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &deltaFRa,
        { "deltaFRa", "fapi_sdk.msg_data.PHY_CONFIG_REQ.deltaFRa",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &restrictedSetConfig,
        { "restrictedSetConfig", "fapi_sdk.msg_data.PHY_CONFIG_REQ.restrictedSetConfig",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numPrachFdOccasions,
        { "numPrachFdOccasions", "fapi_sdk.msg_data.PNF_CONFIG.numPrachFdOccasions",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachRootSequenceIndex,
        { "prachRootSequenceIndex", "fapi_sdk.msg_data.PNF_CONFIG.prachRootSequenceIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numRootSequences,
        { "numRootSequences", "fapi_sdk.msg_data.PNF_CONFIG.numRootSequences",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &k1 ,
        { "k1 ", "fapi_sdk.msg_data.PNF_CONFIG.k1",
        FT_INT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &k11 ,
        { "k1 ", "fapi_sdk.msg_data.PHY_CONFIG.k11",
        FT_INT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numUnusedRootSequences,
        { "numUnusedRootSequences", "fapi_sdk.msg_data.PNF_CONFIG.numUnusedRootSequences",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &unusedRootSequences,
        { "unusedRootSequences", "fapi_sdk.msg_data.PNF_CONFIG.unusedRootSequences",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &downlinkFrequency,
        { "downlinkFrequency", "downlinkFrequency",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &sfn,
        { "sfn", "fapi_sdk.msg_data.TX_CONTROL.sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &slot,
        { "slot", "fapi_sdk.pdsch.slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &PNF_CAPABILITY_RSP_num_TLV,
        { "Num TLV", "PNF_CAPABILITY_RSP_num_TLV",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &downlinkGridStart,
        { "downlinkGridStart", "downlinkGridStart",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &downlinkGridSize,
        { "downlinkGridSize", "downlinkGridSize",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uplinkBandwidth,
        { "uplinkBandwidth", "uplinkBandwidth",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uplinkFrequency,
        { "uplinkFrequency", "uplinkFrequency",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uplinkGridStart,
        { "uplinkGridStart", "uplinkGridStart",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uplinkGridSize,
        { "uplinkGridSize", "uplinkGridSize",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uplinkFrequencyShift7p5khz,
        { "uplinkFrequencyShift7p5khz", "uplinkFrequencyShift7p5khz",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &referenceSsPbchPower,
        { "referenceSsPbchPower", "referenceSsPbchPower",
        FT_INT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &BFtag,
        { "BFtag", "fapi_sdk.msg_data.PNF_CONFIG_REQ.BFtag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &BFlength,
        { "BFlength", "fapi_sdk.msg_data.PNF_CONFIG_REQ.BFlength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &BFnumEntries,
        { "BFnumEntries", "fapi_sdk.msg_data.PNF_CONFIG_REQ.BFnumEntries",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numRows,
        { "numRows", "fapi_sdk.msg_data.PNF_CONFIG_REQ.numRows",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numColumns,
        { "numColumns", "fapi_sdk.msg_data.PNF_CONFIG_REQ.numColumns",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBeamValReal,
        { "digBeamValReal", "fapi_sdk.msg_data.PNF_CONFIG_REQ.digBeamValReal",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBeamValImag,
        { "digBeamValImag", "fapi_sdk.msg_data.PNF_CONFIG_REQ.digBeamValImag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssPbchMultipleCarriersInABand,
        { "ssPbchMultipleCarriersInABand", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.ssPbchMultipleCarriersInABand",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnf_tag,
        { "pnf tag", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pnf_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnf_length,
        { "pnf length", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pnf_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnftype,
        { "pnftype", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pnftype",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnfsize,
        { "pnfsize", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pnfsize",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssPbchDigitalBfTechniques,
        { "pbch Digital technique", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.ssPbchDigitalBfTechniques",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &multipleCellSsPbchsInACarrier,
        { "multiple cells carrier", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.multipleCellSsPbchsInACarrier",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachLongFormats,
        { "prachLongFormats", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.prachLongFormats",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numberOfPrachSlotsAllowedIna60kHzSlot,
        { "numberOfPrachSlotsAllowedIna60kHzSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.numberOfPrachSlotsAllowedIna60kHzSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &respErrorCode,
        { "respErrorCode", "respErrorCode",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &respnCarriers,
        { "respnCarriers", "fapi_sdk.msg_data.PNF_CONFIG_RESP.respnCarriers",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &txDirectCurrentLocation,
        { "txDirectCurrentLocation", "fapi_sdk.msg_data.PNF_CONFIG_RESP.txDirectCurrentLocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachRestrictedSets,
        { "prachRestrictedSets", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.prachRestrictedSets",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachMultipleCarriersInABand,
        { "prachMultipleCarriersInABand", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.prachMultipleCarriersInABand",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nSsPbchsPerRachOccasion,
        { "nSsPbchsPerRachOccasion", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nSsPbchsPerRachOccasion",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_prach_ncs,
        { "Ncs", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.phy_prach_ncs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cceMappingType,
        { "cceMappingType", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.cceMappingType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &coresetOutsideFirst3OfdmSymsOfSlot,
        { "coresetOutsideFirst3OfdmSymsOfSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.coresetOutsideFirst3OfdmSymsOfSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &coresetNonContigFreqDomainAlloc,
        { "coresetNonContigFreqDomainAlloc", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.coresetNonContigFreqDomainAlloc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &precoderGranularityCoreset,
        { "precoderGranularityCoreset", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.precoderGranularityCoreset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdcchMuMimo,
        { "pdcchMuMimo", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdcchMuMimo",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdcchPrecoderCycling,
        { "pdcchPrecoderCycling", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdcchPrecoderCycling",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPdcchPrecoderCycleLength,
        { "maxPdcchPrecoderCycleLength", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPdcchPrecoderCycleLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchFormats,
        { "pucchFormats", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchFormats",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchIntraslotFreqHopping,
        { "pucchIntraslotFreqHopping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchIntraslotFreqHopping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchInterslotFreqHopping,
        { "pucchInterslotFreqHopping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchInterslotFreqHopping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &twoSybmolShortFormat,
        { "twoSybmolShortFormat", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.twoSybmolShortFormat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchGroupHopping,
        { "pucchGroupHopping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchGroupHopping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchNrofSlots,
        { "pucchNrofSlots", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchNrofSlots",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchMultiplexingUe,
        { "pucchMultiplexingUe", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchMultiplexingUe",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &additionalDmrs,
        { "additionalDmrs", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.additionalDmrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pi2Bpsk,
        { "pi2Bpsk", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pi2Bpsk",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschMappingTypes,
        { "pdschMappingTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschMappingTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschAllocationTypes,
        { "pdschAllocationTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschAllocationTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschMuMimoOverlap,
        { "pdschMuMimoOverlap", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschMuMimoOverlap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschVrbToPrbMapping,
        { "pdschVrbToPrbMapping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschVrbToPrbMapping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschAggregationFactor,
        { "pdschAggregationFactor", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschAggregationFactor",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschCbg,
        { "pdschCbg", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschCbg",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschDmrsConfigTypes,
        { "pdschDmrsConfigTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschDmrsConfigTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschDmrsMaxLength,
        { "pdschDmrsMaxLength", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschDmrsMaxLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschDmrsAdditionalPos,
        { "pdschDmrsAdditionalPos", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschDmrsAdditionalPos",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschDataInDmrsSymbols,
        { "pdschDataInDmrsSymbols", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschDataInDmrsSymbols",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &preemptionSupport,
        { "preemptionSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.preemptionSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschNonSlotSupport,
        { "pdschNonSlotSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschNonSlotSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uciMuxUlschInPusch,
        { "uciMuxUlschInPusch", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.uciMuxUlschInPusch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uciOnlyPusch,
        { "uciOnlyPusch", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.uciOnlyPusch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &harqAckNackInUciOnPusch,
        { "harqAckNackInUciOnPusch", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.harqAckNackInUciOnPusch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschDataInDmrsSymbols,
        { "puschDataInDmrsSymbols", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschDataInDmrsSymbols",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschPrecodingType,
        { "puschPrecodingType", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschPrecodingType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschFrequencyHopping,
        { "puschFrequencyHopping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschFrequencyHopping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschDmrsConfigTypes,
        { "puschDmrsConfigTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschDmrsConfigTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschDmrsMaxLen,
        { "puschDmrsMaxLen", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschDmrsMaxLen",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschDmrsAdditionalPos,
        { "puschDmrsAdditionalPos", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschDmrsAdditionalPos",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschCbg,
        { "puschCbg", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschCbg",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschMappingTypes,
        { "puschMappingTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschMappingTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschAllocationTypes,
        { "puschAllocationTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschAllocationTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschMuMimoOverlap ,
        { "puschMuMimoOverlap ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschMuMimoOverlap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschVrbToPrbMapping,
        { "puschVrbToPrbMapping", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschVrbToPrbMapping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschAggregationFactor,
        { "puschAggregationFactor", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschAggregationFactor",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschLbrm,
        { "puschLbrm", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschLbrm",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschMaxPtrsPorts,
        { "puschMaxPtrsPorts", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschMaxPtrsPorts",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cdmDmrsPortsOfMuMimoUsers,
        { "cdmDmrsPortsOfMuMimoUsers", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.cdmDmrsPortsOfMuMimoUsers",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschNonSlotSupport,
        { "puschNonSlotSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschNonSlotSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },//
        { &srsSupport,
        { "srsSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.srsSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &freqHoppingSupport,
        { "freqHoppingSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.freqHoppingSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &groupHoppingSupport,
        { "groupHoppingSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.groupHoppingSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &sequenceHoppingSupport,
        { "sequenceHoppingSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.sequenceHoppingSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &reservedResourceTypes,
        { "reservedResourceTypes", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.reservedResourceTypes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Cp,
        { "Cp", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.Cp",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssPbchRmsiMuxPatterns,
        { "ssPbchRmsiMuxPatterns", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.ssPbchRmsiMuxPatterns",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &sulSupport,
        { "sulSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.sulSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedUserMuxSchemesInSlot,
        { "supportedUserMuxSchemesInSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedUserMuxSchemesInSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dynamicSlotFormatSupport,
        { "dynamicSlotFormatSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.dynamicSlotFormatSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxDigBfTableEntries,
        { "maxDigBfTableEntries", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxDigBfTableEntries",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschCsirsTimeFrequencyOverlap,
        { "pdschCsirsTimeFrequencyOverlap", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschCsirsTimeFrequencyOverlap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschPdcchTimeFrequencyOverlap,
        { "pdschPdcchTimeFrequencyOverlap", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschPdcchTimeFrequencyOverlap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdschSspbchTimeFrequencyOverlap,
        { "pdschSspbchTimeFrequencyOverlap", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschSspbchTimeFrequencyOverlap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nBandCombinations,
        { "nBandCombinations", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nBandCombinations",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nBands,
        { "nBands", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nBands",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &bandList,
        { "bandList", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.bandList",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &featureSetCombinationId,
        { "featureSetCombinationId", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.featureSetCombinationId",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetsDownlinkPerCc,
        { "nFeatureSetsDownlinkPerCc", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetsDownlinkPerCc",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetsDownlink,
        { "nFeatureSetsDownlink", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetsDownlink",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetsUplinkPerCc,
        { "nFeatureSetsUplinkPerCc", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetsUplinkPerCc",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetsUplink,
        { "nFeatureSetsUplink", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetsUplink",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetPerBand,
        { "nFeatureSetPerBand", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetPerBand",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nFeatureSetCombinations,
        { "nFeatureSetCombinations", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nFeatureSetCombinations",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedSubcarrierSpacingsDl,
        { "supportedSubcarrierSpacingsDl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedSubcarrierSpacingsDl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedBandwidthDl,
        { "supportedBandwidthDl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedBandwidthDl",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumberMimoLayersPdsch,
        { "maxNumberMimoLayersPdsch", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumberMimoLayersPdsch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedModulationOrderDl,
        { "supportedModulationOrderDl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedModulationOrderDl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxMuMimoUsersDl,
        { "maxMuMimoUsersDl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxMuMimoUsersDl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedSubcarrierSpacingsUl,
        { "supportedSubcarrierSpacingsUl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedSubcarrierSpacingsUl",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedBandwidthUl,
        { "supportedBandwidthUl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedBandwidthUl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumberMimoLayersNonCbPusch,
        { "maxNumberMimoLayersPusch", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumberMimoLayersNonCbPusch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedModulationOrderUl,
        { "supportedModulationOrderUl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedModulationOrderUl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dftsOfdmSupport,
        { "dftsOfdmSupport", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.dftsOfdmSupport",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxMuMimoUsersUl,
        { "maxMuMimoUsersUl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxMuMimoUsersUl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pnf_start_resp_error_codes,
        { "PNF error code", "fapi_sdk.msg_data.PNF_ERROR_CODES",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &featureSetDownlinkId,
        { "featureSetDownlinkId", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.featureSetDownlinkId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &featureSetUplinkId,
        { "featureSetUplinkId", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.featureSetUplinkId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nTrxus,
        { "nTrxus", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nTrxus",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nCcs,
        { "nCcs", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.nCcs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &featureSetDownlinkPerCcIds,
        { "featureSetDownlinkPerCcIds", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.featureSetDownlinkPerCcIds",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &intraBandFreqSeparationDl,
        { "intraBandFreqSeparationDl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.intraBandFreqSeparationDl",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxTotalBw,
        { "maxTotalBw", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxTotalBw",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &UnCcs,
        { "nCcs", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.UnCcs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &UfeatureSetUplinkCcIds,
        { "featureSetUplinkCcIds", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.UfeatureSetUplinkCcIds",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &UintraBandFreqSeparationUl,
        { "intraBandFreqSeparationUl", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.UintraBandFreqSeparationUl",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &UmaxTotalBw,
        { "maxTotalBw", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.UmaxTotalBw",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &CnBands,
        { "nBands", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.CnBands",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &featureSetPerBandIds,
        { "featureSetPerBandIds", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.featureSetPerBandIds",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxSsbsPerCarrierPerCellInBurst,
        { "maxSsbsPerCarrierPerCellInBurst", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxSsbsPerCarrierPerCellInBurst",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &minSsPbchBurstPeriodicity,
        { "minSsPbchBurstPeriodicity", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.minSsPbchBurstPeriodicity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxSsPbchBlocksPerSlot,
        { "maxSsPbchBlocksPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxSsPbchBlocksPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPrachFdOccasionsInASlot,
        { "maxPrachFdOccasionsInASlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPrachFdOccasionsInASlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPrachFdOccasionsInASlotPerCarrier ,
        { "maxPrachFdOccasionsInASlotPerCarrier ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPrachFdOccasionsInASlotPerCarrier",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxRootSequencesPerSlotPerFd,
        { "maxRootSequencesPerSlotPerFd", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxRootSequencesPerSlotPerFd",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedSubcarrierSpacingsPrachShort,
        { "supportedSubcarrierSpacingsPrachShort", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedSubcarrierSpacingsPrachShort",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &supportedSubcarrierSpacingsPrachLong,
        { "supportedSubcarrierSpacingsPrachLong", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.supportedSubcarrierSpacingsPrachLong",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &N5,
        { "N5", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.N5",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPdcchsPerSlot,
        { "maxPdcchsPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPdcchsPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPdcchRange,
        { "betaPdcchRange", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.betaPdcchRange",
        FT_INT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPdschRange,
        { "betaPdschRange", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.betaPdschRange",
        FT_INT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchPolarMaxK,
        { "pucchMaxK", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchPolarMaxK",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchPolarMaxN,
        { "pucchMaxN", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchPolarMaxN",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchUciMaxE,
        { "pucchUciMaxE", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pucchUciMaxE",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &physCellId,
        { "physCellId", "hf_fapi.msg_data.TX_CONTROL.physCellId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPss,
        { "betaPss", "hf_fapi.msg_data.TX_CONTROL.betaPss",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssbIndex,
        { "ssbIndex", "hf_fapi.msg_data.TX_CONTROL.ssbIndex",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &bchPayload,
        { "bchPayload", "hf_fapi.msg_data.TX_CONTROL.bchPayload",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &msbOfKssbInSspbchTypeA,
        { "msbOfKssbInSspbchTypeA", "hf_fapi.msg_data.TX_CONTROL.msbOfKssbInSspbchTypeA",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssbCrbOffset ,
        { "ssbCrbOffset ", "hf_fapi.msg_data.TX_CONTROL.ssbCrbOffset",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ssbSubCarrierOffset,
        { "ssbSubCarrierOffset", "hf_fapi.msg_data.TX_CONTROL.ssbSubCarrierOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBfMethod,
        { "digBfMethod", "hf_fapi.msg_data.TX_CONTROL.digBfMethod",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBfCyclicDelay ,
        { "digBfCyclicDelay ", "hf_fapi.msg_data.TX_CONTROL.digBfCyclicDelay",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBfBeamIndex ,
        { "digBfBeamIndex ", "hf_fapi.msg_data.TX_CONTROL.digBfBeamIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPucchsPerSlot,
        { "maxPucchsPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPucchsPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &P2,
        { "P2", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.P2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPdschHarqProcesses,
        { "maxPdschHarqProcesses", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPdschHarqProcesses",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPdschTransportBlocks,
        { "maxPdschTransportBlocks", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPdschTransportBlocks",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &N31,
        { "N31", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.N31",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &N32,
        { "N32", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.N32",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &K3,
        { "K3", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.K3",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPuschHarqProcesses,
        { "maxPuschHarqProcesses", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPuschHarqProcesses",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPuschTransportBlocks,
        { "maxPuschTransportBlocks", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPuschTransportBlocks",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxUciOnlyPuschs ,
        { "maxUciOnlyPuschs ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxUciOnlyPuschs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschUciMaxK ,
        { "puschUciMaxK ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschUciMaxK",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschUciMaxN ,
        { "puschUciMaxN ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschUciMaxN",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschUciMaxE ,
        { "puschUciMaxE ", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.puschUciMaxE",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &N41,
        { "N41", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.N41",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &N42,
        { "N42", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.N42",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &P1,
        { "P1", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.P1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &K4,
        { "K4", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.K4",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxDurationWithinReTxInSlots,
        { "maxDurationWithinReTxInSlots", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxDurationWithinReTxInSlots",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumPortsCsirsTxPerSymbol,
        { "maxNumPortsCsirsTxPerSymbol", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumPortsCsirsTxPerSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumAggrPortsCsirsTxPerSlot,
        { "maxNumAggrPortsCsirsTxPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumAggrPortsCsirsTxPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumPortsPerUe,
        { "maxNumPortsPerUe", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumPortsPerUe",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumSymbolsPerUe,
        { "maxNumSymbolsPerUe", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumSymbolsPerUe",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumRepetitionsPerUe,
        { "maxNumRepetitionsPerUe", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumRepetitionsPerUe",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumAggrPortsPerUe,
        { "maxNumAggrPortsPerUe", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumAggrPortsPerUe",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumAggrPortsPerSlot,
        { "maxNumAggrPortsPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumAggrPortsPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlusersPerSlot,
        { "dlusersPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.dlusersPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulusersPerSlot,
        { "ulusersPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.ulusersPerSlot",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumCsiReportsPerSlot,
        { "maxNumCsiReportsPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumCsiReportsPerSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxNumPolarDecodesPerSlot,
        { "maxNumPolarDecodesPerSlot", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxNumPolarDecodesPerSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachShortFormats,
        { "prachShortFormats", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.prachShortFormats",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &upduBitmap,
        { "pduBitmap", "fapi_sdk.msg_data.RX_CONTROL.pduBitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uHandle,
        { "Handle", "fapi_sdk.msg_data.RX_CONTROL.uHandle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulocation,
        { "location", "fapi_sdk.msg_data.RX_CONTROL.ulocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ubandwidth,
        { "bandwidth", "fapi_sdk.msg_data.RX_CONTROL.ubandwidth",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ucyclicPrefix,
        { "cyclicPrefix", "fapi_sdk.msg_data.RX_CONTROL.ucyclicPrefix",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uqamModOrder,
        { "qamModOrder", "fapi_sdk.msg_data.RX_CONTROL.uqamModOrder",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uinitialCodeRate,
        { "initialCodeRate", "fapi_sdk.msg_data.RX_CONTROL.uinitialCodeRate",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &unRnti,
        { "nRti", "fapi_sdk.msg_data.RX_CONTROL.unRnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDataScramblingId,
        { "ulDataScramblingId", "fapi_sdk.msg_data.RX_CONTROL.ulDataScramblingId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDmrsSymPositions,
        { "ulDmrsSymPositions", "fapi_sdk.msg_data.RX_CONTROL.ulDmrsSymPositions",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDmrsConfigType,
        { "ulDmrsConfigType", "fapi_sdk.msg_data.RX_CONTROL.ulDmrsConfigType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDmrsScramblingId,
        { "ulDmrsScramblingId", "fapi_sdk.msg_data.RX_CONTROL.ulDmrsScramblingId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &unScid,
        { "unScid", "fapi_sdk.msg_data.RX_CONTROL.unScid",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDmrsCdmGroupsNoData,
        { "ulDmrsCdmGroupsNoData", "fapi_sdk.msg_data.RX_CONTROL.ulDmrsCdmGroupsNoData",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulDmrsPorts,
        { "ulDmrsPorts", "fapi_sdk.msg_data.RX_CONTROL.ulDmrsPorts",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &urbBitmap,
        { "rbBitmap", "fapi_sdk.msg_data.RX_CONTROL.urbBitmap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ustartSymbol,
        { "startSymbol", "fapi_sdk.msg_data.RX_CONTROL.ustartSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &usymbolLength,
        { "symbolLength", "fapi_sdk.msg_data.RX_CONTROL.usymbolLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ufreqHoppingEnable,
        { "freqHoppingEnable", "fapi_sdk.msg_data.RX_CONTROL.ufreqHoppingEnable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &utxDirectCurrentLocation,
        { "txDirectCurrentLocation", "fapi_sdk.msg_data.RX_CONTROL.utxDirectCurrentLocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uuplinkFrequencyShift7p5khz,
        { "uplinkFrequencyShift7p5khz", "fapi_sdk.msg_data.RX_CONTROL.uuplinkFrequencyShift7p5khz",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_sf_sfn,
        { "SFN/SF", "fapi_sdk.msg_header.sf_sfn",
        FT_UINT16, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_header_sfn,
        { "Frame", "fapi_sdk.msg_header.nf",
        FT_UINT16, BASE_DEC, NULL, SFN_MASK,
        NULL, HFILL } },
        //
        { &pdschPrecoderCycling,
        { "pdschPrecoderCycling", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.pdschPrecoderCycling",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &maxPdschPrecoderCycleLength,
        { "maxPdschPrecoderCycleLength", "fapi_sdk.msg_data.PNF_CAPABILITY_RSP.maxPdschPrecoderCycleLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &harqAckBitLength ,
        { "harqAckBitLength ", "fapi_sdk.msg_data.TX_CONTROL.harqAckBitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &onPuschScalingAlpha,
        { "onPuschScalingAlpha", "fapi_sdk.msg_data.RX_CONTROL.onPuschScalingAlpha",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaOffsetHarqAck,
        { "betaOffsetHarqAck", "fapi_sdk.msg_data.RX_CONTROL.betaOffsetHarqAck",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaOffsetCsi1,
        { "betaOffsetCsiPart1", "fapi_sdk.msg_data.RX_CONTROL.betaOffsetCsi1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaOffsetCsi2,
        { "betaOffsetCsiPart2", "fapi_sdk.msg_data.RX_CONTROL.betaOffsetCsi2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &csiPart2Indicator,
        { "csiPart2Indicator", "fapi_sdk.msg_data.RX_CONTROL.csiPart2Indicator",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numCsiReports ,
        { "numCsiReports ", "fapi_sdk.msg_data.TX_CONTROL.numCsiReports",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //
        { &hf_fapi_sdk_msg_header_sf,
        { "Sub-frame", "fapi_sdk.msg_header.nsf",
        FT_UINT16, BASE_DEC, NULL, SF_MASK,
        NULL, HFILL } },
        /* FAPI DATA(PARAM_RSP) Message */
        { &hf_fapi_sdk_msg_PARAM_RSP_n_tlv,
        { "No of TLVs", "fapi_sdk.msg_data_PARAM_RSP.n_tlv",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(CONFIG_REQ) Message */
        { &hf_fapi_sdk_msg_CONFIG_REQ_n_tlv,
        { "No of TLVs", "fapi_sdk.msg_data_CONFIG_REQ.n_tlv",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(CONFIG_RSP) Message */
        { &hf_fapi_sdk_msg_CONFIG_RSP_err_code,
        { "Error Code", "fapi_sdk.msg_data_CONFIG_RSP.err_code",
        FT_UINT8, BASE_DEC, VALS(&msg_error_codes_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_invalid,
        { "No of Invalid TLVs", "fapi_sdk.msg_data_CONFIG_RSP.n_tlv_invalid",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_missing,
        { "No of TLVs Missing", "fapi_sdk.msg_data_CONFIG_RSP.n_tlv_miss",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(CONFIG_TLV) Message */
        { &hf_fapi_sdk_msg_CONFIG_TLV_tag,
        { "Tag", "fapi_sdk.msg_data_CONFIG_TLV.tag",
        FT_UINT16, BASE_DEC, VALS(&fapi_msg_data_config_tlv_tag_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CONFIG_TLV_tag_rel10,
        { "Tag", "fapi_sdk.msg_data_CONFIG_TLV.tag_r10",
        FT_UINT16, BASE_DEC, VALS(&fapi_msg_data_config_tlv_tag_release10_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CONFIG_TLV_length,
        { "Length", "fapi_sdk.msg_data_CONFIG_TLV.len",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CONFIG_TLV_value,
        { "Value", "fapi_sdk.msg_data_CONFIG_TLV.value",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_Carrier_TLV_value,
        { "hf_fapi_sdk_msg_Carrier_TLV_value", "fapi_sdk.msg_data_CONFIG_TLV.value",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA - UL/DL Messages Common Fields */
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type,
        { "Alloc. Type", "fapi_sdk.msg_data_DL_PDSCH_PDU.allocation",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_all_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding,
        { "RB Coding", "fapi_sdk.msg_data_DCI_PDU.rb_coding",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx,
        { "PMI Codebook Index", "fapi_sdk.msg_data_DL_DCI_PDU.pmi_codebook_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_codeword_id,
        { "Codeword ID", "fapi_sdk.msg_data_DL_PDSCH_PDU.codeword_id",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_n_layers,
        { "No of Layers", "fapi_sdk.msg_data_DL_PDSCH_PDU.rank",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost,
        { "Boosting", "fapi_sdk.msg_data_DL_PDU.boost",
        FT_DOUBLE, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(PDSCH_PDU) Message */
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_size,
        { "Length", "fapi_sdk.msg_data_DL_PDSCH_PDU.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_idx,
        { "PDU Index", "fapi_sdk.msg_data_DL_PDSCH_PDU.pdu_idx",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DLSCH_PDU_rnti,
        { "RNTI", "fapi_sdk.msg_data_DL_PDSCH_PDU.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_vir_rb_type,
        { "Virtual RB Type", "fapi_sdk.msg_data_DL_PDSCH_PDU.vir_rb_type",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_vir_rb_type_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap,
        { "RB Bitmap", "fapi_sdk.msg_data_DL_PDSCH_PDU.rb_bitmap",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_mcs,
        { "Modulation", "fapi_sdk.msg_data_DL_PDSCH_PDU.mcs",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_modulation_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PCH_PDU_mcs,
        { "MCS", "fapi_sdk.msg_data_DL_PCH_PDU.mcs",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_pch_mcs_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_rv_idx,
        { "RV", "fapi_sdk.msg_data_DL_PDSCH_PDU.rv_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_swap_flag,
        { "Swap Flag", "fapi_sdk.msg_data_DL_PDSCH_PDU.swap_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_swap_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_ant_mode,
        { "Transmission Scheme", "fapi_sdk.msg_data_DL_PDSCH_PDU.ant_mode",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_dl_ant_method_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_n_subbands,
        { "No of Subbands", "fapi_sdk.msg_data_DL_PDSCH_PDU.n_subbands",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_category,
        { "UE Category", "fapi_sdk.msg_data_DL_PDSCH_PDU.ue_cat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_pa,
        { "P-A", "fapi_sdk.msg_data_DL_PDSCH_PDU.pa",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_pa_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_pdsch_boost_index,
        { "Delta Power Offset", "fapi_sdk.msg_data_DL_PDSCH_PDU.pdsch_boost_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb,
        { "No of PRB", "fapi_sdk.msg_data_DL_PDSCH_PDU.n_prb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_trans_mode,
        { "UE Transmission Mode", "fapi_sdk.msg_data_DL_PDSCH_PDU.ue_trans_mode",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dmrsSeqGenRefPoint,
        { "refPoint", "fapi_sdk.msg_data.TX_CONTROL.dmrsSeqGenRefPoint",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDmrsSymbPos ,
        { "dlDmrsSymbPos ", "fapi_sdk.msg_data.TX_CONTROL.dlDmrsSymbPos",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb_per_subband,
        { "No of PRB per Subband", "fapi_sdk.msg_data_DL_PDSCH_PDU.sb_n_prb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_n_bf_vectors,
        { "No of BFVs", "fapi_sdk.msg_data_DL_PDSCH_PDU.n_bf_vec",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_subband_idx,
        { "Subband Index", "fapi_sdk.msg_data_DL_PDSCH_PDU.sb_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_n_ant,
        { "No of Antennas", "fapi_sdk.msg_data_DL_PDSCH_PDU.n_ant",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_vector,
        { "BFV Element", "fapi_sdk.msg_data_DL_PDSCH_PDU.bf_vec_elm",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_n_scid,
        { "Nscid", "fapi_sdk.msg_data_DL_PDSCH_PDU.n_scid",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_flag,
        { "CSI-RS Flag", "fapi_sdk.msg_data_DL_PDSCH_PDU.csi_rs_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_valid_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_cdi_rs_resrc_config,
        { "CSI-RS Resource Config", "fapi_sdk.msg_data_DL_PDSCH_PDU.csi_rs_resrc_config",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_tx_pwr_resrc_bitmap,
        { "CSI-RS Zero Tx Power bitmap", "fapi_sdk.msg_data_DL_PDSCH_PDU.csi_rs_pwr_bitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(PDCCH_PDU) Message */
        { &hf_fapi_sdk_msg_DL_DCI_PDU_dci_format,
        { "DCI Format", "fapi_sdk.msg_data_DL_DCI_PDU.dci_format",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_dl_dci_format_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_cce_offset,
        { "CCE Index", "fapi_sdk.msg_data_DL_DCI_PDU.cce_offset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_aggr_level,
        { "Aggregation Level", "fapi_sdk.msg_data_DL_DCI_PDU.aggr_level",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_rnti,
        { "RNTI", "fapi_sdk.msg_data_DL_DCI_PDU.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_vir_rb_type,
        { "Virtual RB Type", "fapi_sdk.msg_data_DL_DCI_PDU.vir_rb_type",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_vir_rb_type_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_mcs1,
        { "MCS 1", "fapi_sdk.msg_data_DL_DCI_PDU.mcs1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_rv1,
        { "RV 1", "fapi_sdk.msg_data_DL_DCI_PDU.rv1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_ndi1,
        { "NDI 1", "fapi_sdk.msg_data_DL_DCI_PDU.ndi1",
        FT_UINT8, BASE_DEC, 0x0, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_swap_flag,
        { "Swap Flag", "fapi_sdk.msg_data_DL_DCI_PDU.swap_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_swap_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_mcs2,
        { "MCS 2", "fapi_sdk.msg_data_DCI_PDU.mcs2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_rv2,
        { "RV 2", "fapi_sdk.msg_data_DL_DCI_PDU.rv2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_ndi2,
        { "NDI 2", "fapi_sdk.msg_data_DCI_PDU.ndi2",
        FT_UINT8, BASE_DEC, 0x0, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_harq,
        { "HARQ Process", "fapi_sdk.msg_data_DL_DCI_PDU.harq",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_pmi_conf,
        { "PMI Confirmation", "fapi_sdk.msg_data_DL_DCI_PDU.pmi_conf",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_pmi_conf_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_precoding,
        { "Precoding", "fapi_sdk.msg_data_DL_DCI_PDU.precoding",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_tpc,
        { "TPC", "fapi_sdk.msg_data_DL_DCI_PDU.tpc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_dai,
        { "DAI", "fapi_sdk.msg_data_DL_DCI_PDU.dai",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_tb_size_1c,
        { "TB Size Index(1C)", "fapi_sdk.msg_data_DL_DCI_PDU.tb_size_idx_1c",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_dl_pa_1d,
        { "PA (1D)", "fapi_sdk.msg_data_DL_DCI_PDU.pa_1d",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_prach_flag_1a,
        { "PRACH Flag (1A)", "fapi_sdk.msg_data_DL_DCI_PDU.prach_flag_1a",
        FT_BOOLEAN, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_prach_preamble_1a,
        { "PRACH Preamble (1A)", "fapi_sdk.msg_data_DL_DCI_PDU.prach_preamble_1a",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_prach_mask_1a,
        { "PRACH Mask (1A)", "fapi_sdk.msg_data_DL_DCI_PDU.prach_mask_1a",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_rnti_type,
        { "RNTI Type", "fapi_sdk.msg_data_DL_DCI_PDU.rnti_type",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_rnti_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_mcch_flag,
        { "MCCH Flag (1C)", "fapi_sdk.msg_data_DL_DCI_PDU.mcch_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_valid_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_mcch_change_note,
        { "MCCH Change Notification (1C)", "fapi_sdk.msg_data_DL_DCI_PDU.mcch_change_note",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_scramb_id,
        { "Scrambling ID (2B)", "fapi_sdk.msg_data_DL_DCI_PDU.scramb_id",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_cross_car_flag,
        { "Cross Carrier Scheduling Flag", "fapi_sdk.msg_data_DL_DCI_PDU.cross_car_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_valid_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_car_ind,
        { "Carrier Indicator", "fapi_sdk.msg_data_DL_DCI_PDU.car_ind",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_srs_flag,
        { "SRS Flag", "fapi_sdk.msg_data_DL_DCI_PDU.srs_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_requested_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DCI_PDU_srs_req,
        { "SRS Request", "fapi_sdk.msg_data_DL_DCI_PDU.srs_req",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_ant_port_scramb_layer,
        { "Ant. Ports, Scramb. & Layers (2C)", "fapi_sdk.msg_data_DL_DCI_PDU.ant_port_scramb_layer",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_associated_ul,
        { "Associated UL", "fapi_sdk.msg_data_DL_DCI_PDU.associated_ul",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_DCI_PDU_size_csi_request,
        { "Size of CSI request field", "fapi_sdk.msg_data_DL_DCI_PDU.size_csi_request",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDU_n_gap,
        { "Ngap", "fapi_sdk.msg_data_DL_PDU.n_gap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_PDU_reserved,
        { "Reserved", "fapi_sdk.msg_data_DL_PDU.resv",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (DL_CONFIG_REQ) Message */
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_length,
        { "Length", "fapi_sdk.msg_data_DL_CONFIG_REQ.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdcch,
        { "No of PDCCH Symbols", "fapi_sdk.msg_data_DL_CONFIG_REQ.n_pdcch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_n_dci,
        { "No of DCI PDUs", "fapi_sdk.msg_data_DL_CONFIG_REQ.n_dci",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdu,
        { "No of PDUs", "fapi_sdk.msg_data_DL_CONFIG_REQ.n_pdu",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdsch,
        { "No of PDSCH RNTIs", "fapi_sdk.msg_data_DL_CONFIG_REQ.n_pdsch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_pcfich_boost,
        { "PCFICH Boost", "fapi_sdk.msg_data_DL_CONFIG_REQ.pcfich_boost",
        FT_DOUBLE, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type,
        { "PDU Type", "fapi_sdk.msg_data_DL_CONFIG_REQ.pdu_type",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_dl_pdu_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type_rel10,
        { "PDU Type", "fapi_sdk.msg_data_DL_CONFIG_REQ.pdu_typerel10",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_dl_pdu_type_release10_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_size,
        { "PDU Size", "fapi_sdk.msg_data_DL_CONFIG_REQ.pdu_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (UL_CONFIG_REQ) Message */
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_length,
        { "Length", "fapi_sdk.msg_data_UL_CONFIG_REQ.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_n_pdu,
        { "No of PDUs", "fapi_sdk.msg_data_UL_CONFIG_REQ.n_pdu",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_prach_freq_res,
        { "RACH Freq. Resources", "fapi_sdk.msg_data_UL_CONFIG_REQ.rach_freq_res",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_srs_present,
        { "SRS Present", "fapi_sdk.msg_data_UL_CONFIG_REQ.srs_present",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_presence_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_type,
        { "PDU Type", "fapi_sdk.msg_data_UL_CONFIG_REQ.pdu_type",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_ul_pdu_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_size,
        { "PDU Size", "fapi_sdk.msg_data_UL_CONFIG_REQ.pdu_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_COMMON_PDU_handle,
        { "Handle", "fapi_sdk.msg_data_UL_PDU.handle",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_COMMON_PDU_rnti,
        { "RNTI", "fapi_sdk.msg_data_UL_PDU.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_COMMON_PDU_ant_ports,
        { "Antenna Ports", "fapi_sdk.msg_data_UL_PDU.ant_ports",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (UCI PDU) Message */
        { &hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx,
        { "PUCCH Index", "fapi_sdk.msg_data_UCI_PDU.pucch_idx",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_UCI_PDU_cqi_size,
        { "DL CQI/PMI No of Bits", "fapi_sdk.msg_data_UCI_PDU.cqi_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_UCI_PDU_harq_size,
        { "ACK/NACK No of Bits", "fapi_sdk.msg_data_UCI_PDU.harq_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_UCI_PDU_acknack_mode,
        { "ACK/NACK Mode", "fapi_sdk.msg_data_UCI_PDU.acknack_mode",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_UCI_PDU_n_res,
        { "No of Responses", "fapi_sdk.msg_data_UCI_PDU.n_res",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (SRS PDU) Message */
        { &hf_fapi_sdk_msg_UL_SRS_PDU_size,
        { "PDU Length", "fapi_sdk.msg_data_SRS_PDU.len",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_srs_bw,
        { "SRS Bandwidth", "fapi_sdk.msg_data_SRS_PDU.srs_bw",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_freq_dom_pos,
        { "Freq. Domain Position", "fapi_sdk.msg_data_SRS_PDU.freq_dom_pos",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_srs_hop_bw,
        { "SRS Freq. Hopping", "fapi_sdk.msg_data_SRS_PDU.srs_hop_bw",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_trans_comb,
        { "Transmission Comb", "fapi_sdk.msg_data_SRS_PDU.trans_comb",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_trans_comb_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_config_idx,
        { "Configuration Index", "fapi_sdk.msg_data_SRS_PDU.config_idx",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_PDU_cs,
        { "Cyclic Shift", "fapi_sdk.msg_data_SRS_PDU.cs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA(PUSCH PDU) Message */
        { &hf_fapi_sdk_msg_PUSCH_PDU_size,
        { "Size", "fapi_sdk.msg_data_PUSCH_PDU.size",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_rb_start,
        { "RB Start", "fapi_sdk.msg_data_PUSCH_PDU.rb_start",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_rb_num,
        { "RB Num", "fapi_sdk.msg_data_PUSCH_PDU.rb_num",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_mcs,
        { "MCS", "fapi_sdk.msg_data_PUSCH_PDU.MCS",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_modulation_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_cs2dmrs,
        { "CS2DMRS", "fapi_sdk.msg_data_PUSCH_PDU.cs2dmrs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_flag,
        { "Freq. Hopping Flag", "fapi_sdk.msg_data_PUSCH_PDU.freq_hop_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_Hop_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_bits,
        { "Freq. Hopping Bits", "fapi_sdk.msg_data_PUSCH_PDU.freq_hop_bits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_ndi,
        { "NDI", "fapi_sdk.msg_data_PUSCH_PDU.ndi",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_rv,
        { "RV", "fapi_sdk.msg_data_PUSCH_PDU.rv",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_harq_re_tx,
        { "HARQ Process Num.", "fapi_sdk.msg_data_PUSCH_PDU.harq_re_tx",
        FT_UINT8, BASE_DEC,NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_ul_tx_mode,
        { "UL Tx Mode", "fapi_sdk.msg_data_PUSCH_PDU.ul_tx_mode",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_ul_tx_mode_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_curr_harq_trans,
        { "Curr. Tx NB", "fapi_sdk.msg_data_PUSCH_PDU.curr_harq_trans",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_srs_present,
        { "SRS Present", "fapi_sdk.msg_data_PUSCH_PDU.srs_present",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_presence_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_trans_scheme,
        { "Transmission Scheme", "fapi_sdk.msg_data_UL_PUSCH_PDU.ant_mode",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_ul_ant_method_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_seq_hopping_disable,
        { "Disable Sequence Hopping Flag", "fapi_sdk.msg_data_UL_PUSCH_PDU.disable_seq_hop",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_disable_flag_tfs), 0x0,
        NULL, HFILL } },
        { &nScid ,
        { "nScid ", "fapi_sdk.msg_data.TX_CONTROL.nScid",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &type,
        { "type", "fapi_sdk.msg_data.TX_CONTROL.type",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &startRb ,
        { "startRb ", "fapi_sdk.msg_data.TX_CONTROL.startRb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numRbs,
        { "numRbs", "fapi_sdk.msg_data.TX_CONTROL.numRbs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Density,
        { "Density", "fapi_sdk.msg_data.TX_CONTROL.Density",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cdmType,
        { "cdmType", "fapi_sdk.msg_data.TX_CONTROL.cdmType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaCsirsSs,
        { "betaCsirsSs", "fapi_sdk.msg_data.TX_CONTROL.betaCsirsSs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &freqStartLocation ,
        { "freqStartLocation ", "fapi_sdk.msg_data.TX_CONTROL.freqStartLocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &timeStartLocation ,
        { "timeStartLocation ", "fapi_sdk.msg_data.TX_CONTROL.timeStartLocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rbBitmap,
        { "rbBitmap", "fapi_sdk.msg_data.TX_CONTROL.rbBitmap",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &symbolLength,
        { "symbolLength", "fapi_sdk.msg_data.TX_CONTROL.symbolLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &vrbToPrbMapping,
        { "vrbToPrbMapping", "fapi_sdk.msg_data.TX_CONTROL.vrbToPrbMapping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prgSize,
        { "prgSize", "fapi_sdk.msg_data.TX_CONTROL.prgSize",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsFreqDensity,
        { "dlPtrsFreqDensity", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsFreqDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsPresent,
        { "dlPtrsPresent", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsPresent",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsTimeDensity,
        { "dlPtrsTimeDensity", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsTimeDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsReOffset,
        { "dlPtrsReOffset", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsReOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPtrs,
        { "betaPtrs", "fapi_sdk.msg_data.TX_CONTROL.betaPtrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &precoderCycleLength,
        { "precoderCycleLength", "fapi_sdk.msg_data.TX_CONTROL.precoderCycleLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &precoderIndices ,
        { "precoderIndices ", "fapi_sdk.msg_data.TX_CONTROL.precoderIndices",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nCsiRsPorts,
        { "nCsiRsPorts", "fapi_sdk.msg_data.TX_CONTROL.nCsiRsPorts",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBfIndices ,
        { "digBfIndices ", "fapi_sdk.msg_data.TX_CONTROL.digBfIndices",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nPrg ,
        { "nPrg ", "fapi_sdk.msg_data.TX_CONTROL.nPrg",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pmi ,
        { "pmi ", "fapi_sdk.msg_data.TX_CONTROL.pmi",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &digBfIndex ,
        { "digBfIndex ", "fapi_sdk.msg_data.TX_CONTROL.digBfIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_pduType,
        { "PDU TYPE", "fapi_sdk.msg_data_INFRA_CMD.infra_pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_pduLength,
        { "PDU TYPE", "fapi_sdk.msg_data_INFRA_CMD.infra_pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_sfn,
        { "SFN", "fapi_sdk.msg_data_INFRA_CMD.infra_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_slot,
        { "Slot", "fapi_sdk.msg_data_INFRA_CMD.infra_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numerology,
        { "Numerology", "fapi_sdk.msg_data_INFRA_CMD.infra_numerology",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numPdu,
        { "Num PDU", "fapi_sdk.msg_data_INFRA_CMD.infra_numPdu",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_tag,
        { "Tag", "fapi_sdk.msg_data_INFRA_CMD.infra_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_length,
        { "Length", "fapi_sdk.msg_data_INFRA_CMD.infra_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_gpio,
        { "Gpio", "fapi_sdk.msg_data_INFRA_CMD.infra_gpio",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_enable,
        { "enable", "fapi_sdk.msg_data_INFRA_CMD.enable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_targetSfn,
        { "targetSfn", "fapi_sdk.msg_data_INFRA_CMD.infra_targetSfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_targetSlot,
        { "targetSlot", "fapi_sdk.msg_data_INFRA_CMD.infra_targetSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_timingAdvance,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_repeat,
        { "repeat", "fapi_sdk.msg_data_INFRA_CMD.repeat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_tag2,
        { "tag", "fapi_sdk.msg_data_INFRA_CMD.infra_tag2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_length2,
        { "Length", "fapi_sdk.msg_data_INFRA_CMD.infra_length2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_gpio2,
        { "Gpio", "fapi_sdk.msg_data_INFRA_CMD.infra_gpio2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_enable2,
        { "enable", "fapi_sdk.msg_data_INFRA_CMD.enable2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_timingAdvance2,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_baudrate,
        { "baudrate", "fapi_sdk.msg_data_INFRA_CMD.infra_baudrate",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        // { &hf_fapi_sdk_msg_PUSCH_PDU_cqi_nbits_ri1,
        // { "CQI Num of Bits (RI=1)", "fapi_sdk.msg_data_PUSCH_PDU.cqi_nbits",
        // FT_UINT8, BASE_DEC, NULL, 0x0,
        // NULL, HFILL } },
        //
        // { &hf_fapi_sdk_msg_PUSCH_PDU_cqi_nbits_ri2,
        // { "CQI Num of Bits (RI=2)", "fapi_sdk.msg_data_PUSCH_PDU.cqi_nbits",
        // FT_UINT8, BASE_DEC, NULL, 0x0,
        // NULL, HFILL } },
        //INFRA_CMD
        /*
        { &infra_timingAdvance2,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        */
        //
        { &infra_sfn2,
        { "Sfn", "fapi_sdk.msg_data_INFRA_CMD.infra_Sfn2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_slot2,
        { "Slot", "fapi_sdk.msg_data_INFRA_CMD.infra_Slot2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numerology2,
        { "numerology", "fapi_sdk.msg_data_INFRA_CMD.infra_numerology2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numDataBits,
        { "numDataBits", "fapi_sdk.msg_data_INFRA_CMD.infra_numDataBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_data,
        { "data", "fapi_sdk.msg_data_INFRA_CMD.infra_data",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &yonScid,
        { "nScid", "fapi_sdk.msg_data.TX_CONTROL.nScid1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numDmrsCdmGrpsNoData,
        { "numDmrsCdmGrpsNoData", "fapi_sdk.msg_data.TX_CONTROL.numDmrsCdmGrpsNoData",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_ri_nbits,
        { "RI Num of Bits", "fapi_sdk.msg_data_PUSCH_PDU.ri_nbits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_acknack_nbits,
        { "ACK/NACK Num of Bits", "fapi_sdk.msg_data_PUSCH_PDU.acknack_nbits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_acknack,
        { "Delta Offset ACK/NACK", "fapi_sdk.msg_data_PUSCH_PDU.beta_offset_acknack",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_acknack_mode_TDD,
        { "ACK/NACK Mode (TDD)", "fapi_sdk.msg_data_PUSCH_PDU.acknack_mode",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_cqi,
        { "Delta Offset CQI", "fapi_sdk.msg_data_PUSCH_PDU.beta_offset_cqi",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_ri,
        { "Delta Offset RI", "fapi_sdk.msg_data_PUSCH_PDU.beta_offset_ri",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_nrb_init,
        { "RB Num Init", "fapi_sdk.msg_data_PUSCH_PDU.rb_num_init",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_srs_init,
        { "SRS Init", "fapi_sdk.msg_data_PUSCH_PDU.srs_init",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_presence_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_report_type,
        { "Report Type", "fapi_sdk.msg_data_PUSCH_PDU.report_type",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_csi_report_type_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_ri_size,
        { "CQI/PMI/RI Size", "fapi_sdk.msg_data_PUSCH_PDU.cqi_pmi_ri_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_n_cc,
        { "CC Num.", "fapi_sdk.msg_data_PUSCH_PDU.n_cc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_ri_size,
        { "RI Size", "fapi_sdk.msg_data_PUSCH_PDU.ri_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_size,
        { "CQI/PMI Size", "fapi_sdk.msg_data_PUSCH_PDU.cqi_pmi_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_n_phy,
        { "num of phy", "fapi_sdk.msg_data_UL_TX_REQ.n_phy",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (TX_REQ) Message */
        { &hf_fapi_sdk_msg_TX_REQ_n_pdu,
        { "No of PDUs", "fapi_sdk.msg_data_UL_TX_REQ.n_pdu",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_length,
        { "PDU Length", "fapi_sdk.msg_data_UL_TX_REQ.pdu_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_idx,
        { "PDU Index", "fapi_sdk.msg_data_UL_TX_REQ.pdu_idx",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_n_tlv,
        { "No of TLVs", "fapi_sdk.msg_data_UL_TX_REQ.n_tlv",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_TLV_tag,
        { "Tag", "fapi_sdk.msg_data_UL_TX_REQ_TLV.tap",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_tx_req_tag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_TLV_length,
        { "Length", "fapi_sdk.msg_data_UL_TX_REQ_TLV.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_TLV_value_tag0,
        { "Value", "fapi_sdk.msg_data_UL_TX_REQ_TLV.value",
        FT_BYTES, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_TX_REQ_TLV_value_tag1,
        { "Value", "fapi_sdk.msg_data_UL_TX_REQ_TLV.value",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_HI_DCI0_REQ) Message */
        { &hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_dci,
        { "No of DCI PDUs", "fapi_sdk.msg_data_UL_HI_DCI0_REQ.n_dci",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prachConfigIdx,
        { "prachConfigIdx", "fapi_sdk.msg_data.RX_CONTROL.prach.prachConfigIdx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cp1,
        { "cp", "fapi_sdk.msg_data.RX_CONTROL.prach.cp1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /*{ &hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_phich,
        { "No of PHICH PDUs", "fapi_sdk.msg_data_UL_HI_DCI0_REQ.n_phich",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_type,
        { "PDU Type", "fapi_sdk.msg_data_UL_HI_DCI0_REQ.type",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_hi_dci0_pdu_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_size,
        { "PDU Size", "fapi_sdk.msg_data_UL_HI_DCI0_REQ.size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_rb_start,
        { "RB Start", "fapi_sdk.msg_data_UL_PHICH_PDU.rb_start",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_cs2dmrs,
        { "CS2DMRS", "fapi_sdk.msg_data_UL_PHICH_PDU.cs2dmrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_value,
        { "HI Value", "fapi_sdk.msg_data_UL_PHICH_PDU.value",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_ACK_NACK_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_i_phich,
        { "I_PHICH", "fapi_sdk.msg_data_UL_PHICH_PDU.i_phich",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        */
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_boost,
        { "Boosting", "fapi_sdk.msg_data_UL_PHICH_PDU.boost",
        FT_DOUBLE, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PHICH_PDU_tb2_flag,
        { "TB2 Flag", "fapi_sdk.msg_data_UL_PHICH_PDU.tb2_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_presence_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_format,
        { "DCI Format", "fapi_sdk.msg_data_UL_DCI_PDU.format",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_ul_dci_format_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_cce_offset,
        { "CCE Offset", "fapi_sdk.msg_data_UL_DCI_PDU.cce_offset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_aggr_lvl,
        { "Aggr. Level", "fapi_sdk.msg_data_UL_DCI_PDU.aggr_lvl",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &getDlTbCrc,
        { "getDlTbCrc", "fapi_sdk.msg_data.TX_CONTROL.getDlTbCrc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &IsLastCbPresent,
        { "IsLastCbPresent", "fapi_sdk.msg_data.TX_CONTROL.IsLastCbPresent",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &isInlineTbCrc,
        { "isInlineTbCrc", "fapi_sdk.msg_data.TX_CONTROL.isInlineTbCrc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlTbCrc,
        { "dlTbCrc", "fapi_sdk.msg_data.TX_CONTROL.dlTbCrc",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs0ReOffset,
        { "ulPtrs0ReOffset", "fapi_sdk.msg_data.RX_CONTROL.ulPtrs0ReOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs1ReOffset,
        { "ulPtrs1ReOffset", "fapi_sdk.msg_data.RX_CONTROL.ulPtrs1ReOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs0DmrsPort,
        { "ulPtrs0DmrsPort", "fapi_sdk.msg_data.RX_CONTROL.ulPtrs0DmrsPort",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs0DmrsAssoc ,
        { "ulPtrs0DmrsAssoc ", "fapi_sdk.msg_data.TX_CONTROL.ulPtrs0DmrsAssoc",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs1DmrsPort,
        { "ulPtrs1DmrsPort", "fapi_sdk.msg_data.RX_CONTROL.ulPtrs1DmrsPort",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrs1DmrsAssoc ,
        { "ulPtrs1DmrsAssoc ", "fapi_sdk.msg_data.TX_CONTROL.ulPtrs1DmrsAssoc",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrsFrequencyDensity,
        { "ulPtrsFrequencyDensity", "fapi_sdk.msg_data.RX_CONTROL.ulPtrsFrequencyDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrsTimeDensity,
        { "ulPtrsTimeDensity", "fapi_sdk.msg_data.RX_CONTROL.ulPtrsTimeDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrsPower,
        { "ulPtrsPower", "fapi_sdk.msg_data.RX_CONTROL.ulPtrsPower",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &lowPaprGroupNumber,
        { "lowPaprGroupNumber", "fapi_sdk.msg_data.RX_CONTROL.lowPaprGroupNumber",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &lowPaprSequenceNumber ,
        { "lowPaprSequenceNumber ", "fapi_sdk.msg_data.TX_CONTROL.lowPaprSequenceNumber",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrsPreDftDensity,
        { "ulPtrsPreDftDensity", "fapi_sdk.msg_data.RX_CONTROL.ulPtrsPreDftDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulPtrsTimeDensityTransformPrecoding,
        { "ulPtrsTimeDensityTransformPrecoding", "fapi_sdk.msg_data.RX_CONTROL.ulPtrsTimeDensityTransformPrecoding",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
/*{ &hf_fapi_sdk_msg_UL_DCI_PDU_rnti,
        { "RNTI", "fapi_sdk.msg_data_UL_DCI_PDU.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_rb_start,
        { "RB_Start", "fapi_sdk.msg_data_UL_DCI_PDU.rb_start",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_n_rb,
        { "No of RBs", "fapi_sdk.msg_data_UL_DCI_PDU.n_rb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_mcs,
        { "MCS", "fapi_sdk.msg_data_UL_DCI_PDU.mcs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_cs2dmrs,
        { "CS2DMRS", "fapi_sdk.msg_data_UL_DCI_PDU.cs2dmrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_flag,
        { "Freq. Hopping Flag", "fapi_sdk.msg_data_UL_DCI_PDU.freq_hop_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_Hop_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_bits,
        { "Freq Hopping Bits", "fapi_sdk.msg_data_UL_DCI_PDU.freq_hop_bits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_ndi,
        { "NDI", "fapi_sdk.msg_data_UL_DCI_PDU.ndi",
        FT_UINT8, BASE_DEC, 0x0, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_ant_sel,
        { "Antenna Selection", "fapi_sdk.msg_data_UL_DCI_PDU.ant_sel",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_antenna_selection_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_tpc,
        { "TPC", "fapi_sdk.msg_data_UL_DCI_PDU.tpc",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
*/
        { &bandwidth ,
        { "bandwidth ", "fapi_sdk.msg_data.TX_CONTROL.bandwidth",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cyclicPrefix,
        { "cyclicPrefix", "fapi_sdk.msg_data.TX_CONTROL.cyclicPrefix",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &tbSizeBytes,
        { "tbSizeBytes", "fapi_sdk.msg_data.TX_CONTROL.tbSizeBytes",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &tbSizeLbrmBytes,
        { "tbSizeLbrmBytes", "fapi_sdk.msg_data.TX_CONTROL.tbSizeLbrmBytes",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_aperiodic_cqi,
        { "Aperiodic CQI", "fapi_sdk.msg_data_UL_DCI_PDU.ap_cqi",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_requested_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_ul_idx,
        { "UL Index", "fapi_sdk.msg_data_UL_DCI_PDU.ul_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_dl_idx,
        { "DL Assigned Index", "fapi_sdk.msg_data_UL_DCI_PDU.dl_idx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_tpc_bitmap,
        { "TPC Commands", "fapi_sdk.msg_data_UL_DCI_PDU.value",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_cqi_csi_size,
        { "CQI/SCI Size", "fapi_sdk.msg_data_UL_DCI_PDU.cqi_csi_size",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_cqi_sci_size_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_ra_flag,
        { "Resource Alloc. Flag", "fapi_sdk.msg_data_UL_DCI_PDU.ra_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_valid_flag_tfs), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_DCI_PDU_trans_mode,
        { "Transmission Mode", "fapi_sdk.msg_data_DL_PDSCH_PDU.trans_mode",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_RX_ULSCH_IND) Message */
        /* FAPI DATA (FAPI_E_RX_CQI_IND) Message */
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_n_pdu,
        { "No of PDUs", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.n_pdu",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_length,
        { "Length", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data_offset,
        { "Data Offset", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.data_offset",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_snr,
        { "SNR", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.SNR",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CQI_EVT_ri,
        { "RI", "fapi_sdk.msg_data_UL_CQI_EVT.ri",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset,
        { "Timing Advance", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.t_a",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset_R9,
        { "Timing Advance R9", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.t_a_r9",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data,
        { "Data", "fapi_sdk.msg_data_UL_PUSCH_CQI_EVT.data",
        FT_BYTES, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_multiSlotTxIndicator,
        { "pucch_multiSlotTxIndicator", "fapi_sdk.msg_data.RX_CONTROL.pucch_multiSlotTxIndicator",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_RX_SR_IND) Message */
        { &hf_fapi_sdk_msg_UL_SR_EVT_n_sr,
        { "No of SR PDUs", "fapi_sdk.msg_data_UL_SR_EVT.n_sr",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_HARQ_IND) Message */
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_n_harq,
        { "No of HARQ PDUs", "fapi_sdk.msg_data_UL_HARQ_EVT.n_harq",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_acknack1,
        { "TB 1 Feedback", "fapi_sdk.msg_data_UL_HARQ_EVT.acknack1",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_harq_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_acknack2,
        { "TB 2 Feedback", "fapi_sdk.msg_data_UL_HARQ_EVT.acknack2",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_harq_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_acknack3,
        { "TB 3 Feedback", "fapi_sdk.msg_data_UL_HARQ_EVT.acknack3",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_harq_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_acknack4,
        { "TB 4 Feedback", "fapi_sdk.msg_data_UL_HARQ_EVT.acknack4",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_harq_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_acknack_mode,
        { "ACK/NACK Mode", "fapi_sdk.msg_data_UL_HARQ_EVT.mode",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_acknack_mode_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_HARQ_EVT_n_acknack,
        { "No of ACK/NACKs", "fapi_sdk.msg_data_UL_HARQ_EVT.n_acknack",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_CRC_IND) Message */
        { &hf_fapi_sdk_msg_UL_CRC_EVT_n_crc,
        { "No of CRC PDUs", "fapi_sdk.msg_data_UL_CRC_IND.n_crc",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_CRC_EVT_crc_flag,
        { "CRC", "fapi_sdk.msg_data_UL_CRC_IND.crc_flag",
        FT_BOOLEAN, BASE_NONE, TFS(&fapi_msg_data_crc_tfs), 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_SRS_IND) Message */
        { &hf_fapi_sdk_msg_UL_SRS_EVT_n_srs,
        { "No of UEs", "fapi_sdk.msg_data_UL_SRS_EVT.n_ue",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_EVT_dopp_est,
        { "FFS", "fapi_sdk.msg_data_UL_SRS_EVT.dopp_est",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_EVT_timing_offset,
        { "Timing Advance", "fapi_sdk.msg_data_UL_SRS_EVT.t_a",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_EVT_n_rb,
        { "No of RBs", "fapi_sdk.msg_data_UL_SRS_EVT.n_rb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_EVT_rb_start,
        { "RB Start", "fapi_sdk.msg_data_UL_SRS_EVT.rb_start",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_SRS_EVT_rb_snr,
        { "SNR", "fapi_sdk.msg_data_UL_SRS_EVT.snr",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI DATA (FAPI_E_RACH_IND) Message */
        { &hf_fapi_sdk_msg_UL_PRACH_RSP_num_preambles,
        { "No of Preambles", "fapi_sdk.msg_data_UL_RACH_RSP.n_preamble",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PRACH_RSP_rnti,
        { "RNTI", "fapi_sdk.msg_data_UL_RACH_RSP.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PRACH_RSP_preamble_id,
        { "Detected Preamble", "fapi_sdk.msg_data_UL_RACH_RSP.preamble",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_UL_PRACH_RSP_timing_offset,
        { "Timing Advance", "fapi_sdk.msg_data_UL_RACH_RSP.t_a",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDmrsConfigType,
        { "dlDmrsConfigType", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.dl_config_sub_error_code",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDmrsScramblingId ,
        { "dlDmrsScramblingId ", "hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &erSFN ,
        { "SFN ", "fapi_sdk.msg_data.ERROR_INDICATION.erSFN",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &erSlot ,
        { "Slot", "fapi_sdk.msg_data.ERROR_INDICATION.erSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ermsgId ,
        { "msgId ", "fapi_sdk.msg_data.ERROR_INDICATION.ermsgId",
        FT_UINT16, BASE_DEC, VALS(fapi_msg_header_string), 0x0,
        NULL, HFILL } },
        { &ererrorCode,
        { "errorCode", "fapi_sdk.msg_data.ERROR_INDICATION.ererrorCode",
        FT_UINT8, BASE_DEC, VALS(FAPI_E_ERROR_CODEs), 0x0,
        NULL, HFILL } },
        { &sub_error_code,
        { "errorOffset", "fapi_sdk.msg_data.ERROR_INDICATION.sub_error_code",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },

        { &pdu_index_error,
        { "Pdu_index", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_index_error",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &duplicate_error ,
        { "Pdu_type", "fapi_sdk.msg_data.ERROR_INDICATION.duplicate_error",
        FT_UINT16, BASE_DEC, VALS(DUPLICATE_ERROR), 0x0,
        NULL, HFILL } },
        { &pdu_type_error ,
        { "Pdu_type", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_type_error",
        FT_UINT16, BASE_DEC, VALS(FAPI_E_TX_PDU_TYPEs), 0x0,
        NULL, HFILL } },
        { &pdu_type_error1 ,
        { "Pdu_type", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_type_error1",
        FT_UINT16, BASE_DEC, VALS(FAPI_E_RX_PDU_TYPEs), 0x0,
        NULL, HFILL } },
        { &error_code ,
        { "Error_code ", "fapi_sdk.msg_data.ERROR_INDICATION.error_code",
        FT_UINT16, BASE_DEC, VALS(invalid_message_type), 0x0,
        NULL, HFILL } },
        { &error_value ,
        { "Error_value ", "fapi_sdk.msg_data.ERROR_INDICATION.error_value",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &expected_value ,
        { "Expected_value ", "fapi_sdk.msg_data.ERROR_INDICATION.expected_value",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &direction,
        { "pduIndex", "fapi_sdk.msg_data.ERROR_INDICATION.direction",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &errnti ,
        { "RNTI ", "fapi_sdk.msg_data.ERROR_INDICATION.errnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pdu_type ,
        { "pduType ", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_type",
        FT_UINT16, BASE_DEC, VALS(FAPI_E_TX_PDU_TYPEs), 0x0,
        NULL, HFILL } },
        { &pdu_type1 ,
        { "pduType ", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_type",
        FT_UINT16, BASE_DEC, VALS(FAPI_E_RX_PDU_TYPEs), 0x0,
        NULL, HFILL } },
        { &pdu_sub_type ,
        { "subPduIndex", "fapi_sdk.msg_data.ERROR_INDICATION.pdu_sub_type",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &target_sfn ,
        { "target_sfn ", "fapi_sdk.msg_data.ERROR_INDICATION.target_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &current_sfn ,
        { "current_sfn ", "fapi_sdk.msg_data.ERROR_INDICATION.current_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &target_slot ,
        { "target_slot ", "fapi_sdk.msg_data.ERROR_INDICATION.target_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &current_slot ,
        { "current_slot ", "fapi_sdk.msg_data.ERROR_INDICATION.current_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_tag,
        { "tag", "fapi_sdk.msg_data.RX_CONTROL.prach.tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_length,
        { "length", "fapi_sdk.msg_data.RX_CONTROL.prach.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_phys_cell_id,
        { "physCellId", "fapi_sdk.msg_data.RX_CONTROL.prach.physCellId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_format,
        { "prachFormat", "fapi_sdk.msg_data.RX_CONTROL.prach.format",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_nRa_Slot_T,
        { "nRaSlotT", "fapi_sdk.msg_data.RX_CONTROL.prach.nRaSlotT",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_nRa,
        { "nRa", "fapi_sdk.msg_data.RX_CONTROL.prach.nRa",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_start_symbol,
        { "prachStartSymbol", "fapi_sdk.msg_data.RX_CONTROL.prach.startSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_ncs,
        { "nCs", "fapi_sdk.msg_data.RX_CONTROL.prach.nCs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_digital_bf_info,
        { "digitalBfInfo", "fapi_sdk.msg_data.RX_CONTROL.prach.digitalBfInfo",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Prach_sfn ,
        { "sfn ", "fapi_sdk.msg_data.PRACH_IND.prach.sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Prach_slot ,
        { "slot ", "fapi_sdk.msg_data.PRACH_IND.prach.slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Prach_numerology ,
        { "numerology ", "fapi_sdk.msg_data.PRACH_IND.prach.numerology",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Prach_pdu_count ,
        { "pdu_count ", "fapi_sdk.msg_data.PRACH_IND.prach.pdu.count",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_pdu_type ,
        { "pdu_type ", "fapi_sdk.msg_data.PRACH_IND.prach.pdu.type",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prch_pdu_length ,
        { "pdu_length ", "fapi_sdk.msg_data.PRACH_IND.prach.pdu.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_ind_tag ,
        { "tag ", "fapi_sdk.msg_data.PRACH_IND.prach.tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_ind_length ,
        { "length ", "fapi_sdk.msg_data.PRACH_IND.prach.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_ind_phys_cell_id ,
        { "physCellId ", "fapi_sdk.msg_data.PRACH_IND.prach.cell.id",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_ind_occasion_td ,
        { "prachOccasionTd", "fapi_sdk.msg_data.PRACH_IND.prach.occasion.td",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_occasion_fd ,
        { "prachOccasionFd ", "fapi_sdk.msg_data.PRACH_IND.prach.occasion.fd",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_avg_rssi_ind_db ,
        { "avgRssiIndb ", "fapi_sdk.msg_data.PRACH_IND.prach.avg.rssi.ind.db",
        FT_INT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_detectionMetricdb ,
        { "prach_detectionMetricdb ", "fapi_sdk.msg_data.PRACH_IND.prachp.detectionMetricdb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_number_of_preamble ,
        { "numDetectedPreambleIndices ", "fapi_sdk.msg_data.PRACH_IND.prach.number.of.preamable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_detected_premable_indices ,
        { "detectedPreambleIndices ", "fapi_sdk.msg_data.PRACH_IND.prach.detected.preamble",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &prach_timing_estimates ,
        { "timingEstimates ", "fapi_sdk.msg_data.PRACH_IND.prach.timing.estimates",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_tag ,
        { "pucch_tag ", "fapi_sdk.msg_data.RX_CONTROL.pucch_tag",
        FT_UINT16, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_length ,
        { "pucch_length ", "fapi_sdk.msg_data.RX_CONTROL.pucch_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_handle,
        { "pucch_handle", "fapi_sdk.msg_data.RX_CONTROL.pucch_handle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_nRnti ,
        { "pucch_nRnti ", "fapi_sdk.msg_data.RX_CONTROL.pucch_nRnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &upucchFormat,
        { "pucchFormat", "fapi_sdk.msg_data.RX_CONTROL.upucchFormat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &upucchGroupHopping,
        { "pucchGroupHopping", "fapi_sdk.msg_data.RX_CONTROL.upucchGroupHopping",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_hoppingId ,
        { "pucch_hoppingId ", "fapi_sdk.msg_data.RX_CONTROL.pucch_hoppingId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_startSymbol,
        { "pucch_startSymbol", "fapi_sdk.msg_data.RX_CONTROL.pucch_startSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_numSyms,
        { "pucch_numSyms", "fapi_sdk.msg_data.RX_CONTROL.pucch_numSyms",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_interSlotFreqHopEnable,
        { "pucch_interSlotFreqHopEnable", "fapi_sdk.msg_data.RX_CONTROL.pucch_interSlotFreqHopEnable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_intraSlotFreqHopEnable,
        { "pucch_intraSlotFreqHopEnable", "fapi_sdk.msg_data.RX_CONTROL.pucch_intraSlotFreqHopEnable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_nPucchRb,
        { "pucch_mPucchRb", "fapi_sdk.msg_data.RX_CONTROL.pucch_nPucchRb",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_startingPrb ,
        { "pucch_startingPrb ", "fapi_sdk.msg_data.RX_CONTROL.pucch_startingPrb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_secondHopPrb ,
        { "pucch_secondHopPrb ", "fapi_sdk.msg_data.RX_CONTROL.pucch_secondHopPrb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_detectType,
        { "pucch_detectType", "fapi_sdk.msg_data.RX_CONTROL.pucch_detectType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_harqAckBitLength,
        { "pucch_harqAckBitLength", "fapi_sdk.msg_data.RX_CONTROL.pucch_harqAckBitLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_initialCyclicShift,
        { "pucch_initialCyclicShift", "fapi_sdk.msg_data.RX_CONTROL.pucch_initialCyclicShift",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_txDirectCurrentLocation ,
        { "pucch_txDirectCurrentLocation ", "fapi_sdk.msg_data.RX_CONTROL.pucch_txDirectCurrentLocation",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_orthoSeqIndexFormat1,
        { "pucch_orthoSeqIndexFormat1", "fapi_sdk.msg_data.RX_CONTROL.pucch_orthoSeqIndexFormat1",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_nId ,
        { "pucch_nId ", "fapi_sdk.msg_data.RX_CONTROL.pucch_nId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &scramblingId0 ,
        { "scramblingId0 ", "fapi_sdk.msg_data.RX_CONTROL.scramblingId0",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &occLengthFormat4,
        { "occLengthFormat4", "fapi_sdk.msg_data.RX_CONTROL.occLengthFormat4",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &occIndexFormat4,
        { "occIndexFormat4", "fapi_sdk.msg_data.RX_CONTROL.occIndexFormat4",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchPayloadLengthBits ,
        { "pucchPayloadLengthBits ", "fapi_sdk.msg_data.RX_CONTROL.pucchPayloadLengthBits",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucchPayloadLenCSIPart2 ,
        { "pucchPayloadLenCSIPart2 ", "fapi_sdk.msg_data.RX_CONTROL.pucchPayloadLenCSIPart2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &desfn ,
        { "sfn ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.desfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &deslot ,
        { "slot ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.deslot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &denumerology ,
        { "numerology ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.denumerology",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &depduCount ,
        { "pduCount ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.depduCount",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &depduType ,
        { "pduType ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.depduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &depduLength ,
        { "pduLength ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.depduLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &detag ,
        { "tag ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.detag",
        FT_UINT16, BASE_HEX_DEC, VALS(FAPI_E_ULSCH_DECODE), 0x0,
        NULL, HFILL } },
        { &delength ,
        { "length ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.delength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &depduBitmap ,
        { "pduBitmap ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.depduBitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dehandle,
        { "handle", "fapi_sdk.msg_data.RX_CONTROL.dehandle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dernti ,
        { "rnti ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.dernti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &deharqId,
        { "harqId", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.deharqId",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &csiPart1BitLength ,
        { "csiPart1BitLength ", "fapi_sdk.msg_data.TX_CONTROL.csiPart1BitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &riOffset,
        { "riOffset", "fapi_sdk.msg_data.RX_CONTROL.riOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &riBitwidth,
        { "riBitwidth", "fapi_sdk.msg_data.RX_CONTROL.riBitwidth",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &csiPart2BitLength ,
        { "csiPart2BitLength ", "fapi_sdk.msg_data.TX_CONTROL.csiPart2BitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &detbCrcStatus,
        { "tbCrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.detbCrcStatus",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &denumCb ,
        { "numCb ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.denumCb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &deulSnr,
        { "ulSnr", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.deulSnr",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &detimingAdvance ,
        { "timingAdvance ", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.detimingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &decbCrcStatus,
        { "cbCrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.decbCrcStatus",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &rx_padding_tag ,
        { "rx_padding_tag ", "fapi_sdk.msg_data.RX_DATA.rx_padding_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rx_length1 ,
        { "rx_length ", "fapi_sdk.msg_data.RX_DATA.rx_length1",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rx_base_uldsch ,
        { "FAPI_E_BASE_ULSCH_DATA_PDU", "fapi_sdk.msg_data.RX_DATA.rx_base_uldsch",
        FT_UINT16, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rx_length2 ,
        { "rx_length ", "fapi_sdk.msg_data.RX_DATA.rx_length2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_sfn ,
        { "uci_sfn ", "fapi_sdk.msg_data.UCI_INDICATION.uci_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_slot ,
        { "uci_slot ", "fapi_sdk.msg_data.UCI_INDICATION.uci_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_numerology ,
        { "uci_numerology ", "fapi_sdk.msg_data.UCI_INDICATION.uci_numerology",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_pduCount ,
        { "uci_pduCount ", "fapi_sdk.msg_data.UCI_INDICATION.uci_pduCount",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_pduType ,
        { "uci_pduType ", "fapi_sdk.msg_data.UCI_INDICATION.uci_pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_pduLength ,
        { "uci_pduLength ", "fapi_sdk.msg_data.UCI_INDICATION.uci_pduLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_tag ,
        { "uci_tag ", "fapi_sdk.msg_data.UCI_INDICATION.uci_tag",
        FT_UINT16, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_length ,
        { "uci_length ", "fapi_sdk.msg_data.UCI_INDICATION.uci_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_pduBitmap ,
        { "uci_pduBitmap ", "fapi_sdk.msg_data.UCI_INDICATION.uci_pduBitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_handle,
        { "uci_handle", "fapi_sdk.msg_data.UCI_INDICATION.uci_handle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_rnti ,
        { "uci_rnti ", "fapi_sdk.msg_data.UCI_INDICATION.uci_rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_uePucchIndex,
        { "uci_uePucchIndex", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.uci_uePucchIndex",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_numUciPayload,
        { "uci_numUciPayload", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.uci_numUciPayload",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uci_uciPayloadCrcStatus,
        { "uci_uciPayloadCrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.uci_uciPayloadCrcStatus",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uciPayloadLength ,
        { "uciPayloadLength ", "fapi_sdk.msg_data.UCI_INDICATION.uciPayloadLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &uciBits,
        { "uciBits", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.uciBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &ulSnr,
        { "ulSnr", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.ulSnr",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &timingAdvance ,
        { "timingAdvance ", "fapi_sdk.msg_data.UCI_INDICATION.timingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pusch_UCI_tag ,
        { "pusch_UCI_tag ", "fapi_sdk.msg_data.UCI_INDICATION.pusch_UCI_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pusch_UCI_length ,
        { "pusch_UCI_length ", "fapi_sdk.msg_data.UCI_INDICATION.pusch_UCI_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_pduBitmap ,
        { "pusch_pduBitmap ", "fapi_sdk.msg_data.UCI_INDICATION.pucch_pduBitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_Handle,
        { "pusch_Handle", "fapi_sdk.msg_data.RX_DATA.pucch_Handle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_RNTI ,
        { "pusch_RNTI ", "fapi_sdk.msg_data.UCI_INDICATION.pucch_RNTI",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch1_harqAckBitLength ,
        { "harqAckBitLength ", "fapi_sdk.msg_data.UCI_INDICATION.pucch1_harqAckBitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_harqAckCrcStatus,
        { "pusch_harqAckCrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_harqAckCrcStatus",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_harqAckBits,
        { "pusch_harqAckBits", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_harqAckBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart1BitLength ,
        { "pusch_csiPart1BitLength ", "fapi_sdk.msg_data.UCI_INDICATION.pucch_csiPart1BitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart1CrcStatus,
        { "pusch_csiPart1CrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_csiPart1CrcStatus",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart1Bits,
        { "pusch_csiPart1Bits", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_csiPart1Bits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart2BitLength ,
        { "pusch_csiPart2BitLength ", "fapi_sdk.msg_data.UCI_INDICATION.pucch_csiPart2BitLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart2CrcStatus,
        { "pusch_csiPart2CrcStatus", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_csiPart2CrcStatus",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pucch_csiPart2Bits,
        { "pusch_csiPart2Bits", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pucch_csiPart2Bits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pusch_ulSnr,
        { "pusch_ulSnr", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pusch_ulSnr",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pusch_timingAdvance,
        { "pusch_timingAdvance", "fapi_sdk.msg_data.RX_DATA_ULSCH_DECODE.pusch_timingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
/*{ &rxdhandle,
        { "rxdhandle", "fapi_sdk.msg_data.RX_DATA.rxdhandle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxdrnti ,
        { "rxdrnti ", "fapi_sdk.msg_data.msg_data.RX_DATA.rxdrnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxdharqId ,
        { "rxdharqId ", "fapi_sdk.msg_data.msg_data.RX_DATA.rxdharqId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxdpayloadSize,
        { "rxdpayloadSize", "fapi_sdk.msg_data.RX_DATA.rxdpayloadSize",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxdpayload,
        { "rxdpayload", "fapi_sdk.msg_data.RX_DATA.rxdpayload",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        */
        /* FAPI DATA (FAPI_E_ERROR_IND) Message */
        { &hf_fapi_sdk_msg_ERROR_IND_msg_id,
        { "Message ID", "fapi_sdk.msg_data_ERROR_IND.message_id",
        FT_UINT8, BASE_HEX, VALS(fapi_msg_header_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_error_code,
        { "Error Code", "fapi_sdk.msg_data_ERROR_IND.error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_error_codes_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.sub_error_code",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_dci_dl_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.dci_dl_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_dci_dl_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_bch_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.bch_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_bch_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_mch_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.mch_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_mch_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_dlsch_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.dlsch_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_dlsch_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_pch_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.pch_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_pch_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.ulsch_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_ulsch_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_cqi_ri_information_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND._cqi_ri_information_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_cqi_ri_information_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_harq_information_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.harq_information_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_harq_information_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_initial_transmission_information_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.initial_transmission_information_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_initial_transmission_information_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_cqi_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_cqi_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_cqi_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_sr_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_sr_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_sr_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_harq_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_harq_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_harq_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_sr_harq_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_sr_harq_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_sr_harq_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_cqi_harq_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_cqi_harq_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_cqi_harq_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_cqi_sr_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_cqi_sr_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_harq_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.uci_cqi_sr_harq_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_uci_cqi_sr_harq_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_srs_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.srs_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_srs_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_harq_buffer_pdu_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.harq_buffer_pdu_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_harq_buffer_pdu_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_tx_request_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.tx_request_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_tx_request_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &location ,
        { "location ", "fapi_sdk.msg_data.TX_CONTROL.location",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dmrsPorts ,
        { "dmrsPorts ", "fapi_sdk.msg_data.TX_CONTROL.location",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nId ,
        { "nId ", "fapi_sdk.msg_data.TX_CONTROL.location",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &config_resp_msg_ok,
        { "ERROR CODE:", "fapi_sdk.msg_data_ERROR_IND.config_resp_msg_ok",
        FT_UINT16, BASE_DEC, VALS(msg_error_codes_string), 0x0,
        NULL, HFILL } },
        { &pnfrspErrorCode,
        { "pnfrspErrorCode", "hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &config_resp_msg_invalid,
        { "config_resp_msg_invalid", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.dl_config_sub_error_code",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &config_resp_msg_invalid_config,
        { "config_resp_msg_invalid_config", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.dl_config_sub_error_code",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_Error_Code,
        { "phy_Error_Code", "hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_Num_TLVs,
        { "phy_Num_TLVs", "fapi_sdk.msg_data.PHY_CAP_RESP.phy_Num_TLVs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_tag,
        { "phy_tag", "fapi_sdk.msg_data.PHY_CAP_RESP.phy_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_Length,
        { "phy_Length", "fapi_sdk.msg_data.PHY_CAP_RESP.phy_Length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &timingInfoMode,
        { "timingInfoMode", "fapi_sdk.msg_data.PHY_CAP_RESP.timingInfoMode",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &timingInfoPeriod,
        { "timingInfoPeriod", "fapi_sdk.msg_data.PHY_CAP_RESP.timingInfoPeriod",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &phy_error_Msg_ID,
        { "phy_error_Msg_ID", "hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_tag,
        { "Tag", "fapi_sdk.msg_data_RX_CONTROL.HARQ_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_length,
        { "Length", "fapi_sdk.msg_data_RX_CONTROL.HARQ_Length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_gpio,
        { "Gpio", "fapi_sdk.msg_data_RX_CONTROL.HARQ_gpio",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_enable,
        { "enable", "fapi_sdk.msg_data.RX_CONTROL.HARQ_enable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_timingAdvance,
        { "TimingAdvance", "fapi_sdk.msg_data_RX_CONTROL.HARQ_timingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_baudrate,
        { "baudrate", "fapi_sdk.msg_data_RX_CONTROL.HARQ_baudrate",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_sfn,
        { "Sfn", "fapi_sdk.msg_data_RX_CONTROL.HARQ_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_slot,
        { "Slot", "fapi_sdk.msg_data_RX_CONTROL.HARQ_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_numerology,
        { "Numerology", "fapi_sdk.msg_data_RX_CONTROL.HARQ_numerology",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_numDataBits,
        { "numDataBits", "fapi_sdk.msg_data.RX_CONTROL.HARQ_numDataBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &HARQ_data,
        { "data", "fapi_sdk.msg_data_RX_CONTROL.HARQ_data",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Error_Code_dependent_values,
        { "Error_Code_dependent_values", "hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &SFN,
        { "SFN", "fapi_sdk.msg_data.TX_DATA.SFN",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxSFN,
        { "rxSFN", "fapi_sdk.msg_data.RX_DATA.SFN",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Slot,
        { "Slot", "fapi_sdk.msg_data.TX_DATA.Slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &isReTx,
        { "isReTx", "fapi_sdk.msg_data.RX_CONTROL.isReTx",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &numCb ,
        { "numCb ", "fapi_sdk.msg_data.TX_CONTROL.numCb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cbPresentAndPosition,
        { "cbPresentAndPosition", "fapi_sdk.msg_data.RX_CONTROL.cbPresentAndPosition",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxSlot,
        { "rxSlot", "fapi_sdk.msg_data.RX_DATA.rxSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Numerology,
        { "Numerology", "fapi_sdk.msg_data.TX_DATA.Numerology",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxNumerology,
        { "rxNumerology", "fapi_sdk.msg_data.RX_DATA.rxNumerology",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &PDUcount,
        { "PDUcount", "fapi_sdk.msg_data.TX_DATA.PDUcount",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxPDUcount,
        { "rxPDUcount", "fapi_sdk.msg_data.RX_DATA.rxPDUcount",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDataPduIndex,
        { "dlDataPduIndex", "fapi_sdk.msg_data.TX_DATA.dlDataPduIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxpduType,
        { "rxpduType", "fapi_sdk.msg_data.RX_DATA.rxpduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &puschrvIndex,
        { "puschrvIndex", "fapi_sdk.msg_data.RX_CONTROL.puschrvIndex",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &tbSize,
        { "tbSize", "fapi_sdk.msg_data.RX_CONTROL.tbSize",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &harqNum,
        { "harqNum", "fapi_sdk.msg_data.RX_CONTROL.harqNum",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDataPduLength,
        { "dlDataPduLength", "fapi_sdk.msg_data.TX_DATA.dlDataPduLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxpduLength,
        { "rxpduLength", "fapi_sdk.msg_data.RX_DATA.rxpduLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlDataPduBytes,
        { "dlDataPduBytes", "fapi_sdk.msg_data.TX_DATAL.dlDataPduBytes",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &Num_PDU_TLVs,
        { "Num_PDU_TLVs", "fapi_sdk.msg_data.TX_CONTROL.Num_PDU_TLVs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxpdu,
        { "rxpdu", "fapi_sdk.msg_data.RX_DATA.rxpdu",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxharqId,
        { "rxharqId", "fapi_sdk.msg_data.RX_DATA.rxharqId",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxtag,
        { "rxtag", "fapi_sdk.msg_data.RX_DATA.rxtag",
        FT_UINT16, BASE_HEX_DEC, VALS(FAPI_E_RX_TAG), 0x0,
        NULL, HFILL } },
        { &rxrnti,
        { "rxrnti", "fapi_sdk.msg_data.RX_DATA.rxrnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxlength,
        { "rxlength", "fapi_sdk.msg_data.RX_DATA.rxlength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxhandle,
        { "rxhandle", "fapi_sdk.msg_data.RX_DATA.rxhandle",
        FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxpayloadSize,
        { "rxpayloadSize", "fapi_sdk.msg_data.RX_DATA.rxpayloadSize",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rxpayload,
        { "rxpayload", "fapi_sdk.msg_data.RX_DATA.rxpayload",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pduType,
        { "pduType", "fapi_sdk.msg_data.TX_CONTROL.pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rvIndex,
        { "rvIndex", "fapi_sdk.msg_data.TX_CONTROL.rvIndex",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pduLength,
        { "pduLength", "fapi_sdk.msg_data.TX_CONTROL.pduLength",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &frequencyDomainResources,
        { "frequencyDomainResources", "fapi_sdk.msg_data.TX_CONTROL.frequencyDomainResources",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &startSymbol,
        { "startSymbol", "fapi_sdk.msg_data.TX_CONTROL.startSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &duration ,
        { "Duration", "fapi_sdk.msg_data.TX_CONTROL.duration",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &regBundleSize,
        { "regBundleSize", "fapi_sdk.msg_data.TX_CONTROL.regBundleSize",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cceRegMappingType,
        { "cceRegMappingType", "fapi_sdk.msg_data.TX_CONTROL.cceRegMappingType",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &interleaverSize,
        { "interleaverSize", "fapi_sdk.msg_data.TX_CONTROL.interleaverSize",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nId1,
        { "nId", "fapi_sdk.msg_data.TX_CONTROL.nId1",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &shiftIndex,
        { "shiftIndex", "fapi_sdk.msg_data.TX_CONTROL.shiftIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &precoderGranularity,
        { "precoderGranularity", "fapi_sdk.msg_data.TX_CONTROL.precoderGranularity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dmrsSubcarrier0Reference,
        { "dmrsSubcarrier0Reference", "fapi_sdk.msg_data.TX_CONTROL.dmrsSubcarrier0Reference",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nPdcch,
        { "nPdcch", "fapi_sdk.msg_data.TX_CONTROL.nPdcch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &rnti,
        { "rnti", "fapi_sdk.msg_data.TX_CONTROL.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &targetCodeRate ,
        { "targetCodeRate ", "fapi_sdk.msg_data.TX_CONTROL.targetCodeRate",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &qamModOrder,
        { "qamModOrder", "fapi_sdk.msg_data.TX_CONTROL.qamModOrder",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nRnti,
        { "nRnti", "fapi_sdk.msg_data.TX_CONTROL.nRnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cceIndex,
        { "cceIndex", "fapi_sdk.msg_data.TX_CONTROL.cceIndex",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &aggegrationLevel,
        { "aggegrationLevel", "fapi_sdk.msg_data.TX_CONTROL.aggegrationLevel",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &payloadSizeInBits,
        { "payloadSizeInBits", "fapi_sdk.msg_data.TX_CONTROL.payloadSizeInBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &payload,
        { "payload", "fapi_sdk.msg_data.TX_CONTROL.payload",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPdcch,
        { "betaPdcch", "fapi_sdk.msg_data.TX_CONTROL.betaPdcch",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &cycleLength,
        { "precoderCycleLength", "fapi_sdk.msg_data.TX_CONTROL.cycleLength",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &nCsiRsPorts ,
        { "nCsiRsPorts ", "fapi_sdk.msg_data.TX_CONTROL.nCsiRsPorts",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &precoderIndices,
        { "precoderIndices", "fapi_sdk.msg_data.TX_CONTROL.precoderIndices",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /*{ &digBfIndices,
        { "digBfIndices", "hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        */
        { &dlDataPduIndex,
        { "dlDataPduIndex", "fapi_sdk.msg_data.TX_CONTROL.dlDataPduIndex",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pduBitmap ,
        { "pduBitmap ", "fapi_sdk.msg_data.TX_CONTROL.pduBitmap",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pduIndex ,
        { "pduIndex ", "fapi_sdk.msg_data.TX_CONTROL.pduIndex",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /*{ &tbSizeBytes,
        { "tbSizeBytes", "fapi_sdk.vsm.cfg_rep.fapi_val",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        */
        { &ssbSubCarrierOffset,
        { "ssbSubCarrierOffset", "fapi_sdk.msg_data.TX_CONTROL.ssbSubCarrierOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //OPTIONAL PARAMS
        { &startSymbol,
        { "startSymbol", "fapi_sdk.msg_data.TX_CONTROL.startSymbol",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsTimeDensity,
        { "dlPtrsTimeDensity", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsTimeDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsFreqDensity,
        { "dlPtrsFreqDensity", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsFreqDensity",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &dlPtrsReOffset,
        { "dlPtrsReOffset", "fapi_sdk.msg_data.TX_CONTROL.dlPtrsReOffset",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &betaPtrs,
        { "betaPtrs", "fapi_sdk.msg_data.TX_CONTROL.betaPtrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_dl_config_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.dl_config_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_GLOB_PARAM_ERR_dl_config_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_ul_config_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.ul_config_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_GLOB_PARAM_ERR_ul_config_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_tx_request_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.GLOB_PARAM_ERR.tx_request_sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_GLOB_PARAM_ERR_tx_request_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_sub_error_code,
        { "Sub Error Code", "fapi_sdk.msg_data_ERROR_IND.N_PDU_LIMIT.sub_error_code",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_N_PDU_LIMIT_sub_error_code_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_received_val,
        { "Received Value", "hf_fapi_sdk_msg_ERROR_IND.N_PDU_LIMIT.received_val",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_limit,
        { "PHY Limitation", "hf_fapi_sdk_msg_ERROR_IND.N_PDU_LIMIT.limit",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_rnti_type,
        { "RNTI Type", "fapi_sdk.msg_data_ERROR_IND.N_PDU_LIMIT.rnti_type",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_N_PDU_LIMIT_rnti_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf,
        { "Received SFN/SF", "hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf,
        { "Expected SFN/SF", "hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_direction,
        { "Direction", "hf_fapi_sdk_msg_ERROR_IND_direction",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_msg_pdu_error_direction_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_rnti,
        { "RNTI", "hf_fapi_sdk_msg_ERROR_IND_rnti",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_dl_pdu_type,
        { "PDU Type", "hf_fapi_sdk_msg_ERROR_IND_dl_pdu_type",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_dl_pdu_type_release10_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_ul_pdu_type,
        { "PDU Type", "hf_fapi_sdk_msg_ERROR_IND_ul_pdu_type",
        FT_UINT8, BASE_DEC, VALS(fapi_msg_data_ul_pdu_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_phich_lowest_ul_rb_index,
        { "PHICH Lowest UL RB Index", "hf_fapi_sdk_msg_ERROR_IND_phich_lowest_ul_rb_index",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_ERROR_IND_pdu_index,
        { "PDU Index", "hf_fapi_sdk_msg_ERROR_IND_pdu_index",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM Header */
        { &hf_fapi_sdk_vsm_header_num_tlvs,
        { "Num TLVs", "fapi_sdk.vsm.header.n_tlvs",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_header_size,
        { "Size", "fapi_sdk.vsm.header.size",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM TLV */
        { &hf_fapi_sdk_vsm_tlv_tag,
        { "TAG", "fapi_sdk.vsm.tlv.tag",
        FT_UINT16, BASE_HEX, VALS(&fapi_vsm_TLV_tag_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_tlv_length,
        { "Length", "fapi_sdk.vsm.tlv.length",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM UL Indications Common Fields */
        { &hf_fapi_sdk_vsm_UL_IND_COMMON_rnti,
        { "RNTI", "fapi_sdk.vsm.UL_IND.rnti",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_UL_IND_COMMON_rssi,
        { "RSSI [dBm]", "fapi_sdk.vsm.UL_IND.rssi",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_UL_IND_COMMON_snr,
        { "SNR [dB]", "fapi_sdk.vsm.UL_IND.snr",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_padding1,
        { "Padding", "fapi_sdk.vsm.tlv.padding",
        FT_UINT8, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_padding2,
        { "Padding", "fapi_sdk.vsm.tlv.padding",
        FT_UINT16, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_padding3,
        { "Padding", "fapi_sdk.vsm.tlv.padding",
        FT_UINT24, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI vend. specific msg data (FAPI_E_VSM_CFG_REPORT) */
        { &hf_fapi_sdk_msg_CFG_PARAMS_REP_n_cat,
        { "Num. of Categories", "fapi_sdk.vsm.cfg_rep.n_cat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CFG_PARAMS_REP_cat,
        { "Category", "fapi_sdk.vsm.cfg_rep.cat",
        FT_UINT16, BASE_DEC, VALS(&fapi_vsm_cfg_rep_cat_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag,
        { "TAG", "fapi_sdk.vsm.cfg_rep.fapi_tag",
        FT_UINT16, BASE_DEC, VALS(&fapi_msg_data_config_tlv_tag_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag_rel10,
        { "TAG", "fapi_sdk.vsm.cfg_rep.fapi_tag_r10",
        FT_UINT16, BASE_DEC, VALS(&fapi_msg_data_config_tlv_tag_release10_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_value,
        { "Value", "fapi_sdk.vsm.cfg_rep.fapi_val",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM MEAS Ind FAPI vend. specific msg data (FAPI_E_RX_VSM_MEAS_IND) */
        { &hf_fapi_sdk_vsm_MEAS_IND_avg_ni,
        { "Average NI [dBm]", "fapi_sdk.vsm.meas.avg_ni",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant0_rssi,
        { "Ant0 RSSI [dBm]", "fapi_sdk.vsm.meas.ant0_rssi",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant1_rssi,
        { "Ant1 RSSI [dBm]", "fapi_sdk.vsm.meas.ant1_rssi",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant2_rssi,
        { "Ant2 RSSI [dBm]", "fapi_sdk.vsm.meas.ant2_rssi",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant3_rssi,
        { "Ant3 RSSI [dBm]", "fapi_sdk.vsm.meas.ant3_rssi",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant0_gain,
        { "Ant0 gain", "fapi_sdk.vsm.meas.ant0_gain",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant1_gain,
        { "Ant1 gain", "fapi_sdk.vsm.meas.ant1_gain",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant2_gain,
        { "Ant2 gain", "fapi_sdk.vsm.meas.ant2_gain",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_MEAS_IND_ant3_gain,
        { "Ant3 gain", "fapi_sdk.vsm.meas.ant3_gain",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM NI Ind FAPI vend. specific msg data (FAPI_E_RX_VSM_NI_IND) */
        { &hf_fapi_sdk_vsm_NI_IND_handle,
        { "Handle", "fapi_sdk.vsm.ni.handle",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_NI_IND_n_rb,
        { "RB Num.", "fapi_sdk.vsm.ni.n_rb",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_NI_IND_ni_per_rb,
        { "NI per RB", "fapi_sdk.vsm.ni.ni_per_rb",
        FT_FLOAT, BASE_NONE, NULL, 0x0,
        NULL, HFILL } },
        /* VSM - HARQ Indication (TDD) */
        { &hf_fapi_sdk_vsm_harq_pusch_tdd_ind_bundling,
        { "Bundling", "fapi_sdk.vsm.harq_pusch_tdd.bundling",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM - RACH Indication */
        { &hf_fapi_sdk_vsm_rach_ind_preamble_id,
        { "Preamble ID", "fapi_sdk.vsm.rach.preamble_id",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_rach_ind_detection_metric,
        { "Detection Metric [dB]", "fapi_sdk.vsm.rach.detection_metric",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM - Configuration : CFI*/
        { &hf_fapi_sdk_vsm_cfg_cfi,
        { "CFI", "fapi_sdk.vsm.cfg_cfi",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* VSM - Configuration : Version*/
        { &hf_fapi_sdk_vsm_cfg_version_major,
        { "VS Version Major", "fapi_sdk.vsm.cfg_version_major",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_cfg_version_minor,
        { "VS Version Minor", "fapi_sdk.vsm.cfg_version_minor",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_cfg_etm_enable,
        { "Test Mode Enabled", "fapi_sdk.vsm.cfg_etm_enable",
        FT_BOOLEAN, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_cfg_etm_all_zeros,
        { "Disable All Zeros", "fapi_sdk.vsm.cfg_etm_all_zeros",
        FT_BOOLEAN, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_cfg_etm_mi_override,
        { "Disable PHICH MI Override", "fapi_sdk.vsm.cfg_etm_mi_override",
        FT_BOOLEAN, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_vsm_param_rsp_max_users,
        { "Max Users per TTI", "fapi_sdk.vsm.param_rsp_max_users",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI vend. specific msg data (FAPI_E_VSM_INFRA_REQ) */
        { &hf_fapi_sdk_msg_INFRA_PARAM_REQ_length,
        { "Length", "fapi_sdk.msg_data_INFRA_REQ.length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_PARAM_REQ_n_pdu,
        { "No of PDUs", "fapi_sdk.msg_data_INFRA_REQ.n_pdu",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_type,
        { "PDU Type", "fapi_sdk.msg_data_INFRA_REQ.pdu_type",
        FT_UINT8, BASE_DEC, VALS(&fapi_msg_data_infra_pdu_type_string), 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_size,
        { "PDU Size", "fapi_sdk.msg_data_INFRA_REQ.pdu_size",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //DPD pdu
        { &hf_fapi_sdk_msg_INFRA_DPD_PDU_valid_bitmap,
        { "Valid Bitmap", "fapi_sdk.msg_data_INFRA_REQ.valid_bitmap",
        FT_UINT32, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        /* FAPI vend. specific msg data (FAPI_E_VSM_INFRA_REQ) TRIGGER_PDU */
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf_sfn,
        { "Target SFN/SF", "fapi_sdk.msg_data_INFRA_REQ.target_sf_sfn",
        FT_UINT16, BASE_HEX, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sfn,
        { "Target Frame", "fapi_sdk.msg_data_INFRA_REQ.target_nf",
        FT_UINT16, BASE_DEC, NULL, SFN_MASK,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf,
        { "Target Sub-frame", "fapi_sdk.msg_data_INFRA_REQ.target_nsf",
        FT_UINT16, BASE_DEC, NULL, SF_MASK,
        NULL, HFILL } },
        //end target SF/SFN
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_io_index,
        { "IO Index", "fapi_sdk.msg_data_INFRA_REQ.io_index",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_pattern,
        { "Pattern", "fapi_sdk.msg_data_INFRA_REQ.pattern",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_bit_interval,
        { "Bit Interval", "fapi_sdk.msg_data_INFRA_REQ.bit_interval",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_start_time_off,
        { "Start Time Offset", "fapi_sdk.msg_data_INFRA_REQ.start_time_off",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        //DPD pdu
        { &hf_fapi_sdk_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU_threshold,
        { "Threshold", "fapi_sdk.msg_data_INFRA_REQ.threshold",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &pi2Bpsk,
        { "pi2Bpsk", "fapi_sdk.msg_data.RX_CONTROL.pi2Bpsk",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &additionalDmrs,
        { "additionalDmrs", "fapi_sdk.msg_data.RX_CONTROL.additionalDmrs",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_pduType,
        { "PDU TYPE", "fapi_sdk.msg_data_INFRA_CMD.infra_pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_pduLength,
        { "PDU TYPE", "fapi_sdk.msg_data_INFRA_CMD.infra_pduType",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_sfn,
        { "SFN", "fapi_sdk.msg_data_INFRA_CMD.infra_sfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        /*
        { &infra_slot,
        { "Slot", "fapi_sdk.msg_data_INFRA_CMD.infra_slot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numerology,
        { "Numerology", "fapi_sdk.msg_data_INFRA_CMD.infra_numerology",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numPdu,
        { "Num PDU", "fapi_sdk.msg_data_INFRA_CMD.infra_numPdu",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_tag,
        { "Tag", "fapi_sdk.msg_data_INFRA_CMD.infra_tag",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_length,
        { "Length", "fapi_sdk.msg_data_INFRA_CMD.infra_length",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_gpio,
        { "Gpio", "fapi_sdk.msg_data_INFRA_CMD.infra_gpio",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_enable,
        { "enable", "fapi_sdk.msg_data_INFRA_CMD.enable",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_targetSfn,
        { "targetSfn", "fapi_sdk.msg_data_INFRA_CMD.infra_targetSfn",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_targetSlot,
        { "targetSlot", "fapi_sdk.msg_data_INFRA_CMD.infra_targetSlot",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_timingAdvance,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_repeat,
        { "repeat", "fapi_sdk.msg_data_INFRA_CMD.repeat",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_tag2,
        { "tag", "fapi_sdk.msg_data_INFRA_CMD.infra_tag2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_length2,
        { "Length", "fapi_sdk.msg_data_INFRA_CMD.infra_length2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_gpio2,
        { "Gpio", "fapi_sdk.msg_data_INFRA_CMD.infra_gpio2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_enable2,
        { "enable", "fapi_sdk.msg_data_INFRA_CMD.enable2",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_timingAdvance2,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_baudrate,
        { "baudrate", "fapi_sdk.msg_data_INFRA_CMD.infra_baudrate",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_timingAdvance2,
        { "timingAdvance", "fapi_sdk.msg_data_INFRA_CMD.infra_timingAdvance2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_sfn2,
        { "Sfn", "fapi_sdk.msg_data_INFRA_CMD.infra_Sfn2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_slot2,
        { "Slot", "fapi_sdk.msg_data_INFRA_CMD.infra_Slot2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numerology2,
        { "numerology", "fapi_sdk.msg_data_INFRA_CMD.infra_numerology2",
        FT_UINT16, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_numDataBits,
        { "numDataBits", "fapi_sdk.msg_data_INFRA_CMD.infra_numDataBits",
        FT_UINT8, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        { &infra_data,
        { "data", "fapi_sdk.msg_data_INFRA_CMD.infra_data",
        FT_UINT32, BASE_DEC, NULL, 0x0,
        NULL, HFILL } },
        */
    };
    static gint* ett[] = {
    &ett_fapi_sdk_msg_header,
    &ett_fapi_sdk_msg_data,
    &ett_fapi_sdk_msg_data_subtree1,
    &ett_fapi_sdk_msg_data_subtree2,
    &ett_fapi_sdk_msg_data_subtree3,
    &ett_fapi_sdk_msg_data_subtree4,
    &ett_msg_fragment,
    &ett_msg_fragments
    };
    module_t* fapi_sdk_module;
    proto_fapi_sdk = proto_register_protocol("FAPI SDK Protocol", "* FAPI SDK *", "fapi_sdk");
    proto_register_field_array(proto_fapi_sdk, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
    register_dissector("fapi_sdk", dissect_fapi_sdk, proto_fapi_sdk);
    fapi_sdk_module = prefs_register_protocol(proto_fapi_sdk, NULL);
    prefs_register_bool_preference(fapi_sdk_module, "dissect_mac_dl",
        "Dissect DL MAC Layers from Data Payload",
        "In Downlink data packets, dissect MAC header layer "
        "Disabling MAC dissection will disable RLC dissection automatically",
        &global_fapi_sdk_dissect_MAC_DL);
    prefs_register_bool_preference(fapi_sdk_module, "dissect_mac_ul",
        "Dissect UL MAC Layers from Data Payload",
        "In Uplink data packets, dissect MAC header layer "
        "Disabling MAC dissection will disable RLC dissection automatically",
        &global_fapi_sdk_dissect_MAC_UL);
    prefs_register_bool_preference(fapi_sdk_module, "big_endian",
        "Use Big Endian",
        "Choose between dissecting in Big Endian or Little Endian ",
        &global_fapi_sdk_BIG_ENDIAN);
    prefs_register_enum_preference(fapi_sdk_module, "parse_as_api",
        "Dissect as API Version",
        "Choose whether to dissect as API 1.7 ",
        &global_fapi_sdk_parse_as_api,
        global_fapi_sdk_parse_as_api_enum, FALSE);
    prefs_register_enum_preference(fapi_sdk_module, "vendor_specifiv_api_version",
        "Vendor Specific API Version",
        "Choose vendor specific fields dissection format",
        &global_fapi_sdk_parse_vsm_api_ver,
        global_fapi_sdk_parse_vsm_api_ver_enum, FALSE);
    prefs_register_enum_preference(fapi_sdk_module, "lte_release_number",
        "LTE Release Number",
        "Choose LTE Release Number (supports either 8 or 10)",
        &global_fapi_sdk_parse_lte_release_ver,
        global_fapi_sdk_parse_lte_release_ver_enum, FALSE);
    prefs_register_bool_preference(fapi_sdk_module, "parse_rel10_dci",
        "Dissect Release 10 New UL and DL DCI Fields",
        "Choose whether or not to dissect 3 new fields in release 10: DL_DCI:associated UL, Size of CSI req field; UL_DCI: trans_mode",
        &global_fapi_sdk_dissect_rel10_dci_fields);
    prefs_register_enum_preference(fapi_sdk_module, "duplex_mode",
        "Select Duplex Mode",
        "What duplex mode are you working with?",
        &global_fapi_sdk_duplex_mode_val,
        global_fapi_sdk_duplex_mode_enum, FALSE);
}
static guint16
sci_tvb_get_ntohs(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntohs(tvb, offset);
    else
        return tvb_get_letohs(tvb, offset);
}
static guint32
sci_tvb_get_ntoh24(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntoh24(tvb, offset);
    else
        return tvb_get_letoh24(tvb, offset);
}
static guint32
sci_tvb_get_ntohl(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntohl(tvb, offset);
    else
        return tvb_get_letohl(tvb, offset);
}
static guint64
sci_tvb_get_ntoh40(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntoh40(tvb, offset);
    else
        return tvb_get_letoh40(tvb, offset);
}
static guint64
sci_tvb_get_ntoh48(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntoh48(tvb, offset);
    else
        return tvb_get_letoh48(tvb, offset);
}
static guint64
sci_tvb_get_ntoh56(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntoh56(tvb, offset);
    else
        return tvb_get_letoh56(tvb, offset);
}
static guint64
sci_tvb_get_ntoh64(tvbuff_t* tvb, const gint offset)
{
    if (global_fapi_sdk_BIG_ENDIAN)
        return tvb_get_ntoh64(tvb, offset);
    else
        return tvb_get_letoh64(tvb, offset);
}
static void
dissect_fapi_SFN_SF(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* sfn_sf_pi;
    proto_tree* fapi_sdk_msg_hdr_sfn_sf = NULL;
    guint32 val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    sfn_sf_pi = proto_tree_add_uint(tree, hf_fapi_sdk_msg_header_sf_sfn, tvb, *plen, 2, val);
    fapi_sdk_msg_hdr_sfn_sf = proto_item_add_subtree(sfn_sf_pi, ett_fapi_sdk_msg_data_subtree1);
    proto_tree_add_item(fapi_sdk_msg_hdr_sfn_sf, hf_fapi_sdk_msg_header_sfn, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
    proto_tree_add_item(fapi_sdk_msg_hdr_sfn_sf, hf_fapi_sdk_msg_header_sf, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
    *plen += 2;
} // end of dissect_fapi_SFN_SF
static void
dissect_fapi_msg_TTI_EVT(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_tti_msg = NULL;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_SF_IND");
    fapi_sdk_tti_msg = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    /* TTI packet */
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_tti_msg, plen);
} // end of dissect_fapi_msg_TTI_EVT
static void
dissect_fapi_msg_DL_CONFIG_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 val;
    guint32 n_pdu;
    guint32 pdu_type;
    guint32 pdu_size;
    double off_val;
    double boost_start_val = (-6);
    double boost_del = (0.001);
    guint32 rnti;
    dl_pdu_lengths_idx = 0;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_DL_CONFIG_REQ");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_length, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdcch, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_n_dci, tvb, *plen, 1, val);
    *plen += 1;
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdu, tvb, *plen, 2, n_pdu);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_n_pdsch, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, boost_start_val, boost_del);
    proto_tree_add_double(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_pcfich_boost, tvb, *plen, 2, off_val);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2, // check if pdu_size can be applied
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        pdu_type = tvb_get_guint8(tvb, *plen);
        if (global_fapi_sdk_parse_lte_release_ver == 8)
        {
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type, tvb, *plen, 1, pdu_type);
            if (pdu_type == rel8ETM_DLSCH_PDU)
            {
                pdu_type = ETM_DLSCH_PDU;
            }
        }
        else
        {
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_type_rel10, tvb, *plen, 1, pdu_type);
        }
        *plen += 1;
        pdu_size = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_DL_CONFIG_REQ_pdu_size, tvb, *plen, 1, pdu_size);
        *plen += 1;
        switch (pdu_type)
        {
        case DCI_DL_PDU:
            rnti = dissect_fapi_msg_DL_DCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            proto_item_append_text(ti, ": (DL DCI, RNTI = %u)", rnti);
            break;
        case BCH_PDU:
            dissect_fapi_msg_DL_BCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            proto_item_append_text(ti, ": (BCH)");
            break;
        case PRS_PDU:
        case CSI_RS_PDU:
        case MCH_PDU:
            proto_item_append_text(ti, " PDU type is not supported");
            *plen += pdu_size - 2;
            break;
        case ETM_DLSCH_PDU:
            rnti = dissect_fapi_msg_DL_DLSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen, ETM_DLSCH_PDU);
            proto_item_append_text(ti, ": (ETM_DLSCH, RNTI = %u)", rnti);
            break;
        case DLSCH_PDU:
            rnti = dissect_fapi_msg_DL_DLSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen, DLSCH_PDU);
            proto_item_append_text(ti, ": (DLSCH, RNTI = %u)", rnti);
            break;
        case PCH_PDU:
            rnti = dissect_fapi_msg_DL_PCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            proto_item_append_text(ti, ": (PCH, RNTI = %u)", rnti);
            break;
            default:
            expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                "DL PDU %u: Illegal PDU Type (%u)",
            i, pdu_type);
            *plen += pdu_size - 2;
        }// end of switch-case (pdu_type)
    }// end of for each PDU
}
static guint32
dissect_fapi_msg_DL_DLSCH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen, FAPI_E_DL_PDU_TYPE DLSCH_type)
{
    proto_item* ti;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_pdu_data = NULL;
    proto_tree* fapi_sdk_msg_pdu_bfv = NULL;
    guint32 i, j;
    guint32 val;
    guint32 rnti;
    guint32 n_sb;
    guint32 n_bfv;
    guint32 n_ant;
    guint32 sb_idx;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_size, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].length = val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_idx, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].pdu_idx = val;
    rnti = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DLSCH_PDU_rnti, tvb, *plen, 2, rnti);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].rnti = rnti;
    dl_pdu_lengths_idx++;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_vir_rb_type, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    if (DLSCH_type == ETM_DLSCH_PDU)
    {
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap, tvb, *plen, 4, val, "RB Bitmap 0: 0x%.8x", val);
        *plen += 4;
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap, tvb, *plen, 4, val, "RB Bitmap 1: 0x%.8x", val);
        *plen += 4;
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap, tvb, *plen, 4, val, "RB Bitmap 2: 0x%.8x", val);
        *plen += 4;
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rb_bitmap, tvb, *plen, 4, val, "RB Bitmap 3: 0x%.8x", val);
        *plen += 4;
    }
    else // DLSCH_PDU: Not Test Mode PDU
    {
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding, tvb, *plen, 4, val);
        *plen += 4;
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_mcs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rv_idx, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_codeword_id, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_swap_flag, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_ant_mode, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_n_layers, tvb, *plen, 1, val);
    *plen += 1;
    n_sb = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_subbands, tvb, *plen, 1, n_sb);
    *plen += 1;
    /* For each Subband */
    for (i = 0; i < n_sb; i++)
    {
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 1,
        "Subband[%d]", i);
        fapi_sdk_msg_pdu_data = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_data, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx, tvb, *plen, 1, val);
        *plen += 1;
    } // end of for each subband
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_category, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_pa, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_pdsch_boost_index, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_n_gap, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb, tvb, *plen, 1, (val + 2));
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //Transmission mode
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_ue_trans_mode, tvb, *plen, 1, val);
        *plen += 1;
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb_per_subband, tvb, *plen, 1, val);
    *plen += 1;
    n_bfv = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_bf_vectors, tvb, *plen, 1, n_bfv);
    *plen += 1;
    /* For each Beam Forming Vector */
    for (i = 0; i < n_bfv; i++)
    {
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 4,
        "Beam Forming Vector[%d]", i);
        fapi_sdk_msg_pdu_data = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
        sb_idx = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_data, hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_subband_idx, tvb, *plen, 1, sb_idx);
        *plen += 1;
        n_ant = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_data, hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_n_ant, tvb, *plen, 1, n_ant);
        *plen += 1;
        /* For each Antenna */
        for (j = 0; j < n_ant; j++)
        {
            ei = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_data, proto_fapi_sdk, tvb, *plen, 2,
            "Antenna[%d]", j);
            fapi_sdk_msg_pdu_bfv = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree2);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_bfv, hf_fapi_sdk_msg_DL_PDSCH_PDU_bf_vector, tvb, *plen, 2, val);
            *plen += 2;
        } // end of for-each antenna
    } // end of for each bf-vector
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //N_SCID
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_scid, tvb, *plen, 1, val);
        *plen += 1;
        //CSI-RS flag
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //CSI-RS resource config
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_cdi_rs_resrc_config, tvb, *plen, 1, val);
        *plen += 1;
        //CSI-RS zero Tx power resource config bitmap
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_csi_rs_tx_pwr_resrc_bitmap, tvb, *plen, 2, val);
        *plen += 2;
    }
    return rnti;
} // end of dissect_fapi_msg_DL_DLSCH_PDU
static void
dissect_fapi_msg_DL_BCH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double boost_start_val = (-6);
    double boost_del = (0.001);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_size, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].length = val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_idx, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].pdu_idx = val;
    dl_pdu_lengths[dl_pdu_lengths_idx].rnti = 0;
    dl_pdu_lengths_idx++;
    val = sci_tvb_get_ntohs(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, boost_start_val, boost_del);
    proto_tree_add_double(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost, tvb, *plen, 2, off_val);
    *plen += 2;
} // end of dissect_fapi_msg_DL_BCH_PDU
static guint32
dissect_fapi_msg_DL_PCH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double boost_start_val = (-6);
    double boost_del = (0.001);
    proto_item* rnti_pi;
    guint32 rnti;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_size, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].length = val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_tb_idx, tvb, *plen, 2, val);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].pdu_idx = val;
    rnti = sci_tvb_get_ntohs(tvb, *plen);
    rnti_pi = proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DLSCH_PDU_rnti, tvb, *plen, 2, rnti);
    *plen += 2;
    dl_pdu_lengths[dl_pdu_lengths_idx].rnti = rnti;
    dl_pdu_lengths_idx++;
    //IF wrong P-RNTI
    if (rnti != 0xFFFE)
    {
        expert_add_info_format(pinfo, rnti_pi, PI_MALFORMED, PI_WARN,
        "P-RNTI must be 0xFFFE for PDU type PCH");
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_vir_rb_type, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding, tvb, *plen, 4, val);
    *plen += 4;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_PCH_PDU_mcs, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_rv_idx, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_codeword_id, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_ant_mode, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_n_layers, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_pa, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, boost_start_val, boost_del);
    proto_tree_add_double(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost, tvb, *plen, 2, off_val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDSCH_PDU_n_prb, tvb, *plen, 1, (val + 2));
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //N_GAP
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_n_gap, tvb, *plen, 1, val);
        *plen += 1;
    }
    return rnti;
} // end of dissect_fapi_msg_DL_PCH_PDU
static guint32
dissect_fapi_msg_DL_DCI_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    guint32 n_sb;
    double off_val;
    double boost_start_val = (-6);
    double boost_del = (0.001);
    guint32 rnti;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_dci_format, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_cce_offset, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_aggr_level, tvb, *plen, 1, val);
    *plen += 1;
    rnti = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_rnti, tvb, *plen, 2, rnti);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_DCI_PDU_vir_rb_type, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding, tvb, *plen, 4, val);
    *plen += 4;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_mcs1, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_rv1, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_ndi1, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_DCI_PDU_swap_flag, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_mcs2, tvb, *plen, 1, val);
    *plen += 1;
    n_sb = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_rv2, tvb, *plen, 1, n_sb);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_ndi2, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_harq, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_DCI_PDU_pmi_conf, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_precoding, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_tpc, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_dai, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_PDU_n_gap, tvb, *plen, 1, val);
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 8)
    {
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_tb_size_1c, tvb, *plen, 1, val);
        *plen += 1;
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_dl_pa_1d, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_DCI_PDU_prach_flag_1a, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_prach_preamble_1a, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_prach_mask_1a, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_rnti_type, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, boost_start_val, boost_del);
    proto_tree_add_double(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost, tvb, *plen, 2, off_val);
    *plen += 2;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //MCCH flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DL_DCI_PDU_mcch_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //MCCH change notification
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_mcch_change_note, tvb, *plen, 1, val);
        *plen += 1;
        //Scrambling identity
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_scramb_id, tvb, *plen, 1, val);
        *plen += 1;
        //Cross carrier scheduling flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DCI_PDU_cross_car_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //Carrier indicator
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_car_ind, tvb, *plen, 1, val);
        *plen += 1;
        //SRS flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DCI_PDU_srs_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //SRS request
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_srs_req, tvb, *plen, 1, val);
        *plen += 1;
        //Antenna ports,scrambling and layers
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_ant_port_scramb_layer, tvb, *plen, 1, val);
        *plen += 1;
        if (global_fapi_sdk_dissect_rel10_dci_fields == TRUE)
        {
            //Size of CSI request field
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_size_csi_request, tvb, *plen, 1, val);
            *plen += 1;
            //associated_UL
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(tree, hf_fapi_sdk_msg_DL_DCI_PDU_associated_ul, tvb, *plen, 1, val);
            *plen += 1;
        }
    }
    return rnti;
} // end of dissect_fapi_msg_DL_DCI_PDU
static void
dissect_fapi_msg_UL_CONFIG_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 val;
    guint32 n_pdu;
    guint32 pdu_type;
    guint32 pdu_size;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_UL_CONFIG_REQ");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_length, tvb, *plen, 2, val);
    *plen += 2;
    n_pdu = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_n_pdu, tvb, *plen, 1, n_pdu);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_prach_freq_res, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_srs_present, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    for (i = 0; i < n_pdu; i++)
    {
        pdu_type = tvb_get_guint8(tvb, *plen);
        pdu_size = tvb_get_guint8(tvb, (*plen) + 1);
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_type, tvb, *plen, 1, pdu_type);
        *plen += 1;
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_CONFIG_REQ_pdu_size, tvb, *plen, 1, pdu_size);
        *plen += 1;
        switch (pdu_type)
        {
        case ULSCH:
            proto_item_append_text(ti, ": (ULSCH)");
            dissect_fapi_msg_UL_PUSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case ULSCH_CQI_RI:
            proto_item_append_text(ti, ": (ULSCH_CQI_RI)");
            dissect_fapi_msg_UL_PUSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_CQI_RI_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_INIT_TRANS_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case ULSCH_HARQ:
            proto_item_append_text(ti, ": (ULSCH_HARQ)");
            dissect_fapi_msg_UL_PUSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_HARQ_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_INIT_TRANS_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case ULSCH_CQI_HARQ_RI:
            proto_item_append_text(ti, ": (ULSCH_CQI_HARQ_RI)");
            dissect_fapi_msg_UL_PUSCH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_CQI_RI_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_HARQ_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            dissect_fapi_msg_UL_INIT_TRANS_INFO(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case UCI_CQI:
            proto_item_append_text(ti, ": (UCI_CQI)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                TRUE, FALSE, FALSE);
            break;
        case UCI_SR:
            proto_item_append_text(ti, ": (UCI_SR)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                FALSE, TRUE, FALSE);
            break;
        case UCI_HARQ:
            proto_item_append_text(ti, ": (UCI_HARQ)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                FALSE, FALSE, TRUE);
            break;
        case UCI_SR_HARQ:
            proto_item_append_text(ti, ": (UCI_SR_HARQ)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                FALSE, TRUE, TRUE);
            break;
        case UCI_CQI_HARQ:
            proto_item_append_text(ti, ": (UCI_CQI_HARQ)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                TRUE, FALSE, TRUE);
            break;
        case UCI_CQI_SR:
            proto_item_append_text(ti, ": (UCI_CQI_SR)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                TRUE, TRUE, FALSE);
            break;
        case UCI_CQI_SR_HARQ:
            proto_item_append_text(ti, ": (UCI_CQI_SR_HARQ)");
            dissect_fapi_msg_UL_UCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen,
                TRUE, TRUE, TRUE);
            break;
        case SRS:
            proto_item_append_text(ti, ": (SRS)");
            dissect_fapi_msg_UL_SRS_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case HARQ_BUFFER:
            proto_item_append_text(ti, ": (HARQ_BUFFER)");
            val = sci_tvb_get_ntohl(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
            *plen += 4;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
            *plen += 2;
            break;
        default:
            expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                "UL PDU %u: Illegal PDU Type (%u)",
                i, pdu_type);
        }// end of switch-case (pdu_type)
    }// end of for each PDU
} // end of dissect_fapi_msg_UL_CONFIG_REQ
static void
dissect_UL_PRACH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_for_subtree = NULL;
    proto_item* subtree = NULL;
    guint16 val16;
    guint8  val8;
    guint8 number_of_preamble = 0, i = 0;
    gint8 valInt8 = 0;
    val16 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, prach_ind_tag, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, prach_ind_length, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, prachConfigIdx, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, prach_ind_phys_cell_id, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, prach_ind_occasion_td, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, prach_occasion_fd, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    valInt8 = (gint8)val8;
    proto_tree_add_int(tree, prach_avg_rssi_ind_db, tvb, *plen, 1, valInt8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    number_of_preamble = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, prach_number_of_preamble, tvb, *plen, 1, number_of_preamble);
    *plen += 1;
    for(i = 0; i < number_of_preamble; i++)
    {
        fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
        subtree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "preambleId[%d]", i);
        fapi_sdk_msg_for_subtree = proto_item_add_subtree(subtree, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_detected_premable_indices, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_timing_estimates, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_detectionMetricdb, tvb, *plen, 1, val8);
        *plen += 1;
    }
}
static void
dissect_fapi_msg_UL_PUSCH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_size, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_rb_start, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_rb_num, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_mcs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_cs2dmrs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_flag, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_freq_hop_bits, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_ndi, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_rv, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_harq_re_tx, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_PUSCH_PDU_ul_tx_mode, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_curr_harq_trans, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_PUSCH_PDU_srs_present, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //ra_type
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type, tvb, *plen, 1, val);
        *plen += 1;
        //rb_coding
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding, tvb, *plen, 4, val);
        *plen += 4;
        //Transport blocks
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_codeword_id, tvb, *plen, 1, val);
        *plen += 1;
        //Transmission scheme
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_PUSCH_PDU_trans_scheme, tvb, *plen, 1, val);
        *plen += 1;
        //N Layers
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_n_layers, tvb, *plen, 1, val);
        *plen += 1;
        //Codebook index
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx, tvb, *plen, 1, val);
        *plen += 1;
        //Disable sequence hopping flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_PUSCH_PDU_seq_hopping_disable, tvb, *plen, 1, val);
        *plen += 1;
    }
} // end of dissect_fapi_msg_UL_PUSCH_PDU
static void
dissect_fapi_msg_UL_CQI_RI_INFO(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_ul_cqi_dsc = NULL;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_ul_cqi_cc = NULL;
    guint32 val;
    guint8 report_type;
    guint8 n_cc;
    guint8 i;
    guint8 rank_idx;
    guint8 rank_loop_size;
    ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 5,
    "ULSCH CQI/RI INFO");
    fapi_sdk_msg_ul_cqi_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
    if (global_fapi_sdk_parse_lte_release_ver == 8)
    {
        val = tvb_get_guint8(tvb, *plen);
        //proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_nbits_ri1, tvb, *plen, 1, val);
        proto_tree_add_uint_format(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_size, tvb, *plen, 1, val, "CQI Num of Bits (RI=1): %u", val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        //proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_nbits_ri2, tvb, *plen, 1, val);
        proto_tree_add_uint_format(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_size, tvb, *plen, 1, val, "CQI Num of Bits (RI=2): %u", val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_ri_nbits, tvb, *plen, 1, val);
        *plen += 1;
    }
    else if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //Report type
        report_type = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_report_type, tvb, *plen, 1, report_type);
        *plen += 1;
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_cqi, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_ri, tvb, *plen, 1, val);
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        if (report_type == 0) // periodic
        {
            //DL CQI/PMI/RI size
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_ri_size, tvb, *plen, 1, val);
            *plen += 1;
        }
        else // aperiodic
        {
            //Number of CC
            n_cc = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_n_cc, tvb, *plen, 1, n_cc);
            *plen += 1;
            for (i = 0; i < n_cc; i++)
            {
                ei = proto_tree_add_protocol_format(fapi_sdk_msg_ul_cqi_dsc, proto_fapi_sdk, tvb, *plen, 2,
                "CC[%u]", i);
                fapi_sdk_msg_ul_cqi_cc = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree3);
                //RI Size
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_ul_cqi_cc, hf_fapi_sdk_msg_PUSCH_PDU_ri_size, tvb, *plen, 1, val);
                *plen += 1;
                rank_loop_size = (guint8)pow(2, val);
                for (rank_idx = 0; rank_idx < rank_loop_size; rank_idx++)
                {
                    //DL CQI/PMI size
                    val = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint_format(fapi_sdk_msg_ul_cqi_cc, hf_fapi_sdk_msg_PUSCH_PDU_cqi_pmi_size, tvb, *plen, 1, val, "CQI/PMI Size (RI=%u): %u", rank_idx, val);
                    *plen += 1;
                }
            }
        }
    }
} // end of dissect_fapi_msg_UL_CQI_RI_INFO
static void
dissect_fapi_msg_UL_HARQ_INFO(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_ul_cqi_dsc = NULL;
    guint32 val;
    ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 5,
    "ULSCH HARQ INFO");
    fapi_sdk_msg_ul_cqi_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_acknack_nbits, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_beta_offset_acknack, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_cqi_dsc, hf_fapi_sdk_msg_PUSCH_PDU_acknack_mode_TDD, tvb, *plen, 1, val);
    *plen += 1;
} // end of dissect_fapi_msg_UL_HARQ_INFO
static void
dissect_fapi_msg_UL_INIT_TRANS_INFO(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_ul_init_dsc = NULL;
    guint32 val;
    ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 2,
    "ULSCH INIT TRANS INFO");
    fapi_sdk_msg_ul_init_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(fapi_sdk_msg_ul_init_dsc, hf_fapi_sdk_msg_PUSCH_PDU_srs_init, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_ul_init_dsc, hf_fapi_sdk_msg_PUSCH_PDU_nrb_init, tvb, *plen, 1, val);
    *plen += 1;
} // end of dissect_fapi_msg_UL_INIT_TRANS_INFO
static void
dissect_fapi_msg_UL_UCI_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen,
gboolean cqi_flag, gboolean sr_flag, gboolean harq_flag)
{
    guint32 val;
    proto_item* ti;
    proto_tree* fapi_sdk_msg_ul_uci_dsc = NULL;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
    *plen += 2;
    if (cqi_flag)
    {
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 3,
        "UCI_CQI");
        fapi_sdk_msg_ul_uci_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "PUCCH Index Port0: %u", val);
        *plen += 2;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_cqi_size, tvb, *plen, 1, val);
        *plen += 1;
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            //Number of PUCCH Resources (number of antenna ports allocated)
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_n_res, tvb, *plen, 1, val);
            *plen += 1;
            //PUCCH Index for antenna port 1
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "PUCCH Index Port1: %u", val);
            *plen += 2;
        }
    }
    if (sr_flag)
    {
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 2,
        "UCI_SR");
        fapi_sdk_msg_ul_uci_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "PUCCH Index Port0: %u", val);
        *plen += 2;
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            //Number of PUCCH Resources (number of antenna ports allocated)
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_n_res, tvb, *plen, 1, val);
            *plen += 1;
            //PUCCH Index for antenna port 1
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "PUCCH Index Port1: %u", val);
            *plen += 2;
        }
    }
    if (harq_flag)
    {
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 3,
        "UCI_HARQ");
        fapi_sdk_msg_ul_uci_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree2);
        if ((global_fapi_sdk_parse_lte_release_ver == 8) && (global_fapi_sdk_duplex_mode_val == FDD_RADIO)) {
            proto_item_append_text(ti, " (FDD)");
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val);
            *plen += 2;
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_harq_size, tvb, *plen, 1, val);
            *plen += 1;
        }
        else
        {
            if (global_fapi_sdk_parse_lte_release_ver == 8)
            {
                proto_item_append_text(ti, " (TDD)");
            }
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_harq_size, tvb, *plen, 1, val);
            *plen += 1;
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_acknack_mode, tvb, *plen, 1, val);
            *plen += 1;
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_n_res, tvb, *plen, 1, val);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "HARQ Resource 0: %u", val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "HARQ Resource 1: %u", val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "HARQ Resource 2: %u", val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint_format(fapi_sdk_msg_ul_uci_dsc, hf_fapi_sdk_msg_UL_UCI_PDU_pucch_idx, tvb, *plen, 2, val, "HARQ Resource 3: %u", val);
            *plen += 2;
        }
    }
} // end of dissect_fapi_msg_UL_UCI_PDU
static void
dissect_fapi_msg_UL_SRS_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_size, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_srs_bw, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_freq_dom_pos, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_srs_hop_bw, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_SRS_PDU_trans_comb, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_config_idx, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_SRS_PDU_cs, tvb, *plen, 1, val);
    *plen += 1;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //Antenna Port
        val = tvb_get_guint8(tvb, *plen);
        //guint8 n_ant = (guint8)(1 << val);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_ant_ports, tvb, *plen, 1, (guint8)(1 << val));
        *plen += 1;
    }
} // end of dissect_fapi_msg_UL_SRS_PDU
static void
dissect_fapi_msg_vsm_UL_NI_TLV(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
    *plen += 4;
} // end of dissect_fapi_msg_UL_NI_PDU
static void
dissect_fapi_msg_HI_DCI0_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 val;
    guint32 n_pdu;
    guint32 pdu_type;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_HI_DCI0_REQ");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_dci, tvb, *plen, 1, val);
    *plen += 1;
    n_pdu = val;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_HI_DCI0_REQ_n_phich, tvb, *plen, 1, val);
    *plen += 1;
    n_pdu += val;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        pdu_type = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_type, tvb, *plen, 1, pdu_type);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_DL_HI_DCI0_REQ_pdu_size, tvb, *plen, 1, val);
        *plen += 1;
        if (pdu_type == HI_PDU)
        {
            proto_item_append_text(ti, ": (HI)");
            dissect_fapi_msg_UL_PHICH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
        }
        else
        {
            proto_item_append_text(ti, ": (UL DCI)");
            dissect_fapi_msg_UL_DCI_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
        }
    }// end of for each PDU
}// end of dissect_fapi_msg_HI_DCI0_REQ
static void
dissect_fapi_msg_UL_PHICH_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double boost_start = (-6);
    double boost_del = (0.001);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_rb_start, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_cs2dmrs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_value, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_i_phich, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, boost_start, boost_del);
    proto_tree_add_double(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_boost, tvb, *plen, 2, off_val);
    *plen += 2;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //Flag TB2
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_tb2_flag, tvb, *plen, 1, val);
        *plen += 1;
        //HI Value 2
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint_format(tree, hf_fapi_sdk_msg_UL_PHICH_PDU_i_phich, tvb, *plen, 1, val, "HI Value 2: %u", val);
        *plen += 1;
    }
} // end of dissect_fapi_msg_UL_PHICH_PDU
static void
dissect_fapi_msg_UL_DCI_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double boost_start_val = (-6);
    double boost_del = (0.001);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_format, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_cce_offset, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_aggr_lvl, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_rb_start, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_n_rb, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_mcs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_cs2dmrs, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_flag, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_freq_hop_bits, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_ndi, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_ant_sel, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_tpc, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_DCI_PDU_aperiodic_cqi, tvb, *plen, 1, (val & 0x1));
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_ul_idx, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_dl_idx, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_tpc_bitmap, tvb, *plen, 4, val);
    *plen += 4;
    if (global_fapi_sdk_parse_lte_release_ver == 10)
    {
        //Transmission power
        val = sci_tvb_get_ntohs(tvb, *plen);
        off_val = api_val_to_lgcl_val(val, boost_start_val, boost_del);
        proto_tree_add_double(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_boost, tvb, *plen, 2, off_val);
        *plen += 2;
        //Cross carrier scheduling
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DCI_PDU_cross_car_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //Carrier indicator
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_car_ind, tvb, *plen, 1, val);
        *plen += 1;
        //Size of CQI/CSI
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_cqi_csi_size, tvb, *plen, 1, val);
        *plen += 1;
        //SRS flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_DCI_PDU_srs_flag, tvb, *plen, 1, (val & 0x1));
        *plen += 1;
        //SRS request
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_srs_req, tvb, *plen, 1, val);
        *plen += 1;
        //Resource allocation flag
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_msg_UL_DCI_PDU_ra_flag, tvb, *plen, 1, val);
        *plen += 1;
        //Resource allocation type
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_ra_type, tvb, *plen, 1, val);
        *plen += 1;
        //Resource block coding
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_rb_coding, tvb, *plen, 4, val);
        *plen += 4;
        //MCS_2
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_mcs2, tvb, *plen, 1, val);
        *plen += 1;
        //NDI_2
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_DCI_PDU_ndi2, tvb, *plen, 1, val);
        *plen += 1;
        //Num Ant. Ports
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_COMMON_PDU_ant_ports, tvb, *plen, 1, (guint8)(1 << val));
        *plen += 1;
        //TPMI
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DL_COMMON_PDU_pmi_codebook_idx, tvb, *plen, 1, val);
        *plen += 1;
        if (global_fapi_sdk_dissect_rel10_dci_fields == TRUE)
        {
            //Trans Mode
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(tree, hf_fapi_sdk_msg_UL_DCI_PDU_trans_mode, tvb, *plen, 1, val);
            *plen += 1;
        }
    }
} // end of dissect_fapi_msg_UL_DCI_PDU
static int
dissect_fapi_mssg_header(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint8 n_pdu;
    guint32 msg_header_val;
    guint32 i;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    n_pdu = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_TX_REQ_n_phy, tvb, *plen, 1, n_pdu);
    *plen += 1;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "PHY[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        msg_header_val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_header_len_phyid, tvb, *plen, 2, msg_header_val);
        *plen += 2;
        msg_header_val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_header_len_phylen, tvb, *plen, 2, msg_header_val);
        *plen += 2;
    }
    return n_pdu;
}
static void
dissect_fapi_msg_TX_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    proto_tree* fapi_sdk_msg_tlv_dsc = NULL;
    guint32 i;
    guint32 tlv_idx;
    gint32 j;
    guint32 val;
    guint32 n_pdu;
    guint32 n_tlv;
    guint32 pdu_len;
    guint32 pdu_idx;
    guint32 rnti;
    guint32 tlv_tag;
    guint32 tlv_length;
    guint32 sfn_offset;
    const guint8* val_ptr;
    tvbuff_t* total_payload = NULL;
    tvbuff_t* tvb_temp = NULL;
    mac_lte_info* mac_info = NULL;
    guint32 expected_len = 0;
    guint32 received_len = 0;
    sfn_offset = *plen;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_TX_REQ");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_TX_REQ_n_pdu, tvb, *plen, 2, n_pdu);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 8,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        pdu_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_TX_REQ_length, tvb, *plen, 2, pdu_len);
        *plen += 2;
        pdu_idx = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_TX_REQ_idx, tvb, *plen, 2, pdu_idx);
        *plen += 2;
        expected_len = 0;
        received_len = 0;
        j = FAPI_MIN(dl_pdu_lengths_idx, i);
        for (; j >= 0; j--)
        {
            if (dl_pdu_lengths[j].pdu_idx == pdu_idx)
            {
                rnti = dl_pdu_lengths[j].rnti;
                expected_len = dl_pdu_lengths[j].length;
                break;
            }
        }
        n_tlv = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_TX_REQ_n_tlv, tvb, *plen, 4, n_tlv);
        *plen += 4;
        if (global_fapi_sdk_dissect_MAC_DL)
        {
            /* Initializing the composed buffer and the temp buffer to be of type TVBUFF_COMPOSITE */
            total_payload = tvb_new_composite();
        }
        /* Loop over data elements */
        for (tlv_idx = 0; tlv_idx < n_tlv; tlv_idx++)
        {
            ei = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 4,
            "TLV[%d]", tlv_idx);
            fapi_sdk_msg_tlv_dsc = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree2);
            tlv_tag = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_boolean(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_TX_REQ_TLV_tag, tvb, *plen, 2, tlv_tag);
            *plen += 2;
            tlv_length = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_TX_REQ_TLV_length, tvb, *plen, 2, tlv_length);
            *plen += 2;
            received_len += tlv_length;
            if (global_fapi_sdk_dissect_MAC_DL)
            {
                /* make the temp buffer to be only the data out of the #j tlv */
                tvb_temp = tvb_new_subset(tvb, *plen, tlv_length, tlv_length);
                val_ptr = tvb_get_ptr(tvb, *plen, tlv_length);
                tvb_temp->real_data = val_ptr;
                /* Compose the final buffer with the temp buffer (add the current data tlv to the total data buffer) */
                tvb_composite_append(total_payload, tvb_temp);
            }
            if (tlv_tag == 1)
            {
                val = sci_tvb_get_ntohl(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_TX_REQ_TLV_value_tag1, tvb, *plen, 4, val);
                *plen += 4;
            }
            else
            {
                val_ptr = tvb_get_ptr(tvb, *plen, tlv_length);
                proto_tree_add_bytes(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_TX_REQ_TLV_value_tag0, tvb, *plen, tlv_length, val_ptr);
                *plen += tlv_length + (4 - (((tlv_length - 1) % 4) + 1));
            }
        }// end of for each TLV
        if ((expected_len != 0) && (expected_len != received_len))
        {
            expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                "TX_REQ: Mismatch data length in PDU[%u]. Expected value is %u, received value is %u",
            pdu_idx, expected_len, received_len);
        }
        /* Dissect the MAC layers only if the user has enabled it in in the preferences menu */
        if (global_fapi_sdk_dissect_MAC_DL)
        {
            /* Finalize the composed buffer */
            tvb_composite_finalize(total_payload);
            if (mac_info == NULL)
            {
                /* Allocate & zero struct */
                mac_info = se_alloc0(sizeof(struct mac_lte_info));
            }
            /* Attach mandatory info to the pinfo of the buffer sent to the MAC dissector */
            mac_info->subframeNumber = (sci_tvb_get_ntohs(tvb, sfn_offset) & SF_MASK);
            mac_info->rnti = rnti;
            switch (rnti) {
            case NO_RNTI_VAL:
                mac_info->rntiType = NO_RNTI;
                break;
            case M_RNTI_VAL:
                mac_info->rntiType = M_RNTI;
                break;
            case P_RNTI_VAL:
                mac_info->rntiType = P_RNTI;
                break;
            case SI_RNTI_VAL:
                mac_info->rntiType = SI_RNTI;
                break;
            default:
                mac_info->rntiType = C_RNTI;
            } // end of switch-case (RNTI)
            mac_info->radioType = global_fapi_sdk_duplex_mode_val;
            mac_info->direction = DIRECTION_DOWNLINK;
            mac_info->length = tvb_length(total_payload);
            mac_info->ueid = i;
            //attach_mac_info_to_dan(pinfo, mac_info);
            p_add_proto_data(pinfo->fd, proto_get_id_by_filter_name("mac-lte"), mac_info);
            DISSECTOR_ASSERT(total_payload != NULL);
            call_dissector_only(mac_lte_handle, total_payload, pinfo, tree);
        }
    }// end of for each PDU
}// end of dissect_fapi_msg_TX_REQ
static void
dissect_fapi_msg_CONFIG_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* tlv_tree;
    proto_tree* fapi_sdk_msg_data_config_tree = NULL;
    proto_tree* fapi_sdk_msg_tlv_dsc = NULL;
    guint32 num_tlv;
    guint32 val;
    guint32 tlv_len;
    guint32 n_tlv;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_CONFIG_REQ");
    fapi_sdk_msg_data_config_tree = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    n_tlv = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_data_config_tree, hf_fapi_sdk_msg_CONFIG_REQ_n_tlv, tvb, *plen, 1, n_tlv);
    *plen += 1;
    for (num_tlv = 0; num_tlv < n_tlv; num_tlv++)
    {
        tlv_tree = proto_tree_add_protocol_format(fapi_sdk_msg_data_config_tree, proto_fapi_sdk, tvb, *plen, 4,
        "TLV[%d]", num_tlv);
        fapi_sdk_msg_tlv_dsc = proto_item_add_subtree(tlv_tree, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_tag_rel10, tvb, *plen, 2, val);
            //proto_item_append_text(ti, ": %s", fapi_msg_data_config_tlv_tag_release10_string[val].strptr);
        }
        else
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_tag, tvb, *plen, 2, val);
            //proto_item_append_text(ti, ": %s", fapi_msg_data_config_tlv_tag_string[val].strptr);
        }
        *plen += 2;
        tlv_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_length, tvb, *plen, 2, tlv_len);
        *plen += 2;
        switch (tlv_len)
        {
        case 1:
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 1, val);
            *plen += 1;
            //padding expected
            *plen += 1;
            break;
        case 2:
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 2, val);
            *plen += 2;
            break;
        default:
            proto_item_append_text(tlv_tree, " Unsupported TLV length");
            *plen += tlv_len + (4 - ((tlv_len - 1) % 4)) - 1;
        }
    }// end of for each TLV
} // end of dissect_fapi_msg_CONFIG_REQ
static void
dissect_fapi_vsm_CFG_CFI(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_cfg_cfi, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding3, tvb, *plen, 3, val);
    *plen += 3;
}
static void
dissect_fapi_vsm_CFG_VERSION(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_cfg_version_major, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_cfg_version_minor, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_vsm_CFG_ETM(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_boolean(tree, hf_fapi_sdk_vsm_cfg_etm_enable, tvb, *plen, 1, (val == 1));
    *plen += 1;
    if (global_fapi_sdk_parse_vsm_api_ver > 3)
    {
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_vsm_cfg_etm_all_zeros, tvb, *plen, 1, (val == 1));
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(tree, hf_fapi_sdk_vsm_cfg_etm_mi_override, tvb, *plen, 1, (val == 1));
        *plen += 1;
    }
    else
    {
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
        *plen += 1;
    }
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_msg_RX_ULSCH_CQI_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen,
gboolean inc_ri)
{
    proto_item* ti;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_cc = NULL;
    guint32 i, j;
    guint32 n_pdu;
    guint32 val;
    double off_val;
    double snr_start = (-64);
    double snr_del = (0.5);
    const guint8* val_ptr;
    guint32 pdu_len = 0;
    guint32 rnti = 0;
    guint32 data_offset = 0;
    guint32 pdu_hdr_len = 13;
    mac_lte_info* mac_info = NULL;
    tvbuff_t* mac_tvb = NULL;
    guint32 sfn_offset = 0;
    guint8 n_cc;
    guint32 ul_pdu_lengths_idx = 0;
    if (inc_ri)
    {
        pdu_hdr_len++;
    }
    else
    {
        sfn_offset = *plen;
    }
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_n_pdu, tvb, *plen, 2, n_pdu);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, pdu_hdr_len,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
        *plen += 4;
        rnti = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, rnti);
        *plen += 2;
        pdu_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_length, tvb, *plen, 2, pdu_len);
        *plen += 2;
        data_offset = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data_offset, tvb, *plen, 2, data_offset);
        *plen += 2;
        if ((pdu_len != 0) && (data_offset != 0))
        {
            if (ul_pdu_lengths_idx < MAX_UL_IND_PDUS) {
                ul_pdu_lengths[ul_pdu_lengths_idx].length = pdu_len;
                ul_pdu_lengths[ul_pdu_lengths_idx].rnti = rnti;
            }
            ul_pdu_lengths_idx++;
        }
        val = tvb_get_guint8(tvb, *plen);
        off_val = api_val_to_lgcl_val(val, snr_start, snr_del);
        proto_tree_add_float(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_snr, tvb, *plen, 1, (float)off_val);
        *plen += 1;
        if (inc_ri)
        {
            if (global_fapi_sdk_parse_lte_release_ver == 10)
            {
                //Number of CC
                n_cc = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_PUSCH_PDU_n_cc, tvb, *plen, 1, n_cc);
                *plen += 1;
                for (j = 0; j < n_cc; j++)
                {
                    ei = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 1,
                    "CC[%d]", j);
                    fapi_sdk_msg_pdu_cc = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree2);
                    //RI
                    val = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_pdu_cc, hf_fapi_sdk_msg_UL_CQI_EVT_ri, tvb, *plen, 1, val);
                    *plen += 1;
                }
            }
            else
            {
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_CQI_EVT_ri, tvb, *plen, 1, val);
                *plen += 1;
            }
        }
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset, tvb, *plen, 2, val);
        *plen += 2;
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            //Timing Advance R9
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset_R9, tvb, *plen, 2, val);
            *plen += 2;
        }
    }// end of for each PDU
    if (ul_pdu_lengths_idx <= MAX_UL_IND_PDUS)
    {
        for (i = 0; i < ul_pdu_lengths_idx; i++)
        {
            ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, ul_pdu_lengths[i].length,
            "PDU DATA[%d]: (RNTI %d)", i, ul_pdu_lengths[i].rnti);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
            /* Dissect the MAC layers only if the user has enabled
                * it in in the preferences menu and if this is not
                * a CQI_IND message
            */
            if ((global_fapi_sdk_dissect_MAC_UL) && (!inc_ri))
            {
                if (mac_info == NULL)
                {
                    /* Allocate & zero struct */
                    mac_info = se_alloc0(sizeof(struct mac_lte_info));
                }// end of if (mac_info == NULL)
                /* Attach mandatory info to the pinfo of the buffer sent to the MAC dissector */
                mac_info->subframeNumber = (sci_tvb_get_ntohs(tvb, sfn_offset) & SF_MASK);
                mac_info->rnti = ul_pdu_lengths[i].rnti;
                mac_info->radioType = global_fapi_sdk_duplex_mode_val;
                mac_info->direction = DIRECTION_UPLINK;
                mac_info->rntiType = C_RNTI;
                mac_info->length = ul_pdu_lengths[i].length;
                mac_info->ueid = i;
                //attach_mac_info_to_dan(pinfo, mac_info);
                p_add_proto_data(pinfo->fd, proto_get_id_by_filter_name("mac-lte"), mac_info);
                mac_tvb = tvb_new_subset(tvb, *plen, ul_pdu_lengths[i].length, ul_pdu_lengths[i].length);
                DISSECTOR_ASSERT(mac_tvb != NULL);
                call_dissector_only(mac_lte_handle, mac_tvb, pinfo, tree);
                *plen += ul_pdu_lengths[i].length;
            }// end of if (global_dan_lte_sdk_dissect_MAC_UL)
            else
            {
                val_ptr = tvb_get_ptr(tvb, *plen, ul_pdu_lengths[i].length);
                proto_tree_add_bytes(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data, tvb, *plen, ul_pdu_lengths[i].length, val_ptr);
                *plen += ul_pdu_lengths[i].length;
            }
        }
    }
    else
    {
        pdu_len = tvb_length_remaining(tvb, *plen);
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, pdu_len,
        "PDU DATA");
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val_ptr = tvb_get_ptr(tvb, *plen, pdu_len);
        proto_tree_add_bytes(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_data, tvb, *plen, pdu_len, val_ptr);
        *plen += pdu_len;
    }
}// end of dissect_fapi_msg_RX_ULSCH_IND
static void
dissect_fapi_msg_HARQ_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 n_pdu;
    guint32 j;
    guint32 n_an;
    guint32 val;
    guint32 pdu_size = 8;
    guint8 acknack_mode = 0;
    if (global_fapi_sdk_duplex_mode_val == TDD_RADIO) pdu_size += 2;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_HARQ_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_n_harq, tvb, *plen, 2, n_pdu);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, pdu_size,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
        *plen += 4;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
        *plen += 2;
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            acknack_mode = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack_mode, tvb, *plen, 1, acknack_mode);
            *plen += 1;
            n_an = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_n_acknack, tvb, *plen, 1, n_an);
            *plen += 1;
            for (j = 0; j < n_an; j++)
            {
                val = tvb_get_guint8(tvb, *plen);
                switch (j + 1)
                {
                case 1:
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack1, tvb, *plen, 1, val);
                    break;
                case 2:
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack2, tvb, *plen, 1, val);
                    break;
                case 3:
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack3, tvb, *plen, 1, val);
                    break;
                case 4:
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack4, tvb, *plen, 1, val);
                    break;
                default:
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack1, tvb, *plen, 1, val);
                }
                *plen += 1;
            }
        }
        else
        {
            if (global_fapi_sdk_duplex_mode_val == TDD_RADIO)
            {
                proto_item_append_text(ti, ": (TDD)");
                acknack_mode = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack_mode, tvb, *plen, 1, acknack_mode);
                *plen += 1;
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_n_acknack, tvb, *plen, 1, val);
                *plen += 1;
            }
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack1, tvb, *plen, 1, val);
            *plen += 1;
            if (acknack_mode != 2) //this also applies to FDD REL 8
            {
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_HARQ_EVT_acknack2, tvb, *plen, 1, val);
                *plen += 1;
            }
        }
    }// end of for each PDU
}// end of dissect_fapi_msg_HARQ_IND
static void
dissect_fapi_msg_CRC_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 n_pdu;
    guint32 val;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_CRC_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_CRC_EVT_n_crc, tvb, *plen, 2, n_pdu);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 7,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
        *plen += 4;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
        *plen += 2;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_boolean(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_CRC_EVT_crc_flag, tvb, *plen, 1, val);
        *plen += 1;
    }// end of for each PDU
}// end of dissect_fapi_msg_CRC_IND
static void
dissect_fapi_msg_RX_SR_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 n_pdu;
    guint32 val;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_RX_SR_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_pdu = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_SR_EVT_n_sr, tvb, *plen, 2, n_pdu);
    *plen += 2;
    for (i = 0; i < n_pdu; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
        *plen += 4;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
        *plen += 2;
    }// end of for each PDU
}// end of dissect_fapi_msg_RX_SR_IND
static void
dissect_fapi_msg_SRS_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_ue_dsc = NULL;
    proto_tree* fapi_sdk_msg_rb_dsc = NULL;
    guint32 i, j;
    guint32 val;
    guint32 n_ue;
    guint32 n_rb;
    guint32 rb_snr;
    double off_val;
    double snr_start = (-64);
    double snr_del = (0.5);
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_SRS_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_ue = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_n_srs, tvb, *plen, 1, n_ue);
    *plen += 1;
    for (i = 0; i < n_ue; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 12,
        "UE[%d]", i);
        fapi_sdk_msg_ue_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_handle, tvb, *plen, 4, val);
        *plen += 4;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_COMMON_PDU_rnti, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_dopp_est, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_timing_offset, tvb, *plen, 2, val);
        *plen += 2;
        n_rb = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_n_rb, tvb, *plen, 1, n_rb);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_rb_start, tvb, *plen, 1, val);
        *plen += 1;
        for (j = 0; j < n_rb; j++)
        {
            ei = proto_tree_add_protocol_format(fapi_sdk_msg_ue_dsc, proto_fapi_sdk, tvb, *plen, 1,
            "RB[%d]", j);
            fapi_sdk_msg_rb_dsc = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree2);
            rb_snr = tvb_get_guint8(tvb, *plen);
            off_val = api_val_to_lgcl_val(rb_snr, snr_start, snr_del);
            proto_tree_add_float(fapi_sdk_msg_rb_dsc, hf_fapi_sdk_msg_UL_SRS_EVT_rb_snr, tvb, *plen, 1, (float)off_val);
            *plen += 1;
        } // end of for each RB
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            //Timing Advance R9
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_ue_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset_R9, tvb, *plen, 2, val);
            *plen += 2;
        }
    }// end of for each PDU
}// end of dissect_fapi_msg_RX_CQI_IND
static void
dissect_fapi_msg_RACH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint32 i;
    guint32 n_preamble;
    guint32 val;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_RACH_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_preamble = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_UL_PRACH_RSP_num_preambles, tvb, *plen, 1, n_preamble);
    *plen += 1;
    for (i = 0; i < n_preamble; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 5,
        "Preamble[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PRACH_RSP_rnti, tvb, *plen, 2, val);
        *plen += 2;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PRACH_RSP_preamble_id, tvb, *plen, 1, val);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PRACH_RSP_timing_offset, tvb, *plen, 2, val);
        *plen += 2;
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            //Timing Advance R9
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_UL_PUSCH_CQI_EVT_timing_offset_R9, tvb, *plen, 2, val);
            *plen += 2;
        }
    }// end of for each Preamble
}// end of dissect_fapi_msg_RACH_IND
static void
dissect_fapi_msg_CONFIG_RSP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* config_rsp_tlv_tree;
    proto_tree* fapi_sdk_msg_config_rsp_tree = NULL;
    proto_tree* fapi_sdk_msg_tlv = NULL;
    guint32 num_invalid_tlv;
    guint32 num_missing_tlv;
    guint32 val;
    guint32 tlv_len;
    guint32 n_inval_tlv;
    guint32 n_miss_tlv;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_CONFIG_RSP");
    fapi_sdk_msg_config_rsp_tree = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_config_rsp_tree, hf_fapi_sdk_msg_CONFIG_RSP_err_code, tvb, *plen, 1, val);
    *plen += 1;
    n_inval_tlv = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_config_rsp_tree, hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_invalid, tvb, *plen, 1, n_inval_tlv);
    *plen += 1;
    n_miss_tlv = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_config_rsp_tree, hf_fapi_sdk_msg_CONFIG_RSP_n_tlv_missing, tvb, *plen, 1, n_miss_tlv);
    *plen += 1;
    for (num_invalid_tlv = 0; num_invalid_tlv < n_inval_tlv; num_invalid_tlv++)
    {
        config_rsp_tlv_tree = proto_tree_add_protocol_format(fapi_sdk_msg_config_rsp_tree, proto_fapi_sdk, tvb, *plen, 4,
        "Invalid TLV[%d]", num_invalid_tlv);
        fapi_sdk_msg_tlv = proto_item_add_subtree(config_rsp_tlv_tree, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_tag_rel10, tvb, *plen, 2, val);
            //proto_item_append_text(config_rsp_tlv_tree, ": %s", fapi_msg_data_config_tlv_tag_release10_string[val].strptr);
        }
        else
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_tag, tvb, *plen, 2, val);
            //proto_item_append_text(config_rsp_tlv_tree, ": %s", fapi_msg_data_config_tlv_tag_string[val].strptr);
        }
        *plen += 2;
        tlv_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_length, tvb, *plen, 1, tlv_len);
        *plen += 2;
        switch (tlv_len)
        {
        case 1:
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 1, val);
            *plen += 1;
            //padding expected
            *plen += 1;
            break;
        case 2:
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 2, val);
            *plen += 2;
            break;
        default:
            proto_item_append_text(config_rsp_tlv_tree, " Unsupported TLV length");
            *plen += tlv_len + (4 - ((tlv_len - 1) % 4)) - 1;
        }
    }// end of for each invalid TLV
    for (num_missing_tlv = 0; num_missing_tlv < n_miss_tlv; num_missing_tlv++)
    {
        config_rsp_tlv_tree = proto_tree_add_protocol_format(fapi_sdk_msg_config_rsp_tree, proto_fapi_sdk, tvb, *plen, 4,
        "Missing TLV[%d]", num_missing_tlv);
        fapi_sdk_msg_tlv = proto_item_add_subtree(config_rsp_tlv_tree, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_tag, tvb, *plen, 2, val);
        *plen += 2;
        tlv_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_length, tvb, *plen, 2, tlv_len);
        *plen += 2;
        switch (tlv_len)
        {
        case 1:
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 1, val);
            *plen += 1;
            //padding expected
            *plen += 1;
            break;
        case 2:
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 2, val);
            *plen += 2;
            break;
        default:
            proto_item_append_text(config_rsp_tlv_tree, " Unsupported TLV length");
            *plen += tlv_len + (4 - ((tlv_len - 1) % 4)) - 1;
        }
    }// end of for each missing TLV
} // end of dissect_fapi_msg_CONFIG_RSP
static void
dissect_fapi_msg_PARAM_RSP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* param_rsp_tlv;
    proto_tree* fapi_sdk_msg_param_rsp_tree = NULL;
    proto_tree* fapi_sdk_msg_tlv_dsc = NULL;
    guint32 num_tlv;
    guint32 val;
    guint32 tlv_len;
    guint32 n_tlv;
    fapi_sdk_msg_param_rsp_tree = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_param_rsp_tree, hf_fapi_sdk_msg_CONFIG_RSP_err_code, tvb, *plen, 1, val);
    *plen += 1;
    n_tlv = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_param_rsp_tree, hf_fapi_sdk_msg_PARAM_RSP_n_tlv, tvb, *plen, 1, n_tlv);
    *plen += 1;
    for (num_tlv = 0; num_tlv < n_tlv; num_tlv++)
    {
        param_rsp_tlv = proto_tree_add_protocol_format(fapi_sdk_msg_param_rsp_tree, proto_fapi_sdk, tvb, *plen, 4,
        "TLV[%d]", num_tlv);
        fapi_sdk_msg_tlv_dsc = proto_item_add_subtree(param_rsp_tlv, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        if (global_fapi_sdk_parse_lte_release_ver == 10)
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_tag_rel10, tvb, *plen, 2, val);
            //proto_item_append_text(param_rsp_tlv, ": %s", fapi_msg_data_config_tlv_tag_release10_string[val].strptr);
        }
        else
        {
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_tag, tvb, *plen, 2, val);
            //proto_item_append_text(param_rsp_tlv, ": %s", fapi_msg_data_config_tlv_tag_string[val].strptr);
        }
        *plen += 2;
        tlv_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_length, tvb, *plen, 2, tlv_len);
        *plen += 2;
        switch (tlv_len)
        {
        case 1:
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 1, val);
            *plen += 1;
            //padding expected
            *plen += 1;
            break;
        case 2:
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CONFIG_TLV_value, tvb, *plen, 2, val);
            *plen += 2;
            break;
        default:
            proto_item_append_text(param_rsp_tlv, " Unsupported TLV length");
            *plen += tlv_len + (4 - ((tlv_len - 1) % 4)) - 1;
        }
    }// end of for each TLV
} // end of dissect_fapi_msg_CONFIG_RSP
static void
dissect_fapi_msg_ERROR_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* sfn_sf_pi;
    proto_tree* fapi_sdk_msg_inval_sfn_sf = NULL;
    guint32 val;
    guint8 err_code;
    guint8 sub_err_code;
    guint8 direction;
    guint16 rnti;
    guint8 pdu_type;
    guint8 msg_id;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_ERROR_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    msg_id = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_msg_id, tvb, *plen, 1, msg_id);
    *plen += 1;
    err_code = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_error_code, tvb, *plen, 1, err_code);
    *plen += 1;
    switch (err_code)
    {
    case SFN_OUT_OF_SYNC:
    case MSG_INVALID_SFN:
        val = sci_tvb_get_ntohs(tvb, *plen);
        sfn_sf_pi = proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_received_sfn_sf, tvb, *plen, 2, val);
        fapi_sdk_msg_inval_sfn_sf = proto_item_add_subtree(sfn_sf_pi, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_item(fapi_sdk_msg_inval_sfn_sf, hf_fapi_sdk_msg_header_sfn, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
        proto_tree_add_item(fapi_sdk_msg_inval_sfn_sf, hf_fapi_sdk_msg_header_sf, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        sfn_sf_pi = proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_expected_sfn_sf, tvb, *plen, 2, val);
        fapi_sdk_msg_inval_sfn_sf = proto_item_add_subtree(sfn_sf_pi, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_item(fapi_sdk_msg_inval_sfn_sf, hf_fapi_sdk_msg_header_sfn, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
        proto_tree_add_item(fapi_sdk_msg_inval_sfn_sf, hf_fapi_sdk_msg_header_sf, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
        *plen += 2;
        break;
    case MSG_PDU_ERR:
        sub_err_code = tvb_get_guint8(tvb, *plen);
        *plen += 1;
        direction = tvb_get_guint8(tvb, *plen);
        *plen += 1;
        rnti = sci_tvb_get_ntohs(tvb, *plen);
        *plen += 2;
        pdu_type = tvb_get_guint8(tvb, *plen);
        *plen += 1;
        if (direction == FAPI_E_DOWNLINK)
        {
            switch (pdu_type)
            {
            case DCI_DL_PDU:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_dci_dl_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case BCH_PDU:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_bch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case MCH_PDU:
                //proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_mch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported PDU Type");
                break;
            case DLSCH_PDU:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_dlsch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case PCH_PDU:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_pch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            default:
                proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported PDU Type");
                break;
            }
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_direction, tvb, *plen, 1, direction);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_rnti, tvb, *plen, 1, rnti);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_dl_pdu_type, tvb, *plen, 1, pdu_type);
        }
        else if (direction == FAPI_E_UPLINK)
        {
            switch (pdu_type)
            {
            case ULSCH:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case ULSCH_CQI_RI:
                if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code <= 14)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code <= 21)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code > 14) && (sub_err_code <= 19)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code > 21) && (sub_err_code <= 28)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_cqi_ri_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_initial_transmission_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                break;
            case ULSCH_HARQ:
                if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code <= 14)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code <= 21)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code > 14) && (sub_err_code <= 17)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code > 21) && (sub_err_code <= 24)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_harq_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_cqi_ri_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                break;
            case ULSCH_CQI_HARQ_RI:
                if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code <= 14)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code <= 21)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_ulsch_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code > 14) && (sub_err_code <= 19)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code > 21) && (sub_err_code <= 28)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_cqi_ri_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else if (((global_fapi_sdk_parse_lte_release_ver == 8) && (sub_err_code > 19) && (sub_err_code <= 22)) ||
                ((global_fapi_sdk_parse_lte_release_ver == 10) && (sub_err_code > 28) && (sub_err_code <= 31)))
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_harq_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                else
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_initial_transmission_information_sub_error_code, tvb, *plen, 1, sub_err_code);
                }
                break;
            case UCI_CQI:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_cqi_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_SR:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_sr_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_HARQ:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_harq_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_SR_HARQ:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_sr_harq_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_CQI_HARQ:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_cqi_harq_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_CQI_SR:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case UCI_CQI_SR_HARQ:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_uci_cqi_sr_harq_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case SRS:
                proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_srs_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                break;
            case HARQ_BUFFER:
                //proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_harq_buffer_pdu_sub_error_code, tvb, *plen, 1, sub_err_code);
                proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported PDU Type");
                break;
            case ULSCH_UCI_CSI:
            case ULSCH_UCI_HARQ:
            case ULSCH_CSI_UCI_HARQ:
            default:
                proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported PDU Type");
                break;
            }
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_direction, tvb, *plen, 1, direction);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_rnti, tvb, *plen, 1, rnti);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_ul_pdu_type, tvb, *plen, 1, pdu_type);
        }
        break;
    case MSG_HI_ERR:
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_sub_error_code, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_phich_lowest_ul_rb_index, tvb, *plen, 1, val);
        *plen += 1;
        break;
    case MSG_TX_ERR:
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_tx_request_sub_error_code, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_pdu_index, tvb, *plen, 1, val);
        *plen += 1;
        break;
    case MSG_INVALID_STATE:
    case MSG_INVALID_CONFIG:
    case MSG_BCH_MISSING:
        break;
    case GLOB_PARAM_ERR:
        val = tvb_get_guint8(tvb, *plen);
        switch (msg_id)
        {
        case FAPI_E_DL_CONFIG_REQ:
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_dl_config_sub_error_code, tvb, *plen, 1, val);
            break;
        case FAPI_E_UL_CONFIG_REQ:
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_ul_config_sub_error_code, tvb, *plen, 1, val);
            break;
        case FAPI_E_TX_REQ:
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_GLOB_PARAM_ERR_tx_request_sub_error_code, tvb, *plen, 1, val);
            break;
        default:
            proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported Message Type for this Error Code Type");
            break;
        }
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
        *plen += 1;
        break;
    case N_PDU_LIMIT_SURPASSED:
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_sub_error_code, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_received_val, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_limit, tvb, *plen, 1, val);
        *plen += 1;
        if (global_fapi_sdk_parse_vsm_api_ver >= 6)
        {
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_ERROR_IND_N_PDU_LIMIT_rnti_type, tvb, *plen, 1, val);
            *plen += 1;
        }
        else
        {
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
            *plen += 1;
        }
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_DL_PDU_reserved, tvb, *plen, 1, val);
        *plen += 1;
    case START_REQ_RECEIVED_BEFORE_HARD_SYNC:
        break;
    default:
        proto_item_append_text(fapi_sdk_msg_dsc, " Unsupported Error Code Type");
        break;
    }
} // end of dissect_fapi_msg_ERROR_IND
static void
dissect_fapi_msg_VSM_CFG_PARAMS_REP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_item* ei;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_cat_dsc = NULL;
    proto_tree* fapi_sdk_msg_tlv_dsc = NULL;
    guint32 i, j;
    guint32 val;
    guint32 n_tlv;
    guint32 n_cat;
    guint32 cat;
    guint32 fapi_vsm_tlv_tag;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    n_cat = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_CFG_PARAMS_REP_n_cat, tvb, *plen, 1, n_cat);
    *plen += 1;
    val = sci_tvb_get_ntoh24(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_padding3, tvb, *plen, 3, val);
    *plen += 3;
    for (i = 0; i < n_cat; i++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Category[%d]", i);
        fapi_sdk_msg_cat_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        cat = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_cat_dsc, hf_fapi_sdk_msg_CFG_PARAMS_REP_cat, tvb, *plen, 2, cat);
        *plen += 2;
        proto_item_append_text(ti, ": %s", fapi_vsm_cfg_rep_cat_string[cat].strptr);
        n_tlv = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_cat_dsc, hf_fapi_sdk_vsm_header_num_tlvs, tvb, *plen, 2, n_tlv);
        *plen += 2;
        for (j = 0; j < n_tlv; j++)
        {
            ei = proto_tree_add_protocol_format(fapi_sdk_msg_cat_dsc, proto_fapi_sdk, tvb, *plen, 4,
            "TLV[%d]", j);
            fapi_sdk_msg_tlv_dsc = proto_item_add_subtree(ei, ett_fapi_sdk_msg_data_subtree2);
            if (cat == FAPI_E_VSM_CAT_VSM)
            {
                fapi_vsm_tlv_tag = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_vsm_tlv_tag, tvb, *plen, 2, fapi_vsm_tlv_tag);
                *plen += 2;
                //proto_item_append_text(ei, ": %s", fapi_vsm_TLV_tag_string[fapi_vsm_tlv_tag].strptr);
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_vsm_tlv_length, tvb, *plen, 1, val);
                *plen += 1;
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
                *plen += 1;
                switch (fapi_vsm_tlv_tag)
                {
                case CFG_CFI_VSM:
                    dissect_fapi_vsm_CFG_CFI(tvb, pinfo, fapi_sdk_msg_tlv_dsc, plen);
                    break;
                case CFG_ETM_VSM:
                    dissect_fapi_vsm_CFG_ETM(tvb, pinfo, fapi_sdk_msg_tlv_dsc, plen);
                    break;
                case PARAM_RSP_VSM:
                    dissect_fapi_vsm_PARAM_RSP(tvb, pinfo, fapi_sdk_msg_tlv_dsc, plen);
                    break;
                default:
                    expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                        "VSM TAG: Not a configuration TLV Type (%#x)",
                    fapi_vsm_tlv_tag);
                }
            }
            else
            {
                val = sci_tvb_get_ntohs(tvb, *plen);
                if (global_fapi_sdk_parse_lte_release_ver == 10)
                {
                    proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag_rel10, tvb, *plen, 2, val);
                    //proto_item_append_text(ei, ": %s", fapi_msg_data_config_tlv_tag_release10_string[val].strptr);
                }
                else
                {
                    proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_tag, tvb, *plen, 2, val);
                    //proto_item_append_text(ei, ": %s", fapi_msg_data_config_tlv_tag_string[val].strptr);
                }
                *plen += 2;
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_vsm_tlv_length, tvb, *plen, 1, val);
                *plen += 1;
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
                *plen += 1;
                val = sci_tvb_get_ntohl(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_tlv_dsc, hf_fapi_sdk_msg_CFG_PARAMS_REP_fapi_tlv_value, tvb, *plen, 4, val);
                *plen += 4;
            }
        }
    }// end of for each TLV
} // end of dissect_fapi_msg_CONFIG_RSP
static void
dissect_fapi_msg_VSM_MEAS_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* sfn_sf = NULL;
    proto_tree* fapi_sdk_msg_hdr_sfn_sf = NULL;
    gint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_VSM_MEAS");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_avg_ni, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant0_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant1_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant2_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant3_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant0_gain, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant1_gain, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant2_gain, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_MEAS_IND_ant3_gain, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_msg_VSM_NI_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* sfn_sf = NULL;
    proto_tree* fapi_sdk_msg_hdr_sfn_sf = NULL;
    gint32 val, n_rb, i;
    double off_val;
    double ni_start_val = (-130);
    double ni_del = (double)(1);
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_VSM_NI_IND");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_NI_IND_handle, tvb, *plen, 4, val);
    *plen += 4;
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    n_rb = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_NI_IND_n_rb, tvb, *plen, 2, n_rb);
    *plen += 2;
    for (i = 0; i < n_rb; i++)
    {
        val = tvb_get_guint8(tvb, *plen);
        off_val = api_val_to_lgcl_val(val, ni_start_val, ni_del);
        proto_tree_add_float_format(fapi_sdk_msg_dsc, hf_fapi_sdk_vsm_NI_IND_ni_per_rb, tvb, *plen, 1, (float)off_val, "NI per RB[%u]: %.1f", i, (float)off_val);
        *plen += 1;
    }
}
static void
dissect_fapi_msg_INFRA_DPD_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU_threshold, tvb, *plen, 4, val);
    *plen += 4;
} // end of dissect_fapi_msg_INFRA_DPD_PDU
static void
dissect_fapi_msg_INFRA_TRIGGER_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    //Target SF/SFN:
    proto_item* sfn_sf_pi;
    proto_tree* fapi_sdk_msg_target_sfn_sf = NULL;
    val = sci_tvb_get_ntohs(tvb, *plen);
    sfn_sf_pi = proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf_sfn, tvb, *plen, 2, val);
    fapi_sdk_msg_target_sfn_sf = proto_item_add_subtree(sfn_sf_pi, ett_fapi_sdk_msg_data_subtree2);
    proto_tree_add_item(fapi_sdk_msg_target_sfn_sf, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sfn, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
    proto_tree_add_item(fapi_sdk_msg_target_sfn_sf, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_target_sf, tvb, *plen, 2, !global_fapi_sdk_BIG_ENDIAN);
    *plen += 2;
    //End Target SF/SFN:
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_io_index, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_pattern, tvb, *plen, 4, val);
    *plen += 4;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_bit_interval, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_TRIGGER_PDU_start_time_off, tvb, *plen, 2, val);
    *plen += 2;
} // end of dissect_fapi_msg_INFRA_TRIGGER_PDU
static void
dissect_fapi_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_msg_INFRA_DPD_PDU_valid_bitmap, tvb, *plen, 4, val);
    *plen += 4;
} // end of dissect_fapi_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU
static void
dissect_fapi_msg_VSM_INFRA_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    proto_item* sfn_sf = NULL;
    proto_tree* fapi_sdk_msg_hdr_sfn_sf = NULL;
    gint32 val, i;
    gint8 n_pdu, pdu_type, pdu_size;
    proto_item* ti;
    col_append_str(pinfo->cinfo, COL_INFO, "FAPI_VSM_INFRA_REQ");
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_data);
    dissect_fapi_SFN_SF(tvb, pinfo, fapi_sdk_msg_dsc, plen);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_INFRA_PARAM_REQ_length, tvb, *plen, 2, val);
    *plen += 2;
    n_pdu = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, hf_fapi_sdk_msg_INFRA_PARAM_REQ_n_pdu, tvb, *plen, 1, n_pdu);
    *plen += 1;
    for (i = 0; i < n_pdu; i++)
    {
        pdu_type = tvb_get_guint8(tvb, *plen);
        pdu_size = tvb_get_guint8(tvb, (*plen) + 1);
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "PDU[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_type, tvb, *plen, 1, pdu_type);
        *plen += 1;
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, hf_fapi_sdk_msg_INFRA_PARAM_REQ_pdu_size, tvb, *plen, 1, pdu_size);
        *plen += 1;
        switch (pdu_type)
        {
        case DPD_CONFIG:
            proto_item_append_text(ti, ": (DPD_CONFIG)");
            dissect_fapi_msg_INFRA_DPD_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case TRIGGER_REQUEST:
            proto_item_append_text(ti, ": (TRIGGER_REQUEST)");
            dissect_fapi_msg_INFRA_TRIGGER_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        case UNSOLICITED_ACK_NACK_THRESHOLD:
            proto_item_append_text(ti, ": (UNSOLICITED_ACK_NACK_THRESHOLD)");
            dissect_fapi_msg_INFRA_UNSOLICITED_ACK_NACK_THRESHOLD_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
            break;
        default:
            expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                "INFRA_REQ PDU %u: Illegal PDU Type (%u)",
            i, pdu_type);
        }// end of switch-case (pdu_type)
    }//end of for (n_pdu)
}
static void
dissect_fapi_vsm_RX_PUSCH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_vsm_HARQ_PUCCH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    double snr_start_val = (-20);
    double snr_del = (float)(0.5);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, snr_start_val, snr_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_snr, tvb, *plen, 1, (float)off_val);
    *plen += 1;
}
static void
dissect_fapi_vsm_HARQ_PUSCH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    if (global_fapi_sdk_duplex_mode_val == FDD_RADIO)
    {
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding2, tvb, *plen, 2, val);
        *plen += 2;
    }
    else // TDD
    {
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_vsm_harq_pusch_tdd_ind_bundling, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
        *plen += 1;
    }
}
static void
dissect_fapi_vsm_SR_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    double snr_start_val = (-64);
    double snr_del = (float)(0.5);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, snr_start_val, snr_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_snr, tvb, *plen, 1, (float)off_val);
    *plen += 1;
}
static void
dissect_fapi_vsm_CQI_PUCCH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    double off_val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_vsm_CQI_PUSCH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding2, tvb, *plen, 2, val);
    *plen += 2;
}
static void
dissect_fapi_vsm_RACH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_rach_ind_preamble_id, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_rach_ind_detection_metric, tvb, *plen, 1, val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_vsm_SRS_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    double off_val;
    double rssi_start_val = (-130);
    double rssi_del = (double)(1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rnti, tvb, *plen, 2, val);
    *plen += 2;
    val = tvb_get_guint8(tvb, *plen);
    off_val = api_val_to_lgcl_val(val, rssi_start_val, rssi_del);
    proto_tree_add_float(tree, hf_fapi_sdk_vsm_UL_IND_COMMON_rssi, tvb, *plen, 1, (float)off_val);
    *plen += 1;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
    *plen += 1;
}
static void
dissect_fapi_vsm_PARAM_RSP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val;
    val = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_param_rsp_max_users, tvb, *plen, 1, val);
    *plen += 1;
    val = sci_tvb_get_ntoh24(tvb, *plen);
    proto_tree_add_uint(tree, hf_fapi_sdk_vsm_padding3, tvb, *plen, 3, val);
    *plen += 3;
}
static void
dissect_fapi_vsm_msg(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, int msg_type, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_vsm_header_tree = NULL;
    proto_tree* fapi_sdk_vsm_tlv_dsc = NULL;
    guint16 fapi_vsm_header_num_tlvs;
    guint16 fapi_vsm_tlv_tag;
    guint32 val;
    guint16 tlv;
    fapi_vsm_header_num_tlvs = sci_tvb_get_ntohs(tvb, *plen);
    // We assume that if num tlvs equals to zero - it is a padding zeros added by the Wireshark for small message below 60 bytes
    if (fapi_vsm_header_num_tlvs == 0)
    return;
    ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 4,
    "Vendor Specific Header");
    fapi_sdk_vsm_header_tree = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data);
    /* and some "little" vsm header parsing */
    proto_tree_add_uint(fapi_sdk_vsm_header_tree, hf_fapi_sdk_vsm_header_num_tlvs, tvb, *plen, 2, fapi_vsm_header_num_tlvs);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_vsm_header_tree, hf_fapi_sdk_vsm_header_size, tvb, *plen, 2, val);
    *plen += 2;
    /* Looping over all TLVs */
    for (tlv = 0; tlv < fapi_vsm_header_num_tlvs; tlv++)
    {
        ti = proto_tree_add_protocol_format(fapi_sdk_vsm_header_tree, proto_fapi_sdk, tvb, *plen, 8,
        "TLV[%d]", tlv);
        fapi_sdk_vsm_tlv_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        /* Some general TLV parsing */
        fapi_vsm_tlv_tag = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_vsm_tlv_dsc, hf_fapi_sdk_vsm_tlv_tag, tvb, *plen, 2, fapi_vsm_tlv_tag);
        *plen += 2;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_vsm_tlv_dsc, hf_fapi_sdk_vsm_tlv_length, tvb, *plen, 1, val);
        *plen += 1;
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_vsm_tlv_dsc, hf_fapi_sdk_vsm_padding1, tvb, *plen, 1, val);
        *plen += 1;
        switch (fapi_vsm_tlv_tag)
        {
        case RPT_ULSCH_VSM:
            proto_item_append_text(ti, ": (ULSCH)");
            dissect_fapi_vsm_RX_PUSCH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_HARQ_PUCCH_VSM:
            proto_item_append_text(ti, ": (UCI_HARQ)");
            dissect_fapi_vsm_HARQ_PUCCH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_HARQ_PUSCH_VSM:
            if (global_fapi_sdk_duplex_mode_val == FDD_RADIO)
            {
                proto_item_append_text(ti, " (ULSCH_HARQ, FDD)");
            }
            else { // TDD
                proto_item_append_text(ti, " (ULSCH_HARQ, TDD)");
            }
            dissect_fapi_vsm_HARQ_PUSCH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_SR_VSM:
            proto_item_append_text(ti, ": (SR)");
            dissect_fapi_vsm_SR_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_CQI_PUCCH_VSM:
            proto_item_append_text(ti, ": (UCI_CQI)");
            dissect_fapi_vsm_CQI_PUCCH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_CQI_PUSCH_VSM:
            proto_item_append_text(ti, ": (ULSCH_CQI)");
            dissect_fapi_vsm_CQI_PUSCH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_RACH_VSM:
            proto_item_append_text(ti, ": (RACH.ind)");
            dissect_fapi_vsm_RACH_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case RPT_SRS_VSM:
            proto_item_append_text(ti, ": (SRS)");
            dissect_fapi_vsm_SRS_IND(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case CFG_CFI_VSM:
            proto_item_append_text(ti, ": (CFI)");
            dissect_fapi_vsm_CFG_CFI(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case CFG_VERSION_VSM:
            proto_item_append_text(ti, ": (Version)");
            dissect_fapi_vsm_CFG_VERSION(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
            break;
        case CFG_ETM_VSM:
            if (global_fapi_sdk_parse_vsm_api_ver >= 3)
            {
                proto_item_append_text(ti, ": (Test Mode)");
                dissect_fapi_vsm_CFG_ETM(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
                break;
            }
        case PARAM_RSP_VSM:
            if (global_fapi_sdk_parse_vsm_api_ver >= 4)
            {
                proto_item_append_text(ti, ": (PARAM.rsp)");
                dissect_fapi_vsm_PARAM_RSP(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
                break;
            }
        case FAPI_E_CFG_NI_VSM:
            if (global_fapi_sdk_parse_vsm_api_ver >= 5)
            {
                proto_item_append_text(ti, ": (NI.req)");
                dissect_fapi_msg_vsm_UL_NI_TLV(tvb, pinfo, fapi_sdk_vsm_tlv_dsc, plen);
                break;
            }
        default:
            expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
                "VSM TAG: Illegal TLV Type (%#x)",
            fapi_vsm_tlv_tag);
        }
    }
}
void dissect_INFRA_CMD(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 type1;
    guint32 val32;
    guint8 val8;
    guint8 val81;
    guint8 Num1_PDU_TLVs;
    proto_item* ti1;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_item* paramtree3;
    proto_item* paramtree2;
    proto_item* paramtree1;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,"INFRA CMD");
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, infra_sfn, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, infra_slot, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, infra_numerology, tvb, *plen, 1, val8);
    *plen += 1;
    val81 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, infra_numPdu, tvb, *plen, 1, val81);
    *plen += 1;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val81; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,"NUM IMFRA CMD PDU[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, infra_pduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, infra_pduLength, tvb, *plen, 2, val);
        *plen += 2;
        if(type1==5)
        {
            fapi_sdk_msg_dsc3 = proto_item_add_subtree(fapi_sdk_msg_dsc2, ett_fapi_sdk_msg_header);
            paramtree2= proto_tree_add_protocol_format(fapi_sdk_msg_dsc3, proto_fapi_sdk, tvb, *plen, 6,"FRAME TRIGGER CMD");
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_tag, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_length, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_gpio, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_enable, tvb, *plen, 1, val8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_targetSfn, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_targetSlot, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_timingAdvance, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, infra_repeat, tvb, *plen, 1, val8);
            *plen += 1;
        }
        if(type1==4)
        {
        fapi_sdk_msg_dsc4 = proto_item_add_subtree(fapi_sdk_msg_dsc2, ett_fapi_sdk_msg_header);
            paramtree3= proto_tree_add_protocol_format(fapi_sdk_msg_dsc4, proto_fapi_sdk, tvb, *plen, 6,"HARQ FEEDBACK");
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_tag2, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_length2, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_gpio2, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_enable2, tvb, *plen, 1, val8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_timingAdvance2, tvb, *plen, 2, val);
            *plen += 2;
            val32 = sci_tvb_get_ntohl(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_baudrate, tvb, *plen, 4,val32);
            *plen += 4;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_sfn2, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_slot2, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_numerology2, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_numDataBits, tvb, *plen, 1, val8);
            *plen += 1;
            val32 = sci_tvb_get_ntohl(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, infra_data, tvb, *plen, 4,val32);
            *plen += 4;
        }
    }
}
dissect_UCI_INDICATION(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 valx;
    guint16 valw;
    guint16 val_bit;
    guint16 val1;
    guint16 valq;
    guint16 type1;
    guint8 val8;
    float val81;
    guint8 valei8;
    proto_item* ti1;
    proto_item* ti2;
    proto_item* ti3;
    proto_item* ti4;
    proto_item* ti5;
    proto_item* ti6;
    proto_item* ti7;
    proto_item* ti8;
    proto_item* ti9;
    guint8 Num1_PDU_TLVs;
    guint8 Num1_PDU_TLVs1;
    guint8 Num1_PDU_TLVs2;
    guint8 Num1_PDU_TLVs3;
    guint8 Num1_PDU_TLVs4;
    guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_dsc6 = NULL;
    proto_tree* fapi_sdk_msg_dsc7 = NULL;
    proto_tree* fapi_sdk_msg_dsc8 = NULL;
    proto_tree* fapi_sdk_msg_dsc9 = NULL;
    proto_tree* fapi_sdk_msg_dsc10 = NULL;
    proto_tree* fapi_sdk_msg_dsc11 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, uci_sfn, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, uci_slot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, uci_numerology, tvb, *plen, 2, val);
    *plen += 2;
    val1 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, uci_pduCount, tvb, *plen, 2, val1);
    *plen += 2;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val1; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Num_PDU_count[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_pduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_pduLength, tvb, *plen, 2, val);
        *plen += 2;
        if(type1==1)
        {
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_tag, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_length, tvb, *plen, 2, val);
            *plen += 2;
            val_bit = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_pduBitmap, tvb, *plen, 2, val_bit);
            *plen += 2;
            val32 = sci_tvb_get_ntohl(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_handle, tvb, *plen, 4,val32);
            *plen += 4;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_rnti, tvb, *plen, 2, val);
            *plen += 2;
            valei8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc2, uci_numUciPayload, tvb, *plen, 1, valei8);
            *plen += 1;
           for (Num1_PDU_TLVs1 = 0; Num1_PDU_TLVs1 < valei8; Num1_PDU_TLVs1++)
            {
                ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 1,"FAPI_S_UCI_PAYLOAD[%d]", Num1_PDU_TLVs1);
                fapi_sdk_msg_dsc3 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc3, uci_uciPayloadCrcStatus, tvb, *plen, 1, val8);
                *plen += 1;
                valq = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc3, uciPayloadLength, tvb, *plen, 2, valq);
                *plen += 2;
                for (Num1_PDU_TLVs2 = 0; Num1_PDU_TLVs2 < (((valq+7)/8)); Num1_PDU_TLVs2++)
                {
                    ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc3, proto_fapi_sdk, tvb, *plen, 1,"uciBits[%d]", Num1_PDU_TLVs2);
                    fapi_sdk_msg_dsc4 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
                    val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc4, uciBits, tvb, *plen, 1, val8);
                    *plen += 1;
                }
                if( (val_bit!=0) && (val_bit & 1))
                {
                    ti3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 1,"pucchMeasurements");
                    fapi_sdk_msg_dsc5 = proto_item_add_subtree(ti3, ett_fapi_sdk_msg_data_subtree1);
                    val8 = tvb_get_guint8(tvb, *plen);
                    val81=(float)val8/2 -64;
                    proto_tree_add_float(fapi_sdk_msg_dsc5, ulSnr, tvb, *plen, 1, val81);
                    *plen += 1;
                    val = sci_tvb_get_ntohs(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc5, timingAdvance, tvb, *plen, 2, val);
                    *plen += 2;
                }
            }
        }
            if(type1==0)
            {
                ti4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 1,
                "Base UCI PUSCH PDU");
                fapi_sdk_msg_dsc6 = proto_item_add_subtree(ti4, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, pusch_UCI_tag, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, pusch_UCI_length, tvb, *plen, 2, val);
                *plen += 2;
                valx = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, pucch_pduBitmap, tvb, *plen, 2, valx);
                *plen += 2;
                val32 = sci_tvb_get_ntohl(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, pucch_Handle, tvb, *plen, 4,val32);
                *plen += 4;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, pucch_RNTI, tvb, *plen, 2, val);
                *plen += 2;
                ti5 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc6, proto_fapi_sdk, tvb, *plen, 1,
                "HARQ INFO");
                fapi_sdk_msg_dsc7 = proto_item_add_subtree(ti5, ett_fapi_sdk_msg_data_subtree1);
                valw = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc7, pucch1_harqAckBitLength, tvb, *plen, 2, valw);
                *plen += 2;
                valq = tvb_get_guint8(tvb, *plen);
                //valq = (valq+7)/8;
                proto_tree_add_uint(fapi_sdk_msg_dsc7, pucch_harqAckCrcStatus, tvb, *plen, 1, valq);
                *plen += 1;
                for (Num1_PDU_TLVs3 = 0; Num1_PDU_TLVs3 < (((int)valw+7)/8); Num1_PDU_TLVs3++)
                {
                    ti6 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 1,
                    "pucch_harqAckBits[%d]", Num1_PDU_TLVs3);
                    fapi_sdk_msg_dsc8= proto_item_add_subtree(ti6, ett_fapi_sdk_msg_data_subtree1);
                    val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc8, pucch_harqAckBits, tvb, *plen, 1, val8);
                    *plen += 1;
                }
                ti7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc6, proto_fapi_sdk, tvb, *plen, 1,
                "UCI CSI1 INFO");
                fapi_sdk_msg_dsc9 = proto_item_add_subtree(ti7, ett_fapi_sdk_msg_data_subtree1);
                valw = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc9, pucch_csiPart1BitLength, tvb, *plen, 2, valw);
                *plen += 2;
                valq = tvb_get_guint8(tvb, *plen);
                //valq = (valq+7)/8;
                proto_tree_add_uint(fapi_sdk_msg_dsc9, pucch_csiPart1CrcStatus, tvb, *plen, 1, valq);
                *plen += 1;
                for (Num1_PDU_TLVs4 = 0; Num1_PDU_TLVs4 < (((int)valw+7)/8); Num1_PDU_TLVs4++)
                {
                    ti8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc9, proto_fapi_sdk, tvb, *plen, 1,
                    "csiPart1Bits[%d]", Num1_PDU_TLVs4);
                    fapi_sdk_msg_dsc10 = proto_item_add_subtree(ti8, ett_fapi_sdk_msg_data_subtree1);
                    val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc10, pucch_csiPart1Bits, tvb, *plen, 1, val8);
                    *plen += 1;
                }
                ti7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc6, proto_fapi_sdk, tvb, *plen, 1,
                "UCI CSI2 INFO");
                fapi_sdk_msg_dsc9 = proto_item_add_subtree(ti7, ett_fapi_sdk_msg_data_subtree1);
                valw = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc9, pucch_csiPart2BitLength, tvb, *plen, 2, valw);
                *plen += 2;
                valq = tvb_get_guint8(tvb, *plen);
                //valq = (valq+7)/8;
                proto_tree_add_uint(fapi_sdk_msg_dsc9, pucch_csiPart2CrcStatus, tvb, *plen, 1, valq);
                *plen += 1;
                for (Num1_PDU_TLVs4 = 0; Num1_PDU_TLVs4 < (((int)valw+7)/8); Num1_PDU_TLVs4++)
                {
                    ti8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc9, proto_fapi_sdk, tvb, *plen, 1,
                    "csiPart2Bits[%d]", Num1_PDU_TLVs4);
                    fapi_sdk_msg_dsc10 = proto_item_add_subtree(ti8, ett_fapi_sdk_msg_data_subtree1);
                    val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc10, pucch_csiPart2Bits, tvb, *plen, 1, val8);
                    *plen += 1;
                }
                if(valx!=0)
                {
                    ti9 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc6, proto_fapi_sdk, tvb, *plen, 1,
                    "DMRS PDU");
                    fapi_sdk_msg_dsc11 = proto_item_add_subtree(ti9, ett_fapi_sdk_msg_data_subtree1);
                    valq = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc11, pusch_ulSnr, tvb, *plen, 1, valq);
                    *plen += 1;
                    val = sci_tvb_get_ntohs(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc11, pusch_timingAdvance, tvb, *plen, 2, val);
                    *plen += 2;
                }
            }
    }
}
dissect_FAPI_ULSCH_DECODE_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 val_bitmap;
    guint16 val1;
    guint16 valq;
    guint16 type1;
    guint8 val8;
    proto_item* ti1;
    proto_item* ti2;
    float vale8;
    guint8 Num1_PDU_TLVs;
    guint8 Num1_PDU_TLVs1;
    guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_item* paramtree1;
    proto_item* paramtree2;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
    "ULSCH_DECODE_IND");
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, desfn, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, deslot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, denumerology, tvb, *plen, 2, val);
    *plen += 2;
    val1 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, depduCount, tvb, *plen, 1, val1);
    *plen += 2;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val1; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Num_PDU_TLVs[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, depduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, depduLength, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, detag, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, delength, tvb, *plen, 2, val);
        *plen += 2;
        val_bitmap = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, depduBitmap, tvb, *plen, 2, val_bitmap);
        *plen += 2;
        val32 = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, dehandle, tvb, *plen, 4,val32);
        *plen += 4;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, dernti, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, deharqId, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, detbCrcStatus, tvb, *plen, 1, val8);
        *plen += 1;
        valq = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, denumCb, tvb, *plen, 2, valq);
        *plen += 2;
        for (Num1_PDU_TLVs1 = 0; Num1_PDU_TLVs1 < valq; Num1_PDU_TLVs1++)
        {
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
            "decbCrcStatus[%d]", Num1_PDU_TLVs1);
            fapi_sdk_msg_dsc3 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc3, decbCrcStatus, tvb, *plen, 1, val8);
            *plen += 1;
        }
        if(val_bitmap!=0)
        {
            fapi_sdk_msg_dsc4 = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
            paramtree2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc4, proto_fapi_sdk, tvb, *plen, 3,
            "ULSCH DMRS");
            vale8 = tvb_get_guint8(tvb, *plen);
            vale8=(float)vale8/2 -64;
            proto_tree_add_float(fapi_sdk_msg_dsc4, deulSnr, tvb, *plen, 1, vale8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, detimingAdvance, tvb, *plen, 2, val);
            *plen += 2;
        }
    }
}
dissect_RACH_IND(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    guint16 val16;
    guint16 pdu_count = 0, i = 0, pdu_type = 0, pdu_length = 0;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Prach_sfn, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Prach_slot, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Prach_numerology, tvb, *plen, 2, val16);
    *plen += 2;
    pdu_count = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Prach_pdu_count, tvb, *plen, 2, pdu_count);
    *plen += 2;
    for (i = 0; i < pdu_count; i++)
    {
        pdu_type = sci_tvb_get_ntohs(tvb, *plen);
        pdu_length = sci_tvb_get_ntohs(tvb, (*plen)+2);
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "pdu[%d]", i);
        fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, prach_pdu_type, tvb, *plen, 2, pdu_type);
        *plen += 2;
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, prch_pdu_length, tvb, *plen,2, pdu_length);
        *plen += 2;
        switch (pdu_type)
        {
            case 0:
                proto_item_append_text(ti, ": (PRACH)");
                dissect_UL_PRACH_PDU(tvb, pinfo, fapi_sdk_msg_pdu_dsc, plen);
                break;
            default:
                expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,"UL PDU %u: Illegal PDU Type (%u)",i, pdu_type);
        }
    }
}
dissect_FAPI_ERROR_INDICATION(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 temp_len;
    guint16 val_error_type;
    guint16 val_type;
    guint8 val8;
    guint8 vale8;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_dsc6 = NULL;
    proto_item* paramtree1;
    proto_item* paramtree2;
    proto_item* paramtree3;
    proto_item* paramtree4;
    proto_item* paramtree5;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,
    "FAPI_T_ERROR_IND_PDU ");
    fapi_sdk_msg_dsc3 = proto_item_add_subtree(paramtree1, ett_fapi_sdk_msg_data_subtree1);
    *plen -= 2;
    temp_len = sci_tvb_get_ntohs(tvb, *plen);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, erSFN, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, erSlot, tvb, *plen, 2, val);
    *plen += 2;
    val_type = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, ermsgId, tvb, *plen, 2, val_type);
    *plen += 2;
    vale8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, ererrorCode, tvb, *plen, 1, vale8);
    *plen += 1;
    if(temp_len>ERROR_LENGTH)
    {
        switch(vale8)
        {
            case 3:
            {
                paramtree2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 8,
                "FAPI_T_SFN_ERR ");
                fapi_sdk_msg_dsc4 = proto_item_add_subtree(paramtree2, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc4, target_sfn, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc4, current_sfn, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc4, target_slot, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc4, current_slot, tvb, *plen, 2, val);
                *plen += 2;
            }
            break;
            case 4:
            {
                paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,
                "FAPI_T_MSG_ERR ");
                fapi_sdk_msg_dsc2 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc2, direction, tvb, *plen, 1, val8);
                *plen += 1;
                val = sci_tvb_get_ntohs(tvb, *plen);
                if(val_type==0x140)
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc2, pdu_type, tvb, *plen, 2, val);
                }
                else if(val_type==0x142)
                {
                    proto_tree_add_uint(fapi_sdk_msg_dsc2, pdu_type1, tvb, *plen, 2, val);
                }
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc2, errnti, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc2, pdu_sub_type, tvb, *plen, 2, val);
                *plen += 2;
                val = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc2, sub_error_code, tvb, *plen, 2, val);
                *plen += 2;
                /*val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc17, direction, tvb, *plen, 1, val8);
                    *plen += 1;
                */
            }
            break;
            case 2:
            {
                paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 0,
                "FAPI_E_MSG_CONFIG_TYPE ");
                fapi_sdk_msg_dsc5 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, pdu_index_error, tvb, *plen, 1, val8);
                *plen += 1;
                val_error_type = sci_tvb_get_ntohs(tvb, *plen);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                if(val==5)
                    {
                     proto_tree_add_uint(fapi_sdk_msg_dsc5, duplicate_error, tvb, *plen, 2, val);
                    }
                else
                    {
                       if(val_type==0x140)
                        {
                            proto_tree_add_uint(fapi_sdk_msg_dsc5, pdu_type_error, tvb, *plen, 2, val_error_type);
                        }
                        else if(val_type==0x142)
                        {
                            proto_tree_add_uint(fapi_sdk_msg_dsc5, pdu_type_error1, tvb, *plen, 2, val_error_type);
                        }
                    }
                proto_tree_add_uint(fapi_sdk_msg_dsc5, error_code, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, error_value, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, expected_value, tvb, *plen, 2, val);
                *plen += 2;
            }
            break;
            case 1:
            {
                paramtree5 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 0,
                "FAPI_E_MSG_INVALID_INVALID_STATE");
                fapi_sdk_msg_dsc6 = proto_item_add_subtree(paramtree5, ett_fapi_sdk_msg_data_subtree1);
            }
            break;
        }
    }
}

dissect_FAPI_PRACH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* subtree;
    proto_tree* fapi_sdk_msg_for_subtree = NULL;
    guint16 val16;
    guint8  val8;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    subtree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 15,
    "PrachCmd ");
    fapi_sdk_msg_for_subtree = proto_item_add_subtree(subtree, ett_fapi_sdk_msg_data_subtree1);
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_tag, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_length, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prachConfigIdx, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_phys_cell_id, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_format, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_nRa_Slot_T, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_nRa, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_start_symbol, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_ncs, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_for_subtree, prach_digital_bf_info, tvb, *plen, 2, val16);
    *plen += 2;
}
dissect_FAPI_PUSCH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val32;
    guint16 val;
    guint32 tmep=1;
    guint16 val_bit;
    guint8 checkop1=0;
    guint8 checkop2=0;
    guint8 checkop3=0;
    guint8 checkop4=0;
    //guint16 val16;
    guint16 i;
    guint16 i1;
    guint16 val1;
    guint16 val13;
    //guint16 val1;
    guint8 val8;
    guint8 val81;
    guint8 val18;
    guint8 exponent;
    //guint8 val81;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc134 = NULL;
    proto_tree* fapi_sdk_msg_dsc17 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_tree* fapi_sdk_msg_dsc19 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc37 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc21 = NULL;
    proto_tree* fapi_sdk_msg_dsc22 = NULL;
    proto_tree* fapi_sdk_msg_dsc23 = NULL;
    proto_tree* fapi_sdk_msg_dsc24 = NULL;
    proto_tree* fapi_sdk_msg_dsc25 = NULL;
    proto_tree* fapi_sdk_msg_dsc26 = NULL;
    proto_tree* fapi_sdk_msg_dsc30 = NULL;
    proto_tree* fapi_sdk_msg_dsc31 = NULL;
    proto_tree* fapi_sdk_msg_dsc32 = NULL;
    proto_tree* fapi_sdk_msg_dsc33 = NULL;
    proto_tree* fapi_sdk_msg_dsc34 = NULL;
    proto_tree* fapi_sdk_msg_dsc35 = NULL;
    proto_tree* fapi_sdk_msg_dsc36 = NULL;
    proto_tree* fapi_sdk_msg_dsc38 = NULL;
    proto_tree* fapi_sdk_msg_dsc39 = NULL;
    proto_tree* fapi_sdk_msg_dsc40 = NULL;
    // proto_item *ti;
    // proto_item *ti1;
    proto_item* ti2;
    proto_item* ti3;
    proto_item* paramtree7;
    proto_item* paramtree8;
    // proto_item *paramtree4;
    proto_item* paramtree9;
    proto_item* paramtree10;
    proto_item* paramtree11;
    proto_item* paramtree12;
    proto_item *paramtree13;
    proto_item *paramtree14;
    proto_item *paramtree15;
    // proto_item *paramtree16;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
    "PuschCmd ");
    fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, length, tvb, *plen, 2, val);
    *plen += 2;
    val_bit = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, upduBitmap, tvb, *plen, 2, val_bit);
    *plen += 2;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, uHandle, tvb, *plen, 4,val32);
    *plen += 4;
    paramtree8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 20,
    "bwp ");
    fapi_sdk_msg_dsc19 = proto_item_add_subtree(paramtree8, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, ulocation, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, ubandwidth, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, ucyclicPrefix, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, uqamModOrder, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, uinitialCodeRate, tvb, *plen, 2, val);
    *plen += 2;
    paramtree10 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PuschDataScrambling ");
    fapi_sdk_msg_dsc31 = proto_item_add_subtree(paramtree10, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc31, unRnti, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc31, ulDataScramblingId, tvb, *plen, 2, val);
    *plen += 2;
    paramtree9 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PuschDmrs");
    fapi_sdk_msg_dsc30 = proto_item_add_subtree(paramtree9, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, ulDmrsSymPositions, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, ulDmrsConfigType, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, ulDmrsScramblingId, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, unScid, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, ulDmrsCdmGroupsNoData, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, ulDmrsPorts, tvb, *plen, 2, val);
    *plen += 2;
    paramtree11 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PuschAllocation ");
    fapi_sdk_msg_dsc32 = proto_item_add_subtree(paramtree11, ett_fapi_sdk_msg_data_subtree1);
    for (i = 0; i < 36; i++)
    {
        ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc32, proto_fapi_sdk, tvb, *plen, 1,
        "rbBitmap[%d]", i);
        fapi_sdk_msg_dsc25 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc25, urbBitmap, tvb, *plen, 1, val8);
        *plen += 1;
    }
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, ustartSymbol, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, usymbolLength, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, ufreqHoppingEnable, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, utxDirectCurrentLocation, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, uuplinkFrequencyShift7p5khz, tvb, *plen, 1, val8);
    *plen += 1;
    if(val_bit & (1<<0))
    {
        checkop1=1;
    }
    if(val_bit & (1<<1))
    {
        checkop2=1;
    }
    if(val_bit & (1<<2))
    {
        checkop3=1;
    }
    if(val_bit & (1<<3))
    {
        checkop4=1;
    }
    if(((val_bit)>>(0)) & 1)
    {
        paramtree12 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
        "puschdecoder");
        fapi_sdk_msg_dsc33 = proto_item_add_subtree(paramtree12, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc33, puschrvIndex, tvb, *plen, 1, val8);
        *plen += 1;
        val32 = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(fapi_sdk_msg_dsc33, tbSize, tvb, *plen, 4, val32,"tbSize: %d",val32);
        *plen += 4;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc33, harqNum, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc33, isReTx, tvb, *plen, 1, val8);
        *plen += 1;
        val13 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc33, numCb, tvb, *plen, 2, val13);
        *plen += 2;
        for (i = 0; i <val13/8 ; i++)
        {
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc33, proto_fapi_sdk, tvb, *plen, 1,
            "cbPresentAndPosition[%d]", i);
            fapi_sdk_msg_dsc25 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, cbPresentAndPosition, tvb, *plen, 1, val8);
            *plen += 1;
        }
    }
    if(((val_bit)>>(1)) & 1)
    {
        paramtree13 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 1,
        "puschUci");
        fapi_sdk_msg_dsc34 = proto_item_add_subtree(paramtree13, ett_fapi_sdk_msg_data_subtree1);
        fapi_sdk_msg_dsc134 = proto_item_add_subtree(paramtree13, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, harqAckBitLength, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, onPuschScalingAlpha, tvb, *plen, 1, val8);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, betaOffsetHarqAck, tvb, *plen, 1, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, betaOffsetCsi1, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, betaOffsetCsi2, tvb, *plen, 1, val8);
        *plen += 1;
        val81 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, csiPart2Indicator, tvb, *plen, 1, val81);
        *plen += 1;
        val1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, numCsiReports , tvb, *plen, 2, val1);
        *plen += 2;
        for (i = 0; i < val1; i++)
        {
            tmep=1;
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc34, proto_fapi_sdk, tvb, *plen, 1,
            "cbPresentAndPosition[%d]", i);
            fapi_sdk_msg_dsc25 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, csiPart1BitLength, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, riOffset, tvb, *plen, 1, val8);
            *plen += 1;
            val18 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, riBitwidth, tvb, *plen, 1, val18);
            *plen += 1;
            if(val81==1)
            {
                for (exponent=val18; exponent>0; exponent--)
                {
                    tmep = tmep * 2;
                }
                for (i1 = 0; i1 <tmep; i1++)
                {
                    ti3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc25, proto_fapi_sdk, tvb, *plen, 1,
                    "csiPart2Bits[%d]", i1);
                    fapi_sdk_msg_dsc26 = proto_item_add_subtree(ti3, ett_fapi_sdk_msg_data_subtree1);
                    val = sci_tvb_get_ntohs(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_dsc26, csiPart2BitLength, tvb, *plen, 2, val);
                    *plen += 2;
                }
            }
        }
    }
    if(((val_bit)>>(2)) & 1)
    {
        paramtree14 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
        "puschPtrs");
        fapi_sdk_msg_dsc35 = proto_item_add_subtree(paramtree14, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs0ReOffset, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs1ReOffset, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs0DmrsPort, tvb, *plen, 1, val8);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs0DmrsAssoc, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs1DmrsPort, tvb, *plen, 1, val8);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrs1DmrsAssoc, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrsFrequencyDensity, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrsTimeDensity, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc35, ulPtrsPower, tvb, *plen, 1, val8);
        *plen += 1;
    }
    if(((val_bit)>>(3)) & 1)
    {
        paramtree15 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 5,
        "dftsOfdmParams");
        fapi_sdk_msg_dsc36 = proto_item_add_subtree(paramtree15, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc36, lowPaprGroupNumber, tvb, *plen, 1, val8);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc36, lowPaprSequenceNumber, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc36, ulPtrsPreDftDensity, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc36, ulPtrsTimeDensityTransformPrecoding, tvb, *plen, 1, val8);
        *plen += 1;
    }
}
static void
dissect_FAPI_HARQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint8 val8;
    guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_item* paramtree7;
    paramtree7 = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 10,"HARQ CMD ");
    fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
    fapi_sdk_msg_dsc = proto_item_add_subtree(fapi_sdk_msg_dsc18, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_length, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_gpio, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_enable, tvb, *plen, 1, val8);
    *plen += 1;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_baudrate, tvb, *plen, 4, val32);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_sfn, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_slot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_numerology, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_numDataBits, tvb, *plen, 1, val8);
    *plen += 1;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, HARQ_data, tvb, *plen, 4, val32);
    *plen += 4;
}
static void
dissect_FAPI_PUCCH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint8 val8;
    guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_item* paramtree8;
    proto_item* paramtree7;
    paramtree7 = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, 10,"PucchCmd ");
    fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
    fapi_sdk_msg_dsc = proto_item_add_subtree(fapi_sdk_msg_dsc18, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_length, tvb, *plen, 2, val);
    *plen += 2;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_handle, tvb, *plen, 4, val32);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_nRnti, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_multiSlotTxIndicator, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, upucchFormat, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, upucchGroupHopping, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_hoppingId, tvb, *plen, 2, val);
    *plen += 2;
    paramtree8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 20,
    "bwp ");
    fapi_sdk_msg_dsc1 = proto_item_add_subtree(paramtree8, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc1, location, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc1, bandwidth, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc1, cyclicPrefix, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_startSymbol, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_numSyms, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_interSlotFreqHopEnable, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_intraSlotFreqHopEnable, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_nPucchRb, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_startingPrb, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_secondHopPrb, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_detectType, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_harqAckBitLength, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_initialCyclicShift, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_txDirectCurrentLocation, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_orthoSeqIndexFormat1, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucch_nId, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, scramblingId0, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, occLengthFormat4, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, occIndexFormat4, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucchPayloadLengthBits, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pucchPayloadLenCSIPart2, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, pi2Bpsk, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, additionalDmrs, tvb, *plen, 1, val8);
    *plen += 1;
}
static void
dissect_FAPI_ULSCH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 val_tag;
    guint16 val_len;
    //guint16 val1;
    //guint16 npdval;
    // guint16 i;
    //guint16 val16;
    //guint16 type1;
    guint8 val8;
    //guint8 Num1_PDU_TLVs;
    guint32 val32;
    guint32 i;
    //guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* ti1;
    proto_item* paramtree16;
    proto_item* paramtree17;
    //proto_item *ti2;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc21 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc16 = NULL;
    proto_tree* fapi_sdk_msg_dsc17 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_tree* fapi_sdk_msg_dsc19 = NULL;
    proto_tree* fapi_sdk_msg_dsc22 = NULL;
    proto_tree* fapi_sdk_msg_dsc23 = NULL;
    proto_tree* fapi_sdk_msg_dsc24 = NULL;
    proto_tree* fapi_sdk_msg_dsc25 = NULL;
    proto_tree* fapi_sdk_msg_dsc26 = NULL;
    proto_tree* fapi_sdk_msg_dsc27 = NULL;
    proto_tree* fapi_sdk_msg_dsc28 = NULL;
    proto_tree* fapi_sdk_msg_dsc29 = NULL;
    proto_tree* fapi_sdk_msg_dsc30 = NULL;
    proto_tree* fapi_sdk_msg_dsc31 = NULL;
    proto_tree* fapi_sdk_msg_dsc32 = NULL;
    proto_tree* fapi_sdk_msg_dsc33 = NULL;
    proto_tree* fapi_sdk_msg_dsc34 = NULL;
    proto_tree* fapi_sdk_msg_dsc35 = NULL;
    proto_tree* fapi_sdk_msg_dsc36 = NULL;
    proto_tree* fapi_sdk_msg_dsc37 = NULL;
    proto_tree* fapi_sdk_msg_dsc38 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val_tag = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, rxtag, tvb, *plen, 2, val_tag);
    *plen += 2;
    if(val_tag == 8624)
    {
        paramtree16 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
        "PADDING TAG PDU");
        fapi_sdk_msg_dsc37 = proto_item_add_subtree(paramtree16, ett_fapi_sdk_msg_data_subtree1);
        val_len = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc37, rx_length1, tvb, *plen, 2, val_len);
        *plen += 2;
        *plen += val_len;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, rx_base_uldsch, tvb, *plen, 2, val);
        *plen += 2;
    }
    paramtree17 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
    "Base ULSCH Data PDU");
    fapi_sdk_msg_dsc38 = proto_item_add_subtree(paramtree17, ett_fapi_sdk_msg_data_subtree1);
    val_len = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc38, rx_length2, tvb, *plen, 2, val_len);
    *plen += 2;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc38, rxhandle, tvb, *plen, 4, val32);
    *plen += 4;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc38, rxrnti, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc38, rxharqId, tvb, *plen, 2, val);
    *plen += 2;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc38, rxpayloadSize, tvb, *plen, 4, val32);
    *plen += 4;
    for (i = 0; i < val32; i++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc38, proto_fapi_sdk, tvb, *plen, 1,
        "payload[%d]", i);
        fapi_sdk_msg_dsc21 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc21, rxpayload, tvb, *plen, 1, val8);
        *plen += 1;
    }
}
static void
dissect_RX_DATA_MESSAGE(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 vale;
    //guint16 val1;
    //guint16 npdval;
    // guint16 i;
    //guint16 val16;
    guint16 type1;
    guint8 Num1_PDU_TLVs;
    //guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* ti1;
    //proto_item *ti2;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, rxSFN, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, rxSlot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, rxNumerology, tvb, *plen, 2, val);
    *plen += 2;
    vale = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, rxPDUcount, tvb, *plen, 2, vale);
    *plen += 2;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < vale; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Num_PDU_TLVs[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, rxpduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, rxpduLength, tvb, *plen, 2, val);
        *plen += 2;
        switch (type1)
        {
            case 0:
            dissect_FAPI_ULSCH(tvb, pinfo, tree, plen);
            //todo
            //dissect fapi prach
            break;
            case 1:
            break;
            case 2:
            //todo
            //dissect fapi pucch
            break;
            case 3:
            //todo
            //dissect fapi srs
            break;
            default:
            {
                //todo
            }
        }
    }
}
static void
dissect_RX_CONTROL_MESSAGE(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    //guint16 val1;
    //guint16 npdval;
    // guint16 i;
    //guint16 val16;
    guint16 type1;
    guint8 val8;
    guint8 Num1_PDU_TLVs;
    //guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* ti1;
    //proto_item *ti2;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    /*val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, tag, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, length, tvb, *plen, 2, val);
        *plen += 2;
    */
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, SFN, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Slot, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Numerology, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Num_PDU_TLVs, tvb, *plen, 1, val8);
    *plen += 1;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val8; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Num_PDU_TLVs[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, pduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, pduLength, tvb, *plen, 2, val);
        *plen += 2;
        switch (type1)
        {
            case 0:
            dissect_FAPI_PRACH(tvb, pinfo, tree, plen);
            break;
            case 1:
            dissect_FAPI_PUSCH(tvb, pinfo, tree, plen);
            break;
            case 2:
            dissect_FAPI_PUCCH(tvb,pinfo,tree,plen);
            break;
            case 4:
            dissect_FAPI_HARQ(tvb,pinfo,tree,plen);
            //dissect fapi srs
            break;
            default:
            {
                //todo
            }
        }
    }
}
static void
dissect_FAPI_SSB(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val16;
    guint16 i;
    guint8 val8;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc21 = NULL;
    proto_tree* fapi_sdk_msg_dsc37 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_item* paramtree16;
    proto_item* ti1;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree16 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
    "SSB");
    fapi_sdk_msg_dsc37 = proto_item_add_subtree(paramtree16, ett_fapi_sdk_msg_data_subtree1);
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, physCellId, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, betaPss, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, ssbIndex, tvb, *plen, 1, val8);
    *plen += 1;
    for (i = 0; i < 3; i++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc37, proto_fapi_sdk, tvb, *plen, 1,
        "bchpayload[%d]", i);
        fapi_sdk_msg_dsc21 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc21, bchPayload, tvb, *plen, 1, val8);
        *plen += 1;
    }
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, msbOfKssbInSspbchTypeA, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, ssbCrbOffset, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, ssbSubCarrierOffset, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, digBfMethod, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, digBfCyclicDelay, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, digBfBeamIndex, tvb, *plen, 2, val16);
    *plen += 2;
}
static void
dissect_FAPI_CSIRS(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    // guint16 val;
    guint16 j=0;
    guint16 val16;
    guint8 val8;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc37 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_item *paramtree4;
    proto_item* paramtree16;
    guint16 valcycle;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree16 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 10,
    "CsiRsCmd ");
    fapi_sdk_msg_dsc37 = proto_item_add_subtree(paramtree16, ett_fapi_sdk_msg_data_subtree1);
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, nScid, tvb, *plen, 2, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, type, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, startRb, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, numRbs, tvb, *plen, 1, val16);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, Density, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, cdmType, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, betaCsirsSs, tvb, *plen, 1, val8);
    *plen += 1;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, freqStartLocation, tvb, *plen, 2, val16);
    *plen += 2;
    val16 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, timeStartLocation, tvb, *plen, 2, val16);
    *plen += 2;
    valcycle = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc37, nCsiRsPorts , tvb, *plen, 1, valcycle);
    *plen += 1;
    j=0;
    for (j = 0; j < valcycle; j++)
    {
        paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc37, proto_fapi_sdk, tvb, *plen, 2,
        "digBfIndices[%d]", j);
        fapi_sdk_msg_dsc20 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_data_subtree1);
        val16 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc20, digBfIndices, tvb, *plen, 2, val16);
        *plen += 2;
    }
}
static void
dissect_FAPI_PDCCH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint8 val8;
    //guint8 pval8;
    guint16 valcycle;
    guint16 npdval;
    guint16 i;
    guint j=0;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc21 = NULL;
    proto_tree* fapi_sdk_msg_dsc22 = NULL;
    proto_tree* fapi_sdk_msg_dsc23 = NULL;
    proto_tree* fapi_sdk_msg_dsc24 = NULL;
    proto_tree* fapi_sdk_msg_dsc25 = NULL;
    proto_item* ti1;
    proto_item* ti2;
    proto_item* paramtree4;
    proto_item* paramtree5;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,
    "CoresetCmd");
    fapi_sdk_msg_dsc2 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_header);
    paramtree5 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 6,
    "BWP");
    fapi_sdk_msg_dsc15 = proto_item_add_subtree(paramtree5, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, location, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, bandwidth, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, cyclicPrefix, tvb, *plen, 1, val8);
    *plen += 1;
    for (i = 0; i < 6; i++)
    {
        ti1 = proto_tree_add_protocol_format(paramtree4, proto_fapi_sdk, tvb, *plen, 1,
        "frequencyDomainResources[%d]", i);
        fapi_sdk_msg_dsc21 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc21, frequencyDomainResources, tvb, *plen, 1, val8);
        *plen += 1;
    }
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, startSymbol, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, duration, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, regBundleSize, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, cceRegMappingType, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, interleaverSize, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(paramtree4, shiftIndex, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(paramtree4, nId1, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, precoderGranularity, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(paramtree4, dmrsSubcarrier0Reference, tvb, *plen, 1, val8);
    *plen += 1;
    npdval = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(paramtree4, nPdcch, tvb, *plen, 2, npdval);
    *plen += 2;
    for (i = 0; i < npdval; i++)
    {
        ti1 = proto_tree_add_protocol_format(paramtree4, proto_fapi_sdk, tvb, *plen, 2,
        "pdcch[%d]", i);
        fapi_sdk_msg_dsc22 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, rnti, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, nRnti, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, cceIndex, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, aggegrationLevel, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, payloadSizeInBits, tvb, *plen, 1, val8);
        *plen += 1;
        j=0;
        for (j = 0; j < 20; j++)
        {
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc22, proto_fapi_sdk, tvb, *plen, 2,
            "payload[%d]", j);
            fapi_sdk_msg_dsc23 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc22, payload, tvb, *plen, 1, val);
            *plen += 1;
        }
        val = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, betaPdcch, tvb, *plen, 1, val);
        *plen += 1;
        valcycle = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, cycleLength, tvb, *plen, 1, valcycle);
        *plen += 1;
        j=0;
        for (j = 0; j < valcycle; j++)
        {
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc22, proto_fapi_sdk, tvb, *plen, 2,
            "precoderIndices[%d]", j);
            fapi_sdk_msg_dsc24 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc24, precoderIndices, tvb, *plen, 2, val);
            *plen += 2;
        }
        valcycle = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc22, nCsiRsPorts , tvb, *plen, 1, valcycle);
        *plen += 1;
        j=0;
        for (j = 0; j < valcycle; j++)
        {
            ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc22, proto_fapi_sdk, tvb, *plen, 2,
            "digBfIndices[%d]", j);
            fapi_sdk_msg_dsc25 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, digBfIndices, tvb, *plen, 2, val);
            *plen += 2;
        }
    }
}
dissect_FAPI_PDSCH(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 val32;
    guint16 val;
    guint16 val16;
    guint16 i;
    //  guint16 val1;
    guint8 val8;
    guint8 val81;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc17 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_tree* fapi_sdk_msg_dsc19 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc37 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc25 = NULL;
    proto_tree* fapi_sdk_msg_dsc30 = NULL;
    proto_tree* fapi_sdk_msg_dsc31 = NULL;
    proto_tree* fapi_sdk_msg_dsc32 = NULL;
    proto_tree* fapi_sdk_msg_dsc33 = NULL;
    proto_tree* fapi_sdk_msg_dsc34 = NULL;
    proto_tree* fapi_sdk_msg_dsc35 = NULL;
    proto_tree* fapi_sdk_msg_dsc36 = NULL;
    proto_tree* fapi_sdk_msg_dsc38 = NULL;
    proto_tree* fapi_sdk_msg_dsc39 = NULL;
    proto_tree* fapi_sdk_msg_dsc40 = NULL;
    // proto_item *ti;
    // proto_item *ti1;
    proto_item* ti2;
    proto_item* paramtree7;
    proto_item* paramtree8;
    // proto_item *paramtree4;
    proto_item* paramtree9;
    proto_item* paramtree10;
    proto_item* paramtree11;
    proto_item* paramtree12;
    //   proto_item *paramtree13;
    proto_item *paramtree14;
    proto_item *paramtree15;
    // proto_item *paramtree16;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 6,
    "PdschCmd ");
    fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
    /*val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, pduBitmap, tvb, *plen, 2, val);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, pduIndex, tvb, *plen, 2, val);
        *plen += 2;
    */
    paramtree8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 20,"bwp ");
    fapi_sdk_msg_dsc19 = proto_item_add_subtree(paramtree8, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, location, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, bandwidth, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc19, cyclicPrefix, tvb, *plen, 1, val8);
    *plen += 1;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint_format(fapi_sdk_msg_dsc18, tbSizeBytes, tvb, *plen, 4, val32, "tbSizeBytes: %d", val32);
    *plen += 4;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint_format(fapi_sdk_msg_dsc18, tbSizeLbrmBytes, tvb, *plen, 4, val32, "tbSizeLbrmBytes:%d", val32);
    *plen += 4;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, rvIndex, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, targetCodeRate, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, qamModOrder, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, dlDataPduIndex, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, dmrsSeqGenRefPoint, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc18, betaPdsch, tvb, *plen, 1, val8);
    *plen += 1;
    paramtree9 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PdschDmrs ");
    fapi_sdk_msg_dsc30 = proto_item_add_subtree(paramtree9, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, dlDmrsSymbPos, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, dlDmrsConfigType, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, dlDmrsScramblingId, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, yonScid, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, numDmrsCdmGrpsNoData, tvb, *plen, 1, val8);
    *plen += 1;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc30, dmrsPorts, tvb, *plen, 2, val);
    *plen += 2;
    paramtree10 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PdschDataScrambling ");
    fapi_sdk_msg_dsc31 = proto_item_add_subtree(paramtree10, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc31, nId, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc31, nRnti, tvb, *plen, 2, val);
    *plen += 2;
    paramtree11 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PdschAllocation ");
    fapi_sdk_msg_dsc32 = proto_item_add_subtree(paramtree11, ett_fapi_sdk_msg_data_subtree1);
    for (i = 0; i < 36; i++)
    {
        ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc32, proto_fapi_sdk, tvb, *plen, 1,
        "rbBitmap[%d]", i);
        fapi_sdk_msg_dsc25 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc25, rbBitmap, tvb, *plen, 1, val8);
        *plen += 1;
    }
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, startSymbol, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, symbolLength, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc32, vrbToPrbMapping, tvb, *plen, 1, val8);
    *plen += 1;
    paramtree15 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "PDSCH PTRS ");
    fapi_sdk_msg_dsc40 = proto_item_add_subtree(paramtree15, ett_fapi_sdk_msg_data_subtree1);
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc40, dlPtrsPresent, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc40, dlPtrsTimeDensity, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc40, dlPtrsFreqDensity, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc40, dlPtrsReOffset, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc40, betaPtrs, tvb, *plen, 1, val8);
    *plen += 1;
    paramtree12 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen, 6,
    "Digital beam forming ");
    fapi_sdk_msg_dsc33 = proto_item_add_subtree(paramtree12, ett_fapi_sdk_msg_data_subtree1);
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc33, prgSize, tvb, *plen, 1, val8);
    *plen += 1;
    val81 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc33, precoderCycleLength, tvb, *plen, 1, val81);
    *plen += 1;
    for (i = 0; i < val81; i++)
    {
        ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc33, proto_fapi_sdk, tvb, *plen, 2,
        "precoderIndices[%d]", i);
        fapi_sdk_msg_dsc34 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
        val16 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, precoderIndices, tvb, *plen, 2, val16);
        *plen += 2;
    }
    val81 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc33, nCsiRsPorts, tvb, *plen, 1, val81);
    *plen += 1;
    for (i = 0; i < val81; i++)
    {
        ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc33, proto_fapi_sdk, tvb, *plen, 2,
        "digBfIndices[%d]", i);
        fapi_sdk_msg_dsc34 = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
        val16 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc34, digBfIndices, tvb, *plen, 2, val16);
        *plen += 2;
    }
    paramtree14 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc18, proto_fapi_sdk, tvb, *plen,6,"CbgRetransmitControl ");
    fapi_sdk_msg_dsc36 = proto_item_add_subtree(paramtree14, ett_fapi_sdk_msg_data_subtree1);
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc36, getDlTbCrc, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc36, IsLastCbPresent, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc36, isInlineTbCrc, tvb, *plen, 1, val8);
    *plen += 1;
    val32 = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint_format(fapi_sdk_msg_dsc36, dlTbCrc, tvb, *plen, 4, val32,"dlTbCrc: %d",val32);
    *plen += 4;
}
static void
dissect_TX_DATA_MESSAGE(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 val1;
    //guint16 val1;
    //guint16 npdval;
    // guint16 i;
    guint32 val32;
    guint32 pay;
    guint8 val8;
    guint32 length1[1024];
    guint8 Num1_PDU_TLVs;
    guint32 Num1_PDU_TLVs1;
    //guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* ti1;
    //proto_item *ti2;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_item* paramtree1;
    proto_item* paramtree3;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, SFN, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Slot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Numerology, tvb, *plen, 2, val);
    *plen += 2;
    paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 0,
    "FAPI_T_TX_DATA_MSG_HDR_TLV");
    fapi_sdk_msg_dsc3 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
    val1 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, PDUcount, tvb, *plen, 2, val1);
    *plen += 2;
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 0,
    "TX_DATA body");
    fapi_sdk_msg_dsc1 = proto_item_add_subtree(paramtree1, ett_fapi_sdk_msg_data_subtree1);
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val1; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 4,
        "PDU[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, dlDataPduIndex, tvb, *plen, 2, val);
        *plen += 2;
        val32 = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint_format(fapi_sdk_msg_dsc2, dlDataPduLength, tvb, *plen, 4, val32,"dlDataPduLength: %d",val32);
        *plen += 4;
        length1[Num1_PDU_TLVs]=val32;
    }
        for(pay=0;pay<Num1_PDU_TLVs;pay++)
            {
        for (Num1_PDU_TLVs1 = 0; Num1_PDU_TLVs1 < length1[pay]; Num1_PDU_TLVs1++)
        {
            ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
            "dlDataPduBytes[%d][%d]",pay, Num1_PDU_TLVs1);
            fapi_sdk_msg_dsc4 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc4, dlDataPduBytes, tvb, *plen, 1, val8);
            *plen += 1;
        }
            }
    }
static void
dissect_TX_CONTROL_MESSAGE(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    //guint16 val1;
    //guint16 npdval;
    // guint16 i;
    //guint16 val16;
    guint16 type1;
    guint8 val8;
    guint8 Num1_PDU_TLVs;
    //guint32 val32;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_item* ti1;
    //proto_item *ti2;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, SFN, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Slot, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Numerology, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, Num_PDU_TLVs, tvb, *plen, 1, val8);
    *plen += 1;
    for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < val8; Num1_PDU_TLVs++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 4,
        "Num_PDU_TLVs[%d]", Num1_PDU_TLVs);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        type1 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, pduType, tvb, *plen, 2, type1);
        *plen += 2;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, pduLength, tvb, *plen, 2, val);
        *plen += 2;
        switch (type1)
        {
            case 0:
            dissect_FAPI_PDCCH(tvb, pinfo, tree, plen);
            break;
            case 1:
            dissect_FAPI_SSB(tvb, pinfo, tree, plen);
            break;
            case 2:
            dissect_FAPI_PDSCH(tvb, pinfo, tree, plen);
            break;
            case 3:
            dissect_FAPI_CSIRS(tvb, pinfo, tree, plen);
            break;
            default:
            {
                //todo
            }
        }
    }
}
static void
dissect_SLOT_INDI(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, sfn, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, slot, tvb, *plen, 2, val);
    *plen += 2;
}
static void
dissect_PHY_STOP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, hell_val,"RB Bitmap 1: 0x%.8x",val);
    //*plen += 4;
    switch (val)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
    }
    *plen += 2;
}
static void
dissect_PHY_START(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = sci_tvb_get_ntohs(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, hell_val,"RB Bitmap 1: 0x%.8x",val);
    //proto_tree_add_uint(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 2, val);
    switch (val)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val);
        break;
        case MSG_INVALID_CONFIG:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val);
        break;
    }
    *plen += 2;
}
static void
dissect_PHY_CONFIG_TLV(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint8 val8;
    guint8 valq8;
    guint8 qval8;
    guint8 i,q;
    proto_item* paramtree4;
    proto_item* paramtree3;
    proto_item* ti;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc14 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc7 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc6 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
    "COnfig tlv");
    fapi_sdk_msg_dsc15 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, length, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, timingInfoMode, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc15, timingInfoPeriod, tvb, *plen, 1, val8);
    *plen += 1;
    paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 11,
    "prachConfig");
    fapi_sdk_msg_dsc14 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
    val8 = tvb_get_guint8(tvb, *plen);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc14, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc14, length, tvb, *plen, 2, val);
    *plen += 2;
    valq8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_float(fapi_sdk_msg_dsc14, nPrachConfigs, tvb, *plen, 1, valq8);
    *plen += 1;
    for(q= 0 ;q <valq8 ; q++)
    {
        paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc14, proto_fapi_sdk, tvb, *plen, 4,
        "prachConfig[%d]",q);
        fapi_sdk_msg_dsc5 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, prachSequenceLength, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, deltaFRa, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, restrictedSetConfig, tvb, *plen, 1, val8);
        *plen += 1;
        qval8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, numPrachFdOccasions, tvb, *plen, 1, qval8);
        *plen += 1;
        for (i = 0; i < qval8; i++)
        {
            ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc5, proto_fapi_sdk, tvb, *plen, 13,
            "numPrachFdOccasions[%d]", i);
            fapi_sdk_msg_pdu_dsc6 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc6, prachRootSequenceIndex, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc6, numRootSequences, tvb, *plen, 1, val8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc6, k1, tvb, *plen, 2, val);
            *plen += 2;
            /*
            wval8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_pdu_dsc6, numUnusedRootSequences, tvb, *plen, 1, wval8);
            *plen += 1;
            for (i = 0; i < wval8; i++)
            {
                ti = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc6, proto_fapi_sdk, tvb, *plen, 1,
                "numUnusedRootSequences[%d]", i);
                fapi_sdk_msg_pdu_dsc7 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc7, unusedRootSequences, tvb, *plen, 1, val8);
                *plen += 1;
            }
            */
        }
    }
}
static void
dissect_PHY_CONFIG_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint8 val;
    guint16 num_invalidTLV;
    guint16 num_msiingTLV;
    guint16 num_configuredintit;
    guint16 num_configuredcellstop;
    guint8 val8;
    guint8 val81=0;
    guint8 val82=0;
    guint8 val83=0;
    guint8 val84=0;
    proto_item* ti1;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val = tvb_get_guint8(tvb, *plen);;
    //proto_tree_add_uint(fapi_sdk_msg_dsc, phy_Error_Code, tvb, *plen, 2, val);
    //*plen += 2;
    switch (val)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
        case MSG_INVALID_CONFIG:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, val);
        break;
    }
    if(val != MSG_INVALID_STATE)
    {
            *plen += 1;
            val81 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, Number_of_invalid_TLVs, tvb, *plen, 1, val81);
            *plen += 1;
            val82 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, Number_of_missing_TLVs, tvb, *plen, 1, val82);
            *plen += 1;
            val83 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, Number_configured_init, tvb, *plen, 1, val83);
            *plen += 1;
            val84 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc, Number_configured_cell_stopped, tvb, *plen, 1, val84);
            *plen += 1;
        }
    for (num_invalidTLV = 0; num_invalidTLV < val81; num_invalidTLV++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
        "invalidTLV[%d]", num_invalidTLV);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, phy_conf_invalidTLV, tvb, *plen, 1, val8);
        *plen += 1;
    }
    for (num_msiingTLV = 0; num_msiingTLV < val82; num_msiingTLV++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
        "msiingTLV[%d]", num_msiingTLV);
        fapi_sdk_msg_dsc3 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc3, phy_conf_msiingTLV, tvb, *plen, 1, val8);
        *plen += 1;
    }
    for (num_configuredintit = 0; num_configuredintit < val83; num_configuredintit++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
        "configuredintit[%d]", num_configuredintit);
        fapi_sdk_msg_dsc4 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc4, phy_conf_configuredintit, tvb, *plen, 1, val8);
        *plen += 1;
    }
    for (num_configuredcellstop = 0; num_configuredcellstop < val84; num_configuredcellstop++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
        "configuredcellstop[%d]", num_configuredcellstop);
        fapi_sdk_msg_dsc5 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, phy_conf_configuredcellstop, tvb, *plen, 1, val8);
        *plen += 1;
    }
}
static void
dissect_PHY_CONFIG_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 nval;
    gint16 valInt16;
    gint8 valq18;
    guint8 val8;
    guint8 q;
    guint8 num_nCells;
    guint8 n1Cells;
    guint8 valq8;
    guint8 num_PrachFdOccasions;
    guint8 num1PrachFdOccasions;
    proto_item* paramtree1;
    proto_item* ti;
    proto_item* paramtree2;
    proto_item* paramtree3;
    proto_item* paramtree10;
    proto_tree* fapi_sdk_msg_pdu_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_dsc10 = NULL;
    proto_tree* fapi_sdk_msg_dsc9 = NULL;
    proto_tree* fapi_sdk_msg_dsc11 = NULL;
    proto_tree* fapi_sdk_msg_dsc14 = NULL;
    proto_tree* prachConfig_tree = NULL;
    proto_tree* Timing_Info_tlv_tree = NULL;
    proto_tree* numPrachFdOccasions_tree = NULL;
    proto_tree* numUnusedRootSequences_tree = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    nval = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, phy_Num_TLVs, tvb, *plen, 2, nval);
    *plen += 2;
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 11,
    "Carrier config TLV");
    prachConfig_tree = proto_item_add_subtree(paramtree1, ett_fapi_sdk_msg_data_subtree1);
    val8 = tvb_get_guint8(tvb, *plen);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(prachConfig_tree, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(prachConfig_tree, length, tvb, *plen, 2, val);
    *plen += 2;
    num_nCells = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(prachConfig_tree, nCells, tvb, *plen, 1, num_nCells);
    *plen += 1;
    for (n1Cells = 0; n1Cells < num_nCells; n1Cells++)
    {
        ti = proto_tree_add_protocol_format(prachConfig_tree, proto_fapi_sdk, tvb, *plen, 7,
        "cellConfig[%d]", n1Cells);
        fapi_sdk_msg_pdu_dsc1 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        paramtree10 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc1, proto_fapi_sdk, tvb, *plen, 7,
        "cellConfig ");
        fapi_sdk_msg_dsc9 = proto_item_add_subtree(paramtree10, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc9, phyCellId, tvb, *plen, 2, val);
        *plen += 2;
        valq8 = tvb_get_guint8(tvb, *plen);
        valq18 = (gint8) valq8;
        proto_tree_add_uint(fapi_sdk_msg_dsc9, nPrachConfigs, tvb, *plen, 1, valq18);
        *plen += 1;
        for(q= 0 ;q <valq18 ; q++)
        {
            paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc9, proto_fapi_sdk, tvb, *plen, 4,
            "prachConfig[%d]",q);
            fapi_sdk_msg_dsc5 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc5, prachSequenceLength, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc5, deltaFRa, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc5, restrictedSetConfig, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc5, cp1, tvb, *plen, 1, val8);
            *plen += 1;
            num_PrachFdOccasions = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc5, numPrachFdOccasions, tvb, *plen, 1, num_PrachFdOccasions);
            *plen += 1;
            for (num1PrachFdOccasions = 0; num1PrachFdOccasions < num_PrachFdOccasions; num1PrachFdOccasions++)//To-DO: Once TestApp is coorected. It should be also corrected
            {
                ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc5, proto_fapi_sdk, tvb, *plen, 6,
                "numPrachFdOccasions[%d]", num1PrachFdOccasions);
                fapi_sdk_msg_pdu_dsc2 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc2, prachRootSequenceIndex, tvb, *plen, 2, val);
                *plen += 2;
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc2, numRootSequences, tvb, *plen, 1, val8);
                *plen += 1;
                val = sci_tvb_get_ntohs(tvb, *plen);
                valInt16 = (gint16)val;
                proto_tree_add_int(fapi_sdk_msg_pdu_dsc2, k1, tvb, *plen, 2, valInt16);
                *plen += 2;
            }
        }
    }
    paramtree2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 1,
    "Timing Info tlv");
    Timing_Info_tlv_tree = proto_item_add_subtree(paramtree2, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(Timing_Info_tlv_tree, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(Timing_Info_tlv_tree, length, tvb, *plen, 2, val);
    *plen += 2;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(Timing_Info_tlv_tree, timingInfoMode, tvb, *plen, 1, val8);
    *plen += 1;
    val8 = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(Timing_Info_tlv_tree, timingInfoPeriod, tvb, *plen, 1, val8);
    *plen += 1;
}
static void
dissect_PHY_CAP_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 val;
    guint16 val_type;
    //guint16 nval;
    //guint16 val16;
    //guint16 i;
    guint16 num_phy_Num_TLVs;
    guint16 numphy_Num_TLVs;
    guint8 val8;
    proto_item* ti1;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    //proto_tree *fapi_sdk_msg_dsc1 = NULL;
    proto_tree* TLV_tree = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    val_type = sci_tvb_get_ntohs(tvb, *plen);
    //proto_tree_add_uint(fapi_sdk_msg_dsc, phy_Error_Code, tvb, *plen, 2, val);
    //*plen += 2;
    switch (val_type)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val_type);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val_type);
        break;
        case MSG_INVALID_CONFIG:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val_type);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, val_type);
        break;
    }
    *plen += 2;
    if(val_type!=1)
    {
        num_phy_Num_TLVs = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc, phy_Num_TLVs, tvb, *plen, 2, num_phy_Num_TLVs);
        *plen += 2;
        for (numphy_Num_TLVs = 0; numphy_Num_TLVs < num_phy_Num_TLVs; numphy_Num_TLVs++)
        {
            ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
            "TLV[%d]", numphy_Num_TLVs);
            TLV_tree = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(TLV_tree, tag, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(TLV_tree, length, tvb, *plen, 2, val);
            *plen += 2;
           val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(TLV_tree, timingInfoMode, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(TLV_tree, timingInfoPeriod, tvb, *plen, 1, val8);
            *plen += 1;
        }
    }
}
static void
dissect_STOP_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 error_cod;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    //proto_tree *fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    error_cod = sci_tvb_get_ntohl(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, error_cod,"RB Bitmap 1: 0x%.8x",error_cod);
    *plen += 4;
    switch (error_cod)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
    }
}
static void
dissect_STOP_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint32 error_cod;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    //proto_tree *fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    error_cod = sci_tvb_get_ntohl(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, error_cod,"RB Bitmap 1: 0x%.8x",error_cod);
    *plen += 4;
    switch (error_cod)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
    }
}
static void
dissect_START_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint16 error_cod;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    //proto_tree *fapi_sdk_msg_dsc1 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    error_cod = sci_tvb_get_ntohs(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, error_cod,"RB Bitmap 1: 0x%.8x",error_cod);
    *plen += 2;
    switch (error_cod)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, error_cod);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, error_cod);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 2, error_cod);
        break;
    }
}
static void
dissect_CONFIG_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    //guint8 val8;
    guint8 num_respnCarriers;
    //guint16 val;
    guint16 numrespnCarriers;
    guint16 val16;
    //guint8 n_comb;
    //guint16 blah_val;
    guint16 error_cod;
    proto_item* ti1;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    error_cod = sci_tvb_get_ntohs(tvb, *plen);
    //proto_tree_add_uint_format(fapi_sdk_msg_dsc, pnf_start_resp_error_codes, tvb, *plen, 4, hell_val,"RB Bitmap 1: 0x%.8x",hell_val);
    *plen += 2;
    switch (error_cod)
    {
        case MSG_OK:
        //proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        case MSG_INVALID_STATE:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
        default:
        proto_tree_add_uint(fapi_sdk_msg_dsc, config_resp_msg_ok, tvb, *plen, 1, error_cod);
        break;
    }
    num_respnCarriers = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc, respnCarriers, tvb, *plen, 1, num_respnCarriers);
    *plen += 1;
    for (numrespnCarriers = 0; numrespnCarriers < num_respnCarriers; numrespnCarriers++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "txDirectCurrentLocation[%d]", numrespnCarriers);
        fapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val16 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc2, txDirectCurrentLocation, tvb, *plen, 2, val16);
        *plen += 2;
    }
}
static void
dissect_CONFIG_REQ(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint8 val8;
    gint8 val18;
    guint valq8;
    guint valq18;
    guint8 num_PhyConfig;
    guint16 num1PhyConfig;
    guint16 i;
    guint16 q;
    guint16 vali1;
    guint8 j;
    guint8 vali8;
    guint8 k;
    guint8 valk8;
    guint8 num_nCarriers;
    guint8 num_nCells;
    guint16 n1Cells;
    guint16 n1Carriers;
    guint8 numdownlinkFrequency;
    guint8 numdownlinkGridStart;
    guint8 numuplinkFrequency;
    guint8 numuplinkGridStart;
    guint8 num_PrachFdOccasions;
    guint8 num1PrachFdOccasions;
    guint16 val;
    gint16 valInt16;
    guint32 val32;
    proto_item* ti;
    proto_item* ti1;
    proto_item* ti2;
    proto_item* ti3;
    proto_item* paramtree;
    proto_item* paramtree1;
    proto_item* paramtree2;
    proto_item* paramtree4;
    proto_item* paramtree5;
    proto_item* paramtree6;
    proto_item* paramtree3;
    proto_item* paramtree10;
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc_carriera = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_dsc4 = NULL;
    proto_tree* fapi_sdk_msg_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_dsc6 = NULL;
    proto_tree* fapi_sdk_msg_dsc7 = NULL;
    proto_tree* fapi_sdk_msg_dsc8 = NULL;
    proto_tree* fapi_sdk_msg_dsc9 = NULL;
    proto_tree* fapi_sdk_msg_dsc10 = NULL;
    proto_tree* fapi_sdk_msg_dsc11 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc2 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc3 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc4 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc5 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc6 = NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
    "TLV");
    ti1 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
    fapi_sdk_msg_dsc1 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
    paramtree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 20,
    "PHY Config Req");
    fapi_sdk_msg_dsc2 = proto_item_add_subtree(paramtree, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc2, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc2, length, tvb, *plen, 2, val);
    *plen += 2;
    num_PhyConfig = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc2, numPhyConfig, tvb, *plen, 1, num_PhyConfig);
    *plen += 1;
    for (num1PhyConfig = 0; num1PhyConfig < num_PhyConfig; num1PhyConfig++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc2, proto_fapi_sdk, tvb, *plen, 5,
        "numpphy[%d]", num1PhyConfig);
        fapi_sdk_msg_dsc7 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc7, phyId, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc7, pnfPhyIndex, tvb, *plen, 1, val8);
        *plen += 1;
    }
    paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 4,
    "CarrierConfig");
    fapi_sdk_msg_dsc3 = proto_item_add_subtree(paramtree1, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, tag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, length, tvb, *plen, 2, val);
    *plen += 2;
    num_nCarriers = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc3, nCarriers, tvb, *plen, 1, num_nCarriers);
    *plen += 1;
    for (n1Carriers = 0; n1Carriers < num_nCarriers; n1Carriers++)
    {
        ti1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc3, proto_fapi_sdk, tvb, *plen, 1,
        "CarrierConfig[%d]", n1Carriers);
        fapi_sdk_msg_dsc8 = proto_item_add_subtree(ti1, ett_fapi_sdk_msg_data_subtree1);
        ti3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc8, proto_fapi_sdk, tvb, *plen, 1,"CellCarrierConfig");
        fapi_sdk_msg_dsc11 = proto_item_add_subtree(ti3, ett_fapi_sdk_msg_data_subtree1);
        num_nCells = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc11, nCells, tvb, *plen, 1, num_nCells);
        *plen += 1;
        for (n1Cells = 0; n1Cells < num_nCells; n1Cells++)
        {
            ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 7,
            "cellConfig[%d]", n1Cells);
            fapi_sdk_msg_pdu_dsc1 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
            paramtree10 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc1, proto_fapi_sdk, tvb, *plen, 7,
            "cellConfig ");
            fapi_sdk_msg_dsc9 = proto_item_add_subtree(paramtree10, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc9, phyCellId, tvb, *plen, 2, val);
            *plen += 2;
            valq8 = tvb_get_guint8(tvb, *plen);
            valq18 = (gint8) valq8;
            proto_tree_add_uint(fapi_sdk_msg_dsc9, nPrachConfigs, tvb, *plen, 1, valq18);
            *plen += 1;
            for(q= 0 ;q <valq18 ; q++)
            {
                paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc9, proto_fapi_sdk, tvb, *plen, 4,
                "prachConfig[%d]",q);
                fapi_sdk_msg_dsc5 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, prachSequenceLength, tvb, *plen, 1, val8);
                *plen += 1;
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, deltaFRa, tvb, *plen, 1, val8);
                *plen += 1;
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, restrictedSetConfig, tvb, *plen, 1, val8);
                *plen += 1;
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, cp1, tvb, *plen, 1, val8);
                *plen += 1;
                num_PrachFdOccasions = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc5, numPrachFdOccasions, tvb, *plen, 1, num_PrachFdOccasions);
                *plen += 1;
                for (num1PrachFdOccasions = 0; num1PrachFdOccasions < num_PrachFdOccasions; num1PrachFdOccasions++)//To-DO: Once TestApp is coorected. It should be also corrected
                {
                    ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc5, proto_fapi_sdk, tvb, *plen, 6,
                    "numPrachFdOccasions[%d]", num1PrachFdOccasions);
                    fapi_sdk_msg_pdu_dsc2 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
                    val = sci_tvb_get_ntohs(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc2, prachRootSequenceIndex, tvb, *plen, 2, val);
                    *plen += 2;
                    val8 = tvb_get_guint8(tvb, *plen);
                    proto_tree_add_uint(fapi_sdk_msg_pdu_dsc2, numRootSequences, tvb, *plen, 1, val8);
                    *plen += 1;
                    val = sci_tvb_get_ntohs(tvb, *plen);
                    valInt16 = (gint16)val;
                    proto_tree_add_int(fapi_sdk_msg_pdu_dsc2, k1, tvb, *plen, 2, valInt16);
                    *plen += 2;
                }
            }
        }
        ti2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc8, proto_fapi_sdk, tvb, *plen, 6,
        "Carrier Config A");
        fapi_sdk_msg_dsc_carriera = proto_item_add_subtree(ti2, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc_carriera, downlinkBandwidth, tvb, *plen, 2, val);
        *plen += 2;
        val32 = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc_carriera, downlinkFrequency, tvb, *plen, 4, val32);
        *plen += 4;
        for (numdownlinkFrequency = 0; numdownlinkFrequency < 5; numdownlinkFrequency++)
        {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc_carriera, proto_fapi_sdk, tvb, *plen, 2,
        "downlinkGridStart[%d]", numdownlinkFrequency);
        fapi_sdk_msg_pdu_dsc2 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc2, downlinkGridStart, tvb, *plen, 2, val);
        *plen += 2;
        }
        for (numdownlinkGridStart = 0; numdownlinkGridStart < 5; numdownlinkGridStart++)
        {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc_carriera, proto_fapi_sdk, tvb, *plen, 2,
        "downlinkGridSize[%d]", numdownlinkGridStart);
        fapi_sdk_msg_pdu_dsc3 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc3, downlinkGridSize, tvb, *plen, 2, val);
        *plen += 2;
        }
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc_carriera, uplinkBandwidth, tvb, *plen, 2, val);
        *plen += 2;
        val32 = sci_tvb_get_ntohl(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc_carriera, uplinkFrequency, tvb, *plen, 4, val32);
        *plen += 4;
        for (numuplinkFrequency = 0; numuplinkFrequency < 5; numuplinkFrequency++)
        {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc_carriera, proto_fapi_sdk, tvb, *plen, 2,
        "uplinkGridStart[%d]", numuplinkFrequency);
        fapi_sdk_msg_pdu_dsc4 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc4, uplinkGridStart, tvb, *plen, 2, val);
        *plen += 2;
        }
        for (numuplinkGridStart = 0; numuplinkGridStart < 5; numuplinkGridStart++)
        {
        ti = proto_tree_add_protocol_format(fapi_sdk_msg_dsc_carriera, proto_fapi_sdk, tvb, *plen, 2,
        "uplinkGridSize[%d]", numuplinkGridStart);
        fapi_sdk_msg_pdu_dsc5 = proto_item_add_subtree(ti, ett_fapi_sdk_msg_data_subtree1);
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_pdu_dsc5, uplinkGridSize, tvb, *plen, 2, val);
        *plen += 2;
        }
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc_carriera, uplinkFrequencyShift7p5khz, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        val18=(gint8)val8;
        proto_tree_add_int(fapi_sdk_msg_dsc_carriera, referenceSsPbchPower, tvb, *plen, 1, val18);
        *plen += 1;
    }
    paramtree2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 6,
    "Digital Beam Config TLV");
    fapi_sdk_msg_dsc4 = proto_item_add_subtree(paramtree2, ett_fapi_sdk_msg_data_subtree1);
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc4, BFtag, tvb, *plen, 2, val);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc4, BFlength, tvb, *plen, 2, val);
    *plen += 2;
    vali1 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(fapi_sdk_msg_dsc4, BFnumEntries, tvb, *plen, 2, vali1);
    *plen += 2;
    for(i=0;i<vali1;i++)
    {
        paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc4, proto_fapi_sdk, tvb, *plen, 2,
        "Digital Beam Config[%d]",i);
        fapi_sdk_msg_dsc5 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_data_subtree1);
        vali8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc5, numRows, tvb, *plen, 1, vali8);
        *plen += 1;
        for(j=0;j<vali8;j++)
        {
            paramtree5 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc5, proto_fapi_sdk, tvb, *plen, 2,
            "Digital Beam Row[%d]",j);
            fapi_sdk_msg_dsc10 = proto_item_add_subtree(paramtree5, ett_fapi_sdk_msg_data_subtree1);
            valk8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc10, numColumns, tvb, *plen, 1, valk8);
            *plen += 1;
            for(k=0;k<valk8;k++)
            {
                paramtree6 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc10, proto_fapi_sdk, tvb, *plen, 4,
                "Digital Beam Values[%d][%d]",j,k);
                fapi_sdk_msg_dsc6 = proto_item_add_subtree(paramtree6, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, digBeamValReal, tvb, *plen, 2, val);
                *plen += 2;
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_dsc6, digBeamValImag, tvb, *plen, 2, val);
                *plen += 2;
            }
        }
    }
}
static void
dissect_PNF_RESP(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
    guint8 val8;
    gint8 val8_neg;
    guint16 val;
    guint16 error_cod;
    guint16 val2;
    guint16 received = 0;
    guint16 expected = 0;
    /*n_comb are the 16 bit variables*/
    guint16 n_comb;
    guint16 n_comb11;
    guint16 n_comb12;
    guint16 n_comb2;
    guint16 n_comb13;
    guint16 n_comb3;
    guint16 n_comb14;
    guint16 n_comb4;
    guint16 n_comb5;
    guint16 n_comb6;
    guint16 n_comb7;
    guint16 num_support_comb;
    guint16 num_nbands;
    guint16 num_featureSetsDownlinkPerCc;
    guint16 num_nFeatureSetsDownlink;
    guint16 num_featureSetDownlinkPerCcIds;
    guint16 num_nFeatureSetsUplinkPerCc;
    guint16 num_nFeatureSetsUplink;
    guint16 num_UfeatureSetUplinkCcIds;
    guint16 num_nFeatureSetPerBand;
    guint16 num_nFeatureSetCombinations;
    guint16 num_featureSetPerBandIds;
    guint16 num_maxPucchsPerSlot;
    guint16 num_betaPdcchRange;
    guint16 num_P2;
    guint16 num_N31;
    guint16 num_N32;
    guint16 num_K3;
    guint16 num_N41;
    guint16 num_N42;
    guint16 num_P1;
    guint16 num_K4;
    guint16 numtlv;
    guint16 i=0;
    //guint16 msg_header_val;
    proto_item* TLV_tree;
    proto_item* TLV_subtree;
    proto_item* paramtree;
    proto_item* paramtree1;
    proto_item* paramtree2;
    proto_item* paramtree3;
    proto_item* paramtree4;
    proto_item* paramtree5;
    proto_item* paramtree6;
    proto_item* paramtree7;
    proto_item* paramtree8;
    proto_item* paramtree9;
    proto_item* paramtree10;
    proto_item* paramtree11;
    proto_item* paramtree12;
    proto_item* paramtree13;
    proto_item* paramtree14;
    proto_item* paramtree15;
    proto_item* paramtree16;
    proto_item* paramtree17;
    proto_item* paramtree18;
    proto_item* paramtree19;
    proto_item* paramtree20;
    proto_item* paramtree21;
    proto_item* paramtree22;
    proto_item* paramtree23;
    proto_item* paramtree24;
    proto_item* support_comb_tree;
    proto_item* nbands_tree;
    proto_item* featureSetsDownlinkPerCc_tree;
    proto_item* nFeatureSetsDownlink_tree;
    proto_item* featureSetDownlinkPerCcIds_tree;
    proto_item* nFeatureSetsUplinkPerCc_tree;
    proto_item* nFeatureSetsUplink_tree;
    proto_item* UfeatureSetUplinkCcIds_tree;
    proto_item* nFeatureSetPerBand_tree;
    proto_item* nFeatureSetCombinations_tree;
    proto_item* featureSetPerBandIds_tree;
    proto_item* num_betaPdcchRange_tree;
    proto_item* num_betaPdschRange_tree;
    proto_item* maxPucchsPerSlot_tree;
    proto_item* N31_tree;
    proto_item* P1_tree;
    proto_item* P2_tree;
    proto_item* P3_tree;
    proto_item* N32_tree;
    proto_item* K3_tree;
    proto_item* N42_tree;
    proto_item* N41_tree;
    proto_item* K4_tree;
    /*fapi_sdk_msg_dsc are the list of different subtrees*/
    proto_tree* fapi_sdk_msg_dsc = NULL;
    proto_tree* fapi_sdk_msg_dsc11 = NULL;
    proto_tree* fapi_sdk_msg_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_dsc12 = NULL;
    proto_tree* fapi_sdk_msg_dsc13 = NULL;
    proto_tree* fapi_sdk_msg_dsc14 = NULL;
    proto_tree* fapi_sdk_msg_dsc15 = NULL;
    proto_tree* fapi_sdk_msg_dsc16 = NULL;
    proto_tree* fapi_sdk_msg_dsc17 = NULL;
    proto_tree* fapi_sdk_msg_dsc18 = NULL;
    proto_tree* fapi_sdk_msg_dsc19 = NULL;
    proto_tree* fapi_sdk_msg_dsc20 = NULL;
    proto_tree* fapi_sdk_msg_dsc21 = NULL;
    proto_tree* fapi_sdk_msg_dsc22 = NULL;
    proto_tree* fapi_sdk_msg_dsc23 = NULL;
    proto_tree* fapi_sdk_msg_dsc24 = NULL;
    proto_tree* fapi_sdk_msg_dsc25 = NULL;
    proto_tree* fapi_sdk_msg_dsc26 = NULL;
    proto_tree* fapi_sdk_msg_dsc27 = NULL;
    proto_tree* fapi_sdk_msg_dsc28 = NULL;
    proto_tree* fapi_sdk_msg_dsc29 = NULL;
    proto_tree* fapi_sdk_msg_dsc30 = NULL;
    proto_tree* fapi_sdk_msg_dsc31 = NULL;
    proto_tree* fapi_sdk_msg_dsc32 = NULL;
    proto_tree* fapi_sdk_msg_dsc33 = NULL;
    proto_tree* fapi_sdk_msg_dsc34 = NULL;
    proto_tree* fapi_sdk_msg_dsc35 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc1 = NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc_beta= NULL;
    proto_tree* fapi_sdk_msg_pdu_dsc_betas= NULL;
    fapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_fapi_sdk_msg_header);
    error_cod = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, PNF_CAPABILITY_RSP_error_code, tvb, *plen, 2, error_cod);
    *plen += 2;
    switch (error_cod)
    {
        case MSG_OK:
        proto_item_append_text(fapi_sdk_msg_dsc, " Message is OK");
        break;
        case MSG_INVALID_STATE:
        proto_item_append_text(fapi_sdk_msg_dsc, " Message recieved in an invalied state");
        break;
    }
    val2 = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, PNF_CAPABILITY_RSP_num_TLV, tvb, *plen, 2, val2);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, pnf_tag, tvb, *plen, 2, val2);
    *plen += 2;
    val = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(tree, pnf_length, tvb, *plen, 2, val2);
    *plen += 2;
    for (numtlv = 0; numtlv < val2; numtlv++)
    {
        TLV_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc, proto_fapi_sdk, tvb, *plen, 2,
        "TLV[%d]", numtlv);
        TLV_subtree = proto_item_add_subtree(TLV_tree, ett_fapi_sdk_msg_data_subtree1);
        fapi_sdk_msg_dsc1 = proto_item_add_subtree(TLV_subtree, ett_fapi_sdk_msg_data_subtree1);
        /*val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc1, pnftype, tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc1, pnfsize, tvb, *plen, 2, val);
            *plen += 2;
            expected = val;
        */
        paramtree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 61,
        "PHYParameters");
        fapi_sdk_msg_dsc11 = proto_item_add_subtree(paramtree, ett_fapi_sdk_msg_data_subtree1);
        paramtree1 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 3,
        "ssPbch");
        fapi_sdk_msg_dsc12 = proto_item_add_subtree(paramtree1, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc12, ssPbchMultipleCarriersInABand, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc12, multipleCellSsPbchsInACarrier, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc12, ssPbchDigitalBfTechniques, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        paramtree2 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 6,
        "Prach");
        fapi_sdk_msg_dsc13 = proto_item_add_subtree(paramtree2, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, prachLongFormats, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, prachShortFormats, tvb, *plen, 2, val);
        *plen += 2;
        received += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, prachRestrictedSets, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, prachMultipleCarriersInABand, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, numberOfPrachSlotsAllowedIna60kHzSlot , tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, nSsPbchsPerRachOccasion, tvb, *plen, 1, val8);
        *plen += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc13, phy_prach_ncs, tvb, *plen, 2, val);
        *plen += 2;
        received += 2;
        paramtree3 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 6,
        "Pdcch");
        fapi_sdk_msg_dsc14 = proto_item_add_subtree(paramtree3, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, cceMappingType, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, coresetOutsideFirst3OfdmSymsOfSlot, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, coresetNonContigFreqDomainAlloc, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, precoderGranularityCoreset, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, pdcchMuMimo, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, pdcchPrecoderCycling, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc14, maxPdcchPrecoderCycleLength, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        paramtree4 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 9,
        "Pucch");
        fapi_sdk_msg_dsc15 = proto_item_add_subtree(paramtree4, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchFormats, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchIntraslotFreqHopping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchInterslotFreqHopping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, twoSybmolShortFormat, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchGroupHopping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchNrofSlots, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pucchMultiplexingUe, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, additionalDmrs, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc15, pi2Bpsk, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        paramtree5 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 11,
        "Pdsch");
        fapi_sdk_msg_dsc16 = proto_item_add_subtree(paramtree5, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschMappingTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschAllocationTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschMuMimoOverlap , tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschVrbToPrbMapping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschAggregationFactor, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschCbg, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschDmrsConfigTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschDmrsMaxLength, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschDmrsAdditionalPos, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschDataInDmrsSymbols, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, preemptionSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschNonSlotSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, pdschPrecoderCycling, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc16, maxPdschPrecoderCycleLength, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        paramtree6 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 16,
        "Pusch");
        fapi_sdk_msg_dsc17 = proto_item_add_subtree(paramtree6, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, uciMuxUlschInPusch, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, uciOnlyPusch, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, harqAckNackInUciOnPusch, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschPrecodingType, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschFrequencyHopping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschDmrsConfigTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschDmrsMaxLen, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschDmrsAdditionalPos, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschDataInDmrsSymbols, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschCbg, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschMappingTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschAllocationTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschMuMimoOverlap, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschVrbToPrbMapping, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschAggregationFactor, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschLbrm, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschMaxPtrsPorts, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, cdmDmrsPortsOfMuMimoUsers, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc17, puschNonSlotSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        paramtree7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 4,
        "Srs");
        fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, srsSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, freqHoppingSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, groupHoppingSupport, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, sequenceHoppingSupport, tvb, *plen, 1, val8);
        *plen += 1;
        paramtree7 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc11, proto_fapi_sdk, tvb, *plen, 6,
        "Misc");
        fapi_sdk_msg_dsc18 = proto_item_add_subtree(paramtree7, ett_fapi_sdk_msg_data_subtree1);
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, reservedResourceTypes, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, Cp, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, ssPbchRmsiMuxPatterns, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, sulSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, supportedUserMuxSchemesInSlot, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, dynamicSlotFormatSupport, tvb, *plen, 1, val8);
        *plen += 1;
        received += 1;
        val = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, maxDigBfTableEntries, tvb, *plen, 2, val);
        *plen += 2;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, pdschCsirsTimeFrequencyOverlap, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, pdschPdcchTimeFrequencyOverlap, tvb, *plen, 1, val8);
        *plen += 1;
        val8 = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc18, pdschSspbchTimeFrequencyOverlap, tvb, *plen, 1, val8);
        *plen += 1;
        paramtree8 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 7,
        "RfParameters");
        fapi_sdk_msg_dsc19 = proto_item_add_subtree(paramtree8, ett_fapi_sdk_msg_data_subtree1);
        n_comb = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc19, nBandCombinations, tvb, *plen, 2, n_comb);
        *plen += 2;
        received += 2;
        for (num_support_comb = 0; num_support_comb < n_comb; num_support_comb++)
        {
            support_comb_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc19, proto_fapi_sdk, tvb, *plen, 5,
            "COMB[%d]", num_support_comb);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(support_comb_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree9 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 5,
            "supportedBandCombinationList ");
            fapi_sdk_msg_dsc20 = proto_item_add_subtree(paramtree9, ett_fapi_sdk_msg_data_subtree1);
            n_comb11 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc20, nBands, tvb, *plen, 1, n_comb11);
            *plen += 1;
            received += 1;
            for (num_nbands = 0; num_nbands < n_comb11; num_nbands++)
            {
                nbands_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc20, proto_fapi_sdk, tvb, *plen, 4,
                "nband[%d]", num_nbands);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nbands_tree, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, bandList, tvb, *plen, 2, val);
                *plen += 2;
                received += 2;
            }
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc20, featureSetCombinationId, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
        }
        n_comb2 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetsDownlinkPerCc, tvb, *plen, 2, n_comb2);
        *plen += 2;
        received += 2;
        for (num_featureSetsDownlinkPerCc = 0; num_featureSetsDownlinkPerCc < n_comb2; num_featureSetsDownlinkPerCc++)
        {
            featureSetsDownlinkPerCc_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 6,
            "featureSetsDownlinkPerCc[%d]", num_featureSetsDownlinkPerCc);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(featureSetsDownlinkPerCc_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree10 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 6,
            "featureSetsDownlinkPerCc");
            fapi_sdk_msg_dsc21 = proto_item_add_subtree(paramtree10, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21, supportedSubcarrierSpacingsDl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21, supportedBandwidthDl, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21, maxNumberMimoLayersPdsch, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21, supportedModulationOrderDl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21, maxMuMimoUsersDl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
        }
        /*val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,supportedSubcarrierSpacingsUl , tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,supportedBandwidthUl , tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,maxNumberMimoLayersNonCbPusch , tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,supportedModulationOrderUl , tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,dftsOfdmSupport , tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc21,maxMuMimoUsersUl , tvb, *plen, 1, val8);
            *plen += 1;
        */
        ///////////////////
        n_comb3 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetsDownlink, tvb, *plen, 2, n_comb3);
        *plen += 2;
        received += 2;
        for (num_nFeatureSetsDownlink = 0; num_nFeatureSetsDownlink < n_comb3; num_nFeatureSetsDownlink++)
        {
            nFeatureSetsDownlink_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetsDownlink[%d]", num_nFeatureSetsDownlink);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nFeatureSetsDownlink_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree22 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetPerBand");
            fapi_sdk_msg_dsc33 = proto_item_add_subtree(paramtree22, ett_fapi_sdk_msg_data_subtree1);
            n_comb12 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc33, nCcs, tvb, *plen, 1, n_comb12);
            *plen += 1;
            received += 1;
            for (num_featureSetDownlinkPerCcIds = 0; num_featureSetDownlinkPerCcIds < n_comb12; num_featureSetDownlinkPerCcIds++)
            {
                featureSetDownlinkPerCcIds_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc33, proto_fapi_sdk, tvb, *plen, 6,
                "featureSetDownlinkPerCcIds[%d]", num_featureSetDownlinkPerCcIds);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(featureSetDownlinkPerCcIds_tree, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, featureSetDownlinkPerCcIds, tvb, *plen, 2, val);
                *plen += 2;
                received += 2;
            }
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc33, intraBandFreqSeparationDl, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc33, maxTotalBw, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
        }
        n_comb4 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetsUplinkPerCc, tvb, *plen, 2, n_comb4);
        *plen += 2;
        received += 2;
        for (num_nFeatureSetsUplinkPerCc = 0; num_nFeatureSetsUplinkPerCc < n_comb4; num_nFeatureSetsUplinkPerCc++)
        {
            nFeatureSetsUplinkPerCc_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetsUplinkPerCc[%d]", num_nFeatureSetsUplinkPerCc);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nFeatureSetsUplinkPerCc_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree24 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetsUplinkPerCc");
            fapi_sdk_msg_dsc35 = proto_item_add_subtree(paramtree24, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, supportedSubcarrierSpacingsUl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, supportedBandwidthUl, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, maxNumberMimoLayersNonCbPusch, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, supportedModulationOrderUl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, dftsOfdmSupport, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc35, maxMuMimoUsersUl, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
        }
        n_comb5 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetsUplink, tvb, *plen, 2, n_comb5);
        *plen += 2;
        received += 2;
        for (num_nFeatureSetsUplink = 0; num_nFeatureSetsUplink < n_comb5; num_nFeatureSetsUplink++)
        {
            nFeatureSetsUplink_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetsUplink[%d]", num_nFeatureSetsUplink);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nFeatureSetsUplink_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree23 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 7,
            "nFeatureSetsUplink");
            fapi_sdk_msg_dsc34 = proto_item_add_subtree(paramtree23, ett_fapi_sdk_msg_data_subtree1);
            n_comb13 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc34, UnCcs, tvb, *plen, 1, n_comb13);
            *plen += 1;
            received += 1;
            for (num_UfeatureSetUplinkCcIds = 0; num_UfeatureSetUplinkCcIds < n_comb13; num_UfeatureSetUplinkCcIds++)
            {
                UfeatureSetUplinkCcIds_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc34, proto_fapi_sdk, tvb, *plen, 6,
                "featureSetUplinkCcIds[%d]", num_UfeatureSetUplinkCcIds);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(UfeatureSetUplinkCcIds_tree, ett_fapi_sdk_msg_data_subtree1);
                val = sci_tvb_get_ntohs(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, UfeatureSetUplinkCcIds, tvb, *plen, 2, val);
                *plen += 2;
                received += 2;
            }
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc34, UintraBandFreqSeparationUl, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc34, UmaxTotalBw, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
        }
        n_comb6 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetPerBand, tvb, *plen, 2, n_comb6);
        *plen += 2;
        received += 2;
        for (num_nFeatureSetPerBand = 0; num_nFeatureSetPerBand < n_comb6; num_nFeatureSetPerBand++)
        {
            nFeatureSetPerBand_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 5,
            "nFeatureSetPerBand[%d]", num_nFeatureSetPerBand);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nFeatureSetPerBand_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree11 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 5,
            "nFeatureSetPerBand");
            fapi_sdk_msg_dsc22 = proto_item_add_subtree(paramtree11, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc22, featureSetDownlinkId, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc22, featureSetUplinkId, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc22, nTrxus, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
        }
        n_comb7 = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(fapi_sdk_msg_dsc1, nFeatureSetCombinations, tvb, *plen, 2, n_comb7);
        *plen += 2;
        received += 2;
        for (num_nFeatureSetCombinations = 0; num_nFeatureSetCombinations < n_comb7; num_nFeatureSetCombinations++)
        {
            nFeatureSetCombinations_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc1, proto_fapi_sdk, tvb, *plen, 4,
            "nFeatureSetCombinations[%d]", num_nFeatureSetCombinations);
            fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(nFeatureSetCombinations_tree, ett_fapi_sdk_msg_data_subtree1);
            paramtree12 = proto_tree_add_protocol_format(fapi_sdk_msg_pdu_dsc, proto_fapi_sdk, tvb, *plen, 2,
            "FeatureSetCombination");
            fapi_sdk_msg_dsc23 = proto_item_add_subtree(paramtree12, ett_fapi_sdk_msg_data_subtree1);
            n_comb14 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc23, CnBands, tvb, *plen, 1, n_comb14);
            *plen += 1;
            received += 1;
            for (num_featureSetPerBandIds = 0; num_featureSetPerBandIds < n_comb14; num_featureSetPerBandIds++)
            {
                featureSetPerBandIds_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 4,
                "featureSetPerBandIds[%d]", num_featureSetPerBandIds);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(featureSetPerBandIds_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, featureSetPerBandIds, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            paramtree13 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 3,
            "ssPbch");
            fapi_sdk_msg_dsc24 = proto_item_add_subtree(paramtree13, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc24, maxSsbsPerCarrierPerCellInBurst, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc24, minSsPbchBurstPeriodicity, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc24, maxSsPbchBlocksPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            paramtree14 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 4,
            "Prach");
            fapi_sdk_msg_dsc25 = proto_item_add_subtree(paramtree14, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, maxPrachFdOccasionsInASlot, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, maxPrachFdOccasionsInASlotPerCarrier , tvb, *plen, 1, val8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, maxRootSequencesPerSlotPerFd, tvb, *plen, 2, val);
            *plen += 2;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, supportedSubcarrierSpacingsPrachShort, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc25, supportedSubcarrierSpacingsPrachLong, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            for (i = 0; i < 4; i++)
            {
                P3_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc25, proto_fapi_sdk, tvb, *plen, 1,
                "N5[%d]", i);
                fapi_sdk_msg_pdu_dsc1 = proto_item_add_subtree(P3_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc1, P2, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            paramtree15 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 1,
            "pdcch");
            fapi_sdk_msg_dsc26 = proto_item_add_subtree(paramtree15, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc26, maxPdcchsPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            for (num_betaPdcchRange = 0; num_betaPdcchRange < 2; num_betaPdcchRange++)
            {
                num_betaPdcchRange_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc26, proto_fapi_sdk, tvb, *plen, 4,
                "betaPdcchRange[%d]", num_betaPdcchRange);
                fapi_sdk_msg_pdu_dsc_beta = proto_item_add_subtree(num_betaPdcchRange_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                val8_neg=(gint8)val8;
                proto_tree_add_int(fapi_sdk_msg_pdu_dsc_beta, betaPdcchRange, tvb, *plen, 1, val8_neg);
                *plen += 1;
                received += 1;
            }
            received += 1;
            paramtree16 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 6,
            "pucch");
            fapi_sdk_msg_dsc27 = proto_item_add_subtree(paramtree16, ett_fapi_sdk_msg_data_subtree1);
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc27, pucchPolarMaxK, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc27, pucchPolarMaxN, tvb, *plen, 2, val);
            *plen += 2;
            received += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc27, pucchUciMaxE , tvb, *plen, 2, val);
            *plen += 2;
            for (num_maxPucchsPerSlot = 0; num_maxPucchsPerSlot < 5; num_maxPucchsPerSlot++)
            {
                maxPucchsPerSlot_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc27, proto_fapi_sdk, tvb, *plen, 4,
                "maxPucchsPerSlot[%d]", num_maxPucchsPerSlot);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(maxPucchsPerSlot_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, maxPucchsPerSlot, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_P2 = 0; num_P2 < 4; num_P2++)
            {
                P2_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc27, proto_fapi_sdk, tvb, *plen, 1,
                "P2[%d]", num_P2);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(P2_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, P2, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            paramtree17 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 5,
            "pdsch");
            fapi_sdk_msg_dsc28 = proto_item_add_subtree(paramtree17, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc28, maxPdschHarqProcesses, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc28, maxPdschTransportBlocks, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            for (num_N31 = 0; num_N31 < 4; num_N31++)
            {
                N31_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc28, proto_fapi_sdk, tvb, *plen, 4,
                "N31[%d]", num_N31);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(N31_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, N31, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_N32 = 0; num_N32 < 4; num_N32++)
            {
                N32_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc28, proto_fapi_sdk, tvb, *plen, 4,
                "N32[%d]", num_N32);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(N32_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, N32, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_K3 = 0; num_K3 < 4; num_K3++)
            {
                K3_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc28, proto_fapi_sdk, tvb, *plen, 4,
                "K3[%d]", num_K3);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(K3_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, K3, tvb, *plen, 1, val8);
                *plen += 1;
            }
            for (num_betaPdcchRange = 0; num_betaPdcchRange < 2; num_betaPdcchRange++)
            {
                num_betaPdschRange_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc28, proto_fapi_sdk, tvb, *plen, 4,
                "betaPdschRange[%d]", num_betaPdcchRange);
                fapi_sdk_msg_pdu_dsc_betas = proto_item_add_subtree(num_betaPdschRange_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                val8_neg= (gint8)val8;
                proto_tree_add_int(fapi_sdk_msg_pdu_dsc_betas, betaPdschRange, tvb, *plen, 1, val8_neg);
                *plen += 1;
                received += 1;
            }
            paramtree18 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 6,
            "pusch");
            fapi_sdk_msg_dsc29 = proto_item_add_subtree(paramtree18, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, maxPuschHarqProcesses, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, maxPuschTransportBlocks, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, maxUciOnlyPuschs , tvb, *plen, 1, val8);
            *plen += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, puschUciMaxK , tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, puschUciMaxN , tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, puschUciMaxE , tvb, *plen, 2, val);
            *plen += 2;
            for (num_N41 = 0; num_N41 < 4; num_N41++)
            {
                N41_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc29, proto_fapi_sdk, tvb, *plen, 4,
                "N41[%d]", num_N41);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(N41_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, N41, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_N42 = 0; num_N42 < 4; num_N42++)
            {
                N42_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc29, proto_fapi_sdk, tvb, *plen, 4,
                "N42[%d]", num_N42);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(N42_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, N42, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_P1 = 0; num_P1 < 4; num_P1++)
            {
                P1_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc29, proto_fapi_sdk, tvb, *plen, 4,
                "P1[%d]", num_P1);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(P1_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, P1, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            for (num_K4 = 0; num_K4 < 4; num_K4++)
            {
                K4_tree = proto_tree_add_protocol_format(fapi_sdk_msg_dsc29, proto_fapi_sdk, tvb, *plen, 4,
                "K4[%d]", num_K4);
                fapi_sdk_msg_pdu_dsc = proto_item_add_subtree(K4_tree, ett_fapi_sdk_msg_data_subtree1);
                val8 = tvb_get_guint8(tvb, *plen);
                proto_tree_add_uint(fapi_sdk_msg_pdu_dsc, K4, tvb, *plen, 1, val8);
                *plen += 1;
                received += 1;
            }
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc29, maxDurationWithinReTxInSlots , tvb, *plen, 1, val8);
            *plen += 1;
            paramtree19 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 2,
            "csiRs");
            fapi_sdk_msg_dsc30 = proto_item_add_subtree(paramtree19, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc30, maxNumPortsCsirsTxPerSymbol, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc30, maxNumAggrPortsCsirsTxPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            paramtree20 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 5,
            "Srs");
            fapi_sdk_msg_dsc31 = proto_item_add_subtree(paramtree20, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc31, maxNumPortsPerUe, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc31, maxNumSymbolsPerUe, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc31, maxNumRepetitionsPerUe, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc31, maxNumAggrPortsPerUe, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc31, maxNumAggrPortsPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            paramtree21 = proto_tree_add_protocol_format(fapi_sdk_msg_dsc23, proto_fapi_sdk, tvb, *plen, 1,
            "Misc");
            fapi_sdk_msg_dsc32 = proto_item_add_subtree(paramtree21, ett_fapi_sdk_msg_data_subtree1);
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc32, dlusersPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            val8 = tvb_get_guint8(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc32, ulusersPerSlot, tvb, *plen, 1, val8);
            *plen += 1;
            received += 1;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc32, maxNumPolarDecodesPerSlot , tvb, *plen, 2, val);
            *plen += 2;
            val = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(fapi_sdk_msg_dsc32, maxNumCsiReportsPerSlot , tvb, *plen, 2, val);
            *plen += 2;
        }
    }
    /*if((expected != 0) && (expected != received))
        {
        expert_add_info_format(pinfo, ti, PI_MALFORMED, PI_WARN,
        "TX_REQ: Expected value is %u, received value is %u",
        expected, received);
        }
    */
}
static void
dissect_fapi_msg_data(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, int msg_type, guint32* plen , guint8 Num_phy)
{
    proto_item* ti;
    guint8 i=0;
    /* Specific Message Data Handling */
    switch (msg_type)
    {
        case FAPI_E_SF_IND:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        dissect_fapi_msg_TTI_EVT(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_CONFIG_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        dissect_fapi_msg_CONFIG_REQ(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_CONFIG_RSP:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        dissect_fapi_msg_CONFIG_RSP(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_PARAM_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_PARAM_REQ");
        break;
        case PNF_CAPABILITY_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_CAPABILITY_REQ");
        break;
        case PHY_CAPABILITY_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_CAPABILITY_REQ");
        break;
        case FAPI_E_PNF_CAPABILITY_RSP:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_CAPABILITY_RSP");
        dissect_PNF_RESP(tvb, pinfo, ti, plen);
        break;
        case PNF_CONFIG_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_CONFIG_REQ");
        dissect_CONFIG_REQ(tvb, pinfo, ti, plen);
        break;
        case PNF_CONFIG_RESP:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_CONFIG_RESP");
        dissect_CONFIG_RESP(tvb, pinfo, ti, plen);
        break;
        case PNF_Start_request:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_START_REQ");
        break;
        case PNF_START_RESP:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_START_RESP");
        dissect_START_RESP(tvb, pinfo, ti, plen);
        break;
        case PNF_STOP_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_STOP_RESP");
        dissect_STOP_REQ(tvb, pinfo, ti, plen);
        break;
        case PNF_STOP_RESP:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PNF_STOP_RESP");
        dissect_STOP_RESP(tvb, pinfo, ti, plen);
        break;
        case PHY_CAPABILITY_RESP:
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_CAPABILITY_RESP");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_PHY_CAP_RESP(tvb, pinfo, ti, plen);
        }
        break;
        case PHY_CONFIG_REQ:
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_CONFIG_REQ");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_PHY_CONFIG_REQ(tvb, pinfo, ti, plen);
        }
        break;
        case PHY_CONFIG_RESP:
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_CONFIG_RESP");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_PHY_CONFIG_RESP(tvb, pinfo, ti, plen);
        }
        break;
        case PHY_START_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_START_REQ");
        break;
        case PHY_STOP_REQ:
        ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
        "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_STOP_REQ");
        //dissect_PHY_START(tvb, pinfo, ti, plen);
        break;
        case PHY_START_RESP:
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_START_RESP");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_PHY_START(tvb, pinfo, ti, plen);
        }
        break;
        case PHY_STOP_RESP:
        col_add_str(pinfo->cinfo, COL_INFO, "PHY_STOP_RESP");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_PHY_STOP(tvb, pinfo, ti, plen);
        }
        break;
        case FAPI_E_SLOT_IND:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_E_SLOT_IND");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_SLOT_INDI(tvb, pinfo, ti, plen);
        }
        break;
        case TX_CONTROL_MESSAGE:
        col_add_str(pinfo->cinfo, COL_INFO, "TX_CONTROL_MESSAGE");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_TX_CONTROL_MESSAGE(tvb, pinfo, ti, plen);
        }
        break;
        case TX_DATA_MESSAGE:
        col_add_str(pinfo->cinfo, COL_INFO, "TX_DATA_MESSAGE");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_TX_DATA_MESSAGE(tvb, pinfo, ti, plen);
        }
        break;
        case RX_CONTROL_MESSAGE:
        col_add_str(pinfo->cinfo, COL_INFO, "RX_CONTROL_MESSAGE");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_RX_CONTROL_MESSAGE(tvb, pinfo, ti, plen);
        }
        break;
        case RX_DATA_MESSAGE:
        col_add_str(pinfo->cinfo, COL_INFO, "RX_DATA_INDICATION");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_RX_DATA_MESSAGE(tvb, pinfo, ti, plen);
        }
        break;
        case Error_Indication:
        col_add_str(pinfo->cinfo, COL_INFO, "Error_Indication");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_FAPI_ERROR_INDICATION(tvb, pinfo, ti, plen);
        }
        break;
        case ULSCH_Decode_indication:
        col_add_str(pinfo->cinfo, COL_INFO, "ULSCH DECODE");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_FAPI_ULSCH_DECODE_IND(tvb, pinfo, ti, plen);
        }
        break;
        case RACH_IND:
        col_add_str(pinfo->cinfo, COL_INFO, "RACH_IND");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_RACH_IND(tvb, pinfo, ti, plen);
        }
        break;
        case UCI_INDICATION:
        col_add_str(pinfo->cinfo, COL_INFO, "UCI_INDICATION");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_UCI_INDICATION(tvb, pinfo, ti, plen);
        }
        break;
        case  FAPI_E_INFRA_CONTROL_CMD:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_E_INFRA_CONTROL_CMD");
        for(i=0;i<Num_phy;i++)
        {
            ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, *plen, -1,
            "Message Data (%s)", val_to_str(msg_type, fapi_msg_header_string, "Unknown"));
            dissect_INFRA_CMD(tvb, pinfo, ti, plen);
        }
        break;
        /*5G Ennds*/
        /*case FAPI_E_VSM_CFG_REPORT:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_VSM_CONFIGURED_REPORT");
        dissect_fapi_msg_VSM_CFG_PARAMS_REP(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_PARAM_RSP:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_PARAM_RSP");
        dissect_fapi_msg_PARAM_RSP(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_START_REQ:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_START_REQ");
        break;
        case FAPI_E_STOP_REQ:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_STOP_REQ");
        break;
        case FAPI_E_STOP_IND:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_STOP_IND");
        break;
        case FAPI_E_DL_CONFIG_REQ:
        dissect_fapi_msg_DL_CONFIG_REQ(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_UL_CONFIG_REQ:
        dissect_fapi_msg_UL_CONFIG_REQ(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_TX_REQ:
        dissect_fapi_msg_TX_REQ(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_HI_DCI0_REQ:
        dissect_fapi_msg_HI_DCI0_REQ(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_RX_ULSCH_IND:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_RX_ULSCH_IND");
        dissect_fapi_msg_RX_ULSCH_CQI_IND(tvb, pinfo, ti, plen, FALSE);
        break;
        case FAPI_E_RX_CQI_IND:
        col_add_str(pinfo->cinfo, COL_INFO, "FAPI_RX_CQI_IND");
        dissect_fapi_msg_RX_ULSCH_CQI_IND(tvb, pinfo, ti, plen, TRUE);
        break;
        case FAPI_E_RX_SR_IND:
        dissect_fapi_msg_RX_SR_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_HARQ_IND:
        dissect_fapi_msg_HARQ_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_CRC_IND:
        dissect_fapi_msg_CRC_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_SRS_IND:
        dissect_fapi_msg_SRS_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_RACH_IND:
        dissect_fapi_msg_RACH_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_RX_VSM_MEAS_IND:
        dissect_fapi_msg_VSM_MEAS_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_ERROR_IND:
        dissect_fapi_msg_ERROR_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_RX_VSM_NI_IND:
        dissect_fapi_msg_VSM_NI_IND(tvb, pinfo, ti, plen);
        break;
        case FAPI_E_VSM_INFRA_REQ:
        dissect_fapi_msg_VSM_INFRA_REQ(tvb, pinfo, ti, plen);
        break;
        */
        default:
        //TODO;
        ;
    }
}
static void
dissect_fapi_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree)
{
    proto_item* ti;
    proto_tree* fapi_sdk_msg_header_tree = NULL;
    guint16 msg_header_val;
    guint8 val8;
    guint8 Num_Phy;
    guint16 msg_header_type1 = 0;
    guint32 msg_header_type = 0;
    guint16 vend_spec_len;
    guint8 i = 0;
    //guint64 val64;
    //gint data_remaining;
    guint32 len = 0;
    if (tree == NULL) return;
    if (check_col(pinfo->cinfo, COL_PROTOCOL))
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_fapi_sdk);
    ti = proto_tree_add_protocol_format(tree, proto_fapi_sdk, tvb, len, 13,
    "Message Header");
    fapi_sdk_msg_header_tree = proto_item_add_subtree(ti, ett_fapi_sdk_msg_header);
    /* FAPI Message Header parsing */
    msg_header_type1 = sci_tvb_get_ntohs(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_type, tvb, len, 2, msg_header_type1);
    len += 2;
    vend_spec_len = sci_tvb_get_ntohs(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_len_ven, tvb, len, 2, vend_spec_len);
    len += 2;
    val8 = tvb_get_guint8(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_len_body, tvb, len, 1, val8);
    len += 1;
    val8 = tvb_get_guint8(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_len_flag, tvb, len, 1, val8);
    len += 1;
    msg_header_val = sci_tvb_get_ntohs(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_len_sequence, tvb, len, 2, msg_header_val);
    len += 2;
    msg_header_type = sci_tvb_get_ntohl(tvb, len);
    proto_tree_add_uint(fapi_sdk_msg_header_tree, hf_fapi_sdk_msg_header_len_timestamp, tvb, len, 4, msg_header_type);
    len += 4;
    Num_Phy= dissect_fapi_mssg_header(tvb, pinfo, fapi_sdk_msg_header_tree, &len);
    /* FAPI Message Data */
    dissect_fapi_msg_data(tvb, pinfo, tree, msg_header_type1, &len, Num_Phy);
    /* Vendor Specific Message Data */
    /*
        if ((global_fapi_sdk_parse_vsm_api_ver != 0) && (vend_spec_len > 0))
        {
        data_remaining = tvb_length_remaining(tvb, len);
        if(data_remaining > 0)
        {
        dissect_fapi_vsm_msg(tvb, pinfo, tree, msg_header_type, &len);
        }
        if(tvb_length(tvb) > 60)
        {
        data_remaining = tvb_length_remaining(tvb, len);
        if(data_remaining > 0)
        {
        //expert_add_undecoded_item(tvb, pinfo, tree, len, data_remaining, PI_WARN);
        }
    */
}
