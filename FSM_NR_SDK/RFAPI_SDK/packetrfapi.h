/* packetrfapi.h
* Copyright (c) 2019-2021 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/
#define ENDIANESS  LITTLE_ENDIAN
#define PROTO_TAG_rfapi_sdk     "RFAPI_SDK"
#if ENDIANESS == BIG_ENDIAN
#define htons(A) (A)
#define htonl(A) (A)
#define ntohs(A) (A)
#define ntohl(A) (A)
#define SIZE_OF_DATA_PER_SLOT 4
#define SIZE_OF_DATA_PER_LAYER 62
#define SIZE_OF_COMMON_MSG_HEADER 11
#elif ENDIANESS == LITTLE_ENDIAN
#define htons(A) ((((unsigned short)(A) & 0xff00) >> 8) | \
                         (((unsigned short)(A) & 0x00ff) << 8))
#define htonl(A) ((((unsigned long)(A) & 0xff000000) >> 24) | \
                         (((unsigned long)(A) & 0x00ff0000) >> 8)  | \
                         (((unsigned long)(A) & 0x0000ff00) << 8)  | \
                         (((unsigned long)(A) & 0x000000ff) << 24))
#define ntohs  htons
#define ntohl  htonl
#else
#error: "Must define one of BIG_ENDIAN or LITTLE_ENDIAN"
#endif
#define ntohll(x) (((_int64)(ntohl((int)((x << 32) >> 32))) << 32) | (unsigned int)ntohl(((int)(x >> 32))))
#define htonll(x) ntohll(x)
#define NUM_SYMBOLS_PER_SLOT          14
#define MAX_ANTS_PER_LAYER             8
#define MAX_TRX_DEVICES                8
#define MAX_NUM_LAYERS                 2
#define NUM_PARAMS_SET_BEAM            9
#define NUM_PARAMS_GET_BEAM           10
#define NUM_PARAMS_GET_WBRSSI          8
#define NUM_PARAMS_GET_TX_POWER        4
#define SET_ITERATORS_SET_BEAM_RSP     1
#define SET_ITERATORS_GET_BEAM_RSP     2
#define SET_ITERATORS_GET_WBRSSI_IND   3
#define SET_ITERATORS_GET_REG_DATA_RSP 4
#define SIZE_PER_ITR_SET_BEAM_REQ     12
#define SIZE_PER_ITR_GET_BEAM_RSP     12
#define NUM_BITS_FOR_SYSTEM_FRAME     10
#define NUM_BITS_FOR_SUBFRAME          6
#define START_BIT_FOR_SYSTEM_FRAME     1
#define START_BIT_FOR_SUBFRAME        11
#define SECOND_HALF_PACKET_BITS        8
#define PARAM_IS_BROKEN                1

typedef enum
{
  RF_SET_HW_CONFIG_REQ     = 0x0001,
  RF_SET_HW_CONFIG_RSP     = 0x0002,
  RF_GET_CAPABILITIES_REQ  = 0x0003,
  RF_GET_CAPABILITIES_RSP  = 0x0004,
  RF_START_REQ             = 0x0005,
  RF_START_RSP             = 0x0006,
  RF_STOP_REQ              = 0x0007,
  RF_STOP_RSP              = 0x0008,
  RF_CONFIG_REQ            = 0x0009,
  RF_CONFIG_RSP            = 0x000a,
  RF_ENABLE_REQ            = 0x000b,
  RF_ENABLE_RSP            = 0x000c,
  RF_DISABLE_REQ           = 0x000d,
  RF_DISABLE_RSP           = 0x000e,
  RF_SET_TX_POWER_REQ      = 0x000f,
  RF_SET_TX_POWER_RSP      = 0x0010,
  RF_SET_BEAM_TABLE_REQ    = 0x0011,
  RF_SET_BEAM_TABLE_RSP    = 0x0012,
  RF_GET_BEAM_TABLE_REQ    = 0x0013,
  RF_GET_BEAM_TABLE_RSP    = 0x0014,
  RF_LOAD_BEAM_REQ         = 0x0015,
  RF_LOAD_BEAM_RSP         = 0x0016,
  RF_TRX_DEVICE_CTRL_REQ   = 0x0017,
  RF_TRX_DEVICE_CTRL_RSP   = 0x0018,
  RF_SET_SLOT_CONFIG_REQ   = 0x0019,
  RF_SET_SLOT_CONFIG_RSP   = 0x001a,
  RF_ALARMS_IND            = 0x001d,
  RF_SET_THRESHOLDS_REQ    = 0x0022,
  RF_SET_THRESHOLDS_RSP    = 0x0023,
  RF_RXAGC_CTRL_REQ        = 0x0026,
  RF_RXAGC_CTRL_RSP        = 0x0027,
  RF_GET_TEMPERATURE_REQ   = 0x0028,
  RF_GET_TEMPERATURE_RSP   = 0x0029,
  RF_GET_WBRSSI_REQ        = 0x002d,
  RF_GET_WBRSSI_RSP        = 0x002e,
  RF_GET_WBRSSI_IND        = 0x002f,
  RF_GET_TSSI_REQ          = 0x0030,
  RF_GET_TSSI_RSP          = 0x0031,
  RF_GET_TSSI_IND          = 0x0032,
  RF_TRX_DEV_REG_DATA_REQ  = 0x0033,
  RF_TRX_DEV_REG_DATA_RSP  = 0x0034,
  RF_GET_TX_POWER_REQ      = 0x0035,
  RF_GET_TX_POWER_RSP      = 0x0036,
  RF_GET_TX_POWER_IND      = 0x0037,
  RF_TEST_REQ              = 0x0038,
  RF_TEST_RSP              = 0x0039,
  RF_TEST_IND              = 0x0040,
} RFAPI_E_MSG_TYPE;

typedef enum
{
  NO_PARAM,
  FIRST_PARAM,
  SECOND_PARAM,
  THIRD_PARAM,
  FOURTH_PARAM,
  FIFTH_PARAM,
  SIXTH_PARAM,
  SEVENTH_PARAM,
  EIGHTH_PARAM,
  NINETH_PARAM,
  TENTH_PARAM,
  ELEVENTH_PARAM,
  TWELFTH_PARAM,
  THIRTEENTH_PARAM,
  FOURTEENTH_PARAM,
  FIFTEENTH_PARAM,
  SIXTEENTH_PARAM,
  SEVENTEENTH_PARAM,
  EIGHTEENTH_PARAM,
  NINETEENTH_PARAM,
  TWENTIETH_PARAM,
  TWENTY_FIRST_PARAM,
  TWENTY_SECOND_PARAM,
  TWENTY_THIRD_PARAM,
  TWENTY_FOURTH_PARAM,
  TWENTY_FIFTH_PARAM,
  TWENTY_SIXTH_PARAM,
  TWENTY_SEVENTH_PARAM,
  TWENTY_EIGHTH_PARAM,
  TWENTY_NINTH_PARAM,
  THIRTIETH_PARAM,
  THIRTY_FIRST_PARAM,
  THIRTY_SECOND_PARAM,
  THIRTY_THIRD_PARAM,
  THIRTY_FOURTH_PARAM,
  THIRTY_FIFTH_PARAM,
  THIRTY_SIXTH_PARAM,
  THIRTY_SEVENTH_PARAM,
  THIRTY_EIGHTH_PARAM,
} RFAPI_PARAM_LOCATION;


static const value_string rfapi_scs_freq_string[] = {
  { 1 , "15kHz"},
  { 2 , "30kHz"},
  { 3 , "60kHz"},
  { 4 , "120kHz"},
  { 5 , "240kHz"},
};

static const value_string rfapi_scs_freq_string_rssi[] = {
  { 0 , "15kHz"},
  { 1 , "30kHz"},
  { 2 , "60kHz"},
  { 3 , "120kHz"},
  { 4 , "240kHz"},
};

static const value_string rfapi_status_string[] = {
  {0x0000 , "RF_MSG_OK"},
  {0x0001 , "RF_ERR_NOT_SUPPORTED"},
  {0x0002 , "RF_ERR_INVALID_MSG"},
  {0x0003 , "RF_ERR_INVALID_PARAM"},
  {0x0004 , "RF_ERR_NO_INIT"},
  {0x0005 , "RF_ERR_DATA_ACCESS"},
  {0x0006 , "RF_ERR_CARR_CONFIGURED"},
  {0x0007 , "RF_ERR_CARR_NOT_CONFIGURED"},
  {0x0008 , "RF_ERR_CARR_ENABLED"},
  {0x0009 , "RF_ERR_CARR_DISABLED"},
  {0x000A , "RF_ERR_INTERNAL_FAULT"},
  {0x000B , "RF_ERR_NULL_POINTER"},
  {0x000C , "RF_ERR_BAND_NOT_SUPPORTED"},
  {0x000D , "RF_ERR_PATH_NOT_SUPPORTED"},
  {0x000E , "RF_ERR_FREQ_NOT_SUPPORTED"},
  {0x000F , "RF_ERR_EXECTIME_LATE"},
  {0x0010 , "RF_ERR_BEAM_NOT_ACTIVE"},
  {0x0011 , "RF_ERR_TRX_DEVICE_FULL"},
  {0x0012 , "RF_ERR_BEAM_ACTIVE"},
  {0x0013 , "RF_ERR_INVALID_BEAM_ID"},
  {0x0014 , "RF_ERR_INVALID_CONFIG"},
  {0x0015 , "RF_ERR_CALIBRATION_FAULT"},
  {0x0016 , "RF_ERR_TIMEOUT"},
  {0x0017 , "RF_ERR_INVALID_REQ"},
  {0x0018 , "RF_ERR_UNRECOVERABLE_FAULT"},
  {0x0019 , "RF_ERR_INVALID_IBW_FAULT"},
  {0x0020 , "RF_ERR_EXCEED_MAX_BS_FAULT"},
  {0x0021 , "RF_ERR_LAST"}
};

static const value_string rfapi_msg_header_string[] = {
  { RF_START_REQ, "RF_START_REQ"},
  { RF_STOP_RSP, "RF_STOP_RSP"},
  { RF_GET_CAPABILITIES_REQ, "RF_GET_CAPABILITIES_REQ"},
  { RF_GET_CAPABILITIES_RSP, "RF_GET_CAPABILITIES_RSP"},
  { RF_START_RSP, "RF_START_RSP"},
  { RF_ENABLE_REQ, "RF_ENABLE_REQ"},
  { RF_DISABLE_REQ, "RF_DISABLE_REQ"},
  { RF_SET_TX_POWER_REQ, "RF_SET_TX_POWER_REQ"},
  { RF_SET_TX_POWER_REQ, "RF_SET_TX_POWER_REQ"},
  { RF_CONFIG_REQ, "RF_CONFIG_REQ"},
  { RF_SET_HW_CONFIG_REQ,"RF_SET_HW_CONFIG_REQ"},
  { RF_STOP_REQ, "RF_STOP_REQ"},
  { RF_SET_HW_CONFIG_RSP , "RF_SET_HW_CONFIG_RSP"},
  { RF_CONFIG_RSP, "RF_CONFIG_RSP"},
  { RF_SET_TX_POWER_RSP, "RF_SET_TX_POWER_RSP"},
  { RF_DISABLE_RSP, "RF_DISABLE_RSP"},
  { RF_ENABLE_RSP, "RF_ENABLE_RSP"},
  { RF_LOAD_BEAM_REQ, "RF_LOAD_BEAM_REQ"},
  { RF_LOAD_BEAM_RSP, "RF_LOAD_BEAM_RSP"},
  { RF_TRX_DEVICE_CTRL_REQ, "RF_TRX_DEVICE_CTRL_REQ"},
  { RF_TRX_DEVICE_CTRL_RSP, "RF_TRX_DEVICE_CTRL_RSP"},
  { RF_SET_SLOT_CONFIG_REQ, "RF_SET_SLOT_CONFIG_REQ"},
  { RF_SET_SLOT_CONFIG_RSP, "RF_SET_SLOT_CONFIG_RSP"},
  { RF_SET_THRESHOLDS_REQ, "RF_SET_THRESHOLDS_REQ"},
  { RF_SET_THRESHOLDS_RSP, "RF_SET_THRESHOLDS_RSP"},
  { RF_SET_BEAM_TABLE_REQ, "RF_SET_BEAM_TABLE_REQ"},
  { RF_SET_BEAM_TABLE_RSP, "RF_SET_BEAM_TABLE_RSP"},
  { RF_GET_BEAM_TABLE_REQ, "RF_GET_BEAM_TABLE_REQ"},
  { RF_GET_BEAM_TABLE_RSP, "RF_GET_BEAM_TABLE_RSP"},
  { RF_RXAGC_CTRL_REQ, "RF_RXAGC_CTRL_REQ"},
  { RF_RXAGC_CTRL_RSP, "RF_RXAGC_CTRL_RSP"},
  { RF_GET_TEMPERATURE_REQ, "RF_RF_GET_TEMPERATURE_REQ"},
  { RF_GET_TEMPERATURE_RSP, "RF_RF_GET_TEMPERATURE_RSP"},
  { RF_GET_WBRSSI_REQ, "RF_GET_WBRSSI_REQ" },
  { RF_GET_WBRSSI_RSP, "RF_GET_WBRSSI_RSP" },
  { RF_GET_WBRSSI_IND, "RF_GET_WBRSSI_IND" },
  { RF_GET_TSSI_REQ, "RF_GET_TSSI_REQ" },
  { RF_GET_TSSI_RSP, "RF_GET_TSSI_RSP" },
  { RF_GET_TSSI_IND, "RF_GET_TSSI_IND" },
  { RF_TRX_DEV_REG_DATA_REQ, "RF_TRX_DEV_REG_DATA_REQ" },
  { RF_TRX_DEV_REG_DATA_RSP, "RF_TRX_DEV_REG_DATA_RSP" },
  { RF_ALARMS_IND, "RF_ALARM_IND" },
  { RF_GET_TX_POWER_REQ, "RF_GET_TX_POWER_REQ" },
  { RF_GET_TX_POWER_RSP, "RF_GET_TX_POWER_RSP" },
  { RF_GET_TX_POWER_IND, "RF_GET_TX_POWER_IND" },
  { RF_TEST_REQ, "RF_TEST_REQ" },
  { RF_TEST_RSP, "RF_TEST_RSP" },
  { RF_TEST_IND, "RF_TEST_IND" },
  { 0, NULL },
};

static gboolean global_rfapi_sdk_BIG_ENDIAN = FALSE;
static int proto_rfapi_sdk = -1;
static dissector_handle_t data_handle = NULL;
static dissector_handle_t rfapi_sdk_handle;

static void dissect_rfapi_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree);

static gint hf_rfapi_sdk_msg_header_type = -1;
static gint hf_rfapi_sdk_msg_header_len_ven = -1;
static gint hf_rfapi_sdk_msg_header_len_body = -1;
static gint hf_rfapi_sdk_msg_header_len_flag = -1;
static gint hf_rfapi_sdk_msg_header_len_sequence = -1;
static gint hf_rfapi_sdk_msg_header_len_timestamp = -1;

static gint  status2 = -1;
static gint  execnow = -1;
static gint  scs = -1;
static gint  slot1 = -1;
static gint  frame = -1;
static gint  systemframe = -1;
static gint  subframe = -1;
static gint  scs1 = -1;
static gint  numTxPaths = -1;
static gint  pathId = -1;
static gint  txPowerDbm = -1;
static gint  numCarriers = -1;
static gint  numPathsPerCarrier = -1;
static gint  band = -1;
static gint  pathId1 = -1;
static gint  bandwidth1 = -1;
static gint  frequency1 = -1;
static gint  txCCGainAdj1 = -1;
static gint  carrierId = -1;
static gint  numTrxDevicesh = -1;
static gint  numAntsPerTrxLayerh = -1;
static gint  maxNumBeamsPerLayer = -1;
static gint  maxBeamsPerTrxDeviceLayer = -1;
static gint  numRfPaths = -1;
static gint  numBands = -1;
static gint  minTxPower = -1;
static gint  maxTxPower = -1;
static gint  minFreq = -1;
static gint  maxFreq = -1;
static gint  maxIbw = -1;
static gint  numBeams = -1;
static gint  newBeamId = -1;
static gint  oldBeamId = -1;
static gint  tddSlotMaskTx = -1;
static gint  tddSlotMaskRx = -1;
static gint  numLayers = -1;
static gint  layerNum = -1;
static gint  beamSwitchMaskTx = -1;
static gint  beamSwitchMaskRx = -1;
static gint  beamId = -1;
static gint  txPowerAdj = -1;
static gint  beamIdTx = -1;
static gint  beamIdRx = -1;
static gint  trxDeviceNum = -1;
static gint  powerOn = -1;
static gint  antElement = -1;
static gint  on_off = -1;
static gint  rxPhase = -1;
static gint  rxGain = -1;
static gint  txPhase = -1;
static gint  txGain = -1;
static gint  CodebookIdx = -1;
static gint  adjustDb = -1;
static gint  pathId2 = -1;
static gint  maxBeamSwitchesPerSlot = -1;
static gint  maxComponentCarriers = -1;
static gint  maxBeamsInTrxDeviceLayer = -1;
static gint  maxBeamsInTablePerLayer = -1;
static gint  minTxSlotLatency = -1;
static gint  minRxSlotLatency = -1;
static gint  getCapMinTxAntGain = -1;
static gint  getCapMaxTxAntGain = -1;
static gint  getCapMinRxAntGain = -1;
static gint  getCapMaxRxAntGain = -1;
static gint  getCapMinPerfTemp = -1;
static gint  getCapMaxPerfTemp = -1;
static gint  getCapMinOperatingTemp = -1;
static gint  getCapMaxOperatingTemp = -1;
static gint  getCapAntHardwareId = -1;
static gint  getCapNumBands = -1;
static gint  getCapBand = -1;
static gint  getCapMinTxPower = -1;
static gint  getCapMaxTxPower = -1;
static gint  getCapLimTxPower = -1;
static gint  getCapMinFreq = -1;
static gint  getCapMaxFreq = -1;
static gint  getCapMaxIbw = -1;
static gint  pathIdRxagc = -1;
static gint  enableRxagc = -1;
static gint  forceGainRxagc = -1;
static gint  gainIdxRxagc = -1;
static gint  attackCountThresholdRxagc = -1;
static gint  recoveryCountThresholdRxagc = -1;
static gint  recoveryDelayRxagc = -1;
static gint  wbrssiPeakWindowRxagc = -1;
static gint  wbrssiTargetRxagc = -1;
static gint  wbrssiThresholdRxagc = -1;
static gint  gainStepRxagc = -1;
static gint  numRxPathsRxagc = -1;
static gint  wbrssiReqIdxWbrssi = -1;
static gint  scsWbrssi = -1;
static gint  sfnWbrssi = -1;
static gint  slotWbrssi = -1;
static gint  numRxPathsWbrssi = -1;
static gint  numSlotsWbrssi = -1;
static gint  dataValidWbrssi = -1;
static gint  rxPathIdWbrssi = -1;
static gint  symSfnWbrssi = -1;
static gint  symSlotWbrssi = -1;
static gint  symIdxWbrssi = -1;
static gint  lnaGainStateWbrssi = -1;
static gint  symbRSSIdBFSWbrssi = -1;
static gint  rxGaindBWbrssi = -1;
static gint  tssiReqIdx = -1;
static gint  sfnTssi = -1;
static gint  slotTssi = -1;
static gint  scsTssi = -1;
static gint  chainTypeTssi = -1;
static gint  carrierIdTssi = -1;
static gint  txPathIdTssi = -1;
static gint  startSymbolTssi = -1;
static gint  numSymbolsTssi = -1;
static gint  avgTssidBfs = -1;
static gint  eirpdBmTssi = -1;
static gint  trpdBmTssi = -1;
static gint  trxDeviceNumRegData = -1;
static gint  regDataFetchTypeRegData = -1;
static gint  beamIdRegData = -1;
static gint  numTrxDevicesRegData = -1;
static gint  trxDevReadStatusRegData = -1;
static gint  rxCodebookIndexRegData = -1;
static gint  txCodebookIndexRegData = -1;
static gint  rxBeamIdsRegData = -1;
static gint  txBeamIdsRegData = -1;
static gint  rxAntElementOnOffRegData = -1;
static gint  txAntElementOnOffRegData = -1;
static gint  numLayersRegData = -1;
static gint  numCodebookEntriesRegData = -1;
static gint  codebookIdxRegData = -1;
static gint  txPhaseShifterQ1Elem12RegData = -1;
static gint  txPhaseShifterQ1Elem34RegData = -1;
static gint  txPhaseShifterQ2Elem12RegData = -1;
static gint  txPhaseShifterQ2Elem34RegData = -1;
static gint  txEnableQ1RegData = -1;
static gint  txEnableQ2RegData = -1;
static gint  txEnableLayer1RegData = -1;
static gint  txEnableLoVcoRegData = -1;
static gint  txFineGainQ1Elem12RegData = -1;
static gint  txFineGainQ1Elem34RegData = -1;
static gint  txFineGainQ2Elem12RegData = -1;
static gint  txFineGainQ2Elem34RegData = -1;
static gint  rxPhaseShifterQ1Elem12RegData = -1;
static gint  rxPhaseShifterQ1Elem34RegData = -1;
static gint  rxPhaseShifterQ2Elem12RegData = -1;
static gint  rxPhaseShifterQ2Elem34RegData = -1;
static gint  rxFineGainQ1Elem12RegData = -1;
static gint  rxFineGainQ1Elem34RegData = -1;
static gint  rxFineGainQ2Elem12RegData = -1;
static gint  rxFineGainQ2Elem34RegData = -1;
static gint  rxEnableQ1RegData = -1;
static gint  rxEnableQ2RegData = -1;
static gint  rxEnableLayer1RegData = -1;
static gint  rxEnableLOVCORegData = -1;
static gint  txPowerReqIdx = -1;
static gint  scsTxPower = -1;
static gint  sfnTxPower = -1;
static gint  slotTxPower = -1;
static gint  numTxPathsTxPower = -1;
static gint  numTrxDevicesTxPower = -1;
static gint  txPathIdTxPower = -1;
static gint  txDeviceIdTxPower = -1;
static gint  txPowerDbmTxPower = -1;
static gint  numLayersAlarm = -1;
static gint  numTrxDevicesAlarm = -1;
static gint  moduleMaskAlarm = -1;
static gint  layerNumAlarm = -1;
static gint  layerMaskAlarm = -1;
static gint  EIRPDbmAlarm = -1;
static gint  rxGaindBAlarm = -1;
static gint  trxDeviceNumAlarm = -1;
static gint  deviceMaskAlarm = -1;
static gint  tempAlarm = -1;
static gint  thresMaxTxOperatingPower = -1;
static gint  thresTxPowerLimit = -1;
static gint  thresMaxPerformanceTemp = -1;
static gint  thresMinPerformanceTemp = -1;
static gint  thresMaxOperatingTemp = -1;
static gint  thresMinOperatingTemp = -1;
static gint  testIdx = -1;
static gint  testType = -1;
static gint  testStatus = -1;
static gint  bandEnable = -1;
static gint  subBandEnable = -1;

static gint ett_rfapi_sdk_msg_header = -1;
static gint ett_rfapi_sdk_msg_phy = -1;
static gint ett_rfapi_sdk_msg_data = -1;
static gint ett_rfapi_sdk_msg_data_subtree1 = -1;
static gint ett_rfapi_sdk_msg_data_subtree2 = -1;
static gint ett_rfapi_sdk_msg_data_subtree3 = -1;
static gint ett_rfapi_sdk_msg_data_subtree4 = -1;
static gint ett_msg_fragment = -1;
static gint ett_msg_fragments = -1;

