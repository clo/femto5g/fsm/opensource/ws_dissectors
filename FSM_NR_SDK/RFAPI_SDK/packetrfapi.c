/* packetrfapi.c
* Copyright (c) 2019-2021 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <stdio.h>
#include <glib.h>
#include <math.h>
#include <epan/packet.h>
#include <epan/expert.h>
#include <epan/proto.h>
#include <epan/prefs.h>
#include <epan/tvbuff-int.h>
#include <epan/tvbuff.h>
#include <epan/pint.h>
#include <epan/reassemble.h>
#include <epan/value_string.h>
#include "packetrfapi.h"
#include <string.h>

guint16  vend_spec_len;
guint8   segment;
guint16  num_beamst_global;
guint16  num_beams_set;
guint16  num_beams_get;
guint8   location_broken = 0;
guint8   first_half_packet;
guint8   if_half_broken = 0;
guint8   last_param_success = 0;
guint16  num_trx_devices_global = 0;
guint16  num_ants_per_trx_layer_global = 0;
guint8   numLayersItrWrssi = 0;
guint8   numSlotsItrWbrssi = 0;
guint8   numSymbolsItrWbrssi = 0;
guint8   locationBrokenWbrssi = 0;
guint8   firstHalfPacketWbrssi;
guint8   ifHalfBrokenWbrssi = 0;
guint8   lastParamSuccessWbrssi = 0;
guint8   firstHalfPacketWbrssi;
guint8   numSlotsWbrssiG;
guint16  numBeamsItrLoadBeam = 0;
guint8   locationBrokenLoadBeam = 0;
guint8   lastParamSuccessLoadBeam = 0;
guint8   ifHalfBrokenLoadBeam = 0;
guint16  numSlotsLoadBeamG = 0;
guint8   firstHalfPacketLoadBeam;
guint8   regDataFetchTypeGlobal;
guint8   numLayersGetRegDataGlobal;
guint8   numCodebookEntriesGetRegDataGlobal;
guint8   numTrxDevicesGetRegDataGlobal;
guint8   locationBrokenGetRegData = 0;
guint8   lastParamSuccessGetRegData = 0;
guint8   ifHalfBrokenGetRegData = 0;
guint8   firstHalfPacketGetRegData = 0;
guint8   numTrxDevicesItrRegData = 1;
guint8   numCodebookEntriesItrRegData = 1;
guint8   numLayersItrRegData = 1;
guint8   numLayersItrTxPower = 0;
guint8   numTrxDevicesItrTxPower = 0;
guint8   locationBrokenTxPower = 0;
guint8   ifHalfBrokenTxPower = 0;
guint8   lastParamSuccessTxPower = 0;
guint8   firstHalfPacketTxPower;

void proto_reg_handoff_rfapi_sdk(void)
{
  static gboolean initialized = FALSE;
  if (!initialized)
  {
    data_handle = find_dissector("rfapi_sdk");
    rfapi_sdk_handle = create_dissector_handle(dissect_rfapi_sdk, proto_rfapi_sdk);
  }
}

void proto_register_rfapi_sdk(void)
{
  static hf_register_info hf[] = {
    { &hf_rfapi_sdk_msg_header_type,
    { "Msg ID", "common_sdk.msg_header.type",
    FT_UINT8, BASE_HEX, VALS(rfapi_msg_header_string), 0x0,
    NULL, HFILL } },
    { &hf_rfapi_sdk_msg_header_len_ven,
    { "Length", "rfapi_sdk.msg_length.len_ven",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &hf_rfapi_sdk_msg_header_len_body,
    { "Segment Number", "rfapi_sdk.msg_seg_num.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &hf_rfapi_sdk_msg_header_len_flag,
    { "Flag", "rfapi_sdk.msg_flag.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &hf_rfapi_sdk_msg_header_len_sequence,
    { "Sequence Num", "rfapi_sdk.msg_seq_num.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &hf_rfapi_sdk_msg_header_len_timestamp,
    { "Timestamp", "rfapi_sdk.msg_timestamp.len_body",
    FT_UINT32, BASE_HEX_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &status2,
    { "status", "rfapi_sdk.msg_status2.type",
    FT_UINT32, BASE_HEX, VALS(rfapi_status_string), 0x0,
    NULL, HFILL } },
    { &bandEnable,
    { "band", "rfapi_sdk.msg_band.type",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &subBandEnable,
    { "subband", "rfapi_sdk.msg_subband.type",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &scs,
    { "scs", "rfapi_sdk.msg_scs.type",
    FT_UINT8, BASE_HEX, VALS(rfapi_scs_freq_string), 0x0,
    NULL, HFILL } },
    { &execnow,
    { "execNow", "rfapi_sdk.msg_execnow.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &slot1,
    { "slot", "rfapi_sdk.msg_slot1.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &systemframe,
    { "system frame", "rfapi_sdk.msg_sys_frame.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &subframe,
    { "sub frame", "rfapi_sdk.msg_subframe.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &frame,
    { "frame", "rfapi_sdk.msg_frame.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numTxPaths,
    { "numTxPaths", "rfapi_sdk.msg_numtxpaths.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &pathId2,
    { "pathId", "rfapi_sdk.msg_pathid2.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPowerDbm,
    { "txPowerDbm", "rfapi_sdk.msg_txpowerdbm.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numCarriers,
    { "numCarriers", "rfapi_sdk.msg_numcarriers.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numPathsPerCarrier,
    { "numPathsPerCarrier", "rfapi_sdk.msg_numpathspercarrier.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &band,
    { "band", "rfapi_sdk.msg_band.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &carrierId,
    { "carrierId", "rfapi_sdk.msg_carrierid.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &pathId1,
    { "pathId", "rfapi_sdk.msg_pathid1.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &pathId,
    { "pathId", "rfapi_sdk.msg_pathid.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &bandwidth1,
    { "bandwidth", "rfapi_sdk.msg_bandwidth1.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &frequency1,
    { "frequency", "rfapi_sdk.msg_frequency1.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txCCGainAdj1,
    { "TXCCGain", "rfapi_sdk.msg_txCCGainAdj1.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numTrxDevicesh,
    { "numTrxDevices", "rfapi_sdk.msg_numTrxDevicesh.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numAntsPerTrxLayerh,
    { "numAntsPerTrxLayer", "rfapi_sdk.msg_numAntsPerTrxLayerh.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxNumBeamsPerLayer,
    { "maxNumBeamsPerLayer", "rfapi_sdk.msg_maxNumBeamsPerLayer.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxBeamsPerTrxDeviceLayer,
    { "maxBeamsPerTrxDeviceLayer", "rfapi_sdk.msg_maxBeamsPerTrxDeviceLayer.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numRfPaths,
    { "numRfPaths", "rfapi_sdk.msg_numRfPaths.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numBands,
    { "numBands", "rfapi_sdk.msg_numBands.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &minTxPower,
    { "minTxPower", "rfapi_sdk.msg_minTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxTxPower,
    { "maxTxPower", "rfapi_sdk.msg_maxTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &minFreq,
    { "minFreq", "rfapi_sdk.msg_minFreq.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxFreq,
    { "maxFreq", "rfapi_sdk.msg_maxFreq.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxIbw,
    { "maxIbw", "rfapi_sdk.msg_maxIbw.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numBeams,
    { "numBeams", "rfapi_sdk.msg_numBeams.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &newBeamId,
    { "newBeamId", "rfapi_sdk.msg_newBeamId.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &oldBeamId,
    { "oldBeamId", "rfapi_sdk.msg_oldBeamId.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &tddSlotMaskTx,
    { "tddSlotMaskTx", "rfapi_sdk.msg_tddSlotMaskTx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &tddSlotMaskRx,
    { "tddSlotMaskRx", "rfapi_sdk.msg_tddSlotMaskRx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numLayers,
    { "numLayers", "rfapi_sdk.msg_numLayers.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &layerNum,
    { "layerNum", "rfapi_sdk.msg_layerNum.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamSwitchMaskTx,
    { "beamSwitchMaskTx", "rfapi_sdk.msg_beamSwitchMaskTx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamSwitchMaskRx,
    { "beamSwitchMaskRx", "rfapi_sdk.msg_beamSwitchMaskRx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamId,
    { "beamId", "rfapi_sdk.msg_beamId.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPowerAdj,
    { "txPowerAdjust", "rfapi_sdk.msg_txPowerAdjust.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamIdTx,
    { "beamIdTx", "rfapi_sdk.msg_beamIdTx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamIdRx,
    { "beamIdRx", "rfapi_sdk.msg_beamIdRx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trxDeviceNum,
    { "trxDeviceNum", "rfapi_sdk.msg_trxDeviceNum.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &powerOn,
    { "powerOn", "rfapi_sdk.msg_powerOn.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &antElement,
    { "antElement", "rfapi_sdk.msg_antElement.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &on_off,
    { "on_off", "rfapi_sdk.msg_on_off.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPhase,
    { "rxPhase", "rfapi_sdk.msg_rxPhase.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxGain,
    { "rxGain", "rfapi_sdk.msg_rxGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPhase,
    { "txPhase", "rfapi_sdk.msg_txPhase.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txGain,
    { "txGain", "rfapi_sdk.msg_txGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &CodebookIdx,
    { "CodebookIdx", "rfapi_sdk.msg_CodebookIdx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &adjustDb,
    { "adjust_db", "rfapi_sdk.msg_adjustDb.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxBeamSwitchesPerSlot,
    { "maxBeamSwitchesPerSlot", "rfapi_sdk.msg_maxBeamSwitchesPerSlot.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxComponentCarriers,
    { "maxComponentCarriers", "rfapi_sdk.msg_maxComponentCarriers.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxBeamsInTrxDeviceLayer,
    { "maxBeamsInTrxDeviceLayer", "rfapi_sdk.msg_maxBeamsInTrxDeviceLayer.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &maxBeamsInTablePerLayer,
    { "maxBeamsInTablePerLayer", "rfapi_sdk.msg_maxBeamsInTablePerLayer.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &minTxSlotLatency,
    { "minTxSlotLatency", "rfapi_sdk.msg_minTxSlotLatency.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &minRxSlotLatency,
    { "minRxSlotLatency", "rfapi_sdk.msg_minRxSlotLatency.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinTxAntGain,
    { "minTxAntGain", "rfapi_sdk.msg_getCapMinTxAntGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxTxAntGain,
    { "maxTxAntGain", "rfapi_sdk.msg_getCapMaxTxAntGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinRxAntGain,
    { "minRxAntGain", "rfapi_sdk.msg_getCapMinRxAntGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxRxAntGain,
    { "maxRxAntGain", "rfapi_sdk.msg_getCapMaxRxAntGain.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinPerfTemp,
    { "minPerformaceTemperature", "rfapi_sdk.msg_getCapMinPerfTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxPerfTemp,
    { "maxPerformanceTemperature", "rfapi_sdk.msg_getCapMaxPerfTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinOperatingTemp,
    { "minOperatingTemperature", "rfapi_sdk.msg_getCapMinOperatingTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxOperatingTemp,
    { "maxOperatingTemperature", "rfapi_sdk.msg_getCapMaxOperatingTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapAntHardwareId,
    { "AntHardwareId", "rfapi_sdk.msg_getCapAntHardwareId.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapNumBands,
    { "numBands", "rfapi_sdk.msg_getCapNumBands.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapBand,
    { "band", "rfapi_sdk.msg_getCapBand.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinTxPower,
    { "minTxPower", "rfapi_sdk.msg_getCapMinTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxTxPower,
    { "maxTxPower", "rfapi_sdk.msg_getCapMaxTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapLimTxPower,
    { "limTxPower", "rfapi_sdk.msg_getCapLimTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMinFreq,
    { "minFreq", "rfapi_sdk.msg_getCapMinFreq.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxFreq,
    { "maxFreq", "rfapi_sdk.msg_getCapMaxFreq.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &getCapMaxIbw,
    { "maxIbw", "rfapi_sdk.msg_getCapMaxIbw.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &pathIdRxagc,
    { "pathId", "rfapi_sdk.msg_pathIdRxagc.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &enableRxagc,
    { "enable", "rfapi_sdk.msg_enableRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &forceGainRxagc,
    { "forceGain", "rfapi_sdk.msg_forceGainRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &gainIdxRxagc,
    { "gainIdx", "rfapi_sdk.msg_gainIdxRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &attackCountThresholdRxagc,
    { "attackCountThreshold", "rfapi_sdk.attackCountThresholdRxagc.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &recoveryCountThresholdRxagc,
    { "recoveryCountThreshold", "rfapi_sdk.recoveryCountThresholdRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &recoveryDelayRxagc,
    { "recoveryDelay", "rfapi_sdk.recoveryDelayRxagc.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &wbrssiPeakWindowRxagc,
    { "wbrssiPeakWindow", "rfapi_sdk.msg_wbrssiPeakWindowRxagc.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &wbrssiTargetRxagc,
    { "wbrssiTarget", "rfapi_sdk.msg_wbrssiTargetRxagc.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &wbrssiThresholdRxagc,
    { "wbrssiThreshold", "rfapi_sdk.msg_wbrssiThresholdRxagc.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &gainStepRxagc,
    { "gainStep", "rfapi_sdk.msg_gainStepRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numRxPathsRxagc,
    { "numRxPaths", "rfapi_sdk.msg_numRxPathsRxagc.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &wbrssiReqIdxWbrssi,
    { "wbrssiReqIdx", "rfapi_sdk.msg_wbrssiReqIdxWbrssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &sfnWbrssi,
    { "sfn", "rfapi_sdk.msg_sfnWbrssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &slotWbrssi,
    { "slot", "rfapi_sdk.msg_slotWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &scsWbrssi,
    { "scs", "rfapi_sdk.msg_scsWbrssi.len_body",
    FT_UINT8, BASE_HEX, VALS(rfapi_scs_freq_string_rssi), 0x0,
    NULL, HFILL } },
    { &numRxPathsWbrssi,
    { "numRxPaths", "rfapi_sdk.msg_numRxPathsWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numSlotsWbrssi,
    { "numSlots", "rfapi_sdk.msg_numSlotsWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &dataValidWbrssi,
    { "dataValid", "rfapi_sdk.msg_dataValidWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPathIdWbrssi,
    { "rxPathId", "rfapi_sdk.msg_rxPathIdWbrssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &symSfnWbrssi,
    { "sfn", "rfapi_sdk.msg_sfnWbrssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &symSlotWbrssi,
    { "slot", "rfapi_sdk.msg_slotWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &symIdxWbrssi,
    { "symbol", "rfapi_sdk.msg_symbolWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &lnaGainStateWbrssi,
    { "lnaGainState", "rfapi_sdk.msg_lnaGainStateWbrssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &symbRSSIdBFSWbrssi,
    { "symbRSSIdBFS", "rfapi_sdk.msg_symbRSSIdBFSWbrssi.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxGaindBWbrssi,
    { "rxGaindB", "rfapi_sdk.msg_rxGaindBWbrssi.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPowerReqIdx,
    { "txPowerReqIdx", "rfapi_sdk.msg_txPowerReqIdx.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &sfnTxPower,
    { "sfn", "rfapi_sdk.msg_sfnTxPower.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &slotTxPower,
    { "slot", "rfapi_sdk.msg_slotTxPower.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &scsTxPower,
    { "scs", "rfapi_sdk.msg_scsTxPower.len_body",
    FT_UINT8, BASE_HEX, VALS(rfapi_scs_freq_string_rssi), 0x0,
    NULL, HFILL } },
    { &numTxPathsTxPower,
    { "numTxPaths", "rfapi_sdk.msg_numTxPathsTxPower.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numTrxDevicesTxPower,
    { "numTrxDevices", "rfapi_sdk.numTrxDevicesTxPower.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPathIdTxPower,
    { "txPathId", "rfapi_sdk.msg_txPathIdTxPower.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txDeviceIdTxPower,
    { "txDeviceId", "rfapi_sdk.msg_txDevicesIdTxPower.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPowerDbmTxPower,
    { "txPowerDbm", "rfapi_sdk.msg_txPowerDbmTxPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &tssiReqIdx,
    { "tssiReqIdx", "rfapi_sdk.msg_tssiReqIdxTssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &sfnTssi,
    { "sfn", "rfapi_sdk.msg_sfnTssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &slotTssi,
    { "slot", "rfapi_sdk.msg_slotTssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &scsTssi,
    { "scs", "rfapi_sdk.msg_scsTssi.len_body",
    FT_UINT8, BASE_HEX, VALS(rfapi_scs_freq_string_rssi), 0x0,
    NULL, HFILL } },
    { &chainTypeTssi,
    { "chainType", "rfapi_sdk.msg_chainTypeTssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &carrierIdTssi,
    { "carrierId", "rfapi_sdk.msg_carrierIdTssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPathIdTssi,
    { "txPathId", "rfapi_sdk.msg_txPathIdTssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &startSymbolTssi,
    { "startSymbol", "rfapi_sdk.msg_startSymbolTssi.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numSymbolsTssi,
    { "numSymbols", "rfapi_sdk.msg_numSymbolsTssi.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &avgTssidBfs,
    { "avgTssidBfs", "rfapi_sdk.msg_avgTssidBfs.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &eirpdBmTssi,
    { "eirpdBm", "rfapi_sdk.msg_eirpdBmTssi.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trpdBmTssi,
    { "trpdBm", "rfapi_sdk.msg_trpdBmTssi.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numLayersAlarm,
    { "numLayers", "rfapi_sdk.msg_numLayersAlarm.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numTrxDevicesAlarm,
    { "numTrxDevices", "rfapi_sdk.msg_numTrxDevicesAlarm.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &moduleMaskAlarm,
    { "moduleAlarmMask", "rfapi_sdk.msg_moduleMaskAlarm.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &layerNumAlarm,
    { "layerNum", "rfapi_sdk.msg_numLayerAlarm.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &layerMaskAlarm,
    { "layerAlarmMask", "rfapi_sdk.msg_layerMaskAlarm.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &EIRPDbmAlarm,
    { "EIRPDbm", "rfapi_sdk.msg_EIRPDbmAlarm.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxGaindBAlarm,
    { "rxGaindb", "rfapi_sdk.msg_rxGaindBAlarm.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trxDeviceNumAlarm,
    { "trxDeviceNum", "rfapi_sdk.msg_trxDeviceNumAlarm.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &deviceMaskAlarm,
    { "deviceAlarmMask", "rfapi_sdk.msg_deviceMaskAlarm.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &tempAlarm,
    { "temperature", "rfapi_sdk.msg_tempAlarm.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresMaxTxOperatingPower,
    { "maxTxOperatingPower", "rfapi_sdk.msg_thresMaxTxOperatingPower.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresTxPowerLimit,
    { "txPowerLimit", "rfapi_sdk.msg_thresTxPowerLimit.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresMaxPerformanceTemp,
    { "maxPerformanceTemp", "rfapi_sdk.msg_thresMaxPerformanceTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresMinPerformanceTemp,
    { "minPerformanceTemp", "rfapi_sdk.msg_thresMinPerformanceTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresMaxOperatingTemp,
    { "maxOperatingTemp", "rfapi_sdk.msg_thresMaxOperatingTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &thresMinOperatingTemp,
    { "minOperatingTemp", "rfapi_sdk.msg_thresMinOperatingTemp.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &testIdx,
    { "testIdx", "rfapi_sdk.msg_testIdx.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &testType,
    { "testType", "rfapi_sdk.msg_testType.len_body",
    FT_INT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &testStatus,
    { "testStatus", "rfapi_sdk.msg_testStatus.len_body",
    FT_UINT32, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trxDeviceNumRegData,
    { "trxDeviceNum", "rfapi_sdk.trxDeviceNumRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &regDataFetchTypeRegData,
    { "regDataFetchType", "rfapi_sdk.regDataFetchTypeRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &beamIdRegData,
    { "beamId", "rfapi_sdk.beamIdRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numTrxDevicesRegData,
    { "numTrxDevices", "rfapi_sdk.numTrxDevicesRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trxDevReadStatusRegData,
    { "trxDevReadStatus", "rfapi_sdk.trxDevReadStatusRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxCodebookIndexRegData,
    { "rxCodebookIndex", "rfapi_sdk.rxCodebookIndexRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txCodebookIndexRegData,
    { "txCodebookIndex", "rfapi_sdk.txCodebookIndexRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxBeamIdsRegData,
    { "rxBeamIds", "rfapi_sdk.rxBeamIdsRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txBeamIdsRegData,
    { "txBeamIds", "rfapi_sdk.txBeamIdsRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxAntElementOnOffRegData,
    { "rxAntElementOnOff", "rfapi_sdk.rxAntElementOnOffRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txAntElementOnOffRegData,
    { "txAntElementOnOff", "rfapi_sdk.txAntElementOnOffRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numLayersRegData,
    { "numLayers", "rfapi_sdk.numLayersRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &numCodebookEntriesRegData,
    { "numCodebookEntries", "rfapi_sdk.numCodebookEntriesRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &codebookIdxRegData,
    { "codebookIdx", "rfapi_sdk.codebookIdxRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPhaseShifterQ1Elem12RegData,
    { "txPhaseShifterQ1Elem12", "rfapi_sdk.txPhaseShifterQ1Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPhaseShifterQ1Elem34RegData,
    { "txPhaseShifterQ1Elem34", "rfapi_sdk.txPhaseShifterQ1Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPhaseShifterQ2Elem12RegData,
    { "txPhaseShifterQ2Elem12", "rfapi_sdk.txPhaseShiferQ2Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txPhaseShifterQ2Elem34RegData,
    { "txPhaseShifterQ2Elem34", "rfapi_sdk.txPhaseShifterQ2Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txEnableQ1RegData,
    { "txEnableQ1", "rfapi_sdk.txEnableQ1RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txEnableQ2RegData,
    { "txEnableQ2", "rfapi_sdk.txEnableQ2RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txEnableLayer1RegData,
    { "txEnableLayer1", "rfapi_sdk.txEnableLayer1RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txEnableLoVcoRegData,
    { "txEnableLoVco", "rfapi_sdk.txEnableLoVcoRegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txFineGainQ1Elem12RegData,
    { "txFineGainQ1Elem12", "rfapi_sdk.txFineGainQ1Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txFineGainQ1Elem34RegData,
    { "txFineGainQ1Elem34", "rfapi_sdk.txFineGainQ1Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txFineGainQ2Elem12RegData,
    { "txFineGainQ2Elem12", "rfapi_sdk.txFineGainQ2Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &txFineGainQ2Elem34RegData,
    { "txFineGainQ2Elem34", "rfapi_sdk.txFineGainQ2Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPhaseShifterQ1Elem12RegData,
    { "rxPhaseShifterQ1Elem12", "rfapi_sdk.rxPhaseShifterQ1Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPhaseShifterQ1Elem34RegData,
    { "rxPhaseShifterQ1Elem34", "rfapi_sdk.rxPhaseShifterQ1Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPhaseShifterQ2Elem12RegData,
    { "rxPhaseShifterQ2Elem12", "rfapi_sdk.rxPhaseShifterQ2Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxPhaseShifterQ2Elem34RegData,
    { "rxPhaseShifterQ2Elem34", "rfapi_sdk.rxPhaseShifterQ2Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxFineGainQ1Elem12RegData,
    { "rxFineGainQ1Elem12", "rfapi_sdk.rxFineGainQ1Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxFineGainQ1Elem34RegData,
    { "rxFineGainQ1Elem34", "rfapi_sdk.rxFineGainQ1Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxFineGainQ2Elem12RegData,
    { "rxFineGainQ2Elem12", "rfapi_sdk.rxFineGainQ2Elem12RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxFineGainQ2Elem34RegData,
    { "rxFineGainQ2Elem34", "rfapi_sdk.rxFineGainQ2Elem34RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxEnableQ1RegData,
    { "rxEnableQ1", "rfapi_sdk.rxEnableQ1RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxEnableQ2RegData,
    { "rxEnableQ2", "rfapi_sdk.rxEnableQ2RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxEnableLayer1RegData,
    { "rxEnableLayer1", "rfapi_sdk.rxEnableLayer1RegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &rxEnableLOVCORegData,
    { "rxEnableLOVCO", "rfapi_sdk.rxEnableLOVCORegData.len_body",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
    { &trxDeviceNumRegData,
    { "trxDeviceNum", "rfapi_sdk.trxDeviceNumRegData.len_body",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL } },
  };

  static gint* ett[] = {
  &ett_rfapi_sdk_msg_header,
  &ett_rfapi_sdk_msg_data,
  &ett_rfapi_sdk_msg_data_subtree1,
  &ett_rfapi_sdk_msg_data_subtree2,
  &ett_rfapi_sdk_msg_data_subtree3,
  &ett_rfapi_sdk_msg_data_subtree4,
  &ett_msg_fragment,
  &ett_msg_fragments
  };
  proto_rfapi_sdk = proto_register_protocol("RFAPI SDK Protocol", "* RFAPI SDK *", "rfapi_sdk");
  proto_register_field_array(proto_rfapi_sdk, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));
  register_dissector("rfapi_sdk", dissect_rfapi_sdk, proto_rfapi_sdk);
}

static guint16
sci_tvb_get_ntohs(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntohs(tvb, offset);
  else
    return tvb_get_letohs(tvb, offset);
}

static guint32
sci_tvb_get_ntoh24(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntoh24(tvb, offset);
  else
    return tvb_get_letoh24(tvb, offset);
}

static guint32
sci_tvb_get_ntohl(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntohl(tvb, offset);
  else
    return tvb_get_letohl(tvb, offset);
}

static guint64
sci_tvb_get_ntoh40(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntoh40(tvb, offset);
  else
    return tvb_get_letoh40(tvb, offset);
}

static guint64
sci_tvb_get_ntoh48(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntoh48(tvb, offset);
  else
    return tvb_get_letoh48(tvb, offset);
}

static guint64
sci_tvb_get_ntoh56(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntoh56(tvb, offset);
  else
    return tvb_get_letoh56(tvb, offset);
}

static guint64
sci_tvb_get_ntoh64(tvbuff_t* tvb, const gint offset)
{
  if (global_rfapi_sdk_BIG_ENDIAN)
    return tvb_get_ntoh64(tvb, offset);
  else
    return tvb_get_letoh64(tvb, offset);
}

int
bitExtracted(guint16 number, int k, int p)
{
  return (((1 << k) - 1) & (number >> (p - 1)));
}

int
has_message_data(guint16 header_type)
{
  switch(header_type)
  {
    case RF_START_REQ:
    case RF_STOP_REQ:
    case RF_GET_BEAM_TABLE_REQ:
    case RF_GET_CAPABILITIES_REQ:
    case RF_GET_TEMPERATURE_REQ:
      return 0;
    default:
      return 1;
  }
}
static void
set_iterators(int id)
{
  if(id == SET_ITERATORS_SET_BEAM_RSP)
  {
    if(num_trx_devices_global != MAX_TRX_DEVICES - 1 &&
       num_ants_per_trx_layer_global == MAX_ANTS_PER_LAYER -1 &&
      if_half_broken == NO_PARAM && location_broken == NO_PARAM
      && last_param_success == NUM_PARAMS_SET_BEAM)
    {
      num_ants_per_trx_layer_global= 0;
    }
    if(num_trx_devices_global == MAX_TRX_DEVICES - 1 &&
       num_ants_per_trx_layer_global == MAX_ANTS_PER_LAYER -1 &&
       if_half_broken == NO_PARAM && location_broken == NO_PARAM
       && last_param_success == NUM_PARAMS_SET_BEAM)
    {
      num_trx_devices_global = 0;
      num_ants_per_trx_layer_global= 0;
    }
  }
  else if(id == SET_ITERATORS_GET_BEAM_RSP)
  {
    if(num_trx_devices_global != MAX_TRX_DEVICES - 1 &&
       num_ants_per_trx_layer_global == MAX_ANTS_PER_LAYER -1 &&
       if_half_broken == NO_PARAM && location_broken == NO_PARAM
       && last_param_success == NUM_PARAMS_GET_BEAM)
    {
      num_ants_per_trx_layer_global= 0;
    }
    if(num_trx_devices_global == MAX_TRX_DEVICES - 1 &&
       num_ants_per_trx_layer_global == MAX_ANTS_PER_LAYER -1 &&
       if_half_broken == NO_PARAM && location_broken == NO_PARAM
       && last_param_success == NUM_PARAMS_GET_BEAM)
    {
      num_trx_devices_global = 0;
      num_ants_per_trx_layer_global= 0;
    }
  }
  else if(id == SET_ITERATORS_GET_WBRSSI_IND)
  {
    if(numSlotsItrWbrssi != numSlotsWbrssiG - 1 &&
       numSymbolsItrWbrssi == NUM_SYMBOLS_PER_SLOT -1 &&
       ifHalfBrokenWbrssi == NO_PARAM && locationBrokenWbrssi == NO_PARAM
       && lastParamSuccessWbrssi == NUM_PARAMS_GET_WBRSSI)
    {
      numSymbolsItrWbrssi= 0;
    }
    if(numSlotsItrWbrssi == numSlotsWbrssiG - 1 &&
       numSymbolsItrWbrssi == NUM_SYMBOLS_PER_SLOT -1 &&
       ifHalfBrokenWbrssi == NO_PARAM && locationBrokenWbrssi == NO_PARAM
       && lastParamSuccessWbrssi == NUM_PARAMS_GET_WBRSSI)
    {
      numSlotsItrWbrssi = 0;
      numSymbolsItrWbrssi= 0;
      numLayersItrWrssi += 1;
    }
  }
  else if(id == SET_ITERATORS_GET_REG_DATA_RSP)
  {
    if(numCodebookEntriesItrRegData == numCodebookEntriesGetRegDataGlobal
       && numLayersItrRegData != numLayersGetRegDataGlobal)
    {
      numCodebookEntriesItrRegData = 1;
      numLayersItrRegData += 1;
    }
    else if(numCodebookEntriesItrRegData == numCodebookEntriesGetRegDataGlobal
            && numLayersItrRegData == numLayersGetRegDataGlobal)
    {
      numCodebookEntriesItrRegData = 1;
      numLayersItrRegData = 1;
      numTrxDevicesItrRegData += 1;
    }
  }
}

static void
dissect_rf_enable_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint8 exec_now;
  guint8 slott;
  guint8 scst;
  guint16 frameT;
  guint32  exectime1;
  proto_item* ti1;
  guint16 systemframet;
  guint8 subframet;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  exec_now = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, execnow, tvb, *plen, sizeof(exec_now), exec_now);
  *plen += sizeof(exec_now);
  exectime1 = sci_tvb_get_ntohl(tvb, *plen);
  ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb,
                                       *plen, sizeof(exectime1),
  "execTime: %d",exectime1);
  rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
  frameT = sci_tvb_get_ntohs(tvb, *plen);
  systemframet = bitExtracted(frameT,NUM_BITS_FOR_SYSTEM_FRAME,START_BIT_FOR_SYSTEM_FRAME);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, systemframe, tvb, *plen,
                      sizeof(subframet), systemframet);
  subframet = bitExtracted(frameT,NUM_BITS_FOR_SUBFRAME,START_BIT_FOR_SUBFRAME);
  *plen += sizeof(subframet);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, subframe, tvb, *plen, sizeof(subframet), subframet);
  *plen += sizeof(subframet);
  slott = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, slot1, tvb, *plen, sizeof(slott), slott);
  *plen += sizeof(slott);
  scst = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, scs, tvb, *plen, sizeof(scst), scst);
  *plen += sizeof(scst);
}

static void
dissect_rf_rxagc_ctrl_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16    pathIdTemp ;
  guint8     enableTemp ;
  guint8     numRxPathsTemp ;
  guint8     forceGainTemp ;
  guint8     gainIdxTemp ;
  guint16    attackCountThresholdTemp ;
  guint8     recoveryCountThresholdTemp ;
  guint16    recoveryDelayTemp ;
  guint8     numRxPathsItr ;
  guint16    wbrssiPeakWindowTemp ;
  gint16     wbrssiTargetTemp ;
  guint16    wbrssiThresholdTemp ;
  guint8     gainStepTemp ;
  gint16     temp  = 0;
  guint8     sizePerIterartion = 0;
  proto_item* ti;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc_expand = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  numRxPathsTemp = tvb_get_guint8(tvb, *plen);
  sizePerIterartion = sizeof(pathIdTemp) + sizeof(enableTemp)+ sizeof(forceGainTemp) +
                      sizeof(gainIdxTemp) + sizeof(attackCountThresholdTemp) +
                      sizeof(recoveryCountThresholdTemp) +sizeof(recoveryDelayTemp) +
                      sizeof(wbrssiPeakWindowTemp) + sizeof(wbrssiThresholdTemp) +
                      sizeof(gainStepTemp) + sizeof(wbrssiTargetTemp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numRxPathsRxagc, tvb, *plen,
                      sizeof(numRxPathsTemp),numRxPathsTemp);
  *plen += sizeof(numRxPathsTemp);
  for(numRxPathsItr = 0; numRxPathsItr < numRxPathsTemp; numRxPathsItr++)
  {
    ti = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
                                        sizePerIterartion,"rxPath: %d",numRxPathsItr);
    rfapi_sdk_msg_dsc_expand = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_data_subtree1);
    pathIdTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, pathIdRxagc, tvb, *plen,
                       sizeof(pathIdTemp),pathIdTemp);
    *plen += sizeof(pathIdTemp);
    enableTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, enableRxagc, tvb, *plen,
                       sizeof(enableTemp),enableTemp);
    *plen += sizeof(enableTemp);
    forceGainTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, forceGainRxagc, tvb, *plen,
                       sizeof(forceGainTemp),forceGainTemp);
    *plen += sizeof(forceGainTemp);
    gainIdxTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, gainIdxRxagc, tvb, *plen,
                       sizeof(gainIdxTemp),gainIdxTemp);
    *plen += sizeof(gainIdxTemp);
    attackCountThresholdTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, attackCountThresholdRxagc, tvb, *plen,
                       sizeof(attackCountThresholdTemp), attackCountThresholdTemp);
    *plen += sizeof(attackCountThresholdTemp);
    recoveryCountThresholdTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, recoveryCountThresholdRxagc, tvb, *plen,
                        sizeof(recoveryCountThresholdTemp), recoveryCountThresholdTemp);
    *plen += sizeof(recoveryCountThresholdTemp);
    recoveryDelayTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, recoveryDelayRxagc, tvb, *plen,
                        sizeof(recoveryDelayTemp), recoveryDelayTemp);
    *plen += sizeof(recoveryDelayTemp);
    wbrssiPeakWindowTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, wbrssiPeakWindowRxagc, tvb, *plen,
                        sizeof(wbrssiPeakWindowTemp), wbrssiPeakWindowTemp);
    *plen += sizeof(wbrssiPeakWindowTemp);
    wbrssiTargetTemp = sci_tvb_get_ntohs(tvb, *plen);
    temp = (short)wbrssiTargetTemp;
    proto_tree_add_int(rfapi_sdk_msg_dsc_expand, wbrssiTargetRxagc, tvb, *plen,
                      sizeof(wbrssiTargetTemp), wbrssiTargetTemp);
    *plen += sizeof(wbrssiTargetTemp);
    wbrssiThresholdTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, wbrssiThresholdRxagc, tvb, *plen,
                       sizeof(wbrssiThresholdTemp), wbrssiThresholdTemp);
    *plen += sizeof(wbrssiThresholdTemp);
    gainStepTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc_expand, gainStepRxagc, tvb, *plen,
                        sizeof(gainStepTemp), gainStepTemp);
    *plen += sizeof(gainStepTemp);
  }
}

static void
dissect_rf_get_wbrssi_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16 wbrssiReqIdxTemp;
  guint8 scsTemp;
  guint16 sfnTemp;
  guint8 slotTemp;
  guint8 numSlotTemp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  wbrssiReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, wbrssiReqIdxWbrssi, tvb, *plen,
                       sizeof(wbrssiReqIdxTemp), wbrssiReqIdxTemp);
  *plen += sizeof(wbrssiReqIdxTemp);
  sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnWbrssi, tvb, *plen,
                      sizeof(sfnTemp), sfnTemp);
  *plen += sizeof(sfnTemp);
  slotTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, slotWbrssi, tvb, *plen,
                      sizeof(slotTemp), slotTemp);
  *plen += sizeof(slotTemp);
  scsTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, scsWbrssi, tvb, *plen,
                      sizeof(scsTemp), scsTemp);
  *plen += sizeof(scsTemp);
  numSlotTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numSlotsWbrssi, tvb, *plen,
                      sizeof(numSlotTemp), numSlotTemp);
  *plen += sizeof(numSlotTemp);
}

static void
dissect_rf_get_tssi_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16   tssiReqIdxTemp;
  guint16   sfnTemp;
  guint8    slotTemp;
  guint8    scsTemp;
  guint8    chainTypeTemp;
  guint16   txPathIdTemp;
  guint8    carrierIdTemp;
  guint8    startSymbolTemp;
  guint16   numSymbolsTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  tssiReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, tssiReqIdx, tvb, *plen,
                       sizeof(tssiReqIdxTemp), tssiReqIdxTemp);
  *plen += sizeof(tssiReqIdxTemp);
  sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnTssi, tvb, *plen,
                      sizeof(sfnTemp), sfnTemp);
  *plen += sizeof(sfnTemp);
  slotTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, slotTssi, tvb, *plen,
                      sizeof(slotTemp), slotTemp);
  *plen += sizeof(slotTemp);
  scsTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, scsTssi, tvb, *plen,
                      sizeof(scsTemp), scsTemp);
  *plen += sizeof(scsTemp);
  chainTypeTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, chainTypeTssi, tvb, *plen,
                      sizeof(chainTypeTemp), chainTypeTemp);
  *plen += sizeof(chainTypeTemp);
  carrierIdTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, carrierIdTssi, tvb, *plen,
                      sizeof(carrierIdTemp), carrierIdTemp);
  *plen += sizeof(carrierIdTemp);
  txPathIdTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, txPathIdTssi, tvb, *plen,
                      sizeof(txPathIdTemp), txPathIdTemp);
  *plen += sizeof(txPathIdTemp);
  startSymbolTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, startSymbolTssi, tvb, *plen,
                      sizeof(startSymbolTemp), startSymbolTemp);
  *plen += sizeof(startSymbolTemp);
  numSymbolsTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numSymbolsTssi, tvb, *plen,
                      sizeof(numSymbolsTemp), numSymbolsTemp);
  *plen += sizeof(numSymbolsTemp);
}

static void
dissect_rf_get_tx_power_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16 txPowerReqIdxTemp;
  guint8 scsTemp;
  guint16 sfnTemp;
  guint8 slotTemp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  txPowerReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, txPowerReqIdx, tvb, *plen,
                       sizeof(txPowerReqIdxTemp), txPowerReqIdxTemp);
  *plen += sizeof(txPowerReqIdxTemp);
  sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnTxPower, tvb, *plen,
                      sizeof(sfnTemp), sfnTemp);
  *plen += sizeof(sfnTemp);
  slotTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, slotTxPower, tvb, *plen,
                      sizeof(slotTemp), slotTemp);
  *plen += sizeof(slotTemp);
  scsTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, scsTxPower, tvb, *plen,
                      sizeof(scsTemp), scsTemp);
  *plen += sizeof(scsTemp);
}

static void
dissect_rf_get_reg_data_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint8    trxDeviceNumTemp;
  guint8    regDataFetchTypeTemp;
  guint16   beamIdTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  trxDeviceNumTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, trxDeviceNumRegData, tvb, *plen,
                      sizeof(trxDeviceNumTemp), trxDeviceNumTemp);
  *plen += sizeof(trxDeviceNumTemp);
  regDataFetchTypeTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, regDataFetchTypeRegData, tvb, *plen,
                      sizeof(regDataFetchTypeTemp), regDataFetchTypeTemp);
  *plen += sizeof(regDataFetchTypeTemp);
  beamIdTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, beamIdRegData, tvb, *plen,
                      sizeof(beamIdTemp), beamIdTemp);
  *plen += sizeof(beamIdTemp);
}

static void
dissect_rf_get_reg_data_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32      statusTemp;
  guint8       trxDeviceNumTemp;
  guint8       trxDevReadStatusTemp;
  guint16      rxCodebookIndexTemp;
  guint16      txCodebookIndexTemp;
  guint16      rxBeamIdsTemp;
  guint16      txBeamIdsTemp;
  guint16      rxAntElementOnOffTemp;
  guint16      txAntElementOnOffTemp;
  guint16      codebookIdxTemp;
  guint16      beamIdTemp;
  guint16      txPhaseShifterQ1Elem12Temp;
  guint16      txPhaseShifterQ1Elem34Temp;
  guint16      txPhaseShifterQ2Elem12Temp;
  guint16      txPhaseShifterQ2Elem34Temp;
  guint16      txEnableQ1Temp;
  guint16      txEnableQ2Temp;
  guint16      txEnableLayer1Temp;
  guint16      txEnableLoVcoTemp;
  guint16      txFineGainQ1Elem12Temp;
  guint16      txFineGainQ1Elem34Temp;
  guint16      txFineGainQ2Elem12Temp;
  guint16      txFineGainQ2Elem34Temp;
  guint16      rxPhaseShifterQ1Elem12Temp;
  guint16      rxPhaseShifterQ1Elem34Temp;
  guint16      rxPhaseShifterQ2Elem12Temp;
  guint16      rxPhaseShifterQ2Elem34Temp;
  guint16      rxFineGainQ1Elem12Temp;
  guint16      rxFineGainQ1Elem34Temp;
  guint16      rxFineGainQ2Elem12Temp;
  guint16      rxFineGainQ2Elem34Temp;
  guint16      rxEnableQ1Temp;
  guint16      rxEnableQ2Temp;
  guint16      rxEnableLayer1Temp;
  guint16      rxEnableLOVCOTemp;
  guint8       numTrxDevicesItr;
  guint8       numLayersItr;
  guint8       second_half_packet;
  guint8       numCodebookEntriesItr;
  gint16 packs = vend_spec_len;
  proto_tree*  rfapi_sdk_msg_dsc = NULL;
  proto_tree*  rfapi_sdk_msg_dsc1 = NULL;
  proto_tree*  rfapi_sdk_msg_dsc2 = NULL;
  proto_tree*  rfapi_sdk_msg_dsc3 = NULL;
  proto_tree*  rfapi_sdk_msg_dsc4 = NULL;
  proto_item*  ti1;
  proto_item*  ti2;
  proto_item*  ti3;
  proto_item*  ti4;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  if(segment == 0)
  {
    statusTemp = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                        sizeof(statusTemp), statusTemp);
    *plen += sizeof(statusTemp);
    regDataFetchTypeGlobal = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, regDataFetchTypeRegData, tvb, *plen,
                        sizeof(regDataFetchTypeGlobal), regDataFetchTypeGlobal);
    *plen += sizeof(regDataFetchTypeGlobal);
    numTrxDevicesGetRegDataGlobal = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numTrxDevicesRegData, tvb, *plen,
                        sizeof(numTrxDevicesGetRegDataGlobal), numTrxDevicesGetRegDataGlobal);
    *plen += sizeof(numTrxDevicesGetRegDataGlobal);

    numTrxDevicesItrRegData = 1;
    numLayersItrRegData = 1;
    numCodebookEntriesItrRegData = 1;

    locationBrokenGetRegData = NO_PARAM;
    lastParamSuccessGetRegData = NO_PARAM;
    ifHalfBrokenGetRegData = NO_PARAM;

  }
  else
  {
    packs += sizeof(statusTemp) + sizeof(regDataFetchTypeGlobal)
           + sizeof(numTrxDevicesGetRegDataGlobal);
  }
  packs = packs - SIZE_OF_COMMON_MSG_HEADER - ( sizeof(statusTemp) +
          sizeof(regDataFetchTypeGlobal) + sizeof(numTrxDevicesGetRegDataGlobal));

  for (numTrxDevicesItr = numTrxDevicesItrRegData; numTrxDevicesItr <=
       numTrxDevicesGetRegDataGlobal; numTrxDevicesItr++)
  {
    numTrxDevicesItrRegData = numTrxDevicesItr;
    ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
                                          0,"TrxDevice[%d]", numTrxDevicesItr);
    rfapi_sdk_msg_dsc1 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);

    if(packs - sizeof(trxDeviceNumTemp) < 0 && packs != 0)
    {
      locationBrokenGetRegData = FIRST_PARAM;
      ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
      firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(trxDeviceNumTemp) && (locationBrokenGetRegData == FIRST_PARAM
       || locationBrokenGetRegData == NO_PARAM))
    {
      packs -= sizeof(trxDeviceNumTemp);
      trxDeviceNumTemp = tvb_get_guint8(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc1, trxDeviceNumRegData, tvb, *plen,
                          sizeof(trxDeviceNumTemp), trxDeviceNumTemp);
      lastParamSuccessGetRegData = FIRST_PARAM;
      locationBrokenGetRegData = NO_PARAM;
      *plen += sizeof(trxDeviceNumTemp);
    }
    if(packs - sizeof(trxDevReadStatusTemp) < 0 && packs != 0)
    {
      locationBrokenGetRegData = SECOND_PARAM;
      ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
      firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(trxDevReadStatusTemp) && (locationBrokenGetRegData == SECOND_PARAM
       || locationBrokenGetRegData == NO_PARAM))
    {
      packs -= sizeof(trxDevReadStatusTemp);
      trxDevReadStatusTemp = tvb_get_guint8(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc1, trxDevReadStatusRegData, tvb, *plen,
                          sizeof(trxDevReadStatusTemp), trxDevReadStatusTemp);
      lastParamSuccessGetRegData = SECOND_PARAM;
      locationBrokenGetRegData = NO_PARAM;
      *plen += sizeof(trxDevReadStatusTemp);
    }
    if(regDataFetchTypeGlobal == 1 || regDataFetchTypeGlobal == 3)
    {
      if(packs < sizeof(rxCodebookIndexTemp) && packs != 0)
      {
        locationBrokenGetRegData = THIRD_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(rxCodebookIndexTemp) && (locationBrokenGetRegData == THIRD_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(rxCodebookIndexTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          rxCodebookIndexTemp = 0;
          rxCodebookIndexTemp = rxCodebookIndexTemp | second_half_packet;
          rxCodebookIndexTemp = (rxCodebookIndexTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxCodebookIndexRegData, tvb, *plen,
                             sizeof(rxCodebookIndexTemp)/2, rxCodebookIndexTemp);
          *plen += sizeof(rxCodebookIndexTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(rxCodebookIndexTemp);
          rxCodebookIndexTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxCodebookIndexRegData, tvb, *plen,
                             sizeof(rxCodebookIndexTemp), rxCodebookIndexTemp);
          *plen += sizeof(rxCodebookIndexTemp);
          lastParamSuccessGetRegData = THIRD_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }

      if(packs < sizeof(txCodebookIndexTemp) && packs != 0)
      {
        locationBrokenGetRegData = FOURTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(txCodebookIndexTemp) && (locationBrokenGetRegData == FOURTH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(txCodebookIndexTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          txCodebookIndexTemp = 0;
          txCodebookIndexTemp = txCodebookIndexTemp | second_half_packet;
          txCodebookIndexTemp = (txCodebookIndexTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txCodebookIndexRegData, tvb, *plen,
                             sizeof(txCodebookIndexTemp)/2, txCodebookIndexTemp);
          *plen += sizeof(txCodebookIndexTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(txCodebookIndexTemp);
          txCodebookIndexTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txCodebookIndexRegData, tvb, *plen,
                             sizeof(txCodebookIndexTemp), txCodebookIndexTemp);
          *plen += sizeof(txCodebookIndexTemp);
          lastParamSuccessGetRegData = FOURTH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
      if(packs < sizeof(rxBeamIdsTemp) && packs != 0)
      {
        locationBrokenGetRegData = FIFTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(rxBeamIdsTemp) && (locationBrokenGetRegData == FIFTH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(rxBeamIdsTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          rxBeamIdsTemp = 0;
          rxBeamIdsTemp = rxBeamIdsTemp | second_half_packet;
          rxBeamIdsTemp = (rxBeamIdsTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxBeamIdsRegData, tvb, *plen,
                             sizeof(rxBeamIdsTemp)/2, rxBeamIdsTemp);
          *plen += sizeof(rxBeamIdsTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(rxBeamIdsTemp);
          rxBeamIdsTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxBeamIdsRegData, tvb, *plen,
                             sizeof(rxBeamIdsTemp), rxBeamIdsTemp);
          *plen += sizeof(rxBeamIdsTemp);
          lastParamSuccessGetRegData = FIFTH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
      if(packs < sizeof(txBeamIdsTemp) && packs != 0)
      {
        locationBrokenGetRegData = SIXTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(txBeamIdsTemp) && (locationBrokenGetRegData == SIXTH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(txBeamIdsTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          txBeamIdsTemp = 0;
          txBeamIdsTemp = txBeamIdsTemp | second_half_packet;
          txBeamIdsTemp = (txBeamIdsTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txBeamIdsRegData, tvb, *plen,
                             sizeof(txBeamIdsTemp)/2, txBeamIdsTemp);
          *plen += sizeof(txBeamIdsTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(txBeamIdsTemp);
          txBeamIdsTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txBeamIdsRegData, tvb, *plen,
                             sizeof(txBeamIdsTemp), txBeamIdsTemp);
          *plen += sizeof(txBeamIdsTemp);
          lastParamSuccessGetRegData = SIXTH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
      if(packs < sizeof(rxAntElementOnOffTemp) && packs != 0)
      {
        locationBrokenGetRegData = SEVENTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(rxAntElementOnOffTemp) && (locationBrokenGetRegData == SEVENTH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(rxAntElementOnOffTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          rxAntElementOnOffTemp = 0;
          rxAntElementOnOffTemp = rxAntElementOnOffTemp | second_half_packet;
          rxAntElementOnOffTemp = (rxAntElementOnOffTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxAntElementOnOffRegData, tvb, *plen,
                             sizeof(rxAntElementOnOffTemp)/2, rxAntElementOnOffTemp);
          *plen += sizeof(rxAntElementOnOffTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(rxAntElementOnOffTemp);
          rxAntElementOnOffTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxAntElementOnOffRegData, tvb, *plen,
                             sizeof(rxAntElementOnOffTemp), rxAntElementOnOffTemp);
          *plen += sizeof(rxAntElementOnOffTemp);
          lastParamSuccessGetRegData = SEVENTH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
      if(packs < sizeof(txAntElementOnOffTemp) && packs != 0)
      {
        locationBrokenGetRegData = EIGHTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(txAntElementOnOffTemp) && (locationBrokenGetRegData == EIGHTH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(txAntElementOnOffTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          txAntElementOnOffTemp = 0;
          txAntElementOnOffTemp = txAntElementOnOffTemp | second_half_packet;
          txAntElementOnOffTemp = (txAntElementOnOffTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txAntElementOnOffRegData, tvb, *plen,
                             sizeof(txAntElementOnOffTemp)/2, txAntElementOnOffTemp);
          *plen += sizeof(txAntElementOnOffTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(txAntElementOnOffTemp);
          txAntElementOnOffTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, txAntElementOnOffRegData, tvb, *plen,
                             sizeof(txAntElementOnOffTemp), txAntElementOnOffTemp);
          *plen += sizeof(txAntElementOnOffTemp);
          lastParamSuccessGetRegData = EIGHTH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
    }
    if(regDataFetchTypeGlobal == 2 || regDataFetchTypeGlobal == 3)
    {
     if(packs < sizeof(beamIdTemp) && packs != 0)
      {
        locationBrokenGetRegData = NINETH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(beamIdTemp) && (locationBrokenGetRegData == NINETH_PARAM ||
         locationBrokenGetRegData == NO_PARAM))
      {
        if(ifHalfBrokenGetRegData)
        {
          packs -= sizeof(beamIdTemp)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          beamIdTemp = 0;
          beamIdTemp = beamIdTemp | second_half_packet;
          beamIdTemp = (beamIdTemp << SECOND_HALF_PACKET_BITS)
                              | (firstHalfPacketGetRegData);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, beamIdRegData, tvb, *plen,
                             sizeof(beamIdTemp)/2, beamIdTemp);
          *plen += sizeof(beamIdTemp)/2;
          ifHalfBrokenGetRegData = NO_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
        else
        {
          packs -= sizeof(beamIdTemp);
          beamIdTemp = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, beamIdRegData, tvb, *plen,
                             sizeof(beamIdTemp), beamIdTemp);
          *plen += sizeof(beamIdTemp);
          lastParamSuccessGetRegData = NINETH_PARAM;
          locationBrokenGetRegData = NO_PARAM;
        }
      }
      if(packs - sizeof(numLayersGetRegDataGlobal) < 0 && packs != 0)
      {
        locationBrokenGetRegData = TENTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(numLayersGetRegDataGlobal) && (locationBrokenGetRegData == TENTH_PARAM
         || locationBrokenGetRegData == NO_PARAM))
      {
        packs -= sizeof(numLayersGetRegDataGlobal);
        numLayersGetRegDataGlobal = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, numLayersRegData, tvb, *plen,
                            sizeof(numLayersGetRegDataGlobal), numLayersGetRegDataGlobal);
        lastParamSuccessGetRegData = TENTH_PARAM;
        locationBrokenGetRegData = NO_PARAM;
        *plen += sizeof(numLayersGetRegDataGlobal);
      }
      if(packs - sizeof(numCodebookEntriesGetRegDataGlobal) < 0 && packs != 0)
      {
        locationBrokenGetRegData = ELEVENTH_PARAM;
        ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
        firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(numCodebookEntriesGetRegDataGlobal) && (locationBrokenGetRegData
         == ELEVENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
      {
        packs -= sizeof(numCodebookEntriesGetRegDataGlobal);
        numCodebookEntriesGetRegDataGlobal = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, numCodebookEntriesRegData, tvb, *plen,
                            sizeof(numCodebookEntriesGetRegDataGlobal),
                            numCodebookEntriesGetRegDataGlobal);
        lastParamSuccessGetRegData = ELEVENTH_PARAM;
        locationBrokenGetRegData = NO_PARAM;
        *plen += sizeof(numCodebookEntriesGetRegDataGlobal);
      }
      for (numLayersItr = numLayersItrRegData; numLayersItr <=
           numLayersGetRegDataGlobal; numLayersItr++)
      {
        for (numCodebookEntriesItr = numCodebookEntriesItrRegData; numCodebookEntriesItr
             <= numCodebookEntriesGetRegDataGlobal; numCodebookEntriesItr++)
        {
          numLayersItrRegData = numLayersItr;
          numCodebookEntriesItrRegData = numCodebookEntriesItr;
          ti2 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc1, proto_rfapi_sdk, tvb, *plen,
                                               0,"Layer[%d]CodebookEntry[%d]",
                                               numLayersItr,numCodebookEntriesItr);
          rfapi_sdk_msg_dsc2 = proto_item_add_subtree(ti2, ett_rfapi_sdk_msg_data_subtree1);
          if(packs < sizeof(codebookIdxTemp) && packs != 0)
          {
            locationBrokenGetRegData = TWELFTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(codebookIdxTemp) && (locationBrokenGetRegData == TWELFTH_PARAM ||
             locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(codebookIdxTemp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              codebookIdxTemp = 0;
              codebookIdxTemp = codebookIdxTemp | second_half_packet;
              codebookIdxTemp = (codebookIdxTemp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc2, codebookIdxRegData, tvb, *plen,
                                 sizeof(codebookIdxTemp)/2, codebookIdxTemp);
              *plen += sizeof(codebookIdxTemp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(codebookIdxTemp);
              codebookIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc2, codebookIdxRegData, tvb, *plen,
                                 sizeof(codebookIdxTemp), codebookIdxTemp);
              *plen += sizeof(codebookIdxTemp);
              lastParamSuccessGetRegData = TWELFTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(beamIdTemp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(beamIdTemp) && (locationBrokenGetRegData == THIRTEENTH_PARAM ||
             locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(beamIdTemp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              beamIdTemp = 0;
              beamIdTemp = beamIdTemp | second_half_packet;
              beamIdTemp = (beamIdTemp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc2, beamIdRegData, tvb, *plen,
                                 sizeof(beamIdTemp)/2, beamIdTemp);
              *plen += sizeof(beamIdTemp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(beamIdTemp);
              beamIdTemp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc2, beamIdRegData, tvb, *plen,
                                 sizeof(beamIdTemp), beamIdTemp);
              *plen += sizeof(beamIdTemp);
              lastParamSuccessGetRegData = THIRTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          ti3 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc2, proto_rfapi_sdk, tvb, *plen,
                                               0,"TX");
          rfapi_sdk_msg_dsc3 = proto_item_add_subtree(ti3, ett_rfapi_sdk_msg_data_subtree1);
          if(packs < sizeof(txPhaseShifterQ1Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = FOURTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txPhaseShifterQ1Elem12Temp) && (locationBrokenGetRegData
             == FOURTEENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txPhaseShifterQ1Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txPhaseShifterQ1Elem12Temp = 0;
              txPhaseShifterQ1Elem12Temp = txPhaseShifterQ1Elem12Temp | second_half_packet;
              txPhaseShifterQ1Elem12Temp = (txPhaseShifterQ1Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ1Elem12RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ1Elem12Temp)/2, txPhaseShifterQ1Elem12Temp);
              *plen += sizeof(txPhaseShifterQ1Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txPhaseShifterQ1Elem12Temp);
              txPhaseShifterQ1Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ1Elem12RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ1Elem12Temp), txPhaseShifterQ1Elem12Temp);
              *plen += sizeof(txPhaseShifterQ1Elem12Temp);
              lastParamSuccessGetRegData = FOURTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txPhaseShifterQ1Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = FIFTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txPhaseShifterQ1Elem34Temp) && (locationBrokenGetRegData
             == FIFTEENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txPhaseShifterQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txPhaseShifterQ1Elem34Temp = 0;
              txPhaseShifterQ1Elem34Temp = txPhaseShifterQ1Elem34Temp | second_half_packet;
              txPhaseShifterQ1Elem34Temp = (txPhaseShifterQ1Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ1Elem34RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ1Elem34Temp)/2, txPhaseShifterQ1Elem34Temp);
              *plen += sizeof(txPhaseShifterQ1Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txPhaseShifterQ1Elem34Temp);
              txPhaseShifterQ1Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ1Elem34RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ1Elem34Temp), txPhaseShifterQ1Elem34Temp);
              *plen += sizeof(txPhaseShifterQ1Elem34Temp);
              lastParamSuccessGetRegData = FIFTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txPhaseShifterQ2Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = SIXTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txPhaseShifterQ2Elem12Temp) && (locationBrokenGetRegData
             == SIXTEENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txPhaseShifterQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txPhaseShifterQ2Elem12Temp = 0;
              txPhaseShifterQ2Elem12Temp = txPhaseShifterQ2Elem12Temp | second_half_packet;
              txPhaseShifterQ2Elem12Temp = (txPhaseShifterQ2Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ2Elem12RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ2Elem12Temp)/2, txPhaseShifterQ2Elem12Temp);
              *plen += sizeof(txPhaseShifterQ2Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txPhaseShifterQ2Elem12Temp);
              txPhaseShifterQ2Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ2Elem12RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ2Elem12Temp), txPhaseShifterQ2Elem12Temp);
              *plen += sizeof(txPhaseShifterQ2Elem12Temp);
              lastParamSuccessGetRegData = SIXTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txPhaseShifterQ2Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = SEVENTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txPhaseShifterQ2Elem34Temp) && (locationBrokenGetRegData
              == SEVENTEENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txPhaseShifterQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txPhaseShifterQ2Elem34Temp = 0;
              txPhaseShifterQ2Elem34Temp = txPhaseShifterQ2Elem34Temp | second_half_packet;
              txPhaseShifterQ2Elem34Temp = (txPhaseShifterQ2Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ2Elem34RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ2Elem34Temp)/2, txPhaseShifterQ2Elem34Temp);
              *plen += sizeof(txPhaseShifterQ2Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txPhaseShifterQ2Elem34Temp);
              txPhaseShifterQ2Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txPhaseShifterQ2Elem34RegData, tvb, *plen,
                                 sizeof(txPhaseShifterQ2Elem34Temp), txPhaseShifterQ2Elem34Temp);
              *plen += sizeof(txPhaseShifterQ2Elem34Temp);
              lastParamSuccessGetRegData = SEVENTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txEnableQ1Temp) && packs != 0)
          {
            locationBrokenGetRegData = EIGHTEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txEnableQ1Temp) && (locationBrokenGetRegData == EIGHTEENTH_PARAM ||
             locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txEnableQ1Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txEnableQ1Temp = 0;
              txEnableQ1Temp = txEnableQ1Temp | second_half_packet;
              txEnableQ1Temp = (txEnableQ1Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableQ1RegData, tvb, *plen,
                                 sizeof(txEnableQ1Temp)/2, txEnableQ1Temp);
              *plen += sizeof(txEnableQ1Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txEnableQ1Temp);
              txEnableQ1Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableQ1RegData, tvb, *plen,
                                 sizeof(txEnableQ1Temp), txEnableQ1Temp);
              *plen += sizeof(txEnableQ1Temp);
              lastParamSuccessGetRegData = EIGHTEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txEnableQ2Temp) && packs != 0)
          {
            locationBrokenGetRegData = NINETEENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txEnableQ2Temp) && (locationBrokenGetRegData == NINETEENTH_PARAM ||
             locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txEnableQ2Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txEnableQ2Temp = 0;
              txEnableQ2Temp = txEnableQ2Temp | second_half_packet;
              txEnableQ2Temp = (txEnableQ2Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableQ2RegData, tvb, *plen,
                                 sizeof(txEnableQ2Temp)/2, txEnableQ2Temp);
              *plen += sizeof(txEnableQ2Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txEnableQ2Temp);
              txEnableQ2Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableQ2RegData, tvb, *plen,
                                 sizeof(txEnableQ2Temp), txEnableQ2Temp);
              *plen += sizeof(txEnableQ2Temp);
              lastParamSuccessGetRegData = NINETEENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txEnableLayer1Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTIETH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txEnableLayer1Temp) && (locationBrokenGetRegData == TWENTIETH_PARAM ||
             locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txEnableLayer1Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txEnableLayer1Temp = 0;
              txEnableLayer1Temp = txEnableLayer1Temp | second_half_packet;
              txEnableLayer1Temp = (txEnableLayer1Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableLayer1RegData, tvb, *plen,
                                 sizeof(txEnableLayer1Temp)/2, txEnableLayer1Temp);
              *plen += sizeof(txEnableLayer1Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txEnableLayer1Temp);
              txEnableLayer1Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableLayer1RegData, tvb, *plen,
                                 sizeof(txEnableLayer1Temp), txEnableLayer1Temp);
              *plen += sizeof(txEnableLayer1Temp);
              lastParamSuccessGetRegData = TWENTIETH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txEnableLoVcoTemp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_FIRST_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txEnableLoVcoTemp) && (locationBrokenGetRegData
             == TWENTY_FIRST_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txEnableLoVcoTemp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txEnableLoVcoTemp = 0;
              txEnableLoVcoTemp = txEnableLoVcoTemp | second_half_packet;
              txEnableLoVcoTemp = (txEnableLoVcoTemp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableLoVcoRegData, tvb, *plen,
                                 sizeof(txEnableLoVcoTemp)/2, txEnableLoVcoTemp);
              *plen += sizeof(txEnableLoVcoTemp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txEnableLoVcoTemp);
              txEnableLoVcoTemp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txEnableLoVcoRegData, tvb, *plen,
                                 sizeof(txEnableLoVcoTemp), txEnableLoVcoTemp);
              *plen += sizeof(txEnableLoVcoTemp);
              lastParamSuccessGetRegData = TWENTY_FIRST_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txFineGainQ1Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_SECOND_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txFineGainQ1Elem12Temp) && (locationBrokenGetRegData
             == TWENTY_SECOND_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txFineGainQ1Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txFineGainQ1Elem12Temp = 0;
              txFineGainQ1Elem12Temp = txFineGainQ1Elem12Temp | second_half_packet;
              txFineGainQ1Elem12Temp = (txFineGainQ1Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ1Elem12RegData, tvb, *plen,
                                 sizeof(txFineGainQ1Elem12Temp)/2, txFineGainQ1Elem12Temp);
              *plen += sizeof(txFineGainQ1Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txFineGainQ1Elem12Temp);
              txFineGainQ1Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ1Elem12RegData, tvb, *plen,
                                 sizeof(txFineGainQ1Elem12Temp), txFineGainQ1Elem12Temp);
              *plen += sizeof(txFineGainQ1Elem12Temp);
              lastParamSuccessGetRegData = TWENTY_SECOND_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txFineGainQ1Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_THIRD_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txFineGainQ1Elem34Temp) && (locationBrokenGetRegData
             == TWENTY_THIRD_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txFineGainQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txFineGainQ1Elem34Temp = 0;
              txFineGainQ1Elem34Temp = txFineGainQ1Elem34Temp | second_half_packet;
              txFineGainQ1Elem34Temp = (txFineGainQ1Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ1Elem34RegData, tvb, *plen,
                                 sizeof(txFineGainQ1Elem34Temp)/2, txFineGainQ1Elem34Temp);
              *plen += sizeof(txFineGainQ1Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txFineGainQ1Elem34Temp);
              txFineGainQ1Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ1Elem34RegData, tvb, *plen,
                                 sizeof(txFineGainQ1Elem34Temp), txFineGainQ1Elem34Temp);
              *plen += sizeof(txFineGainQ1Elem34Temp);
              lastParamSuccessGetRegData = TWENTY_THIRD_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txFineGainQ2Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_FOURTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txFineGainQ2Elem12Temp) && (locationBrokenGetRegData
             == TWENTY_FOURTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txFineGainQ2Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txFineGainQ2Elem12Temp = 0;
              txFineGainQ2Elem12Temp = txFineGainQ2Elem12Temp | second_half_packet;
              txFineGainQ2Elem12Temp = (txFineGainQ2Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ2Elem12RegData, tvb, *plen,
                                 sizeof(txFineGainQ2Elem12Temp)/2, txFineGainQ2Elem12Temp);
              *plen += sizeof(txFineGainQ2Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txFineGainQ2Elem12Temp);
              txFineGainQ2Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ2Elem12RegData, tvb, *plen,
                                 sizeof(txFineGainQ2Elem12Temp), txFineGainQ2Elem12Temp);
              *plen += sizeof(txFineGainQ2Elem12Temp);
              lastParamSuccessGetRegData = TWENTY_FOURTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(txFineGainQ2Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_FIFTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(txFineGainQ2Elem34Temp) && (locationBrokenGetRegData
             == TWENTY_FIFTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(txFineGainQ2Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              txFineGainQ2Elem34Temp = 0;
              txFineGainQ2Elem34Temp = txFineGainQ2Elem34Temp | second_half_packet;
              txFineGainQ2Elem34Temp = (txFineGainQ2Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ2Elem34RegData, tvb, *plen,
                                 sizeof(txFineGainQ2Elem34Temp)/2, txFineGainQ2Elem34Temp);
              *plen += sizeof(txFineGainQ2Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(txFineGainQ2Elem34Temp);
              txFineGainQ2Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc3, txFineGainQ2Elem34RegData, tvb, *plen,
                                 sizeof(txFineGainQ2Elem34Temp), txFineGainQ2Elem34Temp);
              *plen += sizeof(txFineGainQ2Elem34Temp);
              lastParamSuccessGetRegData = TWENTY_FIFTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if( packs == 0)
          {
            locationBrokenGetRegData = lastParamSuccessGetRegData + 1;
            ifHalfBrokenGetRegData = NO_PARAM;
            return;
          }
          ti4 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc2, proto_rfapi_sdk, tvb,
                                               *plen,0,"RX");
          rfapi_sdk_msg_dsc4 = proto_item_add_subtree(ti4, ett_rfapi_sdk_msg_data_subtree1);
          if(packs < sizeof(rxPhaseShifterQ1Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_SIXTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
          }
          if(packs >= sizeof(rxPhaseShifterQ1Elem12Temp) && (locationBrokenGetRegData
             == TWENTY_SIXTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxPhaseShifterQ1Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxPhaseShifterQ1Elem12Temp = 0;
              rxPhaseShifterQ1Elem12Temp = rxPhaseShifterQ1Elem12Temp | second_half_packet;
              rxPhaseShifterQ1Elem12Temp = (rxPhaseShifterQ1Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ1Elem12RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ1Elem12Temp)/2, rxPhaseShifterQ1Elem12Temp);
              *plen += sizeof(rxPhaseShifterQ1Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxPhaseShifterQ1Elem12Temp);
              rxPhaseShifterQ1Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ1Elem12RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ1Elem12Temp), rxPhaseShifterQ1Elem12Temp);
              *plen += sizeof(rxPhaseShifterQ1Elem12Temp);
              lastParamSuccessGetRegData = TWENTY_SIXTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxPhaseShifterQ1Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_SEVENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxPhaseShifterQ1Elem34Temp) && (locationBrokenGetRegData
             == TWENTY_SEVENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxPhaseShifterQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxPhaseShifterQ1Elem34Temp = 0;
              rxPhaseShifterQ1Elem34Temp = rxPhaseShifterQ1Elem34Temp | second_half_packet;
              rxPhaseShifterQ1Elem34Temp = (rxPhaseShifterQ1Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ1Elem34RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ1Elem34Temp)/2, rxPhaseShifterQ1Elem34Temp);
              *plen += sizeof(rxPhaseShifterQ1Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxPhaseShifterQ1Elem34Temp);
              rxPhaseShifterQ1Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ1Elem34RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ1Elem34Temp), rxPhaseShifterQ1Elem34Temp);
              *plen += sizeof(rxPhaseShifterQ1Elem34Temp);
              lastParamSuccessGetRegData = TWENTY_SEVENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxPhaseShifterQ2Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_EIGHTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxPhaseShifterQ2Elem12Temp) && (locationBrokenGetRegData
             == TWENTY_EIGHTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxPhaseShifterQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxPhaseShifterQ2Elem12Temp = 0;
              rxPhaseShifterQ2Elem12Temp = rxPhaseShifterQ2Elem12Temp | second_half_packet;
              rxPhaseShifterQ2Elem12Temp = (rxPhaseShifterQ2Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ2Elem12RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ2Elem12Temp)/2, rxPhaseShifterQ2Elem12Temp);
              *plen += sizeof(rxPhaseShifterQ2Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxPhaseShifterQ2Elem12Temp);
              rxPhaseShifterQ2Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ2Elem12RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ2Elem12Temp), rxPhaseShifterQ2Elem12Temp);
              *plen += sizeof(rxPhaseShifterQ2Elem12Temp);
              lastParamSuccessGetRegData = TWENTY_EIGHTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxPhaseShifterQ2Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = TWENTY_NINTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxPhaseShifterQ2Elem34Temp) && (locationBrokenGetRegData
             == TWENTY_NINTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxPhaseShifterQ2Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxPhaseShifterQ2Elem34Temp = 0;
              rxPhaseShifterQ2Elem34Temp = rxPhaseShifterQ2Elem34Temp | second_half_packet;
              rxPhaseShifterQ2Elem34Temp = (rxPhaseShifterQ2Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ2Elem34RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ2Elem34Temp)/2, rxPhaseShifterQ2Elem34Temp);
              *plen += sizeof(rxPhaseShifterQ2Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxPhaseShifterQ2Elem34Temp);
              rxPhaseShifterQ2Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxPhaseShifterQ2Elem34RegData, tvb, *plen,
                                 sizeof(rxPhaseShifterQ2Elem34Temp), rxPhaseShifterQ2Elem34Temp);
              *plen += sizeof(rxPhaseShifterQ2Elem34Temp);
              lastParamSuccessGetRegData = TWENTY_NINTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxFineGainQ1Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTIETH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxFineGainQ1Elem12Temp) && (locationBrokenGetRegData
             == THIRTIETH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxFineGainQ1Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxFineGainQ1Elem12Temp = 0;
              rxFineGainQ1Elem12Temp = rxFineGainQ1Elem12Temp | second_half_packet;
              rxFineGainQ1Elem12Temp = (rxFineGainQ1Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ1Elem12RegData, tvb, *plen,
                                 sizeof(rxFineGainQ1Elem12Temp)/2, rxFineGainQ1Elem12Temp);
              *plen += sizeof(rxFineGainQ1Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxFineGainQ1Elem12Temp);
              rxFineGainQ1Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ1Elem12RegData, tvb, *plen,
                                 sizeof(rxFineGainQ1Elem12Temp), rxFineGainQ1Elem12Temp);
              *plen += sizeof(rxFineGainQ1Elem12Temp);
              lastParamSuccessGetRegData = THIRTIETH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxFineGainQ1Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_FIRST_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxFineGainQ1Elem34Temp) && (locationBrokenGetRegData
             == THIRTY_FIRST_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxFineGainQ1Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxFineGainQ1Elem34Temp = 0;
              rxFineGainQ1Elem34Temp = rxFineGainQ1Elem34Temp | second_half_packet;
              rxFineGainQ1Elem34Temp = (rxFineGainQ1Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ1Elem34RegData, tvb, *plen,
                                 sizeof(rxFineGainQ1Elem34Temp)/2, rxFineGainQ1Elem34Temp);
              *plen += sizeof(rxFineGainQ1Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxFineGainQ1Elem34Temp);
              rxFineGainQ1Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ1Elem34RegData, tvb, *plen,
                                 sizeof(rxFineGainQ1Elem34Temp), rxFineGainQ1Elem34Temp);
              *plen += sizeof(rxFineGainQ1Elem34Temp);
              lastParamSuccessGetRegData = THIRTY_FIRST_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxFineGainQ2Elem12Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_SECOND_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxFineGainQ2Elem12Temp) && (locationBrokenGetRegData
             == THIRTY_SECOND_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxFineGainQ2Elem12Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxFineGainQ2Elem12Temp = 0;
              rxFineGainQ2Elem12Temp = rxFineGainQ2Elem12Temp | second_half_packet;
              rxFineGainQ2Elem12Temp = (rxFineGainQ2Elem12Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ2Elem12RegData, tvb, *plen,
                                 sizeof(rxFineGainQ2Elem12Temp)/2, rxFineGainQ2Elem12Temp);
              *plen += sizeof(rxFineGainQ2Elem12Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxFineGainQ2Elem12Temp);
              rxFineGainQ2Elem12Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ2Elem12RegData, tvb, *plen,
                                 sizeof(rxFineGainQ2Elem12Temp), rxFineGainQ2Elem12Temp);
              *plen += sizeof(rxFineGainQ2Elem12Temp);
              lastParamSuccessGetRegData = THIRTY_SECOND_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxFineGainQ2Elem34Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_THIRD_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxFineGainQ2Elem34Temp) && (locationBrokenGetRegData
             == THIRTY_THIRD_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxFineGainQ2Elem34Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxFineGainQ2Elem34Temp = 0;
              rxFineGainQ2Elem34Temp = rxFineGainQ2Elem34Temp | second_half_packet;
              rxFineGainQ2Elem34Temp = (rxFineGainQ2Elem34Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ2Elem34RegData, tvb, *plen,
                                 sizeof(rxFineGainQ2Elem34Temp)/2, rxFineGainQ2Elem34Temp);
              *plen += sizeof(rxFineGainQ2Elem34Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxFineGainQ2Elem34Temp);
              rxFineGainQ2Elem34Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxFineGainQ2Elem34RegData, tvb, *plen,
                                 sizeof(rxFineGainQ2Elem34Temp), rxFineGainQ2Elem34Temp);
              *plen += sizeof(rxFineGainQ2Elem34Temp);
              lastParamSuccessGetRegData = THIRTY_THIRD_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxEnableQ1Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_FOURTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxEnableQ1Temp) && (locationBrokenGetRegData
             == THIRTY_FOURTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxEnableQ1Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxEnableQ1Temp = 0;
              rxEnableQ1Temp = rxEnableQ1Temp | second_half_packet;
              rxEnableQ1Temp = (rxEnableQ1Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableQ1RegData, tvb, *plen,
                                 sizeof(rxEnableQ1Temp)/2, rxEnableQ1Temp);
              *plen += sizeof(rxEnableQ1Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxEnableQ1Temp);
              rxEnableQ1Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableQ1RegData, tvb, *plen,
                                 sizeof(rxEnableQ1Temp), rxEnableQ1Temp);
              *plen += sizeof(rxEnableQ1Temp);
              lastParamSuccessGetRegData = THIRTY_FOURTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxEnableQ2Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_FIFTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxEnableQ2Temp) && (locationBrokenGetRegData
             == THIRTY_FIFTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxEnableQ2Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxEnableQ2Temp = 0;
              rxEnableQ2Temp = rxEnableQ2Temp | second_half_packet;
              rxEnableQ2Temp = (rxEnableQ2Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableQ2RegData, tvb, *plen,
                                 sizeof(rxEnableQ2Temp)/2, rxEnableQ2Temp);
              *plen += sizeof(rxEnableQ2Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxEnableQ2Temp);
              rxEnableQ2Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableQ2RegData, tvb, *plen,
                                 sizeof(rxEnableQ2Temp), rxEnableQ2Temp);
              *plen += sizeof(rxEnableQ2Temp);
              lastParamSuccessGetRegData = THIRTY_FIFTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxEnableLayer1Temp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_SIXTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxEnableLayer1Temp) && (locationBrokenGetRegData
             == THIRTY_SIXTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxEnableLayer1Temp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxEnableLayer1Temp = 0;
              rxEnableLayer1Temp = rxEnableLayer1Temp | second_half_packet;
              rxEnableLayer1Temp = (rxEnableLayer1Temp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableLayer1RegData, tvb, *plen,
                                 sizeof(rxEnableLayer1Temp)/2, rxEnableLayer1Temp);
              *plen += sizeof(rxEnableLayer1Temp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxEnableLayer1Temp);
              rxEnableLayer1Temp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableLayer1RegData, tvb, *plen,
                                 sizeof(rxEnableLayer1Temp), rxEnableLayer1Temp);
              *plen += sizeof(rxEnableLayer1Temp);
              lastParamSuccessGetRegData = THIRTY_SIXTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if(packs < sizeof(rxEnableLOVCOTemp) && packs != 0)
          {
            locationBrokenGetRegData = THIRTY_SEVENTH_PARAM;
            ifHalfBrokenGetRegData = PARAM_IS_BROKEN;
            firstHalfPacketGetRegData = tvb_get_guint8(tvb, *plen);
            return ;
          }
          if(packs >= sizeof(rxEnableLOVCOTemp) && (locationBrokenGetRegData
             == THIRTY_SEVENTH_PARAM || locationBrokenGetRegData == NO_PARAM))
          {
            if(ifHalfBrokenGetRegData)
            {
              packs -= sizeof(rxEnableLOVCOTemp)/2;
              second_half_packet = tvb_get_guint8(tvb, *plen);
              rxEnableLOVCOTemp = 0;
              rxEnableLOVCOTemp = rxEnableLOVCOTemp | second_half_packet;
              rxEnableLOVCOTemp = (rxEnableLOVCOTemp << SECOND_HALF_PACKET_BITS)
                                  | (firstHalfPacketGetRegData);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableLOVCORegData, tvb, *plen,
                                 sizeof(rxEnableLOVCOTemp)/2, rxEnableLOVCOTemp);
              *plen += sizeof(rxEnableLOVCOTemp)/2;
              ifHalfBrokenGetRegData = NO_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
            else
            {
              packs -= sizeof(rxEnableLOVCOTemp);
              rxEnableLOVCOTemp = sci_tvb_get_ntohs(tvb, *plen);
              proto_tree_add_uint(rfapi_sdk_msg_dsc4, rxEnableLOVCORegData, tvb, *plen,
                                 sizeof(rxEnableLOVCOTemp), rxEnableLOVCOTemp);
              *plen += sizeof(rxEnableLOVCOTemp);
              lastParamSuccessGetRegData = THIRTY_SEVENTH_PARAM;
              locationBrokenGetRegData = NO_PARAM;
            }
          }
          if( packs == 0)
          {
            locationBrokenGetRegData = lastParamSuccessGetRegData + 1;
            if(locationBrokenGetRegData == THIRTY_EIGHTH_PARAM)
            {
              locationBrokenGetRegData = TWELFTH_PARAM;
            }
            ifHalfBrokenGetRegData = NO_PARAM;
            if(lastParamSuccessGetRegData == THIRTY_SEVENTH_PARAM)
            {
              if(numCodebookEntriesItrRegData != numCodebookEntriesGetRegDataGlobal
                 && numLayersItrRegData != numLayersGetRegDataGlobal &&
                 numTrxDevicesItrRegData != numTrxDevicesGetRegDataGlobal)
              {
                numCodebookEntriesItrRegData += 1;
              }
              else if(numCodebookEntriesItrRegData == numCodebookEntriesGetRegDataGlobal
                      && numLayersItrRegData != numLayersGetRegDataGlobal &&
                      numTrxDevicesItrRegData != numTrxDevicesGetRegDataGlobal)
              {
                numCodebookEntriesItrRegData = 1;
                numLayersItrRegData += 1;
              }
              else if(numCodebookEntriesItrRegData == numCodebookEntriesGetRegDataGlobal
                      && numLayersItrRegData == numLayersGetRegDataGlobal &&
                      numTrxDevicesItrRegData != numTrxDevicesGetRegDataGlobal)
              {
                numCodebookEntriesItrRegData = 1;
                numLayersItrRegData = 1;
                numTrxDevicesItrRegData += 1;
              }
            }
            return;
          }
          set_iterators(SET_ITERATORS_GET_REG_DATA_RSP);
        }
      }
    }
  }
}

static void
dissect_rf_get_tssi_ind(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32      statusTemp;
  guint16      tssiReqIdxTemp;
  guint16      sfnTemp;
  guint8       slotTemp;
  guint8       scsTemp;
  guint8       chainTypeTemp;
  guint8       carrierIdTemp;
  guint16      txPathIdTemp;
  guint8       startSymbolTemp;
  guint16      numSymbolsTemp;
  gint16       avgTSSIdBfsTemp;
  gint16       eirpdBmTemp;
  gint16       trpdBmTemp;
  gint16       var;
  proto_tree*  rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  statusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                        sizeof(statusTemp), statusTemp);
  *plen += sizeof(statusTemp);
  tssiReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, tssiReqIdx, tvb, *plen,
                       sizeof(tssiReqIdxTemp), tssiReqIdxTemp);
  *plen += sizeof(tssiReqIdxTemp);
  sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnTssi, tvb, *plen,
                      sizeof(sfnTemp), sfnTemp);
  *plen += sizeof(sfnTemp);
  slotTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, slotTssi, tvb, *plen,
                      sizeof(slotTemp), slotTemp);
  *plen += sizeof(slotTemp);
  scsTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, scsTssi, tvb, *plen,
                      sizeof(scsTemp), scsTemp);
  *plen += sizeof(scsTemp);
  chainTypeTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, chainTypeTssi, tvb, *plen,
                      sizeof(chainTypeTemp), chainTypeTemp);
  *plen += sizeof(chainTypeTemp);
  carrierIdTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, carrierIdTssi, tvb, *plen,
                      sizeof(carrierIdTemp), carrierIdTemp);
  *plen += sizeof(carrierIdTemp);
  txPathIdTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, txPathIdTssi, tvb, *plen,
                      sizeof(txPathIdTemp), txPathIdTemp);
  *plen += sizeof(txPathIdTemp);
  startSymbolTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, startSymbolTssi, tvb, *plen,
                      sizeof(startSymbolTemp), startSymbolTemp);
  *plen += sizeof(startSymbolTemp);
  numSymbolsTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numSymbolsTssi, tvb, *plen,
                      sizeof(numSymbolsTemp), numSymbolsTemp);
  *plen += sizeof(numSymbolsTemp);
  avgTSSIdBfsTemp = sci_tvb_get_ntohs(tvb, *plen);
  var = (short)avgTSSIdBfsTemp;
  proto_tree_add_int(rfapi_sdk_msg_dsc, avgTssidBfs, tvb, *plen, sizeof(avgTSSIdBfsTemp), var);
  *plen += sizeof(avgTSSIdBfsTemp);
  eirpdBmTemp = sci_tvb_get_ntohs(tvb, *plen);
  var = (short)eirpdBmTemp;
  proto_tree_add_int(rfapi_sdk_msg_dsc, eirpdBmTssi, tvb, *plen, sizeof(eirpdBmTemp), var);
  *plen += sizeof(eirpdBmTemp);
  trpdBmTemp = sci_tvb_get_ntohs(tvb, *plen);
  var = (short)trpdBmTemp;
  proto_tree_add_int(rfapi_sdk_msg_dsc, trpdBmTssi, tvb, *plen, sizeof(trpdBmTemp), var);
  *plen += sizeof(trpdBmTemp);
}

static void
dissect_rf_disable_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint8 exec_now;
  guint8 slott;
  guint8 scst;
  guint16 frameT;
  guint16 systemframet;
  guint8 subframet;
  guint32  exectime1;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  exec_now = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, execnow, tvb, *plen, sizeof(exec_now), exec_now);
  *plen += sizeof(exec_now);
  exectime1 = sci_tvb_get_ntohl(tvb, *plen);
  ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
                                       sizeof(exectime1),"execTime: %d",exectime1);
  rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
  frameT = sci_tvb_get_ntohs(tvb, *plen);
  systemframet = bitExtracted(frameT,NUM_BITS_FOR_SYSTEM_FRAME,START_BIT_FOR_SYSTEM_FRAME);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, systemframe, tvb, *plen,
                      sizeof(subframet), systemframet);
  subframet = bitExtracted(frameT,NUM_BITS_FOR_SUBFRAME,START_BIT_FOR_SUBFRAME);
  *plen += sizeof(subframet);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, subframe, tvb, *plen, sizeof(subframet), subframet);
  *plen += sizeof(subframet);
  slott = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, slot1, tvb, *plen, sizeof(slott), slott);
  *plen += sizeof(slott);
  scst = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, scs, tvb, *plen, sizeof(scst), scst);
  *plen += sizeof(scst);
}

static void
dissect_rf_set_tx_power_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16 pathIdt;
  gint16 txPowerDbmt;
  guint16 txPowerPatht;
  guint16 Num1_PDU_TLVs;
  gint16 var;
  guint8 sizePerIteration = 0;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  txPowerPatht = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numTxPaths, tvb, *plen, sizeof(txPowerPatht),
                      txPowerPatht);
  *plen += sizeof(txPowerPatht);
  sizePerIteration = sizeof(pathIdt) + sizeof(txPowerDbmt);
  for (Num1_PDU_TLVs = 0; Num1_PDU_TLVs < txPowerPatht; Num1_PDU_TLVs++)
  {
    ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb,
                                         *plen, sizePerIteration,"txPath[%d]",
                                          Num1_PDU_TLVs);
    rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
    pathIdt = sci_tvb_get_ntohs(tvb, *plen);
    if(Num1_PDU_TLVs == 0)
    {
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, pathId1, tvb, *plen, sizeof(pathIdt), pathIdt);
      *plen += sizeof(pathIdt);
    }
    if(Num1_PDU_TLVs == 1)
    {
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, pathId2, tvb, *plen, sizeof(pathIdt), pathIdt);
      *plen += sizeof(pathIdt);
    }
    txPowerDbmt = sci_tvb_get_ntohs(tvb, *plen);
    var = (short)txPowerDbmt;
    proto_tree_add_int(rfapi_sdk_msg_dsc20, txPowerDbm, tvb, *plen, sizeof(txPowerDbmt), var);
    *plen += sizeof(txPowerDbmt);
  }
}

static void
dissect_rf_config_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16  pathIdt;
  guint16  numCarrierst;
  guint16  numPathsPerCarriert;
  guint16  bandt;
  guint32  bandwidth1t;
  guint32  frequency1t;
  gint16   txCCGainAdjt;
  gint16   a;
  guint16  carrierIdt;
  guint16  num_carr;
  guint16  num_path;
  guint8 sizePerIteration = 0;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  numCarrierst = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numCarriers, tvb, *plen, sizeof(numCarrierst),
                      numCarrierst);
  *plen += sizeof(numCarrierst);
  numPathsPerCarriert = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numPathsPerCarrier, tvb, *plen,
                      sizeof(numPathsPerCarriert), numPathsPerCarriert);
  *plen += sizeof(numPathsPerCarriert);
  bandt = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, band, tvb, *plen, sizeof(bandt), bandt);
  *plen += sizeof(bandt);
  sizePerIteration = sizeof(carrierIdt) + sizeof(pathIdt) + sizeof(bandwidth1t)
                   + sizeof(frequency1t) + sizeof(txCCGainAdjt);
  for (num_carr = 0; num_carr < numCarrierst; num_carr++)
  {
    for(num_path = 0; num_path < numPathsPerCarriert; num_path++)
    {
      ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
                                           sizePerIteration,"Carrier[%d]Path[%d]",
                                           num_carr,num_path);
      rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
      carrierIdt = sci_tvb_get_ntohs(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, carrierId, tvb, *plen, sizeof(carrierIdt),
                          carrierIdt);
      *plen += sizeof(carrierIdt);
      pathIdt = sci_tvb_get_ntohs(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, pathId, tvb, *plen, sizeof(pathIdt), pathIdt);
      *plen += sizeof(pathIdt);
      bandwidth1t = sci_tvb_get_ntohl(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, bandwidth1, tvb, *plen, sizeof(bandwidth1t),
                          bandwidth1t);
      *plen += sizeof(bandwidth1t);
      frequency1t = sci_tvb_get_ntohl(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc20, frequency1, tvb, *plen, sizeof(frequency1t),
                          frequency1t);
      *plen += sizeof(frequency1t);
      txCCGainAdjt = sci_tvb_get_ntohs(tvb, *plen);
      a = (short)txCCGainAdjt;
      proto_tree_add_int(rfapi_sdk_msg_dsc20, txCCGainAdj1, tvb, *plen, sizeof(txCCGainAdjt),
                         a);
      *plen += sizeof(txCCGainAdjt);
    }
  }
}

static void
dissect_rf_hw_config_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16  bandt;
  guint8  numTrxDevicesht;
  guint8  numAntsPerTrxLayerht;
  guint16  maxNumBeamsPerLayert;
  guint16  maxBeamsPerTrxDeviceLayert;
  guint16  numRfPathst;
  guint16  numBandst;
  guint16  num_Bandst;
  gint16  minTxPowert;
  gint16  maxTxPowert;
  guint32  minFreqt;
  guint32  maxFreqt;
  guint32  maxIbwt;
  guint8 sizePerIteration = 0;
  gint16 a = 0;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  numTrxDevicesht = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numTrxDevicesh, tvb, *plen, sizeof(numTrxDevicesht),
                      numTrxDevicesht);
  *plen += sizeof(numTrxDevicesht);
  numAntsPerTrxLayerht = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numAntsPerTrxLayerh, tvb, *plen,
                      sizeof(numAntsPerTrxLayerht), numAntsPerTrxLayerht);
  *plen += sizeof(numAntsPerTrxLayerht);
  maxNumBeamsPerLayert = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxNumBeamsPerLayer, tvb, *plen,
                      sizeof(maxNumBeamsPerLayert), maxNumBeamsPerLayert);
  *plen += sizeof(maxNumBeamsPerLayert);
  maxBeamsPerTrxDeviceLayert = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxBeamsPerTrxDeviceLayer, tvb, *plen,
                      sizeof(maxBeamsPerTrxDeviceLayert),
                      maxBeamsPerTrxDeviceLayert);
  *plen += sizeof(maxBeamsPerTrxDeviceLayert);
  numRfPathst = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numRfPaths, tvb, *plen, sizeof(numRfPathst), numRfPathst);
  *plen += sizeof(numRfPathst);
  numBandst = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numBands, tvb, *plen, sizeof(numBandst), numBandst);
  *plen += sizeof(numBandst);
  sizePerIteration = sizeof(bandt)  + sizeof(minTxPowert) + sizeof(maxTxPowert) +
                     sizeof(minFreqt) + sizeof(maxFreqt) + sizeof(maxIbwt);

  for (num_Bandst = 0; num_Bandst < numBandst; num_Bandst++)
  {
    ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk,
                                         tvb, *plen, sizePerIteration,
                                         "Band[%d]", num_Bandst);
    rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
    bandt = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, band, tvb, *plen, sizeof(bandt), bandt);
    *plen += sizeof(bandt);
    minTxPowert =sci_tvb_get_ntohs(tvb, *plen);
    a = (short)minTxPowert;
    proto_tree_add_int(rfapi_sdk_msg_dsc20, minTxPower, tvb, *plen, sizeof(minTxPowert), a);
    *plen += sizeof(minTxPowert);
    maxTxPowert = sci_tvb_get_ntohs(tvb, *plen);
    a = (short)maxTxPowert;
    proto_tree_add_int(rfapi_sdk_msg_dsc20, maxTxPower, tvb, *plen, sizeof(maxTxPowert), a);
    *plen += sizeof(maxTxPowert);
    minFreqt = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, minFreq, tvb, *plen, sizeof(minFreqt), minFreqt);
    *plen += sizeof(minFreqt);
    maxFreqt = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, maxFreq, tvb, *plen, sizeof(maxFreqt), maxFreqt);
    *plen += sizeof(maxFreqt);
    maxIbwt = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, maxIbw, tvb, *plen, sizeof(maxIbwt), maxIbwt);
    *plen += sizeof(maxIbwt);
  }
}

static void
dissect_rf_get_capabilities_rsp
(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  guint8  maxBeamSwitchesPerSlot_temp;
  guint32  size;
  guint8  maxComponentCarriers_temp;
  guint16  maxBeamsInTrxDeviceLayer_temp;
  guint16  maxBeamsInTablePerLayer_temp;
  guint16  minTxSlotLatency_temp;
  guint16  minRxSlotLatency_temp;
  gint16  minTxAntGain_temp;
  gint16  maxTxAntGain_temp;
  gint16  minRxAntGain_temp;
  gint16  maxRxAntGain_temp;
  gint16  minPerformanceTemperature_temp;
  gint16  maxPerformanceTemperature_temp;
  gint16  minOperatingTemperature_temp;
  gint16  maxOperatingTemperature_temp;
  guint16 antHardwareId_temp;
  guint16  numBands_temp;
  guint16  numBands_itr;

  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;

  guint16  band_inloop;
  gint16   minTxPower_inloop;
  gint16   maxTxPower_inloop;
  gint16   limTxPower_inloop;
  guint32  minFreq_inloop;
  guint32  maxFreq_inloop;
  guint32  maxIbw_inloop;

  guint8 sizePerIteration = 0;

  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                      sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);

  maxBeamSwitchesPerSlot_temp = tvb_get_guint8(tvb, *plen);
  size = sizeof(maxBeamSwitchesPerSlot_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxBeamSwitchesPerSlot, tvb, *plen,
                      size, maxBeamSwitchesPerSlot_temp);
  *plen += size;
  maxComponentCarriers_temp = tvb_get_guint8(tvb, *plen);
  size = sizeof(maxComponentCarriers_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxComponentCarriers, tvb, *plen,
                      size, maxComponentCarriers_temp);
  *plen += size;
  maxBeamsInTrxDeviceLayer_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxBeamsInTrxDeviceLayer_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxBeamsInTrxDeviceLayer, tvb, *plen,
                      size, maxBeamsInTrxDeviceLayer_temp);
  *plen += size;
  maxBeamsInTablePerLayer_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxBeamsInTablePerLayer_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, maxBeamsInTablePerLayer, tvb, *plen,
                      size, maxBeamsInTablePerLayer_temp);
  *plen += size;
  minTxSlotLatency_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minTxSlotLatency_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, minTxSlotLatency, tvb, *plen, size,
                      minTxSlotLatency_temp);
  *plen += size;
  minRxSlotLatency_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minRxSlotLatency_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, minRxSlotLatency, tvb, *plen, size,
                      minRxSlotLatency_temp);
  *plen += size;

  minTxAntGain_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minTxAntGain_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMinTxAntGain, tvb, *plen, size,
                      minTxAntGain_temp);
  *plen += size;

  maxTxAntGain_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxTxAntGain_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMaxTxAntGain, tvb, *plen, size,
                      maxTxAntGain_temp);
  *plen += size;

  minRxAntGain_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minRxAntGain_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMinRxAntGain, tvb, *plen, size,
                      minRxAntGain_temp);
  *plen += size;

  maxRxAntGain_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxRxAntGain_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMaxRxAntGain, tvb, *plen, size,
                      maxRxAntGain_temp);
  *plen += size;

  minPerformanceTemperature_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minPerformanceTemperature_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMinPerfTemp, tvb, *plen, size,
                      minPerformanceTemperature_temp);
  *plen += size;

  maxPerformanceTemperature_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxPerformanceTemperature_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMaxPerfTemp, tvb, *plen, size,
                      maxPerformanceTemperature_temp);
  *plen += size;

  minOperatingTemperature_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(minOperatingTemperature_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMinOperatingTemp, tvb, *plen, size,
                      minOperatingTemperature_temp);
  *plen += size;

  maxOperatingTemperature_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(maxOperatingTemperature_temp);
  proto_tree_add_int(rfapi_sdk_msg_dsc, getCapMaxOperatingTemp, tvb, *plen, size,
                      maxOperatingTemperature_temp);
  *plen += size;

  antHardwareId_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(antHardwareId_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, getCapAntHardwareId, tvb, *plen, size,
                      antHardwareId_temp);
  *plen += size;

  numBands_temp = sci_tvb_get_ntohs(tvb, *plen);
  size = sizeof(numBands_temp);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, getCapNumBands, tvb, *plen, size,
                      numBands_temp);
  *plen += size;

  sizePerIteration = sizeof(band_inloop) + sizeof(minTxPower_inloop) +
                     sizeof(maxTxPower_inloop) + sizeof(minFreq_inloop) +
                     sizeof(maxFreq_inloop) + sizeof(maxIbw_inloop);

  for(numBands_itr = 0; numBands_itr < numBands_temp; numBands_itr++)
  {
    ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk,
                                         tvb, *plen, sizePerIteration,
                                         "Band[%d]", numBands_itr);
    rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1,
                          ett_rfapi_sdk_msg_data_subtree1);

    band_inloop = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, getCapBand, tvb, *plen,
                        sizeof(band_inloop), band_inloop);
    *plen += sizeof(band_inloop);

    minTxPower_inloop = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc20, getCapMinTxPower, tvb, *plen,
                       sizeof(minTxPower_inloop), minTxPower_inloop);
    *plen += sizeof(minTxPower_inloop);

    maxTxPower_inloop = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc20, getCapMaxTxPower, tvb, *plen,
                       sizeof(maxTxPower_inloop), maxTxPower_inloop);
    *plen += sizeof(maxTxPower_inloop);

    limTxPower_inloop = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc20, getCapLimTxPower, tvb, *plen,
                       sizeof(limTxPower_inloop), limTxPower_inloop);
    *plen += sizeof(limTxPower_inloop);

    minFreq_inloop = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, getCapMinFreq, tvb, *plen,
                        sizeof(minFreq_inloop), minFreq_inloop);
    *plen += sizeof(minFreq_inloop);

    maxFreq_inloop = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, getCapMaxFreq, tvb, *plen,
                        sizeof(maxFreq_inloop), maxFreq_inloop);
    *plen += sizeof(maxFreq_inloop);

    maxIbw_inloop = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc20, getCapMaxIbw, tvb, *plen,
                        sizeof(maxIbw_inloop), maxIbw_inloop);
    *plen += sizeof(maxIbw_inloop);
  }
}

static guint16
get_correct_pack_length_load_beam()
{
  /*packet_size_till_ith_from_last is the sum of size of elements in LoadBeam
  starting from last element to ith element*/
  guint16 packet_size_till_ith_from_last[3] ={0,4,2};
  guint16 sum = 0;
  sum += packet_size_till_ith_from_last[locationBrokenLoadBeam];
  if(ifHalfBrokenLoadBeam)
  {
    sum -= 1;
  }
  return sum;
}

static void
dissect_rf_load_beam_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16  numBeamsTemp;
  guint16  numBeamsItr;
  guint16  newBeamIdTemp;
  guint16  oldBeamIdTemp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_item* ti;
  guint16 packLen;
  guint8 second_half_packet;
  gint16 packs = vend_spec_len;
  proto_tree* rfapi_sdk_msg_dsc1 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  if(segment == 0)
  {
    numBeamsTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numBeams, tvb, *plen,
                        sizeof(numBeamsTemp), numBeamsTemp);
    *plen += sizeof(numBeamsTemp);
    numBeamsItrLoadBeam = 0;
    locationBrokenLoadBeam = NO_PARAM;
    lastParamSuccessLoadBeam = NO_PARAM;
    ifHalfBrokenLoadBeam = NO_PARAM;
    numSlotsLoadBeamG = numBeamsTemp;
  }
  else
  {
    packs += sizeof(numBeamsTemp);
  }
  packs = packs - SIZE_OF_COMMON_MSG_HEADER - sizeof(numBeamsTemp);
  packLen = sizeof(newBeamIdTemp)+sizeof(oldBeamIdTemp);
  for (numBeamsItr = numBeamsItrLoadBeam; numBeamsItr < numSlotsLoadBeamG; numBeamsItr++)
  {
    numBeamsItrLoadBeam = numBeamsItr;
    packLen = sizeof(newBeamIdTemp)+sizeof(oldBeamIdTemp);
    if(locationBrokenLoadBeam != NO_PARAM)
    {
      packLen = get_correct_pack_length_load_beam();
    }
    if( packs < (sizeof(newBeamIdTemp)+sizeof(oldBeamIdTemp)))
    {
      packLen = packs;
    }
    ti = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen, packLen,
    "Beam[%d]", numBeamsItr);
    rfapi_sdk_msg_dsc1 = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_data_subtree1);
    if(packs < sizeof(newBeamIdTemp) && packs != 0)
    {
      locationBrokenLoadBeam = FIRST_PARAM;
      ifHalfBrokenLoadBeam = PARAM_IS_BROKEN;
      firstHalfPacketLoadBeam = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(newBeamIdTemp) && (locationBrokenLoadBeam == FIRST_PARAM ||
    locationBrokenLoadBeam == NO_PARAM))
    {
      if(ifHalfBrokenLoadBeam)
      {
        packs -= sizeof(newBeamIdTemp)/2;
        second_half_packet = tvb_get_guint8(tvb, *plen);
        newBeamIdTemp = 0;
        newBeamIdTemp = newBeamIdTemp | second_half_packet;
        newBeamIdTemp = (newBeamIdTemp << SECOND_HALF_PACKET_BITS)|(firstHalfPacketLoadBeam);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, newBeamId, tvb, *plen,
                          sizeof(newBeamIdTemp)/2, newBeamIdTemp);
        *plen += sizeof(newBeamIdTemp)/2;
        ifHalfBrokenLoadBeam = NO_PARAM;
        locationBrokenLoadBeam = NO_PARAM;
      }
      else
      {
        packs -= sizeof(newBeamIdTemp);
        newBeamIdTemp = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, newBeamId, tvb, *plen,
                           sizeof(newBeamIdTemp), newBeamIdTemp);
        *plen += sizeof(newBeamIdTemp);
        lastParamSuccessLoadBeam = FIRST_PARAM;
        locationBrokenLoadBeam = NO_PARAM;
      }
    }
    if(packs < sizeof(oldBeamIdTemp) && packs != 0)
    {
      locationBrokenLoadBeam = SECOND_PARAM;
      ifHalfBrokenLoadBeam = PARAM_IS_BROKEN;
      firstHalfPacketLoadBeam = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(oldBeamIdTemp) && (locationBrokenLoadBeam == SECOND_PARAM ||
    locationBrokenLoadBeam == NO_PARAM))
    {
      if(ifHalfBrokenLoadBeam)
      {
        packs -= sizeof(oldBeamIdTemp)/2;
        second_half_packet = tvb_get_guint8(tvb, *plen);
        oldBeamIdTemp = 0;
        oldBeamIdTemp = oldBeamIdTemp | second_half_packet;
        oldBeamIdTemp = (oldBeamIdTemp << SECOND_HALF_PACKET_BITS)|(firstHalfPacketLoadBeam);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, oldBeamId, tvb, *plen,
                          sizeof(oldBeamIdTemp)/2, oldBeamIdTemp);
        *plen += sizeof(oldBeamIdTemp)/2;
        ifHalfBrokenLoadBeam = NO_PARAM;
        locationBrokenLoadBeam = NO_PARAM;
      }
      else
      {
        packs -= sizeof(oldBeamIdTemp);
        oldBeamIdTemp = sci_tvb_get_ntohs(tvb, *plen);
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, oldBeamId, tvb, *plen,
                           sizeof(oldBeamIdTemp), oldBeamIdTemp);
        *plen += sizeof(oldBeamIdTemp);
        lastParamSuccessLoadBeam = SECOND_PARAM;
        locationBrokenLoadBeam = NO_PARAM;
      }
    }
  }
  locationBrokenLoadBeam = lastParamSuccessLoadBeam + 1;
  if(locationBrokenLoadBeam == SECOND_PARAM + 1)
  {
    locationBrokenLoadBeam = FIRST_PARAM;
  }
}

static void
dissect_rf_trx_device_ctrl_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint8  trxDeviceNumt;
  guint8  powerOnt;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  trxDeviceNumt = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, trxDeviceNum, tvb, *plen,
                      sizeof(trxDeviceNumt), trxDeviceNumt);
  *plen += sizeof(trxDeviceNumt);
  powerOnt = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, powerOn, tvb, *plen, sizeof(powerOnt), powerOnt);
  *plen += sizeof(powerOnt);
}

static void
dissect_rf_set_slot_config_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint8 slott;
  guint8 scst;
  guint16 frameT;
  guint32  exectime1;
  proto_item* ti1;
  proto_item* ti2;
  guint16 systemframet;
  guint8 subframet;
  guint16  tddSlotMaskt;
  guint8  numLayerst;
  guint8  num_Layerst;
  guint8  num_Symbols;
  guint16  layerNumt;
  guint16  beamSwitchMaskt;
  guint16  beamIdt;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  proto_tree* rfapi_sdk_msg_dsc21 = NULL;
  proto_tree* rfapi_sdk_msg_dsc22 = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  tddSlotMaskt = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, tddSlotMaskTx, tvb, *plen, sizeof(tddSlotMaskt),
                      tddSlotMaskt);
  *plen += sizeof(tddSlotMaskt);
  tddSlotMaskt = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, tddSlotMaskRx, tvb, *plen, sizeof(tddSlotMaskt),
                      tddSlotMaskt);
  *plen += sizeof(tddSlotMaskt);
  exectime1 = sci_tvb_get_ntohl(tvb, *plen);
  ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
                                       sizeof(exectime1),"execTimeTx: %d",exectime1);
  rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
  frameT = sci_tvb_get_ntohs(tvb, *plen);
  systemframet = bitExtracted(frameT,NUM_BITS_FOR_SYSTEM_FRAME,START_BIT_FOR_SYSTEM_FRAME);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, systemframe, tvb, *plen,
                      sizeof(subframet), systemframet);
  subframet = bitExtracted(frameT,NUM_BITS_FOR_SUBFRAME,START_BIT_FOR_SUBFRAME);
  *plen += sizeof(subframet);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, subframe, tvb, *plen, sizeof(subframet),
                      subframet);
  *plen += sizeof(subframet);
  slott = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, slot1, tvb, *plen, sizeof(slott), slott);
  *plen += sizeof(slott);
  scst = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, scs, tvb, *plen, sizeof(scst), scst);
  *plen += sizeof(scst);
  exectime1 = sci_tvb_get_ntohl(tvb, *plen);
  ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb,
                                       *plen, sizeof(exectime1),
                                       "execTimeRx: %d",exectime1);
  rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
  frameT = sci_tvb_get_ntohs(tvb, *plen);
  systemframet = bitExtracted(frameT,NUM_BITS_FOR_SYSTEM_FRAME,START_BIT_FOR_SYSTEM_FRAME);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, systemframe, tvb, *plen,
                      sizeof(subframet), systemframet);
  subframet = bitExtracted(frameT,NUM_BITS_FOR_SUBFRAME,START_BIT_FOR_SUBFRAME);
  *plen += sizeof(subframet);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, subframe, tvb, *plen, sizeof(subframet),
                      subframet);
  *plen += sizeof(subframet);
  slott = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, slot1, tvb, *plen, sizeof(slott), slott);
  *plen += sizeof(slott);
  scst = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc20, scs, tvb, *plen, sizeof(scst), scst);
  *plen += sizeof(scst);
  numLayerst = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numLayers, tvb, *plen, sizeof(numLayerst),
                      numLayerst);
  *plen += sizeof(numLayerst);
  for (num_Layerst = 0; num_Layerst < numLayerst; num_Layerst++)
  {
    ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen,
          SIZE_OF_DATA_PER_LAYER, "Layer[%d]",num_Layerst);
    rfapi_sdk_msg_dsc21 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
    layerNumt = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc21, layerNum, tvb, *plen, sizeof(layerNumt), layerNumt);
    *plen += sizeof(layerNumt);
    beamSwitchMaskt = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc21, beamSwitchMaskTx, tvb, *plen,
                        sizeof(beamSwitchMaskt), beamSwitchMaskt);
    *plen += sizeof(beamSwitchMaskt);
    beamSwitchMaskt = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc21, beamSwitchMaskRx, tvb, *plen,
                        sizeof(beamSwitchMaskt), beamSwitchMaskt);
    *plen += sizeof(beamSwitchMaskt);
    for (num_Symbols = 0; num_Symbols < NUM_SYMBOLS_PER_SLOT; num_Symbols++)
    {
      ti2 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc21, proto_rfapi_sdk, tvb, *plen,
            SIZE_OF_DATA_PER_SLOT,"symbol[%d]",num_Symbols);
      rfapi_sdk_msg_dsc22 = proto_item_add_subtree(ti2, ett_rfapi_sdk_msg_data_subtree1);
      beamIdt = sci_tvb_get_ntohs(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc22, beamIdTx, tvb, *plen, sizeof(beamIdt), beamIdt);
      *plen += sizeof(beamIdt);
      beamIdt = sci_tvb_get_ntohs(tvb, *plen);
      proto_tree_add_uint(rfapi_sdk_msg_dsc22, beamIdRx, tvb, *plen, sizeof(beamIdt), beamIdt);
      *plen += sizeof(beamIdt);
    }
  }
}

static guint16
get_correct_pack_length_set_beam()
{
  /*packet_size_till_ith_from_last is the sum of size of elements in set beam
    starting from last element to ith element.
    NOTE: The first and second parameter are 1 to many.  Just return the
          number of bytes for the many. */
  guint16 packet_size_till_ith_from_last[10] = {0,12,12,12,11,10,8,6,4,2};
  guint16 sum = 0;
  sum += packet_size_till_ith_from_last[location_broken];
  if(if_half_broken)
  {
    sum -= 1;
  }
  return sum;
}

static void
dissect_rf_set_beam_table_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16 num_Beamst;
  guint16 numTrxDevices;
  guint16 numAntsPerTrxLayer;
  guint16 beamIdt;
  gint16  txPowerAdjt;
  guint8  trxDeviceNumt;
  guint8  antElementt;
  gint16  on_offt;
  guint16 rxPhaset;
  gint16  rxGaint;
  guint16 txPhaset;
  guint16 packLen;
  gint16  txGaint;
  guint8  second_half_packet;
  gint16  a = 0;
  proto_item* ti;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc10 = NULL;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  gint16 packs = vend_spec_len;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  if(segment == 0)
  {
    num_beams_set = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numBeams, tvb, *plen, sizeof(num_beams_set),
                        num_beams_set);
    *plen += sizeof(num_beams_set);
    num_beamst_global = 0;
    last_param_success = NO_PARAM;
    location_broken = NO_PARAM;
    if_half_broken = NO_PARAM;
    num_trx_devices_global = 0;
    num_ants_per_trx_layer_global = 0;
  }
  else
  {
    packs += sizeof(num_beams_set);
  }
  packs = packs - SIZE_OF_COMMON_MSG_HEADER - sizeof(num_beams_set);

  for (num_Beamst = num_beamst_global; num_Beamst < num_beams_set && packs > 0;
       num_Beamst++)
  {
    packLen =  sizeof(beamIdt) + sizeof(txPowerAdjt);
    ti = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb,
                                        *plen, packLen, "Beam[%d]", num_Beamst);
    rfapi_sdk_msg_dsc10 = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_data_subtree1);
    if(location_broken == NO_PARAM || location_broken == FIRST_PARAM)
    {
      if(packs - (gint16)sizeof(beamIdt) < 0 && packs != 0)
      {
        location_broken = FIRST_PARAM;
        if_half_broken = PARAM_IS_BROKEN;
        first_half_packet = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(beamIdt) && (location_broken == FIRST_PARAM
          || location_broken == NO_PARAM))
      {
        if(if_half_broken)
        {
          packs -= sizeof(beamIdt)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          beamIdt = 0;
          beamIdt = beamIdt | second_half_packet;
          beamIdt = (beamIdt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
          proto_tree_add_uint(rfapi_sdk_msg_dsc10, beamId, tvb, *plen,
                              sizeof(beamIdt)/2, beamIdt);
          *plen += sizeof(beamIdt)/2;
          if_half_broken = NO_PARAM;
          location_broken = NO_PARAM;
        }
        else
        {
          packs -= sizeof(beamIdt);
          beamIdt = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc10, beamId, tvb, *plen,
                              sizeof(beamIdt), beamIdt);
          last_param_success = FIRST_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(beamIdt);
        }
      }
    }

    if(location_broken == NO_PARAM || location_broken == SECOND_PARAM)
    {
      if(packs - (gint16)sizeof(txPowerAdjt) < 0 && packs != 0)
      {
        location_broken = SECOND_PARAM;
        if_half_broken = PARAM_IS_BROKEN;
        first_half_packet = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(txPowerAdjt) && (location_broken == SECOND_PARAM
          || location_broken == NO_PARAM))
      {
        if(if_half_broken)
        {
          packs -= sizeof(txPowerAdjt)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          txPowerAdjt = 0;
          txPowerAdjt = txPowerAdjt | second_half_packet;
          a = (short)txPowerAdjt;
          txPowerAdjt = (txPowerAdjt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
          proto_tree_add_int(rfapi_sdk_msg_dsc10, txPowerAdj, tvb, *plen,
                              sizeof(txPowerAdjt)/2, a);
          *plen += sizeof(txPowerAdjt)/2;
          if_half_broken = NO_PARAM;
          location_broken = NO_PARAM;
        }
        else
        {
          packs -= sizeof(txPowerAdjt);
          txPowerAdjt = sci_tvb_get_ntohs(tvb, *plen);
          a = (short)txPowerAdjt;
          proto_tree_add_int(rfapi_sdk_msg_dsc10, txPowerAdj, tvb, *plen,
                              sizeof(txPowerAdjt), a);
          last_param_success = SECOND_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(txPowerAdjt);
        }
      }
    }
    for (numTrxDevices = num_trx_devices_global;
         numTrxDevices < MAX_TRX_DEVICES && packs > 0;
         numTrxDevices++)
    {
      for (numAntsPerTrxLayer = num_ants_per_trx_layer_global;
           numAntsPerTrxLayer < MAX_ANTS_PER_LAYER && packs > 0;
           numAntsPerTrxLayer++)
      {
        packLen = SIZE_PER_ITR_SET_BEAM_REQ;
        if(location_broken != NO_PARAM)
        {
          packLen = get_correct_pack_length_set_beam();
        }
        if( packs < SIZE_PER_ITR_SET_BEAM_REQ)
        {
          packLen = packs;
        }
        ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc10,
                proto_rfapi_sdk, tvb, *plen, packLen, "TrxDevices[%d]"
                " AntsPerTrxLayer[%d]", numTrxDevices, numAntsPerTrxLayer);
        rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree2);
        num_beamst_global = num_Beamst;
        num_trx_devices_global = numTrxDevices;
        num_ants_per_trx_layer_global = numAntsPerTrxLayer;
        if(packs - (gint16)sizeof(trxDeviceNumt) < 0 && packs != 0)
        {
          location_broken = THIRD_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(trxDeviceNumt) && (location_broken == THIRD_PARAM
           || location_broken == NO_PARAM))
        {
          packs -= sizeof(trxDeviceNumt);
          trxDeviceNumt = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc20, trxDeviceNum, tvb, *plen,
                              sizeof(trxDeviceNumt), trxDeviceNumt);
          last_param_success = THIRD_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(trxDeviceNumt);
        }
        if(packs - (gint16)sizeof(antElementt) < 0 && packs != 0)
        {
          location_broken = FOURTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(antElementt) && (location_broken == FOURTH_PARAM
           || location_broken == NO_PARAM))
        {
          packs -= sizeof(antElementt);
          antElementt = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc20, antElement, tvb, *plen,
                              sizeof(antElementt), antElementt);
          last_param_success = FOURTH_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(antElementt);
        }
        if(packs - (gint16)sizeof(on_offt) < 0 && packs != 0)
        {
          location_broken = FIFTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(on_offt) && (location_broken == FIFTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(on_offt)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            on_offt = 0;
            on_offt = on_offt | second_half_packet;
            on_offt = (on_offt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            a = (short)on_offt;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, on_off, tvb, *plen,
                               sizeof(on_offt)/2, a);
            *plen += sizeof(on_offt)/2;
            if_half_broken = NO_PARAM;
            location_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(on_offt);
            on_offt = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)on_offt;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, on_off, tvb, *plen,
                               sizeof(on_offt), a);
            last_param_success = FIFTH_PARAM;
            location_broken = NO_PARAM;
            *plen += sizeof(on_offt);
          }
        }
        if(packs - (gint16)sizeof(rxPhaset) < 0 && packs != 0)
        {
          location_broken = SIXTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxPhaset) && (location_broken == SIXTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(rxPhaset)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxPhaset = 0;
            rxPhaset = rxPhaset | second_half_packet;
            rxPhaset = (rxPhaset << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, rxPhase, tvb, *plen,
                                sizeof(rxPhaset)/2, rxPhaset);
            *plen += sizeof(rxPhaset)/2;
            if_half_broken = NO_PARAM;
            location_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxPhaset);
            rxPhaset = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, rxPhase, tvb, *plen,
                                sizeof(rxPhaset), rxPhaset);
            last_param_success = SIXTH_PARAM;
            location_broken = NO_PARAM;
            *plen += sizeof(rxPhaset);
          }
        }
        if(packs - (gint16)sizeof(rxGaint) < 0 && packs != 0)
        {
          location_broken = SEVENTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxGaint) && (location_broken == SEVENTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(rxGaint)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxGaint = 0;
            rxGaint = rxGaint | second_half_packet;
            rxGaint = (rxGaint << SECOND_HALF_PACKET_BITS) | (first_half_packet);
            a = (short)rxGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, rxGain, tvb, *plen,
                               sizeof(rxGaint)/2, a);
            *plen += sizeof(rxGaint)/2;
            if_half_broken = NO_PARAM;
            location_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxGaint);
            rxGaint = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)rxGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, rxGain, tvb, *plen,
                               sizeof(rxGaint), a);
            last_param_success = SEVENTH_PARAM;
            location_broken = NO_PARAM;
            *plen += sizeof(rxGaint);
          }
        }

        if(packs - (gint16)sizeof(txPhaset) < 0 && packs != 0)
        {
          location_broken = EIGHTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(txPhaset) && (location_broken == EIGHTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(txPhaset)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            txPhaset = 0;
            txPhaset = txPhaset | second_half_packet;
            txPhaset = (txPhaset << SECOND_HALF_PACKET_BITS) | (first_half_packet);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, txPhase, tvb, *plen,
                                sizeof(txPhaset)/2, txPhaset);
            *plen += sizeof(txPhaset)/2;
            location_broken = NO_PARAM;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(txPhaset);
            txPhaset = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, txPhase, tvb, *plen,
                                sizeof(txPhaset), txPhaset);
            last_param_success = EIGHTH_PARAM;
            location_broken = NO_PARAM;
            *plen += sizeof(txPhaset);
          }
        }
        if(packs - (gint16)sizeof(txGaint) < 0 && packs != 0)
        {
          location_broken = NINETH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(txGaint) && (location_broken == NINETH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(txGaint)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            txGaint = 0;
            txGaint = txGaint | second_half_packet;
            txGaint = (txGaint << SECOND_HALF_PACKET_BITS) | (first_half_packet);
            a = (short)txGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, txGain, tvb, *plen,
                               sizeof(txGaint)/2, a);
            *plen += sizeof(txGaint)/2;
            if_half_broken = NO_PARAM;
            location_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(txGaint);
            txGaint = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)txGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, txGain, tvb, *plen,
                               sizeof(txGaint), a);
            last_param_success = NINETH_PARAM;
            location_broken = NO_PARAM;
            *plen += sizeof(txGaint);
          }
        }
        if(packs - (gint16)sizeof(trxDeviceNumt) < 0 && packs != 0)
        {
          location_broken = THIRD_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
        }

        if(num_trx_devices_global != MAX_TRX_DEVICES - 1 && packs == 0 &&
          num_ants_per_trx_layer_global != MAX_ANTS_PER_LAYER -1 &&
          if_half_broken == NO_PARAM && location_broken == NO_PARAM
          && last_param_success == NUM_PARAMS_SET_BEAM)
        {
          num_ants_per_trx_layer_global += 1;
        }
        else
        {
          set_iterators(SET_ITERATORS_SET_BEAM_RSP);
        }
      }
    }
  }
  location_broken = last_param_success + 1;
  if(location_broken == NUM_PARAMS_SET_BEAM + 1)
  {
    /* Check if the segment ended on a beam boundary. */
    if(numTrxDevices == MAX_TRX_DEVICES &&
       numAntsPerTrxLayer == MAX_ANTS_PER_LAYER)
    {
      location_broken = FIRST_PARAM;
    }
    else
    {
      location_broken = THIRD_PARAM;
    }
  }
}

static void
dissect_rf_stop_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_rxagc_ctrl_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_get_wbrssi_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_get_tssi_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_get_tx_power_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_start_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_hw_config_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_config_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_set_tx_power_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_disable_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_enable_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  guint16  band_temp;
  guint8   sub_band_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);

  band_temp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, bandEnable, tvb, *plen, sizeof(band_temp), band_temp);
  *plen += sizeof(band_temp);

  sub_band_temp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, subBandEnable, tvb, *plen, sizeof(sub_band_temp),
                      sub_band_temp);
  *plen += sizeof(sub_band_temp);
}

static void
dissect_rf_set_slot_config_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_trx_device_ctrl_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_load_beam_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_set_beam_table_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static void
dissect_rf_test_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  status_temp;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  status_temp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(status_temp), status_temp);
  *plen += sizeof(status_temp);
}

static guint16
get_correct_pack_length_get_beam()
{
  /*packet_size_till_ith_from_last is the sum of size of elements in get beam
  starting from last element to ith element*/
  guint16 packet_size_till_ith_from_last[11] = {0,12,12,12,12,11,10,8,6,4,2};
  guint16 sum = 0;
  sum += packet_size_till_ith_from_last[location_broken];
  if(if_half_broken)
  {
  sum -= 1;
  }
  return sum;
}

static void
dissect_rf_get_beam_table_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint16 num_Beamst;
  guint16 numTrxDevices;
  guint16 numAntsPerTrxLayer;
  guint16 beamIdt;
  guint16 CodebookIdxt;
  gint16 adjustDbt;
  guint8 trxDeviceNumt;
  guint8 antElementt;
  gint16 on_offt;
  guint16 rxPhaset;
  gint16 rxGaint;
  guint16 txPhaset;
  gint16 txGaint;
  guint16 packLen;
  guint32 statust;
  guint8 second_half_packet;
  gint16 a = 0;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc20 = NULL;
  gint16 packs = vend_spec_len;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  if(segment == 0)
  {
    statust = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen, sizeof(statust), statust);
    *plen += sizeof(statust);
    num_beams_get = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numBeams, tvb, *plen, sizeof(num_beams_get),
                        num_beams_get);
    *plen += sizeof(num_beams_get);
    last_param_success = NO_PARAM;
    location_broken = NO_PARAM;
    if_half_broken = NO_PARAM;
    num_beamst_global = 0;
    num_trx_devices_global = 0;
    num_ants_per_trx_layer_global = 0;
  }
  else
  {
    packs += sizeof(statust) + sizeof(num_beams_get);
  }
  packs = packs - SIZE_OF_COMMON_MSG_HEADER - sizeof(statust) - sizeof(num_beams_get);
  packLen = SIZE_PER_ITR_GET_BEAM_RSP;

  for (num_Beamst = num_beamst_global; num_Beamst < num_beams_get; num_Beamst++)
  {
    /* Extract beam level information if we are at the first trx_device, */
    /* antenna and last_param_success.                                   */
    if( (num_trx_devices_global == MAX_TRX_DEVICES - 1 || num_trx_devices_global == 0) &&
        (num_ants_per_trx_layer_global == MAX_ANTS_PER_LAYER - 1 ||
         num_ants_per_trx_layer_global == 0) &&
        (last_param_success == NUM_PARAMS_GET_BEAM || last_param_success == NO_PARAM) )
    {
      if(packs - (gint16)sizeof(beamIdt) < 0 && packs != 0)
      {
        location_broken = FIRST_PARAM;
        if_half_broken = PARAM_IS_BROKEN;
        first_half_packet = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(beamIdt) && (location_broken == FIRST_PARAM
         || location_broken == NO_PARAM))
      {
        if(if_half_broken)
        {
          packs -= sizeof(beamIdt)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          beamIdt = 0;
          beamIdt = beamIdt | second_half_packet;
          beamIdt = (beamIdt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
          proto_tree_add_uint(rfapi_sdk_msg_dsc, beamId, tvb, *plen,
                              sizeof(beamIdt)/2, beamIdt);
          *plen += sizeof(beamIdt)/2;
          if_half_broken = NO_PARAM;
        }
        else
        {
          packs -= sizeof(beamIdt);
          beamIdt = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc, beamId, tvb, *plen,
                              sizeof(beamIdt), beamIdt);
          *plen += sizeof(beamIdt);
        }
        last_param_success = FIRST_PARAM;
        location_broken = NO_PARAM;
      }
      if(packs - (gint16)sizeof(CodebookIdxt) < 0 && packs != 0)
      {
        location_broken = SECOND_PARAM;
        if_half_broken = PARAM_IS_BROKEN;
        first_half_packet = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(CodebookIdxt) && (location_broken == SECOND_PARAM
         || location_broken == NO_PARAM))
      {
        if(if_half_broken)
        {
          packs -= sizeof(CodebookIdxt)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          CodebookIdxt = 0;
          CodebookIdxt = CodebookIdxt | second_half_packet;
          CodebookIdxt = (CodebookIdxt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
          proto_tree_add_uint(rfapi_sdk_msg_dsc, CodebookIdx, tvb, *plen,
                              sizeof(CodebookIdxt)/2, CodebookIdxt);
          *plen += sizeof(CodebookIdxt)/2;
          if_half_broken = NO_PARAM;
        }
        else
        {
          packs -= sizeof(CodebookIdxt);
          CodebookIdxt = sci_tvb_get_ntohs(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc, CodebookIdx, tvb, *plen,
                              sizeof(CodebookIdxt), CodebookIdxt);
          *plen += sizeof(CodebookIdxt);
        }
        last_param_success = SECOND_PARAM;
        location_broken = NO_PARAM;
      }
      if(packs - (gint16)sizeof(adjustDbt) < 0 && packs != 0)
      {
        location_broken = THIRD_PARAM;
        if_half_broken = PARAM_IS_BROKEN;
        first_half_packet = tvb_get_guint8(tvb, *plen);
        return ;
      }
      if(packs >= sizeof(adjustDbt) && (location_broken == THIRD_PARAM
          || location_broken == NO_PARAM))
      {
        if(if_half_broken)
        {
          packs -= sizeof(adjustDbt)/2;
          second_half_packet = tvb_get_guint8(tvb, *plen);
          adjustDbt = 0;
          adjustDbt = adjustDbt | second_half_packet;
          adjustDbt = (adjustDbt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
          a = (short)adjustDbt;
          proto_tree_add_int(rfapi_sdk_msg_dsc, adjustDb, tvb, *plen, sizeof(adjustDbt)/2, a);
          *plen += sizeof(adjustDbt)/2;
          if_half_broken = NO_PARAM;
        }
        else
        {
          packs -= sizeof(adjustDbt);
          adjustDbt = sci_tvb_get_ntohs(tvb, *plen);
          a = (short)adjustDbt;
          proto_tree_add_int(rfapi_sdk_msg_dsc, adjustDb, tvb, *plen, sizeof(adjustDbt), a);
          *plen += sizeof(adjustDbt);
        }
        last_param_success = THIRD_PARAM;
        location_broken = NO_PARAM;
      }
    }

    for (numTrxDevices = num_trx_devices_global; numTrxDevices < MAX_TRX_DEVICES &&
         packs > 0; numTrxDevices++)
    {
      for (numAntsPerTrxLayer = num_ants_per_trx_layer_global;
           numAntsPerTrxLayer < MAX_ANTS_PER_LAYER && packs > 0; numAntsPerTrxLayer++)
      {
        packLen = SIZE_PER_ITR_GET_BEAM_RSP;
        if(location_broken != NO_PARAM)
        {
          packLen = get_correct_pack_length_get_beam();
        }
        if( packs < SIZE_PER_ITR_GET_BEAM_RSP)
        {
          packLen = packs;
        }

        ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk,
                                             tvb, *plen, packLen,
                                             "Beam[%d] TrxDevice[%d] AntsPerTrxLayer[%d]",
                                             num_Beamst, numTrxDevices, numAntsPerTrxLayer);
        rfapi_sdk_msg_dsc20 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree1);
        num_beamst_global = num_Beamst;
        num_trx_devices_global = numTrxDevices;
        num_ants_per_trx_layer_global = numAntsPerTrxLayer;

        if(packs - sizeof(trxDeviceNumt) < 0 && packs != 0)
        {
          location_broken = FOURTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(trxDeviceNumt) && (location_broken == FOURTH_PARAM
           || location_broken == NO_PARAM))
        {
          packs -= sizeof(trxDeviceNumt);
          trxDeviceNumt = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc20, trxDeviceNum, tvb, *plen,
                              sizeof(trxDeviceNumt), trxDeviceNumt);
          last_param_success = FOURTH_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(trxDeviceNumt);
        }
        if(packs - (gint16)sizeof(antElementt) < 0 && packs != 0)
        {
          location_broken = FIFTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(antElementt) && (location_broken == FIFTH_PARAM
           || location_broken == NO_PARAM))
        {
          packs -= sizeof(antElementt);
          antElementt = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc20, antElement, tvb, *plen,
                              sizeof(antElementt), antElementt);
          last_param_success = FIFTH_PARAM;
          location_broken = NO_PARAM;
          *plen += sizeof(antElementt);
        }
        if(packs - (gint16)sizeof(on_offt) < 0 && packs != 0)
        {
          location_broken = SIXTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(on_offt) && (location_broken == SIXTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(on_offt)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            on_offt = 0;
            on_offt = on_offt | second_half_packet;
            on_offt = (on_offt << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            a = (short)on_offt;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, on_off, tvb, *plen, sizeof(on_offt)/2, a);
            *plen += sizeof(on_offt)/2;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(on_offt);
            on_offt = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)on_offt;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, on_off, tvb, *plen, sizeof(on_offt), a);
            *plen += sizeof(on_offt);
          }
          location_broken = NO_PARAM;
          last_param_success = SIXTH_PARAM;
        }
        if(packs - (gint16)sizeof(rxPhaset) < 0 && packs != 0)
        {
          location_broken = SEVENTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxPhaset) && (location_broken == SEVENTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(rxPhaset)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxPhaset = 0;
            rxPhaset = rxPhaset | second_half_packet;
            rxPhaset = (rxPhaset << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, rxPhase, tvb, *plen,
                                sizeof(rxPhaset)/2, rxPhaset);
            *plen += sizeof(rxPhaset)/2;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxPhaset);
            rxPhaset = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, rxPhase, tvb, *plen,
                                sizeof(rxPhaset), rxPhaset);
            *plen += sizeof(rxPhaset);
          }
          last_param_success = SEVENTH_PARAM;
          location_broken = NO_PARAM;
        }
        if(packs - (gint16)sizeof(rxGaint) < 0 && packs != 0)
        {
          location_broken = EIGHTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxGaint) && (location_broken == EIGHTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(rxGaint)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxGaint = 0;
            rxGaint = rxGaint | second_half_packet;
            rxGaint = (rxGaint << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            a = (short)rxGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, rxGain, tvb, *plen, sizeof(rxGaint)/2, a);
            *plen += sizeof(rxGaint)/2;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxGaint);
            rxGaint = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)rxGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, rxGain, tvb, *plen, sizeof(rxGaint), a);
            *plen += sizeof(rxGaint);
          }
          last_param_success = EIGHTH_PARAM;
          location_broken = NO_PARAM;
        }
        if(packs - (gint16)sizeof(txPhaset) < 0 && packs != 0)
        {
          location_broken = NINETH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(txPhaset) && (location_broken == NINETH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(txPhaset)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            txPhaset = 0;
            txPhaset = txPhaset | second_half_packet;
            txPhaset = (txPhaset << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, txPhase, tvb, *plen,
                                sizeof(txPhaset)/2, txPhaset);
            *plen += sizeof(txPhaset)/2;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(txPhaset);
            txPhaset = sci_tvb_get_ntohs(tvb, *plen);
            proto_tree_add_uint(rfapi_sdk_msg_dsc20, txPhase, tvb, *plen,
                                sizeof(txPhaset), txPhaset);
            *plen += sizeof(txPhaset);
          }
          last_param_success = NINETH_PARAM;
          location_broken = NO_PARAM;
        }
        if(packs - (gint16)sizeof(txGaint) < 0 && packs != 0)
        {
          location_broken = TENTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(txGaint) && (location_broken == TENTH_PARAM
           || location_broken == NO_PARAM))
        {
          if(if_half_broken)
          {
            packs -= sizeof(txGaint)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            txGaint = 0;
            txGaint = txGaint | second_half_packet;
            txGaint = (txGaint << SECOND_HALF_PACKET_BITS)| (first_half_packet);
            a = (short)txGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, txGain, tvb, *plen, sizeof(txGaint)/2, a);
            *plen += sizeof(txGaint)/2;
            if_half_broken = NO_PARAM;
          }
          else
          {
            packs -= sizeof(txGaint);
            txGaint = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)txGaint;
            proto_tree_add_int(rfapi_sdk_msg_dsc20, txGain, tvb, *plen, sizeof(txGaint), a);
            *plen += sizeof(txGaint);
          }
          last_param_success = TENTH_PARAM;
          location_broken = NO_PARAM;
        }
        if(packs - sizeof(trxDeviceNumt) < 0 && packs != 0)
        {
          location_broken = FOURTH_PARAM;
          if_half_broken = PARAM_IS_BROKEN;
          first_half_packet = tvb_get_guint8(tvb, *plen);
        }
        if(num_trx_devices_global != MAX_TRX_DEVICES - 1 && packs == 0 &&
           num_ants_per_trx_layer_global != MAX_ANTS_PER_LAYER -1 &&
           if_half_broken == NO_PARAM && location_broken == NO_PARAM && 
           last_param_success == NUM_PARAMS_GET_BEAM)
        {
          num_ants_per_trx_layer_global += 1;
        }
        else
        {
          set_iterators(SET_ITERATORS_GET_BEAM_RSP);
        }
      }
    }
  }
  location_broken = last_param_success + 1;
  if(location_broken == NUM_PARAMS_GET_BEAM + 1)
  {
    /* Check if the segment ended on a beam boundary. */
    if(numTrxDevices == MAX_TRX_DEVICES &&
       numAntsPerTrxLayer == MAX_ANTS_PER_LAYER)
    {
      location_broken = FIRST_PARAM;
    }
    else
    {
      location_broken = FOURTH_PARAM;
    }
  }
}

static guint16
get_correct_pack_length_rssi()
{
  /*packet_size_till_ith_from_last is the sum of size of elements in RSSI ind
  starting from last element to ith element*/
  guint16 packet_size_till_ith_from_last[5] ={0,6,5,4,2};
  guint16 sum = 0;
  sum += packet_size_till_ith_from_last[locationBrokenWbrssi];
  if(ifHalfBrokenWbrssi)
  {
    sum -= 1;
  }
  return sum;
}

static void
dissect_rf_get_wbrssi_ind(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32  statusTemp;
  guint16  wbrssiReqIdxTemp;
  guint8   scsTemp;
  guint16  sfnTemp;
  guint8   slotTemp;
  guint8   numRxPathsTemp;
  guint8   numSlotsTemp;
  guint8   numLayersItr;
  guint8   numSlotsItr;
  guint8   numSymbolsItr;
  guint8   dataValidTemp;
  guint16  rxPathIdTemp;
  guint16  symSfnTemp;
  guint8   symSlotTemp;
  guint8   symIdxTemp;
  guint8   lnaGainStateTemp;
  gint16   symbRSSIdBFSTemp;
  gint16   rxGaindBTemp;
  gint16   a = 0;
  guint8 second_half_packet;
  guint16 packLen;
  proto_item* ti;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc1 = NULL;
  gint16 packs = vend_spec_len;
  gint16 sizeOfDataBeforeLoop;
  gint16 sizeOfDataPacket;
  sizeOfDataBeforeLoop = sizeof(statusTemp) + sizeof(wbrssiReqIdxTemp) +
                         sizeof(sfnTemp) + sizeof(slotTemp) + sizeof(scsTemp) +
                         sizeof(numRxPathsTemp) + sizeof(numSlotsTemp);
  sizeOfDataPacket = sizeof(dataValidTemp) + sizeof(rxPathIdTemp) +
                     sizeof(symSfnTemp) + sizeof(symSlotTemp) +
                     sizeof(symIdxTemp) + sizeof(symbRSSIdBFSTemp) +
                     sizeof(rxGaindBTemp) + sizeof(lnaGainStateTemp);

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  if(segment == 0)
  {
    statusTemp = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                        sizeof(statusTemp), statusTemp);
    *plen += sizeof(statusTemp);
    wbrssiReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, wbrssiReqIdxWbrssi, tvb, *plen,
                        sizeof(wbrssiReqIdxTemp), wbrssiReqIdxTemp);
    *plen += sizeof(wbrssiReqIdxTemp);
    sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnWbrssi, tvb, *plen,
                        sizeof(sfnTemp), sfnTemp);
    *plen += sizeof(sfnTemp);
    slotTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, slotWbrssi, tvb, *plen,
                        sizeof(slotTemp), slotTemp);
    *plen += sizeof(slotTemp);
    scsTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, scsWbrssi, tvb, *plen,
                        sizeof(scsTemp), scsTemp);
    *plen += sizeof(scsTemp);
    numRxPathsTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numRxPathsWbrssi, tvb, *plen,
                        sizeof(numRxPathsTemp), numRxPathsTemp);
    *plen += sizeof(numRxPathsTemp);
    numSlotsTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numSlotsWbrssi, tvb, *plen,
                        sizeof(numSlotsTemp), numSlotsTemp);
    *plen += sizeof(numSlotsTemp);
    numLayersItrWrssi = 0;
    numSlotsItrWbrssi = 0;
    numSymbolsItrWbrssi = 0;
    locationBrokenWbrssi = NO_PARAM;
    lastParamSuccessWbrssi = NO_PARAM;
    ifHalfBrokenWbrssi = NO_PARAM;
    numSlotsWbrssiG = numSlotsTemp;
  }
  else
  {
    packs += sizeOfDataBeforeLoop;
  }
  packs = packs - SIZE_OF_COMMON_MSG_HEADER - sizeOfDataBeforeLoop;
  packLen = sizeOfDataPacket;
  for(numLayersItr = numLayersItrWrssi; numLayersItr < numRxPathsTemp; numLayersItr++)
  {
    for(numSlotsItr = numSlotsItrWbrssi; numSlotsItr < numSlotsWbrssiG && packs > 0;
        numSlotsItr++)
    {
      for(numSymbolsItr = numSymbolsItrWbrssi; numSymbolsItr < NUM_SYMBOLS_PER_SLOT &&
          packs > 0; numSymbolsItr++)
      {
        numLayersItrWrssi = numLayersItr;
        numSlotsItrWbrssi = numSlotsItr;
        numSymbolsItrWbrssi = numSymbolsItr;
        packLen = sizeOfDataPacket;
        if(locationBrokenWbrssi != NO_PARAM)
        {
          packLen = get_correct_pack_length_rssi();
        }
        if( packs < sizeOfDataPacket)
        {
          packLen = packs;
        }
        ti = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk, tvb, *plen, packLen,
        "Layer[%d] Slot[%d] Symbol[%d]",numLayersItr,numSlotsItr,numSymbolsItr);
        rfapi_sdk_msg_dsc1 = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_data_subtree1);
        if(packs < sizeof(dataValidTemp) && packs != 0)
        {
          locationBrokenWbrssi = FIRST_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(dataValidTemp) && (locationBrokenWbrssi == FIRST_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          packs -= sizeof(dataValidTemp);
          dataValidTemp = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, dataValidWbrssi, tvb, *plen,
                              sizeof(dataValidTemp), dataValidTemp);
          *plen += sizeof(dataValidTemp);
          lastParamSuccessWbrssi = FIRST_PARAM;
          locationBrokenWbrssi = NO_PARAM;
        }
        if(packs < sizeof(rxPathIdTemp) && packs != 0)
        {
          locationBrokenWbrssi = SECOND_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxPathIdTemp) && (locationBrokenWbrssi == SECOND_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          if(ifHalfBrokenWbrssi)
          {
            packs -= sizeof(rxPathIdTemp)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxPathIdTemp = 0;
            rxPathIdTemp = rxPathIdTemp | second_half_packet;
            rxPathIdTemp = (rxPathIdTemp << SECOND_HALF_PACKET_BITS)
                            | (firstHalfPacketWbrssi);
            a = (short)rxPathIdTemp;
            proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxPathIdWbrssi, tvb, *plen,
                                sizeof(rxPathIdTemp)/2, a);
            *plen += sizeof(rxPathIdTemp)/2;
            ifHalfBrokenWbrssi = NO_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxPathIdTemp);
            rxPathIdTemp = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)rxPathIdTemp;
            proto_tree_add_uint(rfapi_sdk_msg_dsc1, rxPathIdWbrssi, tvb, *plen,
                                sizeof(rxPathIdTemp), a);
            *plen += sizeof(rxPathIdTemp);
            lastParamSuccessWbrssi = SECOND_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
        }
        if(packs < sizeof(symSfnTemp) && packs != 0)
        {
          locationBrokenWbrssi = THIRD_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(symSfnTemp) && (locationBrokenWbrssi == THIRD_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          if(ifHalfBrokenWbrssi)
          {
            packs -= sizeof(symSfnTemp)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            symSfnTemp = 0;
            symSfnTemp = symSfnTemp | second_half_packet;
            symSfnTemp = (symSfnTemp << SECOND_HALF_PACKET_BITS)
                          | (firstHalfPacketWbrssi);
            a = (short)symSfnTemp;
            proto_tree_add_uint(rfapi_sdk_msg_dsc1, symSfnWbrssi, tvb, *plen,
                                sizeof(symSfnTemp)/2, a);
            *plen += sizeof(symSfnTemp)/2;
            ifHalfBrokenWbrssi = NO_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
          else
          {
            packs -= sizeof(symSfnTemp);
            symSfnTemp = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)symSfnTemp;
            proto_tree_add_uint(rfapi_sdk_msg_dsc1, symSfnWbrssi, tvb, *plen,
                                sizeof(symSfnTemp), a);
            *plen += sizeof(symSfnTemp);
            lastParamSuccessWbrssi = THIRD_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
        }
        if(packs < sizeof(symSlotTemp) && packs != 0)
        {
          locationBrokenWbrssi = FOURTH_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(symSlotTemp) && (locationBrokenWbrssi == FOURTH_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          packs -= sizeof(symSlotTemp);
          symSlotTemp = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, symSlotWbrssi, tvb, *plen,
                              sizeof(symSlotTemp), symSlotTemp);
          *plen += sizeof(symSlotTemp);
          lastParamSuccessWbrssi = FOURTH_PARAM;
          locationBrokenWbrssi = NO_PARAM;
        }
        if(packs < sizeof(symIdxTemp) && packs != 0)
        {
          locationBrokenWbrssi = FIFTH_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(symIdxTemp) && (locationBrokenWbrssi == FIFTH_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          packs -= sizeof(symIdxTemp);
          symIdxTemp = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, symIdxWbrssi, tvb, *plen,
                              sizeof(symIdxTemp), symIdxTemp);
          *plen += sizeof(symIdxTemp);
          lastParamSuccessWbrssi = FIFTH_PARAM;
          locationBrokenWbrssi = NO_PARAM;
        }
        if(packs < sizeof(lnaGainStateTemp) && packs != 0)
        {
          locationBrokenWbrssi = SIXTH_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(lnaGainStateTemp) && (locationBrokenWbrssi == SIXTH_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          packs -= sizeof(lnaGainStateTemp);
          lnaGainStateTemp = tvb_get_guint8(tvb, *plen);
          proto_tree_add_uint(rfapi_sdk_msg_dsc1, lnaGainStateWbrssi, tvb, *plen,
                              sizeof(lnaGainStateTemp), lnaGainStateTemp);
          *plen += sizeof(lnaGainStateTemp);
          lastParamSuccessWbrssi = SIXTH_PARAM;
          locationBrokenWbrssi = NO_PARAM;
        }
        if(packs < sizeof(symbRSSIdBFSTemp) && packs != 0)
        {
          locationBrokenWbrssi = SEVENTH_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(symbRSSIdBFSTemp) && (locationBrokenWbrssi == SEVENTH_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          if(ifHalfBrokenWbrssi)
          {
            packs -= sizeof(symbRSSIdBFSTemp)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            symbRSSIdBFSTemp = 0;
            symbRSSIdBFSTemp = symbRSSIdBFSTemp | second_half_packet;
            symbRSSIdBFSTemp = (symbRSSIdBFSTemp << SECOND_HALF_PACKET_BITS)
                                | (firstHalfPacketWbrssi);
            a = (short)symbRSSIdBFSTemp;
            proto_tree_add_int(rfapi_sdk_msg_dsc1, symbRSSIdBFSWbrssi, tvb, *plen,
                               sizeof(symbRSSIdBFSTemp)/2, a);
            *plen += sizeof(symbRSSIdBFSTemp)/2;
            ifHalfBrokenWbrssi = NO_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
          else
          {
            packs -= sizeof(symbRSSIdBFSTemp);
            symbRSSIdBFSTemp = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)symbRSSIdBFSTemp;
            proto_tree_add_int(rfapi_sdk_msg_dsc1, symbRSSIdBFSWbrssi, tvb, *plen,
                               sizeof(symbRSSIdBFSTemp), a);
            *plen += sizeof(symbRSSIdBFSTemp);
            lastParamSuccessWbrssi = SEVENTH_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
        }
        if(packs < sizeof(rxGaindBTemp) && packs != 0)
        {
          locationBrokenWbrssi = EIGHTH_PARAM;
          ifHalfBrokenWbrssi = PARAM_IS_BROKEN;
          firstHalfPacketWbrssi = tvb_get_guint8(tvb, *plen);
          return ;
        }
        if(packs >= sizeof(rxGaindBTemp) && (locationBrokenWbrssi == EIGHTH_PARAM ||
           locationBrokenWbrssi == NO_PARAM))
        {
          if(ifHalfBrokenWbrssi)
          {
            packs -= sizeof(rxGaindBTemp)/2;
            second_half_packet = tvb_get_guint8(tvb, *plen);
            rxGaindBTemp = 0;
            rxGaindBTemp = rxGaindBTemp | second_half_packet;
            rxGaindBTemp = (rxGaindBTemp << SECOND_HALF_PACKET_BITS)
                            | (firstHalfPacketWbrssi);
            a = (short)rxGaindBTemp;
            proto_tree_add_int(rfapi_sdk_msg_dsc1, rxGaindBWbrssi, tvb, *plen,
                               sizeof(rxGaindBTemp)/2, a);
            *plen += sizeof(rxGaindBTemp)/2;
            ifHalfBrokenWbrssi = NO_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
          else
          {
            packs -= sizeof(rxGaindBTemp);
            rxGaindBTemp = sci_tvb_get_ntohs(tvb, *plen);
            a = (short)rxGaindBTemp;
            proto_tree_add_int(rfapi_sdk_msg_dsc1, rxGaindBWbrssi, tvb, *plen,
                               sizeof(rxGaindBTemp), a);
            *plen += sizeof(rxGaindBTemp);
            lastParamSuccessWbrssi = EIGHTH_PARAM;
            locationBrokenWbrssi = NO_PARAM;
          }
        }
        set_iterators(SET_ITERATORS_GET_WBRSSI_IND);
      }
    }
  }
  locationBrokenWbrssi = lastParamSuccessWbrssi + 1;
  if(locationBrokenWbrssi == NUM_PARAMS_GET_WBRSSI + 1)
  {
    locationBrokenWbrssi = FIRST_PARAM;
  }
}

static void
dissect_rf_get_tx_power_ind(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32 statusTemp;
  guint16 txPowerReqIdxTemp;
  guint8  scsTemp;
  guint16 sfnTemp;
  guint8  slotTemp;
  guint8  numTxPathsTemp;
  guint8  numTrxDevicesTemp;
  guint16 txPathIdTemp;
  gint16  txPowerDbmTemp;
  guint8  txDeviceIdTemp;
  guint8  numLayersItr;
  guint8  numDevicesItr;
  gint16  a = 0;
  guint8  secondHalfPacket;
  gint16  sizeOfDataBeforeLoop = sizeof(statusTemp) + sizeof(txPowerReqIdxTemp) +
                                 sizeof(sfnTemp) + sizeof(slotTemp) +
                                 sizeof(scsTemp) + sizeof(numTxPathsTemp) +
                                 sizeof(numTrxDevicesTemp);
  gint16  sizeOfPathDataPacket = sizeof(txPathIdTemp) + sizeof(txPowerDbmTemp);
  gint16  sizeOfDeviceDataPacket = sizeof(txDeviceIdTemp) + sizeof(txPowerDbmTemp);
  gint16  packs = vend_spec_len;
  guint16 packLen = sizeOfPathDataPacket;
  proto_item* ti;
  proto_item* ti1;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc1 = NULL;
  proto_tree* rfapi_sdk_msg_dsc2 = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  if(segment == 0)
  {
    statusTemp = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                        sizeof(statusTemp), statusTemp);
    *plen += sizeof(statusTemp);
    txPowerReqIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, txPowerReqIdx, tvb, *plen,
                        sizeof(txPowerReqIdxTemp), txPowerReqIdxTemp);
    *plen += sizeof(txPowerReqIdxTemp);
    sfnTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, sfnTxPower, tvb, *plen,
                        sizeof(sfnTemp), sfnTemp);
    *plen += sizeof(sfnTemp);
    slotTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, slotTxPower, tvb, *plen,
                        sizeof(slotTemp), slotTemp);
    *plen += sizeof(slotTemp);
    scsTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, scsTxPower, tvb, *plen,
                        sizeof(scsTemp), scsTemp);
    *plen += sizeof(scsTemp);
    numTxPathsTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numTxPathsTxPower, tvb, *plen,
                        sizeof(numTxPathsTemp), numTxPathsTemp);
    *plen += sizeof(numTxPathsTemp);
    numTrxDevicesTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc, numTrxDevicesTxPower, tvb, *plen,
                        sizeof(numTrxDevicesTemp), numTrxDevicesTemp);
    *plen += sizeof(numTrxDevicesTemp);

    numLayersItrTxPower = 0;
    numTrxDevicesItrTxPower = 0;
    locationBrokenTxPower = NO_PARAM;
    lastParamSuccessTxPower = NO_PARAM;
    ifHalfBrokenTxPower = NO_PARAM;
  }
  else
  {
    packs += sizeOfDataBeforeLoop;
  }

  packs = packs - SIZE_OF_COMMON_MSG_HEADER - sizeOfDataBeforeLoop;

  for(numLayersItr = numLayersItrTxPower; numLayersItr < numTxPathsTemp; numLayersItr++)
  {
    packLen = sizeOfPathDataPacket;
    if(locationBrokenTxPower != NO_PARAM)
    {
      packLen = get_correct_pack_length_rssi();
    }
    if( packs < sizeOfPathDataPacket)
    {
      packLen = packs;
    }

    ti = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc, proto_rfapi_sdk,
           tvb, *plen, packLen, "Layer[%d]", numLayersItr);
    rfapi_sdk_msg_dsc1 = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_data_subtree1);

    if(packs < sizeof(txPathIdTemp) && packs != 0)
    {
      locationBrokenTxPower = FIRST_PARAM;
      ifHalfBrokenTxPower = PARAM_IS_BROKEN;
      firstHalfPacketTxPower = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(txPathIdTemp) && (locationBrokenTxPower == FIRST_PARAM ||
       locationBrokenTxPower == NO_PARAM))
    {
      if(ifHalfBrokenTxPower)
      {
        packs -= sizeof(txPathIdTemp)/2;
        secondHalfPacket = tvb_get_guint8(tvb, *plen);
        txPathIdTemp = 0;
        txPathIdTemp = txPathIdTemp | secondHalfPacket;
        txPathIdTemp = (txPathIdTemp << SECOND_HALF_PACKET_BITS)
                        | (firstHalfPacketTxPower);
        a = (short)txPathIdTemp;
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, txPathIdTxPower, tvb, *plen,
                            sizeof(txPathIdTemp)/2, a);
        *plen += sizeof(txPathIdTemp)/2;
        ifHalfBrokenTxPower = NO_PARAM;
        locationBrokenTxPower = NO_PARAM;
      }
      else
      {
        packs -= sizeof(txPathIdTemp);
        txPathIdTemp = sci_tvb_get_ntohs(tvb, *plen);
        a = (short)txPathIdTemp;
        proto_tree_add_uint(rfapi_sdk_msg_dsc1, txPathIdTxPower, tvb, *plen,
                            sizeof(txPathIdTemp), a);
        *plen += sizeof(txPathIdTemp);
        lastParamSuccessTxPower = FIRST_PARAM;
        locationBrokenTxPower = NO_PARAM;
      }
    }
    if(packs < sizeof(txPowerDbmTemp) && packs != 0)
    {
      locationBrokenTxPower = SECOND_PARAM;
      ifHalfBrokenTxPower = PARAM_IS_BROKEN;
      firstHalfPacketTxPower = tvb_get_guint8(tvb, *plen);
      return ;
    }
    if(packs >= sizeof(txPowerDbmTemp) && (locationBrokenTxPower == SECOND_PARAM ||
       locationBrokenTxPower == NO_PARAM))
    {
      if(ifHalfBrokenTxPower)
      {
        packs -= sizeof(txPowerDbmTemp)/2;
        secondHalfPacket = tvb_get_guint8(tvb, *plen);
        txPowerDbmTemp = 0;
        txPowerDbmTemp = txPowerDbmTemp | secondHalfPacket;
        txPowerDbmTemp = (txPowerDbmTemp << SECOND_HALF_PACKET_BITS)
                         | (firstHalfPacketTxPower);
        a = (short)txPowerDbmTemp;
        proto_tree_add_int(rfapi_sdk_msg_dsc1, txPowerDbmTxPower, tvb, *plen,
                            sizeof(txPowerDbmTemp)/2, a);
        *plen += sizeof(txPowerDbmTemp)/2;
        ifHalfBrokenTxPower = NO_PARAM;
        locationBrokenTxPower = NO_PARAM;
      }
      else
      {
        packs -= sizeof(txPowerDbmTemp);
        txPowerDbmTemp = sci_tvb_get_ntohs(tvb, *plen);
        a = (short)txPowerDbmTemp;
        proto_tree_add_int(rfapi_sdk_msg_dsc1, txPowerDbmTxPower, tvb, *plen,
                           sizeof(txPowerDbmTemp), a);
        *plen += sizeof(txPowerDbmTemp);
        lastParamSuccessTxPower = SECOND_PARAM;
        locationBrokenTxPower = NO_PARAM;
      }
    }

    for(numDevicesItr = numTrxDevicesItrTxPower; numDevicesItr < numTrxDevicesTemp; numDevicesItr++)
    {
      packLen = sizeOfDeviceDataPacket;
      if(locationBrokenTxPower != NO_PARAM)
      {
        packLen = get_correct_pack_length_rssi();
      }
      if( packs < sizeOfDeviceDataPacket)
      {
        packLen = packs;
      }

      ti1 = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc1, proto_rfapi_sdk,
           tvb, *plen, packLen, "Device[%d]", numDevicesItr);
      rfapi_sdk_msg_dsc2 = proto_item_add_subtree(ti1, ett_rfapi_sdk_msg_data_subtree2);

      if(packs >= sizeof(txDeviceIdTemp) && locationBrokenTxPower == NO_PARAM)
      {
        packs -= sizeof(txDeviceIdTemp);
        txDeviceIdTemp = tvb_get_guint8(tvb, *plen);
        proto_tree_add_uint(rfapi_sdk_msg_dsc2, txDeviceIdTxPower, tvb, *plen,
                            sizeof(txDeviceIdTemp), txDeviceIdTemp);
        *plen += sizeof(txDeviceIdTemp);
        lastParamSuccessTxPower = THIRD_PARAM;
        locationBrokenTxPower = NO_PARAM;
      }

      if(packs < sizeof(txPowerDbmTemp) && packs != 0)
      {
        locationBrokenTxPower = FOURTH_PARAM;
        ifHalfBrokenTxPower = PARAM_IS_BROKEN;
        firstHalfPacketTxPower = tvb_get_guint8(tvb, *plen);
        return ;
      }

      if(packs >= sizeof(txPowerDbmTemp) && (locationBrokenTxPower == FOURTH_PARAM ||
       locationBrokenTxPower == NO_PARAM))
      {
        if(ifHalfBrokenTxPower)
        {
          packs -= sizeof(txPowerDbmTemp)/2;
          secondHalfPacket = tvb_get_guint8(tvb, *plen);
          txPowerDbmTemp = 0;
          txPowerDbmTemp = txPowerDbmTemp | secondHalfPacket;
          txPowerDbmTemp = (txPowerDbmTemp << SECOND_HALF_PACKET_BITS)
                           | (firstHalfPacketTxPower);
          a = (short)txPowerDbmTemp;
          proto_tree_add_int(rfapi_sdk_msg_dsc2, txPowerDbmTxPower, tvb, *plen,
                              sizeof(txPowerDbmTemp)/2, a);
          *plen += sizeof(txPowerDbmTemp)/2;
          ifHalfBrokenTxPower = NO_PARAM;
          locationBrokenTxPower = NO_PARAM;
        }
        else
        {
          packs -= sizeof(txPowerDbmTemp);
          txPowerDbmTemp = sci_tvb_get_ntohs(tvb, *plen);
          a = (short)txPowerDbmTemp;
          proto_tree_add_int(rfapi_sdk_msg_dsc2, txPowerDbmTxPower, tvb, *plen,
                             sizeof(txPowerDbmTemp), a);
          *plen += sizeof(txPowerDbmTemp);
          lastParamSuccessTxPower = FOURTH_PARAM;
          locationBrokenTxPower = NO_PARAM;
        }
      }

      if(locationBrokenTxPower == NO_PARAM)
      {
        numTrxDevicesItrTxPower = 0;
      }
      else
      {
        numTrxDevicesItrTxPower = numDevicesItr;
      }
    }

    numLayersItrTxPower = numLayersItr;
  }

  locationBrokenTxPower = lastParamSuccessTxPower + 1;
  if(locationBrokenTxPower == NUM_PARAMS_GET_TX_POWER + 1)
  {
    locationBrokenTxPower = FIRST_PARAM;
  }
}

static void
dissect_rf_alarm_ind(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32 statusTemp;
  guint8  numLayersTemp;
  guint8  numTrxDevicesTemp;
  guint32 moduleAlarmTemp;
  guint8  layerNumTemp;
  guint32 layerAlarmTemp;
  gint16  EIRPDbmTemp;
  gint16  rxGaindBTemp;
  guint8  trxDeviceNumTemp;
  guint32 deviceAlarmTemp;
  gint16  temperatureTemp;
  guint8  numLayersItr;
  guint8  numTrxDevicesItr;
  guint16 packLen;

  proto_item* tiLayer;
  proto_item* tiDevices;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc1 = NULL;
  proto_tree* rfapi_sdk_msg_dsc2 = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);
  statusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                      sizeof(statusTemp), statusTemp);
  *plen += sizeof(statusTemp);
  numLayersTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numLayersAlarm, tvb, *plen,
                      sizeof(numLayersTemp), numLayersTemp);
  *plen += sizeof(numLayersTemp);
  numTrxDevicesTemp = tvb_get_guint8(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, numTrxDevicesAlarm, tvb, *plen,
                      sizeof(numTrxDevicesTemp), numTrxDevicesTemp);
  *plen += sizeof(numTrxDevicesTemp);
  moduleAlarmTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, moduleMaskAlarm, tvb, *plen,
                      sizeof(moduleAlarmTemp), moduleAlarmTemp);
  *plen += sizeof(moduleAlarmTemp);

  packLen = sizeof(layerNumTemp) + sizeof(layerAlarmTemp) +
            sizeof(EIRPDbmTemp) + sizeof(rxGaindBTemp);
  for(numLayersItr = 0; numLayersItr < numLayersTemp; numLayersItr++)
  {
    tiLayer = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc,
      proto_rfapi_sdk, tvb, *plen, packLen, "Layer[%d]", numLayersItr);
    rfapi_sdk_msg_dsc1 = proto_item_add_subtree(tiLayer,
      ett_rfapi_sdk_msg_data_subtree1);
    layerNumTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc1, layerNumAlarm, tvb, *plen,
                        sizeof(layerNumTemp), layerNumTemp);
    *plen += sizeof(layerNumTemp);
    layerAlarmTemp = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc1, layerMaskAlarm, tvb, *plen,
                        sizeof(layerAlarmTemp), layerAlarmTemp);
    *plen += sizeof(layerAlarmTemp);
    EIRPDbmTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc1, EIRPDbmAlarm, tvb, *plen,
                       sizeof(EIRPDbmTemp), EIRPDbmTemp);
    *plen += sizeof(EIRPDbmTemp);
    rxGaindBTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc1, rxGaindBAlarm, tvb, *plen,
                       sizeof(rxGaindBTemp), rxGaindBTemp);
    *plen += sizeof(rxGaindBTemp);
  }

  packLen = sizeof(trxDeviceNumTemp) + sizeof(deviceAlarmTemp) +
            sizeof(temperatureTemp);
  for(numTrxDevicesItr = 0; numTrxDevicesItr < numTrxDevicesTemp; numTrxDevicesItr++)
  {
    tiDevices = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc,
      proto_rfapi_sdk, tvb, *plen, packLen, "Device[%d]", numTrxDevicesItr);
    rfapi_sdk_msg_dsc2 = proto_item_add_subtree(tiDevices,
      ett_rfapi_sdk_msg_data_subtree2);
    trxDeviceNumTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc2, trxDeviceNumAlarm, tvb, *plen,
                        sizeof(trxDeviceNumTemp), trxDeviceNumTemp);
    *plen += sizeof(trxDeviceNumTemp);
    deviceAlarmTemp = sci_tvb_get_ntohl(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc2, deviceMaskAlarm, tvb, *plen,
                        sizeof(deviceAlarmTemp), deviceAlarmTemp);
    *plen += sizeof(deviceAlarmTemp);
    temperatureTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc2, tempAlarm, tvb, *plen,
                        sizeof(temperatureTemp), temperatureTemp);
    *plen += sizeof(temperatureTemp);
  }
}

static void
dissect_rf_get_temperature_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32 statusTemp;
  guint16 numTrxDevicesTemp;
  guint8  trxDeviceNumTemp;
  gint16  temperatureTemp;

  guint16 numTrxDevicesItr;
  guint16 packLen;

  proto_item* tiDevices;
  proto_tree* rfapi_sdk_msg_dsc = NULL;
  proto_tree* rfapi_sdk_msg_dsc1 = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  statusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                      sizeof(statusTemp), statusTemp);
  *plen += sizeof(statusTemp);

  numTrxDevicesTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, trxDeviceNumAlarm, tvb, *plen,
                      sizeof(numTrxDevicesTemp), numTrxDevicesTemp);
  *plen += sizeof(numTrxDevicesTemp);

  packLen = sizeof(trxDeviceNumTemp) + sizeof(temperatureTemp);
  for(numTrxDevicesItr = 0; numTrxDevicesItr < numTrxDevicesTemp; numTrxDevicesItr++)
  {
    tiDevices = proto_tree_add_protocol_format(rfapi_sdk_msg_dsc,
      proto_rfapi_sdk, tvb, *plen, packLen, "Device[%d]", numTrxDevicesItr);

    rfapi_sdk_msg_dsc1 = proto_item_add_subtree(tiDevices,
      ett_rfapi_sdk_msg_data_subtree1);

    trxDeviceNumTemp = tvb_get_guint8(tvb, *plen);
    proto_tree_add_uint(rfapi_sdk_msg_dsc1, trxDeviceNumAlarm, tvb, *plen,
                        sizeof(trxDeviceNumTemp), trxDeviceNumTemp);
    *plen += sizeof(trxDeviceNumTemp);

    temperatureTemp = sci_tvb_get_ntohs(tvb, *plen);
    proto_tree_add_int(rfapi_sdk_msg_dsc1, tempAlarm, tvb, *plen,
                       sizeof(temperatureTemp), temperatureTemp);
    *plen += sizeof(temperatureTemp);
  }
}

static void
dissect_rf_set_thresholds_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  gint16 maxTxOperatingPower;
  gint16 txPowerLimit;
  gint16 maxPerformanceTemp;
  gint16 minPerformanceTemp;
  gint16 maxOperatingTemp;
  gint16 minOperatingTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  maxTxOperatingPower = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxTxOperatingPower, tvb, *plen,
                      sizeof(maxTxOperatingPower), maxTxOperatingPower);
  *plen += sizeof(maxTxOperatingPower);

  txPowerLimit = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresTxPowerLimit, tvb, *plen,
                      sizeof(txPowerLimit), txPowerLimit);
  *plen += sizeof(txPowerLimit);

  maxPerformanceTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxPerformanceTemp, tvb, *plen,
                      sizeof(maxPerformanceTemp), maxPerformanceTemp);
  *plen += sizeof(maxPerformanceTemp);

  minPerformanceTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMinPerformanceTemp, tvb, *plen,
                      sizeof(minPerformanceTemp), minPerformanceTemp);
  *plen += sizeof(minPerformanceTemp);

  maxOperatingTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxOperatingTemp, tvb, *plen,
                      sizeof(maxOperatingTemp), maxOperatingTemp);
  *plen += sizeof(maxOperatingTemp);

  minOperatingTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMinOperatingTemp, tvb, *plen,
                      sizeof(minOperatingTemp), minOperatingTemp);
  *plen += sizeof(minOperatingTemp);
}

static void
dissect_rf_set_thresholds_rsp(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32 statusTemp;
  gint16 maxTxOperatingPower;
  gint16 txPowerLimit;
  gint16 maxPerformanceTemp;
  gint16 minPerformanceTemp;
  gint16 maxOperatingTemp;
  gint16 minOperatingTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  statusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                      sizeof(statusTemp), statusTemp);
  *plen += sizeof(statusTemp);

  maxTxOperatingPower = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxTxOperatingPower, tvb, *plen,
                      sizeof(maxTxOperatingPower), maxTxOperatingPower);
  *plen += sizeof(maxTxOperatingPower);

  txPowerLimit = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresTxPowerLimit, tvb, *plen,
                      sizeof(txPowerLimit), txPowerLimit);
  *plen += sizeof(txPowerLimit);

  maxPerformanceTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxPerformanceTemp, tvb, *plen,
                      sizeof(maxPerformanceTemp), maxPerformanceTemp);
  *plen += sizeof(maxPerformanceTemp);

  minPerformanceTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMinPerformanceTemp, tvb, *plen,
                      sizeof(minPerformanceTemp), minPerformanceTemp);
  *plen += sizeof(minPerformanceTemp);

  maxOperatingTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMaxOperatingTemp, tvb, *plen,
                      sizeof(maxOperatingTemp), maxOperatingTemp);
  *plen += sizeof(maxOperatingTemp);

  minOperatingTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, thresMinOperatingTemp, tvb, *plen,
                      sizeof(minOperatingTemp), minOperatingTemp);
  *plen += sizeof(minOperatingTemp);
}

static void
dissect_rf_test_req(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  gint16 testIdxTemp;
  gint16 testTypeTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  testIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, testIdx, tvb, *plen,
                      sizeof(testIdxTemp), testIdxTemp);
  *plen += sizeof(testIdxTemp);

  testTypeTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, testType, tvb, *plen,
                      sizeof(testTypeTemp), testTypeTemp);
  *plen += sizeof(testTypeTemp);
}

static void
dissect_rf_test_ind(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32* plen)
{
  guint32 statusTemp;
  gint16  testIdxTemp;
  gint16  testTypeTemp;
  guint32 testStatusTemp;

  proto_tree* rfapi_sdk_msg_dsc = NULL;

  rfapi_sdk_msg_dsc = proto_item_add_subtree(tree, ett_rfapi_sdk_msg_header);

  statusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, status2, tvb, *plen,
                      sizeof(statusTemp), statusTemp);
  *plen += sizeof(statusTemp);

  testIdxTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, testIdx, tvb, *plen,
                     sizeof(testIdxTemp), testIdxTemp);
  *plen += sizeof(testIdxTemp);

  testTypeTemp = sci_tvb_get_ntohs(tvb, *plen);
  proto_tree_add_int(rfapi_sdk_msg_dsc, testType, tvb, *plen,
                     sizeof(testTypeTemp), testTypeTemp);
  *plen += sizeof(testTypeTemp);

  testStatusTemp = sci_tvb_get_ntohl(tvb, *plen);
  proto_tree_add_uint(rfapi_sdk_msg_dsc, testStatus, tvb, *plen,
                      sizeof(testStatusTemp), testStatusTemp);
  *plen += sizeof(testStatusTemp);
}

static void
dissect_rfapi_msg_data(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, int msg_type, guint32* plen)
{
  proto_item* ti;
  ti = proto_tree_add_protocol_format(tree, proto_rfapi_sdk, tvb, *plen, -1,
    "Message Data (%s)", val_to_str(msg_type, rfapi_msg_header_string, "Unknown"));
  switch (msg_type)
  {
    case RF_SET_BEAM_TABLE_REQ:
      dissect_rf_set_beam_table_req(tvb, pinfo, ti, plen);
      break;
    case RF_SET_SLOT_CONFIG_REQ:
      dissect_rf_set_slot_config_req(tvb, pinfo, ti, plen);
      break;
    case RF_TRX_DEVICE_CTRL_REQ:
      dissect_rf_trx_device_ctrl_req(tvb, pinfo, ti, plen);
      break;
    case RF_LOAD_BEAM_REQ:
      dissect_rf_load_beam_req(tvb, pinfo, ti, plen);
      break;
    case RF_ENABLE_REQ:
      dissect_rf_enable_req(tvb, pinfo, ti, plen);
      break;
    case RF_DISABLE_REQ:
      dissect_rf_disable_req(tvb, pinfo, ti, plen);
      break;
    case RF_SET_TX_POWER_REQ:
      dissect_rf_set_tx_power_req(tvb, pinfo, ti, plen);
      break;
    case RF_CONFIG_REQ:
      dissect_rf_config_req(tvb, pinfo, ti, plen);
      break;
    case RF_SET_HW_CONFIG_REQ:
      dissect_rf_hw_config_req(tvb, pinfo, ti, plen);
      break;
    case RF_STOP_RSP:
      dissect_rf_stop_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_START_RSP:
      dissect_rf_start_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_SET_HW_CONFIG_RSP:
      dissect_rf_hw_config_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_CONFIG_RSP:
      dissect_rf_config_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_SET_TX_POWER_RSP:
      dissect_rf_set_tx_power_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_DISABLE_RSP:
      dissect_rf_disable_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_SET_SLOT_CONFIG_RSP:
      dissect_rf_set_slot_config_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_TRX_DEVICE_CTRL_RSP:
      dissect_rf_trx_device_ctrl_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_LOAD_BEAM_RSP:
      dissect_rf_load_beam_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_ENABLE_RSP:
      dissect_rf_enable_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_SET_BEAM_TABLE_RSP:
      dissect_rf_set_beam_table_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_BEAM_TABLE_RSP:
      dissect_rf_get_beam_table_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_CAPABILITIES_RSP:
      dissect_rf_get_capabilities_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_RXAGC_CTRL_REQ:
      dissect_rf_rxagc_ctrl_req(tvb, pinfo, ti, plen);
      break;
    case RF_RXAGC_CTRL_RSP:
      dissect_rf_rxagc_ctrl_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_WBRSSI_REQ:
      dissect_rf_get_wbrssi_req(tvb, pinfo, ti, plen);
      break;
    case RF_GET_WBRSSI_RSP:
      dissect_rf_get_wbrssi_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_WBRSSI_IND:
      dissect_rf_get_wbrssi_ind(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TSSI_REQ:
      dissect_rf_get_tssi_req(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TSSI_RSP:
      dissect_rf_get_tssi_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TSSI_IND:
      dissect_rf_get_tssi_ind(tvb, pinfo, ti, plen);
      break;
    case RF_TRX_DEV_REG_DATA_REQ:
      dissect_rf_get_reg_data_req(tvb, pinfo, ti, plen);
      break;
    case RF_TRX_DEV_REG_DATA_RSP:
      dissect_rf_get_reg_data_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TX_POWER_REQ:
      dissect_rf_get_tx_power_req(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TX_POWER_RSP:
      dissect_rf_get_tx_power_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TX_POWER_IND:
      dissect_rf_get_tx_power_ind(tvb, pinfo, ti, plen);
      break;
    case RF_ALARMS_IND:
      dissect_rf_alarm_ind(tvb, pinfo, ti, plen);
      break;
    case RF_GET_TEMPERATURE_RSP:
      dissect_rf_get_temperature_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_SET_THRESHOLDS_REQ:
      dissect_rf_set_thresholds_req(tvb, pinfo, ti, plen);
      break;
    case RF_SET_THRESHOLDS_RSP:
      dissect_rf_set_thresholds_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_TEST_REQ:
      dissect_rf_test_req(tvb, pinfo, ti, plen);
      break;
    case RF_TEST_RSP:
      dissect_rf_test_rsp(tvb, pinfo, ti, plen);
      break;
    case RF_TEST_IND:
      dissect_rf_test_ind(tvb, pinfo, ti, plen);
      break;

    default:
      proto_item_append_text(ti, " Dissecting of this message type is not supported");
  }
}

static void
dissect_rfapi_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree)
{
  proto_item* ti;
  proto_tree* rfapi_sdk_msg_header_tree = NULL;
  guint16 msg_header_val;
  guint8 val8;
  guint8 flag;
  guint16 msg_header_type = 0;
  guint32 msg_header_timestamp = 0;
  guint32 len = 0;
  if (tree == NULL) return;
  if (check_col(pinfo->cinfo, COL_PROTOCOL))
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_rfapi_sdk);
  ti = proto_tree_add_protocol_format(tree, proto_rfapi_sdk, tvb, len, SIZE_OF_COMMON_MSG_HEADER,
    "Common Message Header");
  rfapi_sdk_msg_header_tree = proto_item_add_subtree(ti, ett_rfapi_sdk_msg_header);
  msg_header_type = sci_tvb_get_ntohs(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_type, tvb, len,
                      sizeof(msg_header_type), msg_header_type);
  len += sizeof(msg_header_type);
  vend_spec_len = sci_tvb_get_ntohs(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_len_ven, tvb, len,
                      sizeof(vend_spec_len), vend_spec_len);
  len += sizeof(vend_spec_len);
  segment = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_len_body, tvb, len,
                      sizeof(segment), segment);
  len += sizeof(segment);
  flag = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_len_flag, tvb, len,
                      sizeof(flag), flag);
  len += sizeof(val8);
  msg_header_val = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_len_sequence, tvb, len,
                      sizeof(msg_header_val)/2, msg_header_val);
  len += sizeof(msg_header_val)/2;
  msg_header_timestamp = sci_tvb_get_ntohl(tvb, len);
  proto_tree_add_uint(rfapi_sdk_msg_header_tree, hf_rfapi_sdk_msg_header_len_timestamp, tvb, len,
                      sizeof(msg_header_timestamp), msg_header_timestamp);
  len += sizeof(msg_header_timestamp);
  if(has_message_data(msg_header_type))
  {
     dissect_rfapi_msg_data(tvb, pinfo, tree, msg_header_type, &len);
  }
}
