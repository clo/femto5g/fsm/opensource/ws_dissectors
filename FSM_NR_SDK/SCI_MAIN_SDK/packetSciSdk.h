/* packetSciSdk.h
*Copyright (c) 2019 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#ifndef SCI_UDP_SDK_H
#define SCI_UDP_SDK_H

#define PROTO_TAG_fapi_sdk	                   "SCI_FAPI_SDK"
#define PROTO_TAG_dan_sdk	                   "SCI_DAN_LTE_SDK"
#define PROTO_TAG_qmi_sdk	                   "SCI_QMI_SDK"

#define PROTO_SCI_FAPI_SDK_ETHERNET            (0xFFFE)
#define PROTO_SCI_FAPI_SDK_ETHERNET_END        (0xFEFF)
#define PROTO_DAN_LTE_SDK_ETHERNET             (0xFFFF)
#define PROTO_SCI_QMI_SDK_ETHERNET		       (0xFFFD)

#define PROTO_SCI_UDP_PORT                     (10055 )

/*
 * de-fragmentation of DAN_API
 */
static GHashTable* sci_sdk_fragment_table = NULL;
static GHashTable* sci_sdk_reassembled_table = NULL;

/* PIE-E Header strings */
typedef enum
{
  FR_FULL = 0,
  FR_FIRST = 1,
  FR_MID = 2,
  FR_LAST = 3,
}SCI_E_PIE_E_HDR_FRAG;

static const value_string pie_e_header_fragment_string[] = {
  { FR_FULL , "FULL" },
  { FR_FIRST, "FIRST"},
  { FR_MID  , "MID"  },
  { FR_LAST , "LAST" },
  { 0, NULL }
};

typedef enum
{
  PI_MSG_TYPE_FAPI_L1      = 0x00,
  PI_MSG_TYPE_FAPI_RF      = 0x01,
  PI_MSG_TYPE_FAPI_LB_REQ  = 0xFE,
  PI_MSG_TYPE_FAPI_LB_RES  = 0xFF,
} PI_E_MSG_TYPE;

/* FAPI Vendor Specific TLV Tags */
static const value_string sci_pi_msg_type_string[] = {
  { PI_MSG_TYPE_FAPI_L1	,     "FAPI_L1"},
  { PI_MSG_TYPE_FAPI_RF	,     "FAPI_RF"},
  { PI_MSG_TYPE_FAPI_LB_REQ	, "FAPI_LB_REQ"},
  { PI_MSG_TYPE_FAPI_LB_RES	, "FAPI_LB_RES"},
  { 0, NULL }
};

/* Preferences bool to control whether we are in big or little endians */
static gboolean global_sci_sdk_BIG_ENDIAN = FALSE;

/* Preferences uint to to choose UDP port used for SCI packets (DAN/FAPI) */
static guint global_sci_sdk_udp_port = PROTO_SCI_UDP_PORT;

/* Preferences bool to control whether to dissect as new PI header */
static gboolean global_sci_sdk_dissect_new_PI_hdr = TRUE;

/* Preferences bool to control the duplex mode used when dissecting payload */
static gint global_sci_sdk_frag_dissecting = 1;
static const enum_val_t global_sci_sdk_frag_dissecting_enum[] =
{
  { "NO_FRAG",	"None",				0 },
  { "FIRST_FRAG", "First Frag. Only", 1 },
  { "ALL_FRAG",	"All Fragments",	2 },
  { NULL, NULL, 0},
};

/* Wireshark ID of the SCI Main SDK protocol */
static int proto_sci_main_sdk = -1;

static dissector_handle_t rfapi_sdk_handle;
static dissector_handle_t sci_udp_handle;
static dissector_handle_t sci_eth_handle;
static dissector_handle_t fapi_sdk_handle;
static dissector_handle_t dan_lte_sdk_handle;
static dissector_handle_t qmi_sdk_handle;

/*
 * de-fragmentation of DAN_API
 */
static void dissect_sci_udp_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree);
static void dissect_sci_eth_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree);
static void	dissect_sci_pi(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32 sdk_type);

/* FAPI pi-e header*/
static gint hf_sci_sdk_pi_e_header_type = -1;
static gint hf_sci_sdk_pi_e_header_seq = -1;
static gint hf_sci_sdk_pi_e_header_size = -1;
static gint hf_sci_sdk_pi_e_header_frag = -1;
static gint hf_sci_sdk_pi_e_header_nf = -1;
static gint hf_sci_sdk_pi_e_header_nsf = -1;

static gint hf_sci_sdk_pi_e_header_new_type = -1;
static gint hf_sci_sdk_pi_e_header_new_seq = -1;
static gint hf_sci_sdk_pi_e_header_padding = -1;
static gint hf_sci_sdk_pi_e_header_timestamp = -1;
static gint hf_sci_sdk_pi_e_header_sector_id = -1;


static gint version = -1;
static gint type = -1;
static gint unused = -1;
static gint reserved = -1;
static gint length = -1;
static gint sequence = -1;

/* Fragmentation hf */
static gint hf_msg_fragments = -1;
static gint hf_msg_fragment = -1;
static gint hf_msg_fragment_overlap = -1;
static gint hf_msg_fragment_overlap_conflicts = -1;
static gint hf_msg_fragment_multiple_tails = -1;
static gint hf_msg_fragment_too_long_fragment = -1;
static gint hf_msg_fragment_error = -1;
static gint hf_msg_fragment_count = -1;
static gint hf_msg_reassembled_in = -1;
static gint hf_msg_reassembled_length = -1;

/* These are the ids of the subtrees that we may be creating */
static gint ett_sci_main_sdk_pi_e_header = -1;
static gint ett_msg_fragment = -1;
static gint ett_msg_fragments = -1;

static const fragment_items sci_sdk_frag_items = {
  /* Fragment subtrees */
  &ett_msg_fragment,
  &ett_msg_fragments,
  /* Fragment fields */
  &hf_msg_fragments,
  &hf_msg_fragment,
  &hf_msg_fragment_overlap,
  &hf_msg_fragment_overlap_conflicts,
  &hf_msg_fragment_multiple_tails,
  &hf_msg_fragment_too_long_fragment,
  &hf_msg_fragment_error,
  &hf_msg_fragment_count,
  /* Reassembled in field */
  &hf_msg_reassembled_in,
  /* Reassembled length field */
  &hf_msg_reassembled_length,
  /* Tag */
  "Message fragments"
};


#endif
