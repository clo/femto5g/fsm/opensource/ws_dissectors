/* packetSciSdk.c
*Copyright (c) 2019 The Linux Foundation. All rights reserved.
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; version 2
* of the License only
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <glib.h>
#include <epan/packet.h>
#include <epan/expert.h>
#include <epan/proto.h>
#include <epan/prefs.h>
#include <epan/tvbuff-int.h>
#include <epan/tvbuff.h>
#include <epan/reassemble.h>
#include <epan/value_string.h>
#include "packetSciSdk.h"

#include <string.h>

static void
sci_main_defragment_init(void)
{
  fragment_table_init(&sci_sdk_fragment_table);
  reassembled_table_init(&sci_sdk_reassembled_table);

} // end of sci_main_defragment_init


//The dissectors are found and added and handles are made for them
void proto_reg_handoff_sci_sdk(void)
{
  static gboolean initialized = FALSE;
  static unsigned int old_udp_port;
 if (!initialized)
  {
    rfapi_sdk_handle = find_dissector("rfapi_sdk");
    fapi_sdk_handle = find_dissector("fapi_sdk");
    dan_lte_sdk_handle = find_dissector("dan_lte_sdk");
    qmi_sdk_handle = find_dissector("qmi_sdk");
    sci_udp_handle = create_dissector_handle(dissect_sci_udp_sdk, proto_sci_main_sdk);
    sci_eth_handle = create_dissector_handle(dissect_sci_eth_sdk, proto_sci_main_sdk);
    dissector_add("ethertype", PROTO_SCI_FAPI_SDK_ETHERNET, sci_eth_handle);
    dissector_add("ethertype", PROTO_DAN_LTE_SDK_ETHERNET, sci_eth_handle);
    dissector_add("ethertype", PROTO_SCI_QMI_SDK_ETHERNET, sci_eth_handle);
    initialized = TRUE;
  }
  else
  {
    dissector_delete_uint("udp.port", old_udp_port, sci_udp_handle);
  }
 old_udp_port = global_sci_sdk_udp_port;
  dissector_add_uint("udp.port", global_sci_sdk_udp_port, sci_udp_handle);

}


//The fields and its strucutre is defined for the header.
void proto_register_sci_sdk(void)
{
  /* A header field is something you can search/filter on.
  *
  * We create a structure to register our fields. It consists of an
  * array of hf_register_info structures, each of which are of the format
  * {&(field id), {name, abbrev, type, display, strings, bitmask, blurb, HFILL}}.
  */
  static hf_register_info hf[] = {
   /* PI-E Header */
   {&version ,
    {"Version", "sci_sdk.pi_e_header.version",
    FT_UINT8, BASE_DEC, NULL, 0x00, NULL, HFILL } },
   {&type ,
    {"Type", "sci_sdk.pi_e_header.type",
    FT_UINT8, BASE_DEC, VALS(sci_pi_msg_type_string), 0x00, NULL, HFILL } },
   {&unused ,
    {"Unused", "sci_sdk.pi_e_header.unused",
    FT_UINT8, BASE_DEC, NULL, 0x00, NULL, HFILL } },
   {&reserved ,
    {"Reserved", "sci_sdk.pi_e_header.reserved",
    FT_UINT8, BASE_DEC, NULL, 0x00, NULL, HFILL } },
   {&length ,
    {"Length", "sci_sdk.pi_e_header.length",
    FT_UINT16, BASE_DEC, NULL, 0x00, NULL, HFILL } },
   {   &sequence,
    {   "Sequence", "sci_sdk.pi_e_header.sequence",
    FT_UINT16, BASE_DEC, NULL, 0x0,
    NULL, HFILL }   },
   {   &hf_sci_sdk_pi_e_header_sector_id,
    {   "Carrier ID", "sci_sdk.pi_e_header.sec_id",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL }   },
   {   &hf_sci_sdk_pi_e_header_timestamp,
    {   "Timestamp", "sci_sdk.pi_e_header.timestamp",
    FT_UINT64, BASE_DEC, NULL, 0x0,
    NULL, HFILL }   },
   {   &hf_sci_sdk_pi_e_header_padding,
    {   "PLatfomr Name", "sci_sdk.pi_e_header.padding",
    FT_UINT8, BASE_DEC, NULL, 0x0,
    NULL, HFILL }   },
   /* Fragmentation data */
    {&hf_msg_fragments,
    {"Message fragments", "msg.fragments",
    FT_NONE, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment,
    {"Message fragment", "msg.fragment",
    FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_overlap,
    {"Message fragment overlap", "msg.fragment.overlap",
    FT_BOOLEAN, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_overlap_conflicts,
    {"Message fragment overlapping with conflicting data",
    "msg.fragment.overlap.conflicts",
    FT_BOOLEAN, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_multiple_tails,
    {"Message has multiple tail fragments",
    "msg.fragment.multiple_tails",
    FT_BOOLEAN, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_too_long_fragment,
    {"Message fragment too long", "msg.fragment.too_long_fragment",
    FT_BOOLEAN, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_error,
    {"Message defragmentation error", "msg.fragment.error",
    FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_fragment_count,
    {"Message fragment count", "msg.fragment.count",
    FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_reassembled_in,
    {"Reassembled in", "msg.reassembled.in",
    FT_FRAMENUM, BASE_NONE, NULL, 0x00, NULL, HFILL } },
   {&hf_msg_reassembled_length,
    {"Reassembled length", "msg.reassembled.length",
    FT_UINT32, BASE_DEC, NULL, 0x00, NULL, HFILL } }
  };
 static gint* ett[] = {
      &ett_sci_main_sdk_pi_e_header,
      &ett_msg_fragment,
      &ett_msg_fragments
  };
 //Registration of the header field.
  module_t* sci_main_sdk_module;
 proto_sci_main_sdk = proto_register_protocol("SCI SDK Protocol", "* SCI SDK *", "sci_main_sdk");
 register_init_routine(sci_main_defragment_init);
 proto_register_field_array(proto_sci_main_sdk, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));
  register_dissector("sci_udp_sdk", dissect_sci_udp_sdk, proto_sci_main_sdk);
  register_dissector("sci_eth_sdk", dissect_sci_eth_sdk, proto_sci_main_sdk);
 sci_main_sdk_module = prefs_register_protocol(proto_sci_main_sdk, proto_reg_handoff_sci_sdk);
 prefs_register_uint_preference(sci_main_sdk_module, "udp.port",
    "SCI UDP Port",
    "Choose the destination UDP port used to send SCI packets",
    10, /* Integer base */
    &global_sci_sdk_udp_port);
 prefs_register_bool_preference(sci_main_sdk_module, "big_endian",
    "Use Big Endian",
    "Choose between dissecting in Big Endian or Little Endian ",
    &global_sci_sdk_BIG_ENDIAN);
 prefs_register_bool_preference(sci_main_sdk_module, "new_pi_hdr",
    "New PI-E Header",
    "Choose whether to dissect as new PI header ",
    &global_sci_sdk_dissect_new_PI_hdr);
 prefs_register_enum_preference(sci_main_sdk_module, "diss_frag",
    "Dissect Fragments",
    "Would you like to dissect fragmented messages? ",
    &global_sci_sdk_frag_dissecting,
    global_sci_sdk_frag_dissecting_enum, FALSE);

}

//functions used to dissect packets
static guint16
sci_tvb_get_ntohs(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntohs(tvb, offset);
  else
    return tvb_get_letohs(tvb, offset);
}

static guint32
sci_tvb_get_ntoh24(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntoh24(tvb, offset);
  else
    return tvb_get_letoh24(tvb, offset);
}

static guint32
sci_tvb_get_ntohl(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntohl(tvb, offset);
  else
    return tvb_get_letohl(tvb, offset);
}

static guint64
sci_tvb_get_ntoh40(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntoh40(tvb, offset);
  else
    return tvb_get_letoh40(tvb, offset);
}

static guint64
sci_tvb_get_ntoh48(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntoh48(tvb, offset);
  else
    return tvb_get_letoh48(tvb, offset);
}

static guint64
sci_tvb_get_ntoh56(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntoh56(tvb, offset);
  else
    return tvb_get_letoh56(tvb, offset);
}

static guint64
sci_tvb_get_ntoh64(tvbuff_t* tvb, const gint offset)
{
  if (global_sci_sdk_BIG_ENDIAN)
    return tvb_get_ntoh64(tvb, offset);
  else
    return tvb_get_letoh64(tvb, offset);
}

static void
dissect_sci_udp_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree)
{
  tvbuff_t* new_tvb = NULL;
 guint32 len = 0;
  guint16	sdk_type;
 /* Clear out stuff in the info column */
  if (check_col(pinfo->cinfo, COL_INFO))
  {
    col_clear(pinfo->cinfo, COL_INFO);
  }
 sdk_type = tvb_get_ntohs(tvb, len); //This is a part of the packet header and not FAPI, therefore it will arrive as big endian allways
 len += 2;
 new_tvb = tvb_new_subset_remaining(tvb, len);
  DISSECTOR_ASSERT(new_tvb != NULL);
  dissect_sci_pi(new_tvb, pinfo, tree, sdk_type);
}

static void
dissect_sci_eth_sdk(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree)
{
  dissect_sci_pi(tvb, pinfo, tree, pinfo->ethertype);
}

static void
dissect_sci_pi(tvbuff_t* tvb, packet_info* pinfo, proto_tree* tree, guint32 sdk_type)
{
  proto_item* ti;
  proto_tree* sci_pi_e_header_tree = NULL;
  tvbuff_t* new_tvb = NULL;
  tvbuff_t* next_tvb = NULL;
  guint32 pi_e_header_type = 0;
  //guint32 pi_e_header_seq;
  guint32 pi_e_header_size = 0;
  guint8  pi_e_header_frag = 0;
  guint32 len = 0;
  guint8 val8, pi_e_type;
  guint32 val = 0;
  guint16 val16 = 0;
  gboolean  save_fragmented;
  int       offset = 0;
  int       frag_offset = 0;
  gboolean  parse_frag = FALSE;
  guint32   frag_remaining;
 //guint64	  val64;
 static guint32  patch_seq[6] = { 0, 0, 0, 0, 0, 0 };
  static guint16  msg_id_gen[6] = { 0, 0, 0, 0, 0, 0 };
 if (tree == NULL) return;
 /* Clear out stuff in the info column */
  if (check_col(pinfo->cinfo, COL_INFO))
  {
    col_clear(pinfo->cinfo, COL_INFO);
  }
  if ((sdk_type == PROTO_SCI_FAPI_SDK_ETHERNET) || (sdk_type == PROTO_SCI_FAPI_SDK_ETHERNET_END) || (sdk_type == 0xEFFF) || (sdk_type == 0xFFEF))
  {
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_fapi_sdk);
  }
  else if (sdk_type == PROTO_DAN_LTE_SDK_ETHERNET)
  {
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_dan_sdk);
  }
  else if (sdk_type == PROTO_SCI_QMI_SDK_ETHERNET)
  {
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_qmi_sdk);
  }
 /* PI-E Header */
  ti = proto_tree_add_protocol_format(tree, proto_sci_main_sdk, tvb, 0, 8,
    "PI-E Header");
  sci_pi_e_header_tree = proto_item_add_subtree(ti, ett_sci_main_sdk_pi_e_header);

  val8 = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(sci_pi_e_header_tree, version, tvb, len, 1, val8);
  len += 1;
  val8 = tvb_get_guint8(tvb, len);
  pi_e_type = val8;
  proto_tree_add_uint(sci_pi_e_header_tree, type, tvb, len, 1, val8);
  len += 1;
  val8 = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(sci_pi_e_header_tree, unused, tvb, len, 1, val8);
  len += 1;
  val8 = tvb_get_guint8(tvb, len);
  proto_tree_add_uint(sci_pi_e_header_tree, reserved, tvb, len, 1, val8);
  len += 1;
  val16 = sci_tvb_get_ntohs(tvb, len);
  proto_tree_add_uint(sci_pi_e_header_tree, length, tvb, len, 2, val16);
  len += 2;
 val16 = sci_tvb_get_ntohs(tvb, len);
  proto_tree_add_uint(sci_pi_e_header_tree, sequence, tvb, len, 2, val16);
  len += 2;

/* Fragmentation Test (Yosi) */
/*ask amit sir*/
  save_fragmented = pinfo->fragmented;
 if (pi_e_header_frag != FR_FULL)
  {
    tvbuff_t* new_tvb = NULL;
    fragment_data* frag_msg = NULL;
   if (pi_e_header_frag == FR_FIRST)
    {
      offset = 0;
      patch_seq[pi_e_header_type] = 0;
      msg_id_gen[pi_e_header_type] = pinfo->fd->abs_ts.nsecs;
     frag_remaining = pi_e_header_size + len;
      if (global_sci_sdk_frag_dissecting != 0)/* parse message fragment */
        parse_frag = TRUE;
    }
    else
    {
      offset = len;
      patch_seq[pi_e_header_type]++;
     frag_remaining = pi_e_header_size;
      if (global_sci_sdk_frag_dissecting == 2)/* parse message fragment */
        parse_frag = TRUE;
   }
   pinfo->fragmented = TRUE;
   frag_msg = fragment_add_seq_check(tvb, offset, pinfo,
      msg_id_gen[pi_e_header_type],
      sci_sdk_fragment_table,
      sci_sdk_reassembled_table,
      patch_seq[pi_e_header_type],
      frag_remaining, (pi_e_header_frag != FR_LAST));
   new_tvb = process_reassembled_data(tvb, offset, pinfo, "Reassembled Message ", frag_msg, &sci_sdk_frag_items,
      NULL, tree);
   if (frag_msg)
      col_append_str(pinfo->cinfo, COL_INFO, "Message Reassembled ");
    else
      col_append_fstr(pinfo->cinfo, COL_INFO, "Message Fragment %u ", patch_seq[pi_e_header_type]);
   if (new_tvb)
    {
      next_tvb = new_tvb;
      parse_frag = TRUE;
    }
    else
      next_tvb = tvb;
  }
  else
  {
    parse_frag = TRUE;
    next_tvb = tvb;
  }
 //End Fragmentation
  parse_frag = TRUE;
  next_tvb = tvb;
  if(pi_e_type == PI_MSG_TYPE_FAPI_RF)
  {
    new_tvb = tvb_new_subset_remaining(next_tvb, len);
    DISSECTOR_ASSERT(new_tvb != NULL);
    call_dissector_only(rfapi_sdk_handle, new_tvb, pinfo, tree);
    pinfo->fragmented = save_fragmented;
    parse_frag = FALSE;
  }
  if (parse_frag && pi_e_type == PI_MSG_TYPE_FAPI_L1)
  {
   new_tvb = tvb_new_subset_remaining(next_tvb, len);
    DISSECTOR_ASSERT(new_tvb != NULL);
    call_dissector_only(fapi_sdk_handle, new_tvb, pinfo, tree);
   if (sdk_type == PROTO_DAN_LTE_SDK_ETHERNET)
    {
      new_tvb = tvb_new_subset_remaining(next_tvb, len);
      DISSECTOR_ASSERT(new_tvb != NULL);
      call_dissector_only(dan_lte_sdk_handle, new_tvb, pinfo, tree);
    }
    else if (sdk_type == PROTO_SCI_QMI_SDK_ETHERNET)
    {
      new_tvb = tvb_new_subset_remaining(next_tvb, len);
      DISSECTOR_ASSERT(new_tvb != NULL);
      call_dissector_only(qmi_sdk_handle, new_tvb, pinfo, tree);
    }
  }
  pinfo->fragmented = save_fragmented;

}
