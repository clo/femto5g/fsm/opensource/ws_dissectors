Copyright (c) 2019, The Linux Foundation. All rights reserved.

Redistribution and use in source form and compiled forms (SGML, HTML, PDF, PostScript, RTF and so forth) with or without modification, are permitted provided that the following conditions are met:

Redistributions in source form must retain the above copyright notice, this list of conditions and the following disclaimer as the first lines of this file unmodified.

Redistributions in compiled form (transformed to other DTDs, converted to PDF, PostScript, RTF and other formats) must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS DOCUMENTATION IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL THE FREEBSD DOCUMENTATION PROJECT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Creation of wireshark development environment for Windows
---------------------------------------------------------
(1.1) Download & Install Visual Studio 2010 Express Edition. Install SP1 along with this package.

(1.2) Download Wireshark 1.8.6 code tar ball from
        https://www.wireshark.org/download/src/all-versions/wireshark-1.8.6.tar.bz2

(1.3) Install Cygwin for 32-bit Windows - https://cygwin.com/install.html

		At the "Select Packages" page, select some additional packages which are
        not installed by default. Navigate to the required Category/Package row and,
		if the package has a "Skip" item in the "New" column, click on the "Skip" item
		so it shows a version number for:
            - Archive/unzip
            - Devel/bison
            - Devel/flex
            - Devel/git
            - Interpreters/perl
            - Utils/patch
            - Web/wget
            - asciidoc
            - Interpreters/m4
		Select the top of the version number and then finish the installation by clicking
		the next button.

(1.4) Get the Python 2.7 installer from http://python.org/download/ and install Python into the
        default location (C:\Python27)

Compilation of base wireshark build
-----------------------------------
(2.1) Create a development directory under C:\
        For example - C:\devt\

(2.2) Extract wireshark-1.8.6 source code (downloaded tar file from section 1.2) to
        C:\<your development directory, created at section 2.1>

(2.3) Open Visual Studio Command Prompt (2010).

(2.4) Set environment variables for Visual C++ 2010 Express Edition. Run:
        -> "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\vcvars32.bat

(2.5) On the same command prompt, Select target platform (build 32-bit binaries) type:
        > set WIRESHARK_TARGET_PLATFORM=win32

(2.6) Go to the path : C:\<your_development_directory>\wireshark-1.8.6
		For eg." C:\Devt\wireshark-1.8.6"

(2.7) Verify installed tools

		For verification, type in the visual studio command prompt:
		> nmake -f Makefile.nmake verify_tools

		This will check for the various tools needed to build Wireshark. Expected output,
		is as follows:
		> Checking for required applications:
        cl: /cygdrive/c/Program Files (x86)/Microsoft Visual Studio 10.0/VC/Bin/amd64/cl
        link: /cygdrive/c/Program Files (x86)/Microsoft Visual Studio 10.0/VC/Bin/amd64/link
        nmake: /cygdrive/c/Program Files (x86)/Microsoft Visual Studio 10.0/VC/Bin/amd64/nmake
        bash: /usr/bin/bash
        bison: /usr/bin/bison
        flex: /usr/bin/flex
        env: /usr/bin/env
        grep: /usr/bin/grep
        /usr/bin/find: /usr/bin/find
        peflags: /usr/bin/peflags
        perl: /usr/bin/perl
        C:\Python27\python.exe: /cygdrive/c/Python27/python.exe
        sed: /usr/bin/sed
        unzip: /usr/bin/unzip
        wget: /usr/bin/wget

		It may give an error like "The contents of C:\wireshark-win32-libs\current_tag.txt is (unknown)".
		Please ignore the same as this error comes on new PCs.

(2.8) Install libraries
		Run  > nmake -f Makefile.nmake setup
		This command will download and install libraries using wget. This may take a while.

(2.9) Build Wireshark:
		Within prepared cmd.exe window (see above), set working directory:
		> cd C:\<Your development dir>\wireshark-1.8.6

		Clean before build:
		> nmake -f Makefile.nmake distclean

		Eventually build:
		> nmake -f Makefile.nmake all
		This build takes a while and creates Wireshark.exe and all accompanying files in
		C:\<Your development dir>\wireshark-1.8.x\wireshark\ directory

Compiling FAPI/SCI_MAIN SDK dissectors
--------------------------------------
(3.1) In the source code packge goto ws_dissectors/FSM_NR_L1_SDK folder, it contains
		FAPI_SDK and SCI_MAIN_SDK folders

        Copy folder ws_dissectors/FSM_NR_L1_SDK/FAPI_SDK to C:\<your_development_directory>\wireshark-1.8.6\epan\dissectors\

		Similarly

		Copy folder ws_dissectors/FSM_NR_L1_SDK/SCI_MAIN_SDK to C:\<your_development_directory>\wireshark-1.8.6\epan\dissectors\

(3.2) Open Visual Studio command prompt
        -> cd C:\your_development_directory>\wireshark-1.8.6\epan\dissectors\FAPI_SDK\
        -> nmake –f Makefile.nmake clean
        -> nmake –f Makefile.nmake

		-> It generats the DLL "fapiSdk.dll"

        Repeat the same for SCI_MAIN_SDK to generate sciMainSdk.dll

Windows plugin installation
---------------------------

After compiling and building the FAPI dissectors (as described under section 3.2)
Copy all DLL files into “C:\Program Files (x86)\Wireshark\plugins\1.8.6”.